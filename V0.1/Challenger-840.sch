<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting keepoldvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.05" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="14" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="13" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="14" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="13" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="11" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="no" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="no" active="yes"/>
<layer number="90" name="Modules" color="7" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Mittellin" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="Stiffner" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tBridges" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="tBPL" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="bBPL" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="MPL" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="114" name="ANT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="usb" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="sName" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bPlace" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="145" name="DrillLegend_01-02" color="7" fill="1" visible="yes" active="yes"/>
<layer number="146" name="DrillLegend_01-15" color="7" fill="1" visible="yes" active="yes"/>
<layer number="147" name="DrillLegend_01-16" color="7" fill="1" visible="yes" active="yes"/>
<layer number="148" name="DrillLegend_01-20" color="7" fill="1" visible="yes" active="yes"/>
<layer number="149" name="DrillLegend_02-15" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="microbuilder">
<description>&lt;h2&gt;&lt;b&gt;microBuilder.eu&lt;/b&gt; Eagle Footprint Library&lt;/h2&gt;

&lt;p&gt;Footprints for common components used in our projects and products.  This is the same library that we use internally, and it is regularly updated.  The newest version can always be found at &lt;b&gt;www.microBuilder.eu&lt;/b&gt;.  If you find this library useful, please feel free to purchase something from our online store. Please also note that all holes are optimised for metric drill bits!&lt;/p&gt;

&lt;h3&gt;Obligatory Warning&lt;/h3&gt;
&lt;p&gt;While it probably goes without saying, there are no guarantees that the footprints or schematic symbols in this library are flawless, and we make no promises of fitness for production, prototyping or any other purpose. These libraries are provided for information puposes only, and are used at your own discretion.  While we make every effort to produce accurate footprints, and many of the items found in this library have be proven in production, we can't make any promises of suitability for a specific purpose. If you do find any errors, though, please feel free to contact us at www.microbuilder.eu to let us know about it so that we can update the library accordingly!&lt;/p&gt;

&lt;h3&gt;License&lt;/h3&gt;
&lt;p&gt;This work is placed in the public domain, and may be freely used for commercial and non-commercial work with the following conditions:&lt;/p&gt;
&lt;p&gt;THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
&lt;/p&gt;</description>
<packages>
<package name="MOUNTINGHOLE_3.0_PLATED">
<wire x1="-2.159" y1="0" x2="0" y2="-2.159" width="2.4892" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.159" x2="2.159" y2="0" width="2.4892" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="3.429" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="3.048" width="2.032" layer="39"/>
<circle x="0" y="0" radius="3.048" width="2.032" layer="43"/>
<circle x="0" y="0" radius="3.048" width="2.032" layer="40"/>
<circle x="0" y="0" radius="3.048" width="2.032" layer="41"/>
<circle x="0" y="0" radius="3.048" width="2.032" layer="42"/>
<pad name="P$1" x="0" y="0" drill="3" diameter="6.4516"/>
<text x="-1.27" y="-3.81" size="1.27" layer="48">3,0</text>
</package>
<package name="MOUNTINGHOLE_1.0_PLATED">
<circle x="0" y="0" radius="1.75" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="1" width="2.032" layer="39"/>
<circle x="0" y="0" radius="1" width="2.032" layer="43"/>
<circle x="0" y="0" radius="1" width="2.032" layer="40"/>
<circle x="0" y="0" radius="1" width="2.032" layer="41"/>
<circle x="0" y="0" radius="1" width="2.032" layer="42"/>
<pad name="P$1" x="0" y="0" drill="1" diameter="3"/>
<text x="-0.87" y="-2.74" size="0.8128" layer="48">1,0</text>
</package>
<package name="MOUNTINGHOLE_2.0_PLATED">
<circle x="0" y="0" radius="1.8" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="1" width="2.032" layer="39"/>
<circle x="0" y="0" radius="1" width="2.032" layer="43"/>
<circle x="0" y="0" radius="1" width="2.032" layer="40"/>
<circle x="0" y="0" radius="1" width="2.032" layer="41"/>
<circle x="0" y="0" radius="1" width="2.032" layer="42"/>
<pad name="P$1" x="0" y="0" drill="2.2" diameter="3"/>
<text x="-0.87" y="-2.74" size="0.8128" layer="48">2,0</text>
</package>
<package name="MOUNTINGHOLE_3.0_PLATEDTHIN">
<circle x="0" y="0" radius="2.2" width="0.2" layer="21"/>
<circle x="0" y="0" radius="1" width="2.032" layer="39"/>
<circle x="0" y="0" radius="1" width="2.032" layer="43"/>
<circle x="0" y="0" radius="1" width="2.032" layer="40"/>
<circle x="0" y="0" radius="1" width="2.032" layer="41"/>
<circle x="0" y="0" radius="1" width="2.032" layer="42"/>
<pad name="P$1" x="0" y="0" drill="3" diameter="3.6"/>
</package>
<package name="MOUNTINGHOLE_2.5_PLATED">
<circle x="0" y="0" radius="2" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="1" width="2.032" layer="39"/>
<circle x="0" y="0" radius="1" width="2.032" layer="40"/>
<circle x="0" y="0" radius="1" width="2.032" layer="41"/>
<circle x="0" y="0" radius="1" width="2.032" layer="42"/>
<pad name="P$1" x="0" y="0" drill="2.5" diameter="3.2"/>
</package>
<package name="MOUNTINGHOLE_2.5_PLATED_THICK">
<circle x="0" y="0" radius="2.25" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="1.25" width="2.032" layer="39"/>
<circle x="0" y="0" radius="1.25" width="2.032" layer="40"/>
<circle x="0" y="0" radius="1.25" width="2.032" layer="41"/>
<circle x="0" y="0" radius="1.25" width="2.032" layer="42"/>
<pad name="P$1" x="0" y="0" drill="2.5" diameter="4"/>
</package>
<package name="MOUNTINGHOLE_3.0_PLATED_VIAS">
<wire x1="-2.159" y1="0" x2="0" y2="-2.159" width="2.4892" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.159" x2="2.159" y2="0" width="2.4892" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="3.429" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="3.048" width="2.032" layer="39"/>
<circle x="0" y="0" radius="3.048" width="2.032" layer="41"/>
<pad name="P$1" x="0" y="0" drill="3" diameter="6.4516"/>
<pad name="P$2" x="0" y="2.5" drill="0.4"/>
<pad name="P$3" x="2.5" y="0" drill="0.4"/>
<pad name="P$4" x="0" y="-2.5" drill="0.4"/>
<pad name="P$5" x="-2.5" y="0" drill="0.4"/>
<pad name="P$6" x="-1.8" y="1.7" drill="0.4"/>
<pad name="P$7" x="-1.8" y="-1.7" drill="0.4"/>
<pad name="P$8" x="1.8" y="-1.7" drill="0.4"/>
<pad name="P$9" x="1.8" y="1.7" drill="0.4"/>
</package>
<package name="MOUNTINGHOLE_3.0_PLATED_SQUAREVIAS">
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<pad name="P$1" x="0" y="0" drill="3" diameter="6.4" shape="square"/>
<rectangle x1="-3.75" y1="-3.75" x2="3.75" y2="3.75" layer="39"/>
<rectangle x1="-3.75" y1="-3.75" x2="3.75" y2="3.75" layer="41"/>
<wire x1="-3.4" y1="3.4" x2="-3.4" y2="-3.4" width="0.127" layer="21"/>
<wire x1="-3.4" y1="-3.4" x2="3.4" y2="-3.4" width="0.127" layer="21"/>
<wire x1="3.4" y1="-3.4" x2="3.4" y2="3.4" width="0.127" layer="21"/>
<wire x1="3.4" y1="3.4" x2="-3.4" y2="3.4" width="0.127" layer="21"/>
<pad name="P$2" x="-2.25" y="0" drill="0.4"/>
<pad name="P$3" x="0" y="2.25" drill="0.4"/>
<pad name="P$4" x="2.25" y="0" drill="0.4"/>
<pad name="P$5" x="0" y="-2.25" drill="0.4"/>
<pad name="P$6" x="-2.25" y="2.25" drill="0.4"/>
<pad name="P$7" x="2.25" y="2.25" drill="0.4"/>
<pad name="P$8" x="2.25" y="-2.25" drill="0.4"/>
<pad name="P$9" x="-2.25" y="-2.25" drill="0.4"/>
</package>
<package name="SOT23-5">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt; - 5 Pin</description>
<wire x1="1.4224" y1="0.8104" x2="1.4224" y2="-0.8104" width="0.2032" layer="51"/>
<wire x1="1.4224" y1="-0.8104" x2="-1.4224" y2="-0.8104" width="0.2032" layer="51"/>
<wire x1="-1.4224" y1="-0.8104" x2="-1.4224" y2="0.8104" width="0.2032" layer="51"/>
<wire x1="-1.4224" y1="0.8104" x2="1.4224" y2="0.8104" width="0.2032" layer="51"/>
<wire x1="-1.65" y1="0.8" x2="-1.65" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="1.65" y1="0.8" x2="1.65" y2="-0.8" width="0.2032" layer="21"/>
<smd name="1" x="-0.95" y="-1.3001" dx="0.55" dy="1.2" layer="1"/>
<smd name="2" x="0" y="-1.3001" dx="0.55" dy="1.2" layer="1"/>
<smd name="3" x="0.95" y="-1.3001" dx="0.55" dy="1.2" layer="1"/>
<smd name="4" x="0.95" y="1.3001" dx="0.55" dy="1.2" layer="1"/>
<smd name="5" x="-0.95" y="1.3001" dx="0.55" dy="1.2" layer="1"/>
<text x="1.978" y="0" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="1.978" y="-0.635" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2" y1="-1.5" x2="-0.7" y2="-0.85" layer="51"/>
<rectangle x1="-0.25" y1="-1.5" x2="0.25" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="-1.5" x2="1.2" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="0.85" x2="1.2" y2="1.5" layer="51"/>
<rectangle x1="-1.2" y1="0.85" x2="-0.7" y2="1.5" layer="51"/>
<wire x1="-0.4" y1="1.05" x2="0.4" y2="1.05" width="0.2032" layer="21"/>
</package>
<package name="SMADIODE">
<description>&lt;b&gt;SMA Surface Mount Diode&lt;/b&gt;</description>
<wire x1="-2.15" y1="1.3" x2="2.15" y2="1.3" width="0.2032" layer="51"/>
<wire x1="2.15" y1="1.3" x2="2.15" y2="-1.3" width="0.2032" layer="51"/>
<wire x1="2.15" y1="-1.3" x2="-2.15" y2="-1.3" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.3" x2="-2.15" y2="1.3" width="0.2032" layer="51"/>
<wire x1="-3.889" y1="-1.146" x2="-3.889" y2="1.6" width="0.2032" layer="21"/>
<wire x1="-3.889" y1="1.6" x2="3.916" y2="1.6" width="0.2032" layer="21"/>
<wire x1="3.916" y1="1.6" x2="3.916" y2="1.394" width="0.2032" layer="21"/>
<wire x1="3.916" y1="1.394" x2="3.916" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="3.916" y1="-1.6" x2="-3.889" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-3.889" y1="-1.6" x2="-3.889" y2="-1.146" width="0.2032" layer="21"/>
<wire x1="0.254" y1="0.762" x2="0.254" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="0.254" y1="-0.762" x2="-0.508" y2="0" width="0.2032" layer="21"/>
<wire x1="-0.508" y1="0" x2="0.254" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-0.508" y1="0.762" x2="-0.508" y2="0" width="0.2032" layer="21"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-0.762" width="0.2032" layer="21"/>
<smd name="C" x="-2.3495" y="0" dx="2.54" dy="2.54" layer="1"/>
<smd name="A" x="2.3495" y="0" dx="2.54" dy="2.54" layer="1" rot="R180"/>
<text x="-2.54" y="1.905" size="0.8128" layer="25" font="vector" ratio="18">&gt;NAME</text>
<text x="-2.54" y="-2.286" size="0.4064" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.825" y1="-1.1" x2="-2.175" y2="1.1" layer="51"/>
<rectangle x1="2.175" y1="-1.1" x2="2.825" y2="1.1" layer="51" rot="R180"/>
<rectangle x1="-1.75" y1="-1.225" x2="-1.075" y2="1.225" layer="51"/>
</package>
<package name="DO-1N4148">
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="1.905" y1="0.635" x2="1.905" y2="-0.635" width="0.2032" layer="21"/>
<pad name="A" x="-3.81" y="0" drill="0.9"/>
<pad name="C" x="3.81" y="0" drill="0.9"/>
<text x="-2.286" y="1.143" size="0.8128" layer="25" ratio="18">&gt;Name</text>
<text x="-2.286" y="-0.381" size="0.8128" layer="21">&gt;Value</text>
</package>
<package name="SOT23-R">
<description>&lt;b&gt;SOT23&lt;/b&gt; - Reflow soldering</description>
<wire x1="1.5724" y1="0.6604" x2="1.5724" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.5724" y1="-0.6604" x2="-1.5724" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="-0.6604" x2="-1.5724" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="0.6604" x2="1.5724" y2="0.6604" width="0.2032" layer="51"/>
<wire x1="-1.5724" y1="-0.6524" x2="-1.5724" y2="0.6604" width="0.2032" layer="21"/>
<wire x1="-1.5724" y1="0.6604" x2="-0.5636" y2="0.6604" width="0.2032" layer="21"/>
<wire x1="1.5724" y1="0.6604" x2="1.5724" y2="-0.6524" width="0.2032" layer="21"/>
<wire x1="0.5636" y1="0.6604" x2="1.5724" y2="0.6604" width="0.2032" layer="21"/>
<wire x1="0.3724" y1="-0.6604" x2="-0.3864" y2="-0.6604" width="0.2032" layer="21"/>
<smd name="3" x="0" y="1" dx="0.635" dy="1.016" layer="1"/>
<smd name="2" x="0.95" y="-1" dx="0.635" dy="1.016" layer="1"/>
<smd name="1" x="-0.95" y="-1" dx="0.635" dy="1.016" layer="1"/>
<text x="1.778" y="-0.127" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="1.778" y="-0.635" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
<package name="SOD-523">
<description>SOD-523 (0.8x1.2mm)

&lt;p&gt;Source: http://www.rohm.com/products/databook/di/pdf/rb751s-40.pdf&lt;/p&gt;</description>
<smd name="K" x="0" y="0.75" dx="0.8" dy="0.6" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.6" layer="1"/>
<text x="0.716" y="0.016" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="0.716" y="-0.724" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<wire x1="0.4254" y1="0.6" x2="0.4254" y2="-0.6" width="0.127" layer="21"/>
<wire x1="0.4" y1="-0.6" x2="-0.4" y2="-0.6" width="0.127" layer="51"/>
<wire x1="-0.4254" y1="-0.6" x2="-0.4254" y2="0.6" width="0.127" layer="21"/>
<wire x1="-0.4" y1="0.6" x2="0.4" y2="0.6" width="0.127" layer="51"/>
<wire x1="0" y1="0.05" x2="0.25" y2="-0.2" width="0.127" layer="21"/>
<wire x1="0.25" y1="-0.2" x2="-0.25" y2="-0.2" width="0.127" layer="21"/>
<wire x1="-0.25" y1="-0.2" x2="0" y2="0.05" width="0.127" layer="21"/>
<rectangle x1="-0.1" y1="0.45" x2="0.1" y2="0.85" layer="51" rot="R270"/>
<rectangle x1="-0.1" y1="-0.85" x2="0.1" y2="-0.45" layer="51" rot="R270"/>
<rectangle x1="-0.1" y1="-0.2254" x2="0.1" y2="0.5746" layer="21" rot="R270"/>
<polygon width="0.0508" layer="21">
<vertex x="0" y="0.05"/>
<vertex x="0.25" y="-0.2"/>
<vertex x="-0.25" y="-0.2"/>
</polygon>
</package>
<package name="SOD-323">
<description>&lt;b&gt;SOD323&lt;/b&gt; (2.5x1.2mm)</description>
<smd name="C" x="-1.27" y="0" dx="1.35" dy="0.8" layer="1"/>
<smd name="A" x="1.27" y="0" dx="1.35" dy="0.8" layer="1"/>
<text x="-1.1" y="1" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.1" y="-1.792" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<wire x1="-1" y1="0.7" x2="1" y2="0.7" width="0.2032" layer="21"/>
<wire x1="1" y1="0.7" x2="1" y2="-0.7" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.7" x2="-1" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="-1" y1="-0.7" x2="-1" y2="0.7" width="0.2032" layer="51"/>
<wire x1="-0.25" y1="0" x2="0.35" y2="0.4" width="0.2032" layer="21"/>
<wire x1="0.35" y1="0.4" x2="0.35" y2="-0.4" width="0.2032" layer="21"/>
<wire x1="0.35" y1="-0.4" x2="-0.25" y2="0" width="0.2032" layer="21"/>
<rectangle x1="-0.45" y1="-0.5" x2="-0.25" y2="0.5" layer="21"/>
<polygon width="0.2032" layer="21">
<vertex x="-0.1" y="0"/>
<vertex x="0.2" y="0.2"/>
<vertex x="0.2" y="-0.2"/>
</polygon>
</package>
<package name="SOD-123">
<description>&lt;b&gt;SOD-123&lt;/b&gt;
&lt;p&gt;Source: http://www.diodes.com/datasheets/ds30139.pdf&lt;/p&gt;</description>
<smd name="C" x="-1.85" y="0" dx="1.4" dy="1.4" layer="1" rot="R90"/>
<smd name="A" x="1.85" y="0" dx="1.4" dy="1.4" layer="1" rot="R90"/>
<text x="-1.27" y="1.016" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.27" y="-1.778" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<wire x1="-0.873" y1="0.7" x2="0.873" y2="0.7" width="0.2032" layer="21"/>
<wire x1="0.873" y1="0.7" x2="0.873" y2="-0.7" width="0.2032" layer="51"/>
<wire x1="0.873" y1="-0.7" x2="-0.873" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="-0.873" y1="-0.7" x2="-0.873" y2="0.7" width="0.2032" layer="51"/>
<wire x1="-0.3" y1="0" x2="0.3" y2="0.4" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0.4" x2="0.3" y2="-0.4" width="0.2032" layer="21"/>
<wire x1="0.3" y1="-0.4" x2="-0.3" y2="0" width="0.2032" layer="21"/>
<rectangle x1="-1.723" y1="-0.45" x2="-0.973" y2="0.4" layer="51"/>
<rectangle x1="0.973" y1="-0.45" x2="1.723" y2="0.4" layer="51"/>
<rectangle x1="-0.5" y1="-0.5" x2="-0.3" y2="0.5" layer="21"/>
<polygon width="0.2032" layer="21">
<vertex x="-0.1" y="0"/>
<vertex x="0.2" y="0.2"/>
<vertex x="0.2" y="-0.2"/>
</polygon>
</package>
<package name="SOT23-WIDE">
<wire x1="1.5724" y1="0.6604" x2="1.5724" y2="-0.6604" width="0.2032" layer="51"/>
<wire x1="1.5724" y1="-0.6604" x2="-1.5724" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="-0.6604" x2="-1.5724" y2="0.6604" width="0.2032" layer="51"/>
<wire x1="-1.5724" y1="0.6604" x2="1.5724" y2="0.6604" width="0.2032" layer="51"/>
<wire x1="-1.6724" y1="-0.6524" x2="-1.6724" y2="0.6604" width="0.2032" layer="21"/>
<wire x1="-1.6724" y1="0.6604" x2="-0.7136" y2="0.6604" width="0.2032" layer="21"/>
<wire x1="1.6724" y1="0.6604" x2="1.6724" y2="-0.6524" width="0.2032" layer="21"/>
<wire x1="0.7136" y1="0.6604" x2="1.6724" y2="0.6604" width="0.2032" layer="21"/>
<wire x1="0.2224" y1="-0.6604" x2="-0.2364" y2="-0.6604" width="0.2032" layer="21"/>
<smd name="3" x="0" y="1" dx="1" dy="1.2" layer="1"/>
<smd name="2" x="0.95" y="-1" dx="1" dy="1.2" layer="1"/>
<smd name="1" x="-0.95" y="-1" dx="1" dy="1.2" layer="1"/>
<text x="1.905" y="0" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="1.905" y="-0.635" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
<package name="POWERDI-5">
<smd name="A1" x="-0.92" y="-2.84" dx="1.39" dy="1.4" layer="1"/>
<smd name="A2" x="0.92" y="-2.84" dx="1.39" dy="1.4" layer="1"/>
<smd name="C" x="0" y="1.142" dx="3.36" dy="4.86" layer="1"/>
<wire x1="-2" y1="3.25" x2="2" y2="3.25" width="0.127" layer="51"/>
<wire x1="2" y1="3.25" x2="2" y2="-3.25" width="0.127" layer="51"/>
<wire x1="2" y1="-3.25" x2="-2" y2="-3.25" width="0.127" layer="51"/>
<wire x1="-2" y1="-3.25" x2="-2" y2="3.25" width="0.127" layer="51"/>
<wire x1="-1.8" y1="-3" x2="-2.1" y2="-3" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-3" x2="-2.1" y2="3" width="0.127" layer="21"/>
<wire x1="-2.1" y1="3" x2="-1.9" y2="3" width="0.127" layer="21"/>
<wire x1="1.9" y1="3.1" x2="2.1" y2="3.1" width="0.127" layer="21"/>
<wire x1="2.1" y1="3.1" x2="2.1" y2="-3" width="0.127" layer="21"/>
<wire x1="2.1" y1="-3" x2="1.8" y2="-3" width="0.127" layer="21"/>
<text x="-2.34" y="-2.99" size="0.8128" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<text x="2.66" y="-3.04" size="0.4064" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<polygon width="0.3048" layer="51">
<vertex x="-1.5" y="-0.6"/>
<vertex x="0" y="1.9"/>
<vertex x="1.5" y="-0.6"/>
</polygon>
<rectangle x1="-1.6" y1="2" x2="1.6" y2="2.4" layer="51"/>
</package>
<package name="1X16_OVAL">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-20.32" y1="0.635" x2="-20.32" y2="-0.635" width="0.2032" layer="51"/>
<pad name="1" x="-19.05" y="0" drill="1" shape="long" rot="R90"/>
<pad name="2" x="-16.51" y="0" drill="1" shape="long" rot="R90"/>
<pad name="3" x="-13.97" y="0" drill="1" shape="long" rot="R90"/>
<pad name="4" x="-11.43" y="0" drill="1" shape="long" rot="R90"/>
<pad name="5" x="-8.89" y="0" drill="1" shape="long" rot="R90"/>
<pad name="6" x="-6.35" y="0" drill="1" shape="long" rot="R90"/>
<pad name="7" x="-3.81" y="0" drill="1" shape="long" rot="R90"/>
<pad name="8" x="-1.27" y="0" drill="1" shape="long" rot="R90"/>
<pad name="9" x="1.27" y="0" drill="1" shape="long" rot="R90"/>
<pad name="10" x="3.81" y="0" drill="1" shape="long" rot="R90"/>
<pad name="11" x="6.35" y="0" drill="1" shape="long" rot="R90"/>
<pad name="12" x="8.89" y="0" drill="1" shape="long" rot="R90"/>
<pad name="13" x="11.43" y="0" drill="1" shape="long" rot="R90"/>
<pad name="14" x="13.97" y="0" drill="1" shape="long" rot="R90"/>
<pad name="15" x="16.51" y="0" drill="1" shape="long" rot="R90"/>
<pad name="16" x="19.05" y="0" drill="1" shape="long" rot="R90"/>
<text x="-20.3962" y="1.8288" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-20.32" y="-3.175" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="16.256" y1="-0.254" x2="16.764" y2="0.254" layer="51"/>
<rectangle x1="13.716" y1="-0.254" x2="14.224" y2="0.254" layer="51"/>
<rectangle x1="11.176" y1="-0.254" x2="11.684" y2="0.254" layer="51"/>
<rectangle x1="8.636" y1="-0.254" x2="9.144" y2="0.254" layer="51"/>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="-9.144" y1="-0.254" x2="-8.636" y2="0.254" layer="51"/>
<rectangle x1="-11.684" y1="-0.254" x2="-11.176" y2="0.254" layer="51"/>
<rectangle x1="-14.224" y1="-0.254" x2="-13.716" y2="0.254" layer="51"/>
<rectangle x1="-16.764" y1="-0.254" x2="-16.256" y2="0.254" layer="51"/>
<rectangle x1="-19.304" y1="-0.254" x2="-18.796" y2="0.254" layer="51"/>
<rectangle x1="18.796" y1="-0.254" x2="19.304" y2="0.254" layer="51"/>
</package>
<package name="1X16_ROUND">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-20.32" y1="0.635" x2="-20.32" y2="-0.635" width="0.2032" layer="51"/>
<pad name="1" x="-19.05" y="0" drill="1" diameter="1.6764" rot="R90"/>
<pad name="2" x="-16.51" y="0" drill="1" diameter="1.6764" rot="R90"/>
<pad name="3" x="-13.97" y="0" drill="1" diameter="1.6764" rot="R90"/>
<pad name="4" x="-11.43" y="0" drill="1" diameter="1.6764" rot="R90"/>
<pad name="5" x="-8.89" y="0" drill="1" diameter="1.6764" rot="R90"/>
<pad name="6" x="-6.35" y="0" drill="1" diameter="1.6764" rot="R90"/>
<pad name="7" x="-3.81" y="0" drill="1" diameter="1.6764" rot="R90"/>
<pad name="8" x="-1.27" y="0" drill="1" diameter="1.6764" rot="R90"/>
<pad name="9" x="1.27" y="0" drill="1" diameter="1.6764" rot="R90"/>
<pad name="10" x="3.81" y="0" drill="1" diameter="1.6764" rot="R90"/>
<pad name="11" x="6.35" y="0" drill="1" diameter="1.6764" rot="R90"/>
<pad name="12" x="8.89" y="0" drill="1" diameter="1.6764" rot="R90"/>
<pad name="13" x="11.43" y="0" drill="1" diameter="1.6764" rot="R90"/>
<pad name="14" x="13.97" y="0" drill="1" diameter="1.6764" rot="R90"/>
<pad name="15" x="16.51" y="0" drill="1" diameter="1.6764" rot="R90"/>
<pad name="16" x="19.05" y="0" drill="1" diameter="1.6764" rot="R90"/>
<text x="-20.3962" y="1.8288" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-20.32" y="-3.175" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="16.256" y1="-0.254" x2="16.764" y2="0.254" layer="51"/>
<rectangle x1="13.716" y1="-0.254" x2="14.224" y2="0.254" layer="51"/>
<rectangle x1="11.176" y1="-0.254" x2="11.684" y2="0.254" layer="51"/>
<rectangle x1="8.636" y1="-0.254" x2="9.144" y2="0.254" layer="51"/>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="-9.144" y1="-0.254" x2="-8.636" y2="0.254" layer="51"/>
<rectangle x1="-11.684" y1="-0.254" x2="-11.176" y2="0.254" layer="51"/>
<rectangle x1="-14.224" y1="-0.254" x2="-13.716" y2="0.254" layer="51"/>
<rectangle x1="-16.764" y1="-0.254" x2="-16.256" y2="0.254" layer="51"/>
<rectangle x1="-19.304" y1="-0.254" x2="-18.796" y2="0.254" layer="51"/>
<rectangle x1="18.796" y1="-0.254" x2="19.304" y2="0.254" layer="51"/>
</package>
<package name="1X16_2MM_OVAL">
<wire x1="-16.32" y1="0.635" x2="-16.32" y2="-0.635" width="0.2032" layer="51"/>
<pad name="P$1" x="-15" y="0" drill="0.8" diameter="1.27" shape="long" rot="R90"/>
<pad name="P$2" x="-13" y="0" drill="0.8" diameter="1.27" shape="long" rot="R90"/>
<pad name="P$3" x="-11" y="0" drill="0.8" diameter="1.27" shape="long" rot="R90"/>
<pad name="P$4" x="-9" y="0" drill="0.8" diameter="1.27" shape="long" rot="R90"/>
<pad name="P$5" x="-7" y="0" drill="0.8" diameter="1.27" shape="long" rot="R90"/>
<pad name="P$6" x="-5" y="0" drill="0.8" diameter="1.27" shape="long" rot="R90"/>
<pad name="P$7" x="-3" y="0" drill="0.8" diameter="1.27" shape="long" rot="R90"/>
<pad name="P$8" x="-1" y="0" drill="0.8" diameter="1.27" shape="long" rot="R90"/>
<pad name="P$9" x="1" y="0" drill="0.8" diameter="1.27" shape="long" rot="R90"/>
<pad name="P$10" x="3" y="0" drill="0.8" diameter="1.27" shape="long" rot="R90"/>
<pad name="P$11" x="5" y="0" drill="0.8" diameter="1.27" shape="long" rot="R90"/>
<pad name="P$12" x="7" y="0" drill="0.8" diameter="1.27" shape="long" rot="R90"/>
<pad name="P$13" x="9" y="0" drill="0.8" diameter="1.27" shape="long" rot="R90"/>
<pad name="P$14" x="11" y="0" drill="0.8" diameter="1.27" shape="long" rot="R90"/>
<pad name="P$15" x="13" y="0" drill="0.8" diameter="1.27" shape="long" rot="R90"/>
<pad name="P$16" x="15" y="0" drill="0.8" diameter="1.27" shape="long" rot="R90"/>
<text x="-15.3962" y="1.8288" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-15.32" y="-3.175" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-15.254" y1="-0.254" x2="-14.746" y2="0.254" layer="51"/>
<rectangle x1="-13.254" y1="-0.254" x2="-12.746" y2="0.254" layer="51"/>
<rectangle x1="-11.254" y1="-0.254" x2="-10.746" y2="0.254" layer="51"/>
<rectangle x1="-9.254" y1="-0.254" x2="-8.746" y2="0.254" layer="51"/>
<rectangle x1="-7.254" y1="-0.254" x2="-6.746" y2="0.254" layer="51"/>
<rectangle x1="-5.254" y1="-0.254" x2="-4.746" y2="0.254" layer="51"/>
<rectangle x1="-3.254" y1="-0.254" x2="-2.746" y2="0.254" layer="51"/>
<rectangle x1="-1.254" y1="-0.254" x2="-0.746" y2="0.254" layer="51"/>
<rectangle x1="0.746" y1="-0.254" x2="1.254" y2="0.254" layer="51"/>
<rectangle x1="2.746" y1="-0.254" x2="3.254" y2="0.254" layer="51"/>
<rectangle x1="4.746" y1="-0.254" x2="5.254" y2="0.254" layer="51"/>
<rectangle x1="6.746" y1="-0.254" x2="7.254" y2="0.254" layer="51"/>
<rectangle x1="8.746" y1="-0.254" x2="9.254" y2="0.254" layer="51"/>
<rectangle x1="10.746" y1="-0.254" x2="11.254" y2="0.254" layer="51"/>
<rectangle x1="12.746" y1="-0.254" x2="13.254" y2="0.254" layer="51"/>
<rectangle x1="14.746" y1="-0.254" x2="15.254" y2="0.254" layer="51"/>
</package>
<package name="JSTPH2">
<description>2-Pin JST PH Series Right-Angle Connector (+/- for batteries)</description>
<wire x1="-4" y1="3" x2="4" y2="3" width="0.2032" layer="51"/>
<wire x1="4" y1="3" x2="4" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-4" y1="-4.5" x2="-4" y2="3" width="0.2032" layer="51"/>
<wire x1="3.2" y1="-2" x2="-3.2" y2="-2" width="0.2032" layer="51"/>
<wire x1="-3.2" y1="-2" x2="-3.2" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-3.2" y1="-4.5" x2="-4" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="4" y1="-4.5" x2="3.2" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="3.2" y1="-4.5" x2="3.2" y2="-2" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="3" x2="2.25" y2="3" width="0.2032" layer="21"/>
<wire x1="4" y1="-0.5" x2="4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="4" y1="-4.5" x2="3.15" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="3.15" y1="-4.5" x2="3.15" y2="-2" width="0.2032" layer="21"/>
<wire x1="3.15" y1="-2" x2="1.75" y2="-2" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2" x2="-3.15" y2="-2" width="0.2032" layer="21"/>
<wire x1="-3.15" y1="-2" x2="-3.15" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-3.15" y1="-4.5" x2="-4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-4" y1="-4.5" x2="-4" y2="-0.5" width="0.2032" layer="21"/>
<smd name="1" x="-1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="2" x="1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="NC1" x="-3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="NC2" x="3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<text x="-2.2225" y="1.9685" size="0.8128" layer="25" ratio="18">&gt;Name</text>
<text x="-2.2225" y="1.27" size="0.4064" layer="27" ratio="10">&gt;Value</text>
</package>
<package name="JSTSH2">
<wire x1="2" y1="2.125" x2="-2" y2="2.125" width="0.127" layer="20"/>
<wire x1="-2" y1="2.125" x2="-2" y2="-2.125" width="0.127" layer="20"/>
<wire x1="-2" y1="-2.125" x2="2" y2="-2.125" width="0.127" layer="20"/>
<wire x1="2" y1="-2.125" x2="2" y2="2.125" width="0.127" layer="20"/>
<wire x1="-0.889" y1="2.159" x2="0.889" y2="2.159" width="0.2032" layer="21"/>
<wire x1="-2.032" y1="-0.127" x2="-2.032" y2="-2.159" width="0.2032" layer="21"/>
<wire x1="-2.032" y1="-2.159" x2="-1.143" y2="-2.159" width="0.2032" layer="21"/>
<wire x1="2.032" y1="-0.127" x2="2.032" y2="-2.159" width="0.2032" layer="21"/>
<wire x1="2.032" y1="-2.159" x2="1.143" y2="-2.159" width="0.2032" layer="21"/>
<smd name="NC2" x="1.8" y="1.125" dx="1.2" dy="2" layer="1" rot="R180"/>
<smd name="NC1" x="-1.8" y="1.125" dx="1.2" dy="2" layer="1" rot="R180"/>
<smd name="2" x="0.5" y="-2.55" dx="1.35" dy="0.5" layer="1" rot="R270"/>
<smd name="1" x="-0.5" y="-2.55" dx="1.35" dy="0.5" layer="1" rot="R270"/>
<text x="-1.063" y="-2.404" size="1.4224" layer="21" ratio="12" rot="R180">-</text>
<text x="2.683" y="-2.404" size="1.4224" layer="21" ratio="12" rot="R180">+</text>
<text x="2.794" y="1.397" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="2.794" y="0.635" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="JSTPH2_NOTHERMALS">
<wire x1="-4" y1="3" x2="4" y2="3" width="0.2032" layer="51"/>
<wire x1="4" y1="3" x2="4" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-4" y1="-4.5" x2="-4" y2="3" width="0.2032" layer="51"/>
<wire x1="3.2" y1="-2" x2="-3.2" y2="-2" width="0.2032" layer="51"/>
<wire x1="-3.2" y1="-2" x2="-3.2" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-3.2" y1="-4.5" x2="-4" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="4" y1="-4.5" x2="3.2" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="3.2" y1="-4.5" x2="3.2" y2="-2" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="3" x2="2.25" y2="3" width="0.2032" layer="21"/>
<wire x1="4" y1="-0.5" x2="4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="4" y1="-4.5" x2="3.15" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="3.15" y1="-4.5" x2="3.15" y2="-2" width="0.2032" layer="21"/>
<wire x1="3.15" y1="-2" x2="1.75" y2="-2" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2" x2="-3.15" y2="-2" width="0.2032" layer="21"/>
<wire x1="-3.15" y1="-2" x2="-3.15" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-3.15" y1="-4.5" x2="-4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-4" y1="-4.5" x2="-4" y2="-0.5" width="0.2032" layer="21"/>
<smd name="1" x="-1" y="-3.7" dx="1" dy="4.6" layer="1" thermals="no"/>
<smd name="2" x="1" y="-3.7" dx="1" dy="4.6" layer="1" thermals="no"/>
<smd name="NC1" x="-3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="NC2" x="3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<text x="-2.2225" y="1.9685" size="0.8128" layer="25" ratio="18">&gt;Name</text>
<text x="-2.2225" y="1.27" size="0.4064" layer="27" ratio="10">&gt;Value</text>
</package>
<package name="1X16_2MM">
<wire x1="-16.32" y1="0.635" x2="-16.32" y2="-0.635" width="0.2032" layer="51"/>
<pad name="1" x="-15" y="0" drill="0.8" diameter="1.27"/>
<pad name="2" x="-13" y="0" drill="0.8" diameter="1.27"/>
<pad name="3" x="-11" y="0" drill="0.8" diameter="1.27"/>
<pad name="4" x="-9" y="0" drill="0.8" diameter="1.27"/>
<pad name="5" x="-7" y="0" drill="0.8" diameter="1.27"/>
<pad name="6" x="-5" y="0" drill="0.8" diameter="1.27"/>
<pad name="7" x="-3" y="0" drill="0.8" diameter="1.27"/>
<pad name="8" x="-1" y="0" drill="0.8" diameter="1.27"/>
<pad name="9" x="1" y="0" drill="0.8" diameter="1.27"/>
<pad name="10" x="3" y="0" drill="0.8" diameter="1.27"/>
<pad name="11" x="5" y="0" drill="0.8" diameter="1.27"/>
<pad name="12" x="7" y="0" drill="0.8" diameter="1.27"/>
<pad name="13" x="9" y="0" drill="0.8" diameter="1.27"/>
<pad name="14" x="11" y="0" drill="0.8" diameter="1.27"/>
<pad name="15" x="13" y="0" drill="0.8" diameter="1.27"/>
<pad name="16" x="15" y="0" drill="0.8" diameter="1.27"/>
<text x="-15.3962" y="1.8288" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-15.32" y="-3.175" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-15.254" y1="-0.254" x2="-14.746" y2="0.254" layer="51"/>
<rectangle x1="-13.254" y1="-0.254" x2="-12.746" y2="0.254" layer="51"/>
<rectangle x1="-11.254" y1="-0.254" x2="-10.746" y2="0.254" layer="51"/>
<rectangle x1="-9.254" y1="-0.254" x2="-8.746" y2="0.254" layer="51"/>
<rectangle x1="-7.254" y1="-0.254" x2="-6.746" y2="0.254" layer="51"/>
<rectangle x1="-5.254" y1="-0.254" x2="-4.746" y2="0.254" layer="51"/>
<rectangle x1="-3.254" y1="-0.254" x2="-2.746" y2="0.254" layer="51"/>
<rectangle x1="-1.254" y1="-0.254" x2="-0.746" y2="0.254" layer="51"/>
<rectangle x1="0.746" y1="-0.254" x2="1.254" y2="0.254" layer="51"/>
<rectangle x1="2.746" y1="-0.254" x2="3.254" y2="0.254" layer="51"/>
<rectangle x1="4.746" y1="-0.254" x2="5.254" y2="0.254" layer="51"/>
<rectangle x1="6.746" y1="-0.254" x2="7.254" y2="0.254" layer="51"/>
<rectangle x1="8.746" y1="-0.254" x2="9.254" y2="0.254" layer="51"/>
<rectangle x1="10.746" y1="-0.254" x2="11.254" y2="0.254" layer="51"/>
<rectangle x1="12.746" y1="-0.254" x2="13.254" y2="0.254" layer="51"/>
<rectangle x1="14.746" y1="-0.254" x2="15.254" y2="0.254" layer="51"/>
</package>
<package name="1X12_ROUND">
<wire x1="-15.24" y1="0.635" x2="-15.24" y2="-0.635" width="0.2032" layer="51"/>
<pad name="1" x="-13.97" y="0" drill="1" diameter="1.6764" rot="R90"/>
<pad name="2" x="-11.43" y="0" drill="1" diameter="1.6764" rot="R90"/>
<pad name="3" x="-8.89" y="0" drill="1" diameter="1.6764" rot="R90"/>
<pad name="4" x="-6.35" y="0" drill="1" diameter="1.6764" rot="R90"/>
<pad name="5" x="-3.81" y="0" drill="1" diameter="1.6764" rot="R90"/>
<pad name="6" x="-1.27" y="0" drill="1" diameter="1.6764" rot="R90"/>
<pad name="7" x="1.27" y="0" drill="1" diameter="1.6764" rot="R90"/>
<pad name="8" x="3.81" y="0" drill="1" diameter="1.6764" rot="R90"/>
<pad name="9" x="6.35" y="0" drill="1" diameter="1.6764" rot="R90"/>
<pad name="10" x="8.89" y="0" drill="1" diameter="1.6764" rot="R90"/>
<pad name="11" x="11.43" y="0" drill="1" diameter="1.6764" rot="R90"/>
<pad name="12" x="13.97" y="0" drill="1" diameter="1.6764" rot="R90"/>
<text x="-15.3162" y="1.8288" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-15.24" y="-3.175" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="-9.144" y1="-0.254" x2="-8.636" y2="0.254" layer="51"/>
<rectangle x1="-11.684" y1="-0.254" x2="-11.176" y2="0.254" layer="51"/>
<rectangle x1="-14.224" y1="-0.254" x2="-13.716" y2="0.254" layer="51"/>
<rectangle x1="8.636" y1="-0.254" x2="9.144" y2="0.254" layer="51"/>
<rectangle x1="11.176" y1="-0.254" x2="11.684" y2="0.254" layer="51"/>
<rectangle x1="13.716" y1="-0.254" x2="14.224" y2="0.254" layer="51"/>
</package>
<package name="1X12_OVAL">
<wire x1="-15.24" y1="0.635" x2="-15.24" y2="-0.635" width="0.2032" layer="51"/>
<pad name="1" x="-13.97" y="0" drill="1" diameter="1.6764" shape="long" rot="R90"/>
<pad name="2" x="-11.43" y="0" drill="1" diameter="1.6764" shape="long" rot="R90"/>
<pad name="3" x="-8.89" y="0" drill="1" diameter="1.6764" shape="long" rot="R90"/>
<pad name="4" x="-6.35" y="0" drill="1" diameter="1.6764" shape="long" rot="R90"/>
<pad name="5" x="-3.81" y="0" drill="1" diameter="1.6764" shape="long" rot="R90"/>
<pad name="6" x="-1.27" y="0" drill="1" diameter="1.6764" shape="long" rot="R90"/>
<pad name="7" x="1.27" y="0" drill="1" diameter="1.6764" shape="long" rot="R90"/>
<pad name="8" x="3.81" y="0" drill="1" diameter="1.6764" shape="long" rot="R90"/>
<pad name="9" x="6.35" y="0" drill="1" diameter="1.6764" shape="long" rot="R90"/>
<pad name="10" x="8.89" y="0" drill="1" diameter="1.6764" shape="long" rot="R90"/>
<pad name="11" x="11.43" y="0" drill="1" diameter="1.6764" shape="long" rot="R90"/>
<pad name="12" x="13.97" y="0" drill="1" diameter="1.6764" shape="long" rot="R90"/>
<text x="-15.3162" y="1.8288" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-15.24" y="-3.175" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="-9.144" y1="-0.254" x2="-8.636" y2="0.254" layer="51"/>
<rectangle x1="-11.684" y1="-0.254" x2="-11.176" y2="0.254" layer="51"/>
<rectangle x1="-14.224" y1="-0.254" x2="-13.716" y2="0.254" layer="51"/>
<rectangle x1="8.636" y1="-0.254" x2="9.144" y2="0.254" layer="51"/>
<rectangle x1="11.176" y1="-0.254" x2="11.684" y2="0.254" layer="51"/>
<rectangle x1="13.716" y1="-0.254" x2="14.224" y2="0.254" layer="51"/>
</package>
<package name="1X12_ROUND_80MIL">
<wire x1="-15.24" y1="0.635" x2="-15.24" y2="-0.635" width="0.2032" layer="51"/>
<pad name="1" x="-13.97" y="0" drill="1" diameter="2.032" rot="R90"/>
<pad name="2" x="-11.43" y="0" drill="1" diameter="2.032" rot="R90"/>
<pad name="3" x="-8.89" y="0" drill="1" diameter="2.032" rot="R90"/>
<pad name="4" x="-6.35" y="0" drill="1" diameter="2.032" rot="R90"/>
<pad name="5" x="-3.81" y="0" drill="1" diameter="2.032" rot="R90"/>
<pad name="6" x="-1.27" y="0" drill="1" diameter="2.032" rot="R90"/>
<pad name="7" x="1.27" y="0" drill="1" diameter="2.032" rot="R90"/>
<pad name="8" x="3.81" y="0" drill="1" diameter="2.032" rot="R90"/>
<pad name="9" x="6.35" y="0" drill="1" diameter="2.032" rot="R90"/>
<pad name="10" x="8.89" y="0" drill="1" diameter="2.032" rot="R90"/>
<pad name="11" x="11.43" y="0" drill="1" diameter="2.032" rot="R90"/>
<pad name="12" x="13.97" y="0" drill="1" diameter="2.032" rot="R90"/>
<text x="-15.3162" y="1.8288" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-15.24" y="-3.175" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="-9.144" y1="-0.254" x2="-8.636" y2="0.254" layer="51"/>
<rectangle x1="-11.684" y1="-0.254" x2="-11.176" y2="0.254" layer="51"/>
<rectangle x1="-14.224" y1="-0.254" x2="-13.716" y2="0.254" layer="51"/>
<rectangle x1="8.636" y1="-0.254" x2="9.144" y2="0.254" layer="51"/>
<rectangle x1="11.176" y1="-0.254" x2="11.684" y2="0.254" layer="51"/>
<rectangle x1="13.716" y1="-0.254" x2="14.224" y2="0.254" layer="51"/>
</package>
<package name="1X12_ROUND_76MIL">
<wire x1="-15.24" y1="0.635" x2="-15.24" y2="-0.635" width="0.2032" layer="51"/>
<pad name="1" x="-13.97" y="0" drill="1" diameter="1.9304" rot="R90"/>
<pad name="2" x="-11.43" y="0" drill="1" diameter="1.9304" rot="R90"/>
<pad name="3" x="-8.89" y="0" drill="1" diameter="1.9304" rot="R90"/>
<pad name="4" x="-6.35" y="0" drill="1" diameter="1.9304" rot="R90"/>
<pad name="5" x="-3.81" y="0" drill="1" diameter="1.9304" rot="R90"/>
<pad name="6" x="-1.27" y="0" drill="1" diameter="1.9304" rot="R90"/>
<pad name="7" x="1.27" y="0" drill="1" diameter="1.9304" rot="R90"/>
<pad name="8" x="3.81" y="0" drill="1" diameter="1.9304" rot="R90"/>
<pad name="9" x="6.35" y="0" drill="1" diameter="1.9304" rot="R90"/>
<pad name="10" x="8.89" y="0" drill="1" diameter="1.9304" rot="R90"/>
<pad name="11" x="11.43" y="0" drill="1" diameter="1.9304" rot="R90"/>
<pad name="12" x="13.97" y="0" drill="1" diameter="1.9304" rot="R90"/>
<text x="-15.3162" y="1.8288" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-15.24" y="-3.175" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="-9.144" y1="-0.254" x2="-8.636" y2="0.254" layer="51"/>
<rectangle x1="-11.684" y1="-0.254" x2="-11.176" y2="0.254" layer="51"/>
<rectangle x1="-14.224" y1="-0.254" x2="-13.716" y2="0.254" layer="51"/>
<rectangle x1="8.636" y1="-0.254" x2="9.144" y2="0.254" layer="51"/>
<rectangle x1="11.176" y1="-0.254" x2="11.684" y2="0.254" layer="51"/>
<rectangle x1="13.716" y1="-0.254" x2="14.224" y2="0.254" layer="51"/>
</package>
<package name="WS2812B">
<wire x1="2.5" y1="-2.5" x2="-2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="1.6" width="0.127" layer="21"/>
<wire x1="-2.5" y1="1.6" x2="-2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="2.5" x2="-1.6" y2="2.5" width="0.127" layer="21"/>
<wire x1="-1.6" y1="2.5" x2="2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="2.5" x2="2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="1.6" x2="-1.6" y2="2.5" width="0.127" layer="21"/>
<smd name="1-VDD" x="2.45" y="-1.65" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<smd name="2-DOUT" x="2.45" y="1.65" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<smd name="4-DIN" x="-2.45" y="-1.65" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<smd name="3-GND" x="-2.45" y="1.65" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<circle x="0" y="0" radius="1.7204625" width="0.127" layer="21"/>
<text x="3.4925" y="1.5875" size="0.8128" layer="25" ratio="10" rot="R270">&gt;NAME</text>
<wire x1="-1.6" y1="2.5" x2="-1.3" y2="2.8" width="0.127" layer="21"/>
<wire x1="-1.3" y1="2.8" x2="-1.7" y2="3.2" width="0.127" layer="21"/>
<wire x1="-1.7" y1="3.2" x2="-2.5" y2="2.5" width="0.127" layer="21"/>
</package>
<package name="WS2812B-NARROW">
<wire x1="2.5" y1="-2.5" x2="-2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="1.6" width="0.127" layer="21"/>
<wire x1="-2.5" y1="1.6" x2="-2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="2.5" x2="-1.6" y2="2.5" width="0.127" layer="21"/>
<wire x1="-1.6" y1="2.5" x2="2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="2.5" x2="2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="1.6" x2="-1.6" y2="2.5" width="0.127" layer="21"/>
<smd name="1-VDD" x="2.35" y="-1.65" dx="1.3" dy="1.2" layer="1" rot="R180"/>
<smd name="2-DOUT" x="2.35" y="1.65" dx="1.3" dy="1.2" layer="1" rot="R180"/>
<smd name="4-DIN" x="-2.35" y="-1.65" dx="1.3" dy="1.2" layer="1" rot="R180"/>
<smd name="3-GND" x="-2.35" y="1.65" dx="1.3" dy="1.2" layer="1" rot="R180"/>
<circle x="0" y="0" radius="1.7204625" width="0.127" layer="21"/>
<text x="3.4925" y="1.5875" size="0.8128" layer="25" ratio="10" rot="R270">&gt;NAME</text>
<wire x1="-1.6" y1="2.5" x2="-1.25" y2="2.85" width="0.127" layer="21"/>
<wire x1="-1.25" y1="2.85" x2="-1.7" y2="3.3" width="0.127" layer="21"/>
<wire x1="-1.7" y1="3.3" x2="-2.5" y2="2.5" width="0.127" layer="21"/>
</package>
<package name="LED3535">
<smd name="1" x="-1.75" y="0.875" dx="0.85" dy="1" layer="1" rot="R90"/>
<smd name="4" x="1.75" y="0.875" dx="0.85" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-1.75" y="-0.875" dx="0.85" dy="1" layer="1" rot="R90"/>
<smd name="3" x="1.75" y="-0.875" dx="0.85" dy="1" layer="1" rot="R90"/>
<wire x1="-1.75" y1="1.75" x2="1.75" y2="1.75" width="0.127" layer="51"/>
<wire x1="1.75" y1="1.75" x2="1.75" y2="-1.75" width="0.127" layer="51"/>
<wire x1="1.75" y1="-1.75" x2="-1.75" y2="-1.75" width="0.127" layer="51"/>
<wire x1="-1.75" y1="-1.75" x2="-1.75" y2="1.75" width="0.127" layer="51"/>
<circle x="0" y="0" radius="1.4" width="0.127" layer="51"/>
<wire x1="-1.9" y1="1.6" x2="-1.9" y2="1.9" width="0.127" layer="21"/>
<wire x1="-1.9" y1="1.9" x2="1.9" y2="1.9" width="0.127" layer="21"/>
<wire x1="1.9" y1="1.9" x2="1.9" y2="1.6" width="0.127" layer="21"/>
<wire x1="-1.9" y1="-1.6" x2="-1.9" y2="-1.9" width="0.127" layer="21"/>
<wire x1="-1.9" y1="-1.9" x2="1.9" y2="-1.9" width="0.127" layer="21"/>
<wire x1="1.9" y1="-1.9" x2="1.9" y2="-1.6" width="0.127" layer="21"/>
<text x="-1.905" y="2.159" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.778" y="-2.54" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<polygon width="0.127" layer="21">
<vertex x="-1.905" y="1.905"/>
<vertex x="-1.905" y="1.524"/>
<vertex x="-1.524" y="1.524"/>
<vertex x="-1.143" y="1.905"/>
</polygon>
</package>
<package name="SOIC8_150MIL">
<description>&lt;b&gt;Small Outline IC - 150mil Wide&lt;/b&gt;</description>
<wire x1="2.4" y1="1.9" x2="2.4" y2="-1.4" width="0.2032" layer="51"/>
<wire x1="2.4" y1="-1.4" x2="2.4" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="2.4" y1="-1.9" x2="-2.4" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="-1.9" x2="-2.4" y2="-1.4" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="-1.4" x2="-2.4" y2="1.9" width="0.2032" layer="51"/>
<wire x1="2.4" y1="-1.4" x2="-2.4" y2="-1.4" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="1.9" x2="2.4" y2="1.9" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="-1.9" x2="-2.4" y2="1.9" width="0.2032" layer="21"/>
<wire x1="2.4" y1="1.9" x2="2.4" y2="-1.9" width="0.2032" layer="21"/>
<circle x="-1.9304" y="-0.889" radius="0.254" width="0.2032" layer="21"/>
<smd name="2" x="-0.635" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="7" x="-0.635" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="1" x="-1.905" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="3" x="0.635" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="4" x="1.905" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="8" x="-1.905" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="6" x="0.635" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="5" x="1.905" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<text x="-1.905" y="0.381" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.905" y="-0.381" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.15" y1="-3.1" x2="-1.66" y2="-2" layer="51"/>
<rectangle x1="-0.88" y1="-3.1" x2="-0.39" y2="-2" layer="51"/>
<rectangle x1="0.39" y1="-3.1" x2="0.88" y2="-2" layer="51"/>
<rectangle x1="1.66" y1="-3.1" x2="2.15" y2="-2" layer="51"/>
<rectangle x1="1.66" y1="2" x2="2.15" y2="3.1" layer="51"/>
<rectangle x1="0.39" y1="2" x2="0.88" y2="3.1" layer="51"/>
<rectangle x1="-0.88" y1="2" x2="-0.39" y2="3.1" layer="51"/>
<rectangle x1="-2.15" y1="2" x2="-1.66" y2="3.1" layer="51"/>
</package>
<package name="SOIC8_208MIL">
<description>&lt;b&gt;Small Outline IC - 208mil Wide&lt;/b&gt;</description>
<wire x1="2.4" y1="2.615" x2="2.4" y2="-2.615" width="0.2032" layer="51"/>
<wire x1="2.4" y1="-2.615" x2="-2.4" y2="-2.615" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="-2.615" x2="-2.4" y2="2.615" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="2.615" x2="2.4" y2="2.615" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="-1.9" x2="-2.4" y2="1.9" width="0.2032" layer="21"/>
<wire x1="2.4" y1="1.9" x2="2.4" y2="-1.9" width="0.2032" layer="21"/>
<circle x="-1.9304" y="-0.889" radius="0.254" width="0.2032" layer="21"/>
<smd name="2" x="-0.635" y="-3.315" dx="0.6" dy="2.2" layer="1"/>
<smd name="7" x="-0.635" y="3.315" dx="0.6" dy="2.2" layer="1"/>
<smd name="1" x="-1.905" y="-3.315" dx="0.6" dy="2.2" layer="1"/>
<smd name="3" x="0.635" y="-3.315" dx="0.6" dy="2.2" layer="1"/>
<smd name="4" x="1.905" y="-3.315" dx="0.6" dy="2.2" layer="1"/>
<smd name="8" x="-1.905" y="3.315" dx="0.6" dy="2.2" layer="1"/>
<smd name="6" x="0.635" y="3.315" dx="0.6" dy="2.2" layer="1"/>
<smd name="5" x="1.905" y="3.315" dx="0.6" dy="2.2" layer="1"/>
<text x="-1.905" y="0.635" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.905" y="-0.381" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.15" y1="-3.815" x2="-1.66" y2="-2.715" layer="51"/>
<rectangle x1="-0.88" y1="-3.815" x2="-0.39" y2="-2.715" layer="51"/>
<rectangle x1="0.39" y1="-3.815" x2="0.88" y2="-2.715" layer="51"/>
<rectangle x1="1.66" y1="-3.815" x2="2.15" y2="-2.715" layer="51"/>
<rectangle x1="1.66" y1="2.715" x2="2.15" y2="3.815" layer="51"/>
<rectangle x1="0.39" y1="2.715" x2="0.88" y2="3.815" layer="51"/>
<rectangle x1="-0.88" y1="2.715" x2="-0.39" y2="3.815" layer="51"/>
<rectangle x1="-2.15" y1="2.715" x2="-1.66" y2="3.815" layer="51"/>
</package>
<package name="1X16_ROUND_76MIL">
<pad name="1" x="-19.05" y="0" drill="1" diameter="1.9304" rot="R90"/>
<pad name="2" x="-16.51" y="0" drill="1" diameter="1.9304" rot="R90"/>
<pad name="3" x="-13.97" y="0" drill="1" diameter="1.9304" rot="R90"/>
<pad name="4" x="-11.43" y="0" drill="1" diameter="1.9304" rot="R90"/>
<pad name="5" x="-8.89" y="0" drill="1" diameter="1.9304" rot="R90"/>
<pad name="6" x="-6.35" y="0" drill="1" diameter="1.9304" rot="R90"/>
<pad name="7" x="-3.81" y="0" drill="1" diameter="1.9304" rot="R90"/>
<pad name="8" x="-1.27" y="0" drill="1" diameter="1.9304" rot="R90"/>
<pad name="9" x="1.27" y="0" drill="1" diameter="1.9304" rot="R90"/>
<pad name="10" x="3.81" y="0" drill="1" diameter="1.9304" rot="R90"/>
<pad name="11" x="6.35" y="0" drill="1" diameter="1.9304" rot="R90"/>
<pad name="12" x="8.89" y="0" drill="1" diameter="1.9304" rot="R90"/>
<pad name="13" x="11.43" y="0" drill="1" diameter="1.9304" rot="R90"/>
<pad name="14" x="13.97" y="0" drill="1" diameter="1.9304" rot="R90"/>
<pad name="15" x="16.51" y="0" drill="1" diameter="1.9304" rot="R90"/>
<pad name="16" x="19.05" y="0" drill="1" diameter="1.9304" rot="R90"/>
<text x="-20.3962" y="1.8288" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-20.32" y="-3.175" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="16.256" y1="-0.254" x2="16.764" y2="0.254" layer="51"/>
<rectangle x1="13.716" y1="-0.254" x2="14.224" y2="0.254" layer="51"/>
<rectangle x1="11.176" y1="-0.254" x2="11.684" y2="0.254" layer="51"/>
<rectangle x1="8.636" y1="-0.254" x2="9.144" y2="0.254" layer="51"/>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="-9.144" y1="-0.254" x2="-8.636" y2="0.254" layer="51"/>
<rectangle x1="-11.684" y1="-0.254" x2="-11.176" y2="0.254" layer="51"/>
<rectangle x1="-14.224" y1="-0.254" x2="-13.716" y2="0.254" layer="51"/>
<rectangle x1="-16.764" y1="-0.254" x2="-16.256" y2="0.254" layer="51"/>
<rectangle x1="-19.304" y1="-0.254" x2="-18.796" y2="0.254" layer="51"/>
<rectangle x1="18.796" y1="-0.254" x2="19.304" y2="0.254" layer="51"/>
</package>
<package name="1X16_ROUND_MIN">
<wire x1="-20.32" y1="0.635" x2="-20.32" y2="-0.635" width="0.2032" layer="51"/>
<pad name="1" x="-19.05" y="0" drill="0.9" diameter="1.5748" rot="R90"/>
<pad name="2" x="-16.51" y="0" drill="0.9" diameter="1.5748" rot="R90"/>
<pad name="3" x="-13.97" y="0" drill="0.9" diameter="1.5748" rot="R90"/>
<pad name="4" x="-11.43" y="0" drill="0.9" diameter="1.5748" rot="R90"/>
<pad name="5" x="-8.89" y="0" drill="0.9" diameter="1.5748" rot="R90"/>
<pad name="6" x="-6.35" y="0" drill="0.9" diameter="1.5748" rot="R90"/>
<pad name="7" x="-3.81" y="0" drill="0.9" diameter="1.5748" rot="R90"/>
<pad name="8" x="-1.27" y="0" drill="0.9" diameter="1.5748" rot="R90"/>
<pad name="9" x="1.27" y="0" drill="0.9" diameter="1.5748" rot="R90"/>
<pad name="10" x="3.81" y="0" drill="0.9" diameter="1.5748" rot="R90"/>
<pad name="11" x="6.35" y="0" drill="0.9" diameter="1.5748" rot="R90"/>
<pad name="12" x="8.89" y="0" drill="0.9" diameter="1.5748" rot="R90"/>
<pad name="13" x="11.43" y="0" drill="0.9" diameter="1.5748" rot="R90"/>
<pad name="14" x="13.97" y="0" drill="0.9" diameter="1.5748" rot="R90"/>
<pad name="15" x="16.51" y="0" drill="0.9" diameter="1.5748" rot="R90"/>
<pad name="16" x="19.05" y="0" drill="0.9" diameter="1.5748" rot="R90"/>
<text x="-20.3962" y="1.8288" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-20.32" y="-3.175" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="16.256" y1="-0.254" x2="16.764" y2="0.254" layer="51"/>
<rectangle x1="13.716" y1="-0.254" x2="14.224" y2="0.254" layer="51"/>
<rectangle x1="11.176" y1="-0.254" x2="11.684" y2="0.254" layer="51"/>
<rectangle x1="8.636" y1="-0.254" x2="9.144" y2="0.254" layer="51"/>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="-9.144" y1="-0.254" x2="-8.636" y2="0.254" layer="51"/>
<rectangle x1="-11.684" y1="-0.254" x2="-11.176" y2="0.254" layer="51"/>
<rectangle x1="-14.224" y1="-0.254" x2="-13.716" y2="0.254" layer="51"/>
<rectangle x1="-16.764" y1="-0.254" x2="-16.256" y2="0.254" layer="51"/>
<rectangle x1="-19.304" y1="-0.254" x2="-18.796" y2="0.254" layer="51"/>
<rectangle x1="18.796" y1="-0.254" x2="19.304" y2="0.254" layer="51"/>
</package>
<package name="1X12_ROUND_MIN">
<wire x1="-15.24" y1="0.635" x2="-15.24" y2="-0.635" width="0.2032" layer="51"/>
<pad name="1" x="-13.97" y="0" drill="0.9" diameter="1.5748" rot="R90"/>
<pad name="2" x="-11.43" y="0" drill="0.9" diameter="1.5748" rot="R90"/>
<pad name="3" x="-8.89" y="0" drill="0.9" diameter="1.5748" rot="R90"/>
<pad name="4" x="-6.35" y="0" drill="0.9" diameter="1.5748" rot="R90"/>
<pad name="5" x="-3.81" y="0" drill="0.9" diameter="1.5748" rot="R90"/>
<pad name="6" x="-1.27" y="0" drill="0.9" diameter="1.5748" rot="R90"/>
<pad name="7" x="1.27" y="0" drill="0.9" diameter="1.5748" rot="R90"/>
<pad name="8" x="3.81" y="0" drill="0.9" diameter="1.5748" rot="R90"/>
<pad name="9" x="6.35" y="0" drill="0.9" diameter="1.5748" rot="R90"/>
<pad name="10" x="8.89" y="0" drill="0.9" diameter="1.5748" rot="R90"/>
<pad name="11" x="11.43" y="0" drill="0.9" diameter="1.5748" rot="R90"/>
<pad name="12" x="13.97" y="0" drill="0.9" diameter="1.5748" rot="R90"/>
<text x="-15.3162" y="1.8288" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-15.24" y="-3.175" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="-9.144" y1="-0.254" x2="-8.636" y2="0.254" layer="51"/>
<rectangle x1="-11.684" y1="-0.254" x2="-11.176" y2="0.254" layer="51"/>
<rectangle x1="-14.224" y1="-0.254" x2="-13.716" y2="0.254" layer="51"/>
<rectangle x1="8.636" y1="-0.254" x2="9.144" y2="0.254" layer="51"/>
<rectangle x1="11.176" y1="-0.254" x2="11.684" y2="0.254" layer="51"/>
<rectangle x1="13.716" y1="-0.254" x2="14.224" y2="0.254" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="FRAME_A4">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="36.83" width="0.1016" layer="94"/>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
</symbol>
<symbol name="FRAME_DOCFIELD">
<description>&lt;p&gt;Frame doc - Courtesy Robert Starr (bobstarr.net)&lt;/p&gt;</description>
<wire x1="20.0152" y1="1.5748" x2="95.5802" y2="1.5748" width="0.6096" layer="94"/>
<wire x1="20.0152" y1="1.5748" x2="20.0152" y2="6.6548" width="0.3048" layer="94"/>
<wire x1="20.0152" y1="6.6548" x2="88.5952" y2="6.6548" width="0.3048" layer="94"/>
<wire x1="20.0152" y1="6.6548" x2="20.0152" y2="10.4648" width="0.3048" layer="94"/>
<wire x1="20.0152" y1="10.4648" x2="20.0152" y2="16.8148" width="0.3048" layer="94"/>
<wire x1="121.6152" y1="16.8148" x2="121.6152" y2="6.6548" width="0.6096" layer="94"/>
<wire x1="95.5802" y1="6.6548" x2="95.5802" y2="1.5748" width="0.3048" layer="94"/>
<wire x1="95.5802" y1="1.5748" x2="121.6152" y2="1.5748" width="0.6096" layer="94"/>
<wire x1="88.5952" y1="16.8148" x2="88.5952" y2="6.6548" width="0.3048" layer="94"/>
<wire x1="88.5952" y1="6.6548" x2="95.5802" y2="6.6548" width="0.3048" layer="94"/>
<wire x1="121.6152" y1="16.8148" x2="110.1852" y2="16.8148" width="0.3048" layer="94"/>
<wire x1="110.1852" y1="16.8148" x2="88.5952" y2="16.8148" width="0.3048" layer="94"/>
<wire x1="88.5952" y1="16.8148" x2="20.0152" y2="16.8148" width="0.3048" layer="94"/>
<wire x1="95.5802" y1="6.6548" x2="121.6152" y2="6.6548" width="0.3048" layer="94"/>
<wire x1="121.6152" y1="6.6548" x2="121.6152" y2="1.5748" width="0.6096" layer="94"/>
<wire x1="20.0152" y1="16.8148" x2="20.0152" y2="19.3548" width="0.3048" layer="94"/>
<wire x1="20.0152" y1="19.3548" x2="20.0152" y2="28.2448" width="0.3048" layer="94"/>
<wire x1="121.6152" y1="34.5948" x2="105.7402" y2="34.5948" width="0.6096" layer="94"/>
<wire x1="105.7402" y1="34.5948" x2="20.0152" y2="34.5948" width="0.6096" layer="94"/>
<wire x1="121.6152" y1="34.5948" x2="121.6152" y2="28.2448" width="0.6096" layer="94"/>
<wire x1="20.0152" y1="28.2448" x2="105.7402" y2="28.2448" width="0.3048" layer="94"/>
<wire x1="105.7402" y1="28.2448" x2="110.1852" y2="28.2448" width="0.3048" layer="94"/>
<wire x1="110.1852" y1="28.2448" x2="121.6152" y2="28.2448" width="0.3048" layer="94"/>
<wire x1="20.0152" y1="28.2448" x2="20.0152" y2="34.5948" width="0.3048" layer="94"/>
<wire x1="121.6152" y1="28.2448" x2="121.6152" y2="16.8148" width="0.6096" layer="94"/>
<wire x1="101.2952" y1="13.3223" x2="102.2477" y2="13.3223" width="0.254" layer="94"/>
<wire x1="110.1852" y1="28.2448" x2="110.1852" y2="16.8148" width="0.3048" layer="94"/>
<wire x1="105.7402" y1="28.2448" x2="105.7402" y2="34.5948" width="0.3048" layer="94"/>
<wire x1="20.0152" y1="34.5948" x2="-0.3048" y2="34.5948" width="0.6096" layer="94"/>
<wire x1="-0.3048" y1="34.5948" x2="-0.3048" y2="28.2448" width="0.6096" layer="94"/>
<wire x1="-0.3048" y1="28.2448" x2="-0.3048" y2="19.3548" width="0.6096" layer="94"/>
<wire x1="-0.3048" y1="19.3548" x2="-0.3048" y2="10.4648" width="0.6096" layer="94"/>
<wire x1="-0.3048" y1="10.4648" x2="-0.3048" y2="1.5748" width="0.6096" layer="94"/>
<wire x1="-0.3048" y1="1.5748" x2="20.0152" y2="1.5748" width="0.6096" layer="94"/>
<wire x1="-0.3048" y1="28.2448" x2="20.0152" y2="28.2448" width="0.3048" layer="94"/>
<wire x1="-0.3048" y1="19.3548" x2="20.0152" y2="19.3548" width="0.3048" layer="94"/>
<wire x1="-0.3048" y1="10.4648" x2="20.0152" y2="10.4648" width="0.3048" layer="94"/>
<circle x="109.2327" y="31.4198" radius="1.4199" width="0.254" layer="94"/>
<text x="108.9152" y="2.8448" size="2.54" layer="94" font="vector" ratio="10">&gt;SHEET</text>
<text x="112.4077" y="24.4348" size="2.54" layer="94" font="vector" ratio="12">REV</text>
<text x="21.2852" y="24.4348" size="2.54" layer="94" font="vector" ratio="12">TITLE</text>
<text x="89.8652" y="13.0048" size="2.54" layer="94" font="vector" ratio="12">DRG</text>
<text x="100.9777" y="13.9573" size="2.1844" layer="94" ratio="12">o</text>
<text x="33.0327" y="2.8448" size="2.54" layer="94" ratio="10">&gt;DRAWING_NAME</text>
<text x="98.4377" y="13.0048" size="2.54" layer="94" font="vector" ratio="12">N</text>
<text x="21.2852" y="13.0048" size="2.54" layer="94" font="vector" ratio="12">DATE</text>
<text x="32.7152" y="9.1948" size="2.54" layer="94" ratio="10">&gt;LAST_DATE_TIME</text>
<text x="112.4077" y="30.4673" size="1.778" layer="94" ratio="12">2015</text>
<text x="108.7564" y="30.7848" size="1.27" layer="94" ratio="12">C</text>
<text x="5.0927" y="30.4673" size="1.9304" layer="94" ratio="12">ISSUE</text>
<text x="0.9652" y="25.0698" size="1.778" layer="94" ratio="12">DRAWN</text>
<text x="0.9652" y="16.1798" size="1.778" layer="94" ratio="12">CHECKED</text>
<text x="0.9652" y="7.2898" size="1.778" layer="94" ratio="12">DATE</text>
<text x="23.349" y="29.6735" size="3.175" layer="94" ratio="18">&gt;COMPANY</text>
<text x="21.2852" y="2.8448" size="2.54" layer="94" font="vector" ratio="12">FILE:</text>
<text x="96.8502" y="2.8448" size="2.54" layer="94" font="vector" ratio="12">PAGE:</text>
<text x="2.2352" y="20.6248" size="1.778" layer="94">&gt;DRAWN</text>
<text x="2.2352" y="11.7348" size="1.778" layer="94">&gt;CHECKED</text>
<text x="2.2352" y="2.8448" size="1.778" layer="94">&gt;DATE</text>
<text x="91.1352" y="9.1948" size="2.1844" layer="94">&gt;DRGNO</text>
<text x="113.9952" y="18.7198" size="3.81" layer="94" ratio="12">&gt;REV</text>
</symbol>
<symbol name="GND">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.524" y="-2.54" size="1.27" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="MOUNTINGHOLE">
<circle x="0" y="0" radius="2.54" width="0.254" layer="94"/>
<circle x="0" y="0" radius="1.905" width="0.254" layer="94"/>
</symbol>
<symbol name="VBUS">
<text x="-1.524" y="1.016" size="1.27" layer="96">&gt;VALUE</text>
<pin name="VBUS" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
<wire x1="-1.27" y1="-1.27" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
</symbol>
<symbol name="VBAT">
<text x="-1.524" y="1.016" size="1.27" layer="96">&gt;VALUE</text>
<pin name="VBAT" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
<wire x1="-1.27" y1="-1.27" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
</symbol>
<symbol name="VREG_SOT23-5">
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<text x="-7.62" y="6.096" size="1.27" layer="95">&gt;NAME</text>
<text x="-7.62" y="-7.62" size="1.27" layer="95">&gt;VALUE</text>
<pin name="IN" x="-10.16" y="2.54" length="short" direction="pwr"/>
<pin name="OUT" x="10.16" y="2.54" length="short" direction="sup" rot="R180"/>
<pin name="EN" x="-10.16" y="0" length="short" direction="in"/>
<pin name="GND" x="-10.16" y="-2.54" length="short" direction="pwr"/>
<pin name="P4" x="10.16" y="-2.54" length="short" rot="R180"/>
</symbol>
<symbol name="DIODE-SCHOTTKY">
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="0" y="2.54" size="1.27" layer="95" align="center">&gt;NAME</text>
<text x="0" y="-2.5" size="1.27" layer="96" align="center">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<wire x1="1.27" y1="-1.27" x2="1.778" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="0.762" y2="1.27" width="0.254" layer="94"/>
</symbol>
<symbol name="PINHD16">
<wire x1="-6.35" y1="-22.86" x2="1.27" y2="-22.86" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-22.86" x2="1.27" y2="20.32" width="0.4064" layer="94"/>
<wire x1="1.27" y1="20.32" x2="-6.35" y2="20.32" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="20.32" x2="-6.35" y2="-22.86" width="0.4064" layer="94"/>
<text x="-6.35" y="20.955" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-25.4" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="17.78" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="15.24" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="-2.54" y="10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="5" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="7" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="9" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="11" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="12" x="-2.54" y="-10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="13" x="-2.54" y="-12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="14" x="-2.54" y="-15.24" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="15" x="-2.54" y="-17.78" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="16" x="-2.54" y="-20.32" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="PINHD2">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="PINHD12">
<wire x1="-6.35" y1="-15.24" x2="1.27" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-15.24" x2="1.27" y2="17.78" width="0.4064" layer="94"/>
<wire x1="1.27" y1="17.78" x2="-6.35" y2="17.78" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="17.78" x2="-6.35" y2="-15.24" width="0.4064" layer="94"/>
<text x="-6.35" y="18.415" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="15.24" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="5" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="7" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="9" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="11" x="-2.54" y="-10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="12" x="-2.54" y="-12.7" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="MCP73831/2">
<pin name="VDD" x="-12.7" y="2.54" length="short" direction="pwr"/>
<pin name="STAT" x="-12.7" y="-2.54" length="short" direction="out"/>
<pin name="VBAT" x="12.7" y="2.54" length="short" direction="pwr" rot="R180"/>
<pin name="PROG" x="12.7" y="0" length="short" direction="in" rot="R180"/>
<pin name="VSS" x="12.7" y="-2.54" length="short" direction="pwr" rot="R180"/>
<wire x1="-10.16" y1="5.08" x2="10.16" y2="5.08" width="0.254" layer="94" style="shortdash"/>
<wire x1="10.16" y1="5.08" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="-10.16" y2="-5.08" width="0.254" layer="94" style="shortdash"/>
<wire x1="-10.16" y1="-5.08" x2="-10.16" y2="5.08" width="0.254" layer="94"/>
<text x="-5.715" y="8.255" size="1.27" layer="94">MCP73831/2</text>
<text x="-6.35" y="6.35" size="1.27" layer="94">LIPO Charger</text>
<text x="-8.255" y="-9.525" size="1.27" layer="94">Temp:</text>
<text x="0" y="-9.525" size="1.27" layer="94">-40-85°C</text>
<text x="-8.255" y="-7.62" size="1.27" layer="94">VDD:</text>
<text x="0" y="-7.62" size="1.27" layer="94">3.75-6V</text>
<wire x1="10.16" y1="-10.16" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="10.16" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="10.16" x2="-10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-5.08" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<text x="-10.16" y="11.43" size="1.27" layer="95">&gt;NAME</text>
<text x="-10.16" y="-12.7" size="1.27" layer="95">&gt;VALUE</text>
</symbol>
<symbol name="WS2812BLED">
<pin name="VDD" x="5.08" y="15.24" visible="pin" length="middle" direction="pwr" rot="R270"/>
<pin name="DI" x="-12.7" y="-2.54" visible="pin" length="middle" direction="in"/>
<pin name="GND" x="0" y="-10.16" visible="pin" length="middle" direction="pwr" rot="R90"/>
<pin name="DO" x="12.7" y="-2.54" visible="pin" length="middle" direction="out" rot="R180"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="5.08" y2="10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="10.16" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-6.35" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="3.81" y2="5.08" width="0.254" layer="94"/>
<wire x1="3.81" y1="5.08" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-6.35" y1="2.54" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="3.81" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-1.27" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="7.62" x2="-1.27" y2="7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-3.81" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-3.81" y2="5.08" width="0.254" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.27" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-5.08" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-4.064" x2="0" y2="-5.08" width="0.254" layer="94"/>
<text x="-4.064" y="8.382" size="1.27" layer="94">WS2812B</text>
<wire x1="-3.81" y1="2.54" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="5.08" y1="10.16" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="7.62" x2="5.08" y2="7.62" width="0.254" layer="94"/>
</symbol>
<symbol name="M25P16">
<wire x1="-12.7" y1="10.16" x2="12.7" y2="10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="10.16" x2="12.7" y2="-7.62" width="0.254" layer="94"/>
<wire x1="12.7" y1="-7.62" x2="-12.7" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-7.62" x2="-12.7" y2="10.16" width="0.254" layer="94"/>
<text x="-12.7" y="12.7" size="1.778" layer="95">&gt;NAME</text>
<text x="-12.7" y="-10.16" size="1.778" layer="95">&gt;VALUE</text>
<pin name="SCK" x="-15.24" y="7.62" length="short"/>
<pin name="MOSI" x="-15.24" y="5.08" length="short"/>
<pin name="MISO" x="-15.24" y="2.54" length="short"/>
<pin name="SSEL" x="-15.24" y="0" length="short"/>
<pin name="WP#/IO2" x="-15.24" y="-2.54" length="short"/>
<pin name="HOLD#/IO3" x="-15.24" y="-5.08" length="short"/>
<pin name="VCC" x="15.24" y="7.62" length="short" rot="R180"/>
<pin name="VSS" x="15.24" y="-5.08" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FRAME_A4" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="FRAME_A4" x="0" y="0"/>
<gate name="G$3" symbol="FRAME_DOCFIELD" x="134.62" y="3.81"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND">
<description>&lt;b&gt;GND&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MOUNTINGHOLE">
<description>&lt;b&gt;Mounting Hole&lt;/b&gt;
&lt;p&gt;For #2 screws (0.086"/2.18mm width, 0.094"/2.4mm hole) use 2.5mm&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="MOUNTINGHOLE" x="0" y="0"/>
</gates>
<devices>
<device name="3.0" package="MOUNTINGHOLE_3.0_PLATED">
<technologies>
<technology name="">
<attribute name="BOM" value="EXCLUDE" constant="no"/>
</technology>
</technologies>
</device>
<device name="1.0" package="MOUNTINGHOLE_1.0_PLATED">
<technologies>
<technology name="">
<attribute name="BOM" value="EXCLUDE" constant="no"/>
</technology>
</technologies>
</device>
<device name="2.0" package="MOUNTINGHOLE_2.0_PLATED">
<technologies>
<technology name="">
<attribute name="BOM" value="EXCLUDE" constant="no"/>
</technology>
</technologies>
</device>
<device name="3.0THIN" package="MOUNTINGHOLE_3.0_PLATEDTHIN">
<technologies>
<technology name="">
<attribute name="BOM" value="EXCLUDE" constant="no"/>
</technology>
</technologies>
</device>
<device name="2.5" package="MOUNTINGHOLE_2.5_PLATED">
<technologies>
<technology name="">
<attribute name="BOM" value="EXCLUDE" constant="no"/>
</technology>
</technologies>
</device>
<device name="2.5_THICK" package="MOUNTINGHOLE_2.5_PLATED_THICK">
<technologies>
<technology name="">
<attribute name="BOM" value="EXCLUDE" constant="no"/>
</technology>
</technologies>
</device>
<device name="" package="MOUNTINGHOLE_3.0_PLATED_VIAS">
<technologies>
<technology name="">
<attribute name="BOM" value="EXCLUDE" constant="no"/>
</technology>
</technologies>
</device>
<device name="3.0SQUARE" package="MOUNTINGHOLE_3.0_PLATED_SQUAREVIAS">
<technologies>
<technology name="">
<attribute name="BOM" value="EXCLUDE" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VBUS">
<description>&lt;p&gt;VBUS Supply Symbole&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="VBUS" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VBAT">
<description>VBAT Supply Sumbol</description>
<gates>
<gate name="G$1" symbol="VBAT" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VREG_SOT23-5" prefix="U" uservalue="yes">
<description>&lt;p&gt;&lt;b&gt;SOT23-5 Fixed Voltage Regulators&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;&lt;/p&gt;
&lt;table width="700"&gt;
&lt;tr bgcolor="#EEEEEE" &gt;
&lt;td&gt;&lt;b&gt;Part&lt;/b&gt;&lt;/td&gt;
&lt;td&gt;&lt;b&gt;Current Out&lt;/b&gt;&lt;/td&gt;
&lt;td&gt;&lt;b&gt;V Out&lt;/b&gt;&lt;/td&gt;
&lt;td&gt;&lt;b&gt;V In&lt;/b&gt;&lt;/td&gt;
&lt;td&gt;&lt;b&gt;V Dropout&lt;/b&gt;&lt;/td&gt;
&lt;td&gt;&lt;b&gt;θJA (°C/W)&lt;/b&gt;&lt;/td&gt;
&lt;td&gt;&lt;b&gt;TJ (°C)&lt;/b&gt;&lt;/td&gt;
&lt;td&gt;&lt;b&gt;Digikey Part No.&lt;/b&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;ADP121&lt;/td&gt;
  &lt;td&gt;150mA&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;3.3V&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;3.4-5.5V&lt;/td&gt;
  &lt;td&gt;0.09V @ 150mA&lt;/td&gt;
  &lt;td&gt;--&lt;/td&gt;
  &lt;td&gt;--&lt;/td&gt;
  &lt;td&gt;ADP121-AUJZ33R7CT-ND&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;ADP121&lt;/td&gt;
  &lt;td&gt;150mA&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;3.0V&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;3.1-5.5V&lt;/td&gt;
  &lt;td&gt;0.09V @ 150mA&lt;/td&gt;
  &lt;td&gt;--&lt;/td&gt;
  &lt;td&gt;--&lt;/td&gt;
  &lt;td&gt;ADP121-AUJZ30R7CT-ND&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;ADP122&lt;/td&gt;
  &lt;td&gt;&lt;strong&gt;300mA&lt;/strong&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;3.3V&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;3.4-5.5V&lt;/td&gt;
  &lt;td&gt;0.085V @ 300mA&lt;/td&gt;
  &lt;td&gt;--&lt;/td&gt;
  &lt;td&gt;--&lt;/td&gt;
  &lt;td&gt;ADP122AUJZ-3.3-R7CT-ND&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;ADP1712&lt;/td&gt;
  &lt;td&gt;&lt;strong&gt;300mA&lt;/strong&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;3.3V&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;3.5-5.5V&lt;/td&gt;
  &lt;td&gt;0.17V @ 300mA&lt;/td&gt;
  &lt;td&gt;--&lt;/td&gt;
  &lt;td&gt;--&lt;/td&gt;
  &lt;td&gt;ADP1712AUJZ-3.3-R7TR-ND&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;AP7311 (&lt;b&gt;Low Cost&lt;/b&gt;)&lt;/td&gt;
  &lt;td&gt;150mA&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;3.3V&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;3.5-6V&lt;/td&gt;
  &lt;td&gt;0.15V @ 150mA&lt;/td&gt;
  &lt;td&gt;--&lt;/td&gt;
  &lt;td&gt;--&lt;/td&gt;
  &lt;td&gt;AP7311-33WG-7DICT-ND&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
  &lt;td&gt;LD39015M18R&lt;/td&gt;
  &lt;td&gt;150mA&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;1.8V&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;1.9V-5.5V&lt;/td&gt;
  &lt;td&gt;0.08V @ 100mA&lt;/td&gt;
  &lt;td&gt;--&lt;/td&gt;
  &lt;td&gt;--&lt;/td&gt;
  &lt;td&gt;497-6977-1-ND&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
  &lt;td&gt;LP2985A-33DBVR&lt;/td&gt;
  &lt;td&gt;150mA&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;3.3V&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;3.3-16V&lt;/td&gt;
  &lt;td&gt;0.28V @ 150mA&lt;/td&gt;
  &lt;td&gt;206&lt;/td&gt;
  &lt;td&gt;150&lt;/td&gt;
  &lt;td&gt;296-18479-1-ND&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
  &lt;td&gt;MCP1824T-3302E/OT&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;300mA&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;3.3V&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;3.5V-6V&lt;/td&gt;
  &lt;td&gt;0.2V @ 300mA&lt;/td&gt;
  &lt;td&gt;--&lt;/td&gt;
  &lt;td&gt;--&lt;/td&gt;
  &lt;td&gt;MCP1824T-3302E/OTCT-ND&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
  &lt;td&gt;MIC5205-2.5YM5 TR&lt;/td&gt;
  &lt;td&gt;150mA&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;2.5V&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;2.7-16V&lt;/td&gt;
  &lt;td&gt;0.165V @ 150mA&lt;/td&gt;
  &lt;td&gt;--&lt;/td&gt;
  &lt;td&gt;--&lt;/td&gt;
  &lt;td&gt;576-1257-2-ND&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;MIC5205-3.0YM5 TR&lt;/td&gt;
  &lt;td&gt;150mA&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;3.0V&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;3.2V-16V&lt;/td&gt;
  &lt;td&gt;0.165V @ 150mA&lt;/td&gt;
  &lt;td&gt;--&lt;/td&gt;
  &lt;td&gt;--&lt;/td&gt;
  &lt;td&gt;576-1258-2-ND&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;MIC5205-3.3YM5 TR&lt;/td&gt;
  &lt;td&gt;150mA&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;3.3V&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;3.5V-16V&lt;/td&gt;
  &lt;td&gt;0.165V @ 150mA&lt;/td&gt;
  &lt;td&gt;--&lt;/td&gt;
  &lt;td&gt;--&lt;/td&gt;
  &lt;td&gt;576-1259-2-ND&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;TPS780330220&lt;/td&gt;
  &lt;td&gt;150mA&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;3.3V+2.2V&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;3.6-5.5V&lt;/td&gt;
  &lt;td&gt;250mV Max&lt;/td&gt;
  &lt;td&gt;--&lt;/td&gt;
  &lt;td&gt;--&lt;/td&gt;
  &lt;td&gt;296-23332-1-ND&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;TDA3663/N1,135&lt;/td&gt;
  &lt;td&gt;100mA&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;3.3V&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;3.5-&lt;b&gt;45V&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;0.18V @ 50mA&lt;/td&gt;
  &lt;td&gt;--&lt;/td&gt;
  &lt;td&gt;--&lt;/td&gt;
  &lt;td&gt;568-5343-1-ND&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
&lt;/tr&gt;

&lt;/table&gt;
&lt;p&gt;Max operating temp can be calculated using θJA, TJ (max junction temperature), and power in watts.  Set the "Maximum Ambient Temperature" until it reaches TJ ("Max Junction Temperature"), which is the absolute limit for safe use of the regulator: &lt;a href="http://www.daycounter.com/Calculators/Heat-Sink-Temperature-Calculator.phtml"&gt;Heat Sink Temperature Calculator&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;For example, With 12V source into a 3.3V LP2985 and a 30mA load, we are dissipating (12V-3.3V) * 0.03A = 0.261W.  With a θJA of 206 °C/W, a TJ of 150°C, and 261mW we can safely use the chip without a heat sink up to 75°C (=147.1°C Junction Temperature).&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="VREG_SOT23-5" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23-5">
<connects>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="IN" pad="1"/>
<connect gate="G$1" pin="OUT" pad="5"/>
<connect gate="G$1" pin="P4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE-SCHOTTKY" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="DIODE-SCHOTTKY" x="0" y="0"/>
</gates>
<devices>
<device name="SMA" package="SMADIODE">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DO-1N4148" package="DO-1N4148">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT23_REFLOW" package="SOT23-R">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-523" package="SOD-523">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-323" package="SOD-323">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-123" package="SOD-123">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT23_WIDE" package="SOT23-WIDE">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POWERDI" package="POWERDI-5">
<connects>
<connect gate="G$1" pin="A" pad="A1 A2"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HEADER-1X16" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINHD16" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X16_OVAL">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ROUND" package="1X16_ROUND">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2MM" package="1X16_2MM">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2MM_OVAL" package="1X16_2MM_OVAL">
<connects>
<connect gate="A" pin="1" pad="P$1"/>
<connect gate="A" pin="10" pad="P$10"/>
<connect gate="A" pin="11" pad="P$11"/>
<connect gate="A" pin="12" pad="P$12"/>
<connect gate="A" pin="13" pad="P$13"/>
<connect gate="A" pin="14" pad="P$14"/>
<connect gate="A" pin="15" pad="P$15"/>
<connect gate="A" pin="16" pad="P$16"/>
<connect gate="A" pin="2" pad="P$2"/>
<connect gate="A" pin="3" pad="P$3"/>
<connect gate="A" pin="4" pad="P$4"/>
<connect gate="A" pin="5" pad="P$5"/>
<connect gate="A" pin="6" pad="P$6"/>
<connect gate="A" pin="7" pad="P$7"/>
<connect gate="A" pin="8" pad="P$8"/>
<connect gate="A" pin="9" pad="P$9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_76MIL" package="1X16_ROUND_76MIL">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_MIN" package="1X16_ROUND_MIN">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CON_JST_PH_2PIN" prefix="X" uservalue="yes">
<description>JST 2-Pin Right-Angle Connector
&lt;ul&gt;
&lt;li&gt;PH-Series - 4UConnector: 17311&lt;/li&gt;
&lt;li&gt;SH-Series - 4UConnector: 07278&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="PINHD2" x="2.54" y="0"/>
</gates>
<devices>
<device name="" package="JSTPH2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SH2" package="JSTSH2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PH2_NOTHERMALS" package="JSTPH2_NOTHERMALS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HEADER-1X12" prefix="JP" uservalue="yes">
<gates>
<gate name="G$1" symbol="PINHD12" x="2.54" y="-2.54"/>
</gates>
<devices>
<device name="" package="1X12_ROUND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="OVAL" package="1X12_OVAL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="80MIL" package="1X12_ROUND_80MIL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="76MIL" package="1X12_ROUND_76MIL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_MIN" package="1X12_ROUND_MIN">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MCP73831/2" prefix="U" uservalue="yes">
<description>&lt;p&gt;&lt;b&gt;MCP73831/2 LIPO Charger&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;
&lt;ul&gt;
&lt;li&gt;VDD: 3.75 - 6V&lt;/li&gt;
&lt;li&gt;Temp -40 - 85°C&lt;/li&gt;
&lt;li&gt;Programmable Charge Rate: 15-500mA&lt;/li&gt;
&lt;/ul&gt;
&lt;/p&gt;
&lt;p&gt;&lt;b&gt;NOTE:&lt;/b&gt; STAT is a tri-state logic output on the MCP73831 and an open-drain output on the MCP73832.&lt;/p&gt;
&lt;p&gt;SOT23-5 with 4.2V output and tri-state logic on STAT = MCP73831T-2ACI/OT&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="MCP73831/2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23-5">
<connects>
<connect gate="G$1" pin="PROG" pad="5"/>
<connect gate="G$1" pin="STAT" pad="1"/>
<connect gate="G$1" pin="VBAT" pad="3"/>
<connect gate="G$1" pin="VDD" pad="4"/>
<connect gate="G$1" pin="VSS" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="WS2812B" prefix="LED">
<gates>
<gate name="G$1" symbol="WS2812BLED" x="0" y="-2.54"/>
</gates>
<devices>
<device name="5050" package="WS2812B">
<connects>
<connect gate="G$1" pin="DI" pad="4-DIN"/>
<connect gate="G$1" pin="DO" pad="2-DOUT"/>
<connect gate="G$1" pin="GND" pad="3-GND"/>
<connect gate="G$1" pin="VDD" pad="1-VDD"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_5050N" package="WS2812B-NARROW">
<connects>
<connect gate="G$1" pin="DI" pad="4-DIN"/>
<connect gate="G$1" pin="DO" pad="2-DOUT"/>
<connect gate="G$1" pin="GND" pad="3-GND"/>
<connect gate="G$1" pin="VDD" pad="1-VDD"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3535" package="LED3535">
<connects>
<connect gate="G$1" pin="DI" pad="1"/>
<connect gate="G$1" pin="DO" pad="3"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="VDD" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SPIFLASH_SOIC8" prefix="U" uservalue="yes">
<description>&lt;b&gt;SOIC8 SPI Flash&lt;/b&gt;
&lt;p&gt;Be careful with the size since SOIC8 flash comes in several 'widths'&lt;/p&gt;
&lt;p&gt;&lt;b&gt;SOIC8 150 mil&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;b&gt;M25P16&lt;/b&gt; - 16Mbit (2Mbit x 8) Serial Flash (75MHz SPI Bus), Supply: 2.7-3.6V &lt;br/&gt;&lt;b&gt;Digikey: &lt;/b&gt; SOIC8 - M25P16-VMN6P-ND&lt;/li&gt;
&lt;/ul&gt;
&lt;/p&gt;

&lt;p&gt;&lt;b&gt;SOIC8 208 mil&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;b&gt;W25Q16BVSSIG&lt;/b&gt; - 16Mbit (2Mbit x 8) Serial Flash (104MHz SPI Bus), Supply: 2.7-3.6V &lt;br/&gt;&lt;b&gt;Digikey: &lt;/b&gt; SOIC8 - W25Q16BVSSIG-ND&lt;/li&gt;
&lt;/ul&gt;
&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="M25P16" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC8_150MIL">
<connects>
<connect gate="G$1" pin="HOLD#/IO3" pad="7"/>
<connect gate="G$1" pin="MISO" pad="2"/>
<connect gate="G$1" pin="MOSI" pad="5"/>
<connect gate="G$1" pin="SCK" pad="6"/>
<connect gate="G$1" pin="SSEL" pad="1"/>
<connect gate="G$1" pin="VCC" pad="8"/>
<connect gate="G$1" pin="VSS" pad="4"/>
<connect gate="G$1" pin="WP#/IO2" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="208MIL" package="SOIC8_208MIL">
<connects>
<connect gate="G$1" pin="HOLD#/IO3" pad="7"/>
<connect gate="G$1" pin="MISO" pad="2"/>
<connect gate="G$1" pin="MOSI" pad="5"/>
<connect gate="G$1" pin="SCK" pad="6"/>
<connect gate="G$1" pin="SSEL" pad="1"/>
<connect gate="G$1" pin="VCC" pad="8"/>
<connect gate="G$1" pin="VSS" pad="4"/>
<connect gate="G$1" pin="WP#/IO2" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+3V3">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" prefix="+3V3">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="adafruit">
<packages>
<package name="EVQ-Q2">
<wire x1="-3.3" y1="3" x2="3.3" y2="3" width="0.127" layer="21"/>
<wire x1="3.3" y1="3" x2="3.3" y2="-3" width="0.127" layer="21"/>
<wire x1="3.3" y1="-3" x2="-3.3" y2="-3" width="0.127" layer="21"/>
<wire x1="-3.3" y1="-3" x2="-3.3" y2="3" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1.5033" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1" width="0.127" layer="21"/>
<smd name="B" x="-3.4" y="2" dx="3.2" dy="1.2" layer="1"/>
<smd name="B'" x="3.4" y="2" dx="3.2" dy="1.2" layer="1"/>
<smd name="A'" x="3.4" y="-2" dx="3.2" dy="1.2" layer="1"/>
<smd name="A" x="-3.4" y="-2" dx="3.2" dy="1.2" layer="1"/>
<text x="-3" y="3.5" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.2" y="-4.8" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="KMR2">
<wire x1="-2.1" y1="1.4" x2="2.1" y2="1.4" width="0.127" layer="21"/>
<wire x1="2.1" y1="1.4" x2="2.1" y2="-1.4" width="0.127" layer="21"/>
<wire x1="2.1" y1="-1.4" x2="-2.1" y2="-1.4" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-1.4" x2="-2.1" y2="1.4" width="0.127" layer="21"/>
<wire x1="-0.5" y1="-0.8" x2="-1.1" y2="-0.2" width="0.127" layer="21" curve="-90"/>
<wire x1="-1.1" y1="-0.2" x2="-1.1" y2="0.2" width="0.127" layer="21"/>
<wire x1="-1.1" y1="0.2" x2="-0.5" y2="0.8" width="0.127" layer="21" curve="-90"/>
<wire x1="-0.5" y1="0.8" x2="0.5" y2="0.8" width="0.127" layer="21"/>
<wire x1="0.5" y1="0.8" x2="1.1" y2="0.2" width="0.127" layer="21" curve="-90"/>
<wire x1="1.1" y1="0.2" x2="1.1" y2="-0.2" width="0.127" layer="21"/>
<wire x1="1.1" y1="-0.2" x2="0.5" y2="-0.8" width="0.127" layer="21" curve="-90"/>
<wire x1="0.5" y1="-0.8" x2="-0.5" y2="-0.8" width="0.127" layer="21"/>
<smd name="1" x="2" y="0.8" dx="1" dy="1" layer="1"/>
<smd name="2" x="2" y="-0.8" dx="1" dy="1" layer="1"/>
<smd name="4" x="-2" y="-0.8" dx="1" dy="1" layer="1"/>
<smd name="3" x="-2" y="0.8" dx="1" dy="1" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="TS2">
<wire x1="0" y1="1.905" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-3.175" y2="1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-1.905" x2="-3.175" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-4.445" y2="0" width="0.254" layer="94"/>
<wire x1="-4.445" y1="0" x2="-4.445" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.905" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="0" x2="-3.175" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="1.905" width="0.254" layer="94"/>
<circle x="0" y="-2.54" radius="0.127" width="0.4064" layer="94"/>
<circle x="0" y="2.54" radius="0.127" width="0.4064" layer="94"/>
<text x="-6.35" y="-2.54" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-3.81" y="3.175" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="P" x="0" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
<pin name="S" x="0" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="S1" x="2.54" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="P1" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SPST_TACT" prefix="SW">
<description>SMT 6mm switch, EVQQ2 series
&lt;p&gt;http://www.ladyada.net/library/eagle&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="TS2" x="0" y="0"/>
</gates>
<devices>
<device name="-EVQQ2" package="EVQ-Q2">
<connects>
<connect gate="G$1" pin="P" pad="A"/>
<connect gate="G$1" pin="P1" pad="A'"/>
<connect gate="G$1" pin="S" pad="B"/>
<connect gate="G$1" pin="S1" pad="B'"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-KMR2" package="KMR2">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
<connect gate="G$1" pin="P1" pad="3"/>
<connect gate="G$1" pin="S" pad="2"/>
<connect gate="G$1" pin="S1" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Capacitor" urn="urn:adsk.eagle:library:16290819">
<description>&lt;B&gt;Capacitors - Fixed, Variable, Trimmers</description>
<packages>
<package name="CAPC1005X60" urn="urn:adsk.eagle:footprint:16290849/2" library_version="4">
<description>Chip, 1.00 X 0.50 X 0.60 mm body
&lt;p&gt;Chip package with body size 1.00 X 0.50 X 0.60 mm&lt;/p&gt;</description>
<wire x1="0.55" y1="0.6286" x2="-0.55" y2="0.6286" width="0.127" layer="21"/>
<wire x1="0.55" y1="-0.6286" x2="-0.55" y2="-0.6286" width="0.127" layer="21"/>
<wire x1="0.55" y1="-0.3" x2="-0.55" y2="-0.3" width="0.12" layer="51"/>
<wire x1="-0.55" y1="-0.3" x2="-0.55" y2="0.3" width="0.12" layer="51"/>
<wire x1="-0.55" y1="0.3" x2="0.55" y2="0.3" width="0.12" layer="51"/>
<wire x1="0.55" y1="0.3" x2="0.55" y2="-0.3" width="0.12" layer="51"/>
<smd name="1" x="-0.4846" y="0" dx="0.56" dy="0.6291" layer="1"/>
<smd name="2" x="0.4846" y="0" dx="0.56" dy="0.6291" layer="1"/>
<text x="0" y="1.2636" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.2636" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC1110X102" urn="urn:adsk.eagle:footprint:16290845/2" library_version="4">
<description>Chip, 1.17 X 1.02 X 1.02 mm body
&lt;p&gt;Chip package with body size 1.17 X 1.02 X 1.02 mm&lt;/p&gt;</description>
<wire x1="0.66" y1="0.9552" x2="-0.66" y2="0.9552" width="0.127" layer="21"/>
<wire x1="0.66" y1="-0.9552" x2="-0.66" y2="-0.9552" width="0.127" layer="21"/>
<wire x1="0.66" y1="-0.635" x2="-0.66" y2="-0.635" width="0.12" layer="51"/>
<wire x1="-0.66" y1="-0.635" x2="-0.66" y2="0.635" width="0.12" layer="51"/>
<wire x1="-0.66" y1="0.635" x2="0.66" y2="0.635" width="0.12" layer="51"/>
<wire x1="0.66" y1="0.635" x2="0.66" y2="-0.635" width="0.12" layer="51"/>
<smd name="1" x="-0.5388" y="0" dx="0.6626" dy="1.2823" layer="1"/>
<smd name="2" x="0.5388" y="0" dx="0.6626" dy="1.2823" layer="1"/>
<text x="0" y="1.5902" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.5902" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC1608X85" urn="urn:adsk.eagle:footprint:16290847/2" library_version="4">
<description>Chip, 1.60 X 0.80 X 0.85 mm body
&lt;p&gt;Chip package with body size 1.60 X 0.80 X 0.85 mm&lt;/p&gt;</description>
<wire x1="0.875" y1="0.7991" x2="-0.875" y2="0.7991" width="0.127" layer="21"/>
<wire x1="0.875" y1="-0.7991" x2="-0.875" y2="-0.7991" width="0.127" layer="21"/>
<wire x1="0.875" y1="-0.475" x2="-0.875" y2="-0.475" width="0.12" layer="51"/>
<wire x1="-0.875" y1="-0.475" x2="-0.875" y2="0.475" width="0.12" layer="51"/>
<wire x1="-0.875" y1="0.475" x2="0.875" y2="0.475" width="0.12" layer="51"/>
<wire x1="0.875" y1="0.475" x2="0.875" y2="-0.475" width="0.12" layer="51"/>
<smd name="1" x="-0.7746" y="0" dx="0.9209" dy="0.9702" layer="1"/>
<smd name="2" x="0.7746" y="0" dx="0.9209" dy="0.9702" layer="1"/>
<text x="0" y="1.4341" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.4341" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC2012X110" urn="urn:adsk.eagle:footprint:16290848/2" library_version="4">
<description>Chip, 2.00 X 1.25 X 1.10 mm body
&lt;p&gt;Chip package with body size 2.00 X 1.25 X 1.10 mm&lt;/p&gt;</description>
<wire x1="1.1" y1="1.0467" x2="-1.1" y2="1.0467" width="0.127" layer="21"/>
<wire x1="1.1" y1="-1.0467" x2="-1.1" y2="-1.0467" width="0.127" layer="21"/>
<wire x1="1.1" y1="-0.725" x2="-1.1" y2="-0.725" width="0.12" layer="51"/>
<wire x1="-1.1" y1="-0.725" x2="-1.1" y2="0.725" width="0.12" layer="51"/>
<wire x1="-1.1" y1="0.725" x2="1.1" y2="0.725" width="0.12" layer="51"/>
<wire x1="1.1" y1="0.725" x2="1.1" y2="-0.725" width="0.12" layer="51"/>
<smd name="1" x="-0.8754" y="0" dx="1.1646" dy="1.4653" layer="1"/>
<smd name="2" x="0.8754" y="0" dx="1.1646" dy="1.4653" layer="1"/>
<text x="0" y="1.6817" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.6817" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC3216X135" urn="urn:adsk.eagle:footprint:16290836/2" library_version="4">
<description>Chip, 3.20 X 1.60 X 1.35 mm body
&lt;p&gt;Chip package with body size 3.20 X 1.60 X 1.35 mm&lt;/p&gt;</description>
<wire x1="1.7" y1="1.2217" x2="-1.7" y2="1.2217" width="0.127" layer="21"/>
<wire x1="1.7" y1="-1.2217" x2="-1.7" y2="-1.2217" width="0.127" layer="21"/>
<wire x1="1.7" y1="-0.9" x2="-1.7" y2="-0.9" width="0.12" layer="51"/>
<wire x1="-1.7" y1="-0.9" x2="-1.7" y2="0.9" width="0.12" layer="51"/>
<wire x1="-1.7" y1="0.9" x2="1.7" y2="0.9" width="0.12" layer="51"/>
<wire x1="1.7" y1="0.9" x2="1.7" y2="-0.9" width="0.12" layer="51"/>
<smd name="1" x="-1.4754" y="0" dx="1.1646" dy="1.8153" layer="1"/>
<smd name="2" x="1.4754" y="0" dx="1.1646" dy="1.8153" layer="1"/>
<text x="0" y="1.8567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.8567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC3225X135" urn="urn:adsk.eagle:footprint:16290843/2" library_version="4">
<description>Chip, 3.20 X 2.50 X 1.35 mm body
&lt;p&gt;Chip package with body size 3.20 X 2.50 X 1.35 mm&lt;/p&gt;</description>
<wire x1="1.7" y1="1.6717" x2="-1.7" y2="1.6717" width="0.127" layer="21"/>
<wire x1="1.7" y1="-1.6717" x2="-1.7" y2="-1.6717" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.35" x2="-1.7" y2="-1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="-1.35" x2="-1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="1.35" x2="1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="1.7" y1="1.35" x2="1.7" y2="-1.35" width="0.12" layer="51"/>
<smd name="1" x="-1.4754" y="0" dx="1.1646" dy="2.7153" layer="1"/>
<smd name="2" x="1.4754" y="0" dx="1.1646" dy="2.7153" layer="1"/>
<text x="0" y="2.3067" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.3067" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC4532X135" urn="urn:adsk.eagle:footprint:16290841/2" library_version="4">
<description>Chip, 4.50 X 3.20 X 1.35 mm body
&lt;p&gt;Chip package with body size 4.50 X 3.20 X 1.35 mm&lt;/p&gt;</description>
<wire x1="2.4" y1="2.0217" x2="-2.4" y2="2.0217" width="0.127" layer="21"/>
<wire x1="2.4" y1="-2.0217" x2="-2.4" y2="-2.0217" width="0.127" layer="21"/>
<wire x1="2.4" y1="-1.7" x2="-2.4" y2="-1.7" width="0.12" layer="51"/>
<wire x1="-2.4" y1="-1.7" x2="-2.4" y2="1.7" width="0.12" layer="51"/>
<wire x1="-2.4" y1="1.7" x2="2.4" y2="1.7" width="0.12" layer="51"/>
<wire x1="2.4" y1="1.7" x2="2.4" y2="-1.7" width="0.12" layer="51"/>
<smd name="1" x="-2.0565" y="0" dx="1.3973" dy="3.4153" layer="1"/>
<smd name="2" x="2.0565" y="0" dx="1.3973" dy="3.4153" layer="1"/>
<text x="0" y="2.6567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.6567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPM3216X180" urn="urn:adsk.eagle:footprint:16290835/2" library_version="4">
<description>Molded Body, 3.20 X 1.60 X 1.80 mm body
&lt;p&gt;Molded Body package with body size 3.20 X 1.60 X 1.80 mm&lt;/p&gt;</description>
<wire x1="-1.7" y1="0.9084" x2="1.7" y2="0.9084" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-0.9084" x2="1.7" y2="-0.9084" width="0.127" layer="21"/>
<wire x1="1.7" y1="-0.9" x2="-1.7" y2="-0.9" width="0.12" layer="51"/>
<wire x1="-1.7" y1="-0.9" x2="-1.7" y2="0.9" width="0.12" layer="51"/>
<wire x1="-1.7" y1="0.9" x2="1.7" y2="0.9" width="0.12" layer="51"/>
<wire x1="1.7" y1="0.9" x2="1.7" y2="-0.9" width="0.12" layer="51"/>
<smd name="1" x="-1.3099" y="0" dx="1.7955" dy="1.1887" layer="1"/>
<smd name="2" x="1.3099" y="0" dx="1.7955" dy="1.1887" layer="1"/>
<text x="0" y="1.5434" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.5434" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPM3528X210" urn="urn:adsk.eagle:footprint:16290844/2" library_version="4">
<description>Molded Body, 3.50 X 2.80 X 2.10 mm body
&lt;p&gt;Molded Body package with body size 3.50 X 2.80 X 2.10 mm&lt;/p&gt;</description>
<wire x1="-1.85" y1="1.5" x2="1.85" y2="1.5" width="0.127" layer="21"/>
<wire x1="-1.85" y1="-1.5" x2="1.85" y2="-1.5" width="0.127" layer="21"/>
<wire x1="1.85" y1="-1.5" x2="-1.85" y2="-1.5" width="0.12" layer="51"/>
<wire x1="-1.85" y1="-1.5" x2="-1.85" y2="1.5" width="0.12" layer="51"/>
<wire x1="-1.85" y1="1.5" x2="1.85" y2="1.5" width="0.12" layer="51"/>
<wire x1="1.85" y1="1.5" x2="1.85" y2="-1.5" width="0.12" layer="51"/>
<smd name="1" x="-1.4599" y="0" dx="1.7955" dy="2.2036" layer="1"/>
<smd name="2" x="1.4599" y="0" dx="1.7955" dy="2.2036" layer="1"/>
<text x="0" y="2.135" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.135" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPM6032X280" urn="urn:adsk.eagle:footprint:16290839/2" library_version="4">
<description>Molded Body, 6.00 X 3.20 X 2.80 mm body
&lt;p&gt;Molded Body package with body size 6.00 X 3.20 X 2.80 mm&lt;/p&gt;</description>
<wire x1="-3.15" y1="1.75" x2="3.15" y2="1.75" width="0.127" layer="21"/>
<wire x1="-3.15" y1="-1.75" x2="3.15" y2="-1.75" width="0.127" layer="21"/>
<wire x1="3.15" y1="-1.75" x2="-3.15" y2="-1.75" width="0.12" layer="51"/>
<wire x1="-3.15" y1="-1.75" x2="-3.15" y2="1.75" width="0.12" layer="51"/>
<wire x1="-3.15" y1="1.75" x2="3.15" y2="1.75" width="0.12" layer="51"/>
<wire x1="3.15" y1="1.75" x2="3.15" y2="-1.75" width="0.12" layer="51"/>
<smd name="1" x="-2.4712" y="0" dx="2.368" dy="2.2036" layer="1"/>
<smd name="2" x="2.4712" y="0" dx="2.368" dy="2.2036" layer="1"/>
<text x="0" y="2.385" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.385" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPM7343X310" urn="urn:adsk.eagle:footprint:16290840/2" library_version="4">
<description>Molded Body, 7.30 X 4.30 X 3.10 mm body
&lt;p&gt;Molded Body package with body size 7.30 X 4.30 X 3.10 mm&lt;/p&gt;</description>
<wire x1="-3.8" y1="2.3" x2="3.8" y2="2.3" width="0.127" layer="21"/>
<wire x1="-3.8" y1="-2.3" x2="3.8" y2="-2.3" width="0.127" layer="21"/>
<wire x1="3.8" y1="-2.3" x2="-3.8" y2="-2.3" width="0.12" layer="51"/>
<wire x1="-3.8" y1="-2.3" x2="-3.8" y2="2.3" width="0.12" layer="51"/>
<wire x1="-3.8" y1="2.3" x2="3.8" y2="2.3" width="0.12" layer="51"/>
<wire x1="3.8" y1="2.3" x2="3.8" y2="-2.3" width="0.12" layer="51"/>
<smd name="1" x="-3.1212" y="0" dx="2.368" dy="2.4036" layer="1"/>
<smd name="2" x="3.1212" y="0" dx="2.368" dy="2.4036" layer="1"/>
<text x="0" y="2.935" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.935" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC4564X110" urn="urn:adsk.eagle:footprint:16290837/2" library_version="4">
<description>Chip, 4.50 X 6.40 X 1.10 mm body
&lt;p&gt;Chip package with body size 4.50 X 6.40 X 1.10 mm&lt;/p&gt;</description>
<wire x1="2.4" y1="3.7179" x2="-2.4" y2="3.7179" width="0.127" layer="21"/>
<wire x1="2.4" y1="-3.7179" x2="-2.4" y2="-3.7179" width="0.127" layer="21"/>
<wire x1="2.4" y1="-3.4" x2="-2.4" y2="-3.4" width="0.12" layer="51"/>
<wire x1="-2.4" y1="-3.4" x2="-2.4" y2="3.4" width="0.12" layer="51"/>
<wire x1="-2.4" y1="3.4" x2="2.4" y2="3.4" width="0.12" layer="51"/>
<wire x1="2.4" y1="3.4" x2="2.4" y2="-3.4" width="0.12" layer="51"/>
<smd name="1" x="-2.0565" y="0" dx="1.3973" dy="6.8078" layer="1"/>
<smd name="2" x="2.0565" y="0" dx="1.3973" dy="6.8078" layer="1"/>
<text x="0" y="4.3529" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4.3529" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPRD550W60D1025H1250B" urn="urn:adsk.eagle:footprint:16290829/2" library_version="4">
<description>Radial Non-Polarized Capacitor, 5.50 mm pitch, 10.25 mm body diameter, 12.50 mm body height
&lt;p&gt;Radial Non-Polarized Capacitor package with 5.50 mm pitch (lead spacing), 0.60 mm lead diameter, 10.25 mm body diameter and 12.50 mm body height&lt;/p&gt;</description>
<circle x="0" y="0" radius="5.25" width="0.127" layer="21"/>
<circle x="0" y="0" radius="5.25" width="0.12" layer="51"/>
<pad name="1" x="-2.75" y="0" drill="0.8" diameter="1.4"/>
<pad name="2" x="2.75" y="0" drill="0.8" diameter="1.4"/>
<text x="0" y="5.885" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-5.885" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPRD2261W240D5080H5555B" urn="urn:adsk.eagle:footprint:16290850/2" library_version="4">
<description>Radial Non-Polarized Capacitor, 22.61 mm pitch, 50.80 mm body diameter, 55.55 mm body height
&lt;p&gt;Radial Non-Polarized Capacitor package with 22.61 mm pitch (lead spacing), 2.40 mm lead diameter, 50.80 mm body diameter and 55.55 mm body height&lt;/p&gt;</description>
<circle x="0" y="0" radius="25.79" width="0.127" layer="21"/>
<circle x="0" y="0" radius="25.79" width="0.12" layer="51"/>
<pad name="1" x="-11.305" y="0" drill="2.6" diameter="3.9"/>
<pad name="2" x="11.305" y="0" drill="2.6" diameter="3.9"/>
<text x="0" y="26.425" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-26.425" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="CAPC1005X60" urn="urn:adsk.eagle:package:16290895/2" type="model" library_version="4">
<description>Chip, 1.00 X 0.50 X 0.60 mm body
&lt;p&gt;Chip package with body size 1.00 X 0.50 X 0.60 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1005X60"/>
</packageinstances>
</package3d>
<package3d name="CAPC1110X102" urn="urn:adsk.eagle:package:16290904/2" type="model" library_version="4">
<description>Chip, 1.17 X 1.02 X 1.02 mm body
&lt;p&gt;Chip package with body size 1.17 X 1.02 X 1.02 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1110X102"/>
</packageinstances>
</package3d>
<package3d name="CAPC1608X85" urn="urn:adsk.eagle:package:16290898/2" type="model" library_version="4">
<description>Chip, 1.60 X 0.80 X 0.85 mm body
&lt;p&gt;Chip package with body size 1.60 X 0.80 X 0.85 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1608X85"/>
</packageinstances>
</package3d>
<package3d name="CAPC2012X110" urn="urn:adsk.eagle:package:16290897/2" type="model" library_version="4">
<description>Chip, 2.00 X 1.25 X 1.10 mm body
&lt;p&gt;Chip package with body size 2.00 X 1.25 X 1.10 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC2012X110"/>
</packageinstances>
</package3d>
<package3d name="CAPC3216X135" urn="urn:adsk.eagle:package:16290893/2" type="model" library_version="4">
<description>Chip, 3.20 X 1.60 X 1.35 mm body
&lt;p&gt;Chip package with body size 3.20 X 1.60 X 1.35 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC3216X135"/>
</packageinstances>
</package3d>
<package3d name="CAPC3225X135" urn="urn:adsk.eagle:package:16290903/2" type="model" library_version="4">
<description>Chip, 3.20 X 2.50 X 1.35 mm body
&lt;p&gt;Chip package with body size 3.20 X 2.50 X 1.35 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC3225X135"/>
</packageinstances>
</package3d>
<package3d name="CAPC4532X135" urn="urn:adsk.eagle:package:16290900/2" type="model" library_version="4">
<description>Chip, 4.50 X 3.20 X 1.35 mm body
&lt;p&gt;Chip package with body size 4.50 X 3.20 X 1.35 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC4532X135"/>
</packageinstances>
</package3d>
<package3d name="CAPM3216X180" urn="urn:adsk.eagle:package:16290894/2" type="model" library_version="4">
<description>Molded Body, 3.20 X 1.60 X 1.80 mm body
&lt;p&gt;Molded Body package with body size 3.20 X 1.60 X 1.80 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPM3216X180"/>
</packageinstances>
</package3d>
<package3d name="CAPM3528X210" urn="urn:adsk.eagle:package:16290902/2" type="model" library_version="4">
<description>Molded Body, 3.50 X 2.80 X 2.10 mm body
&lt;p&gt;Molded Body package with body size 3.50 X 2.80 X 2.10 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPM3528X210"/>
</packageinstances>
</package3d>
<package3d name="CAPM6032X280" urn="urn:adsk.eagle:package:16290896/2" type="model" library_version="4">
<description>Molded Body, 6.00 X 3.20 X 2.80 mm body
&lt;p&gt;Molded Body package with body size 6.00 X 3.20 X 2.80 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPM6032X280"/>
</packageinstances>
</package3d>
<package3d name="CAPM7343X310" urn="urn:adsk.eagle:package:16290891/2" type="model" library_version="4">
<description>Molded Body, 7.30 X 4.30 X 3.10 mm body
&lt;p&gt;Molded Body package with body size 7.30 X 4.30 X 3.10 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPM7343X310"/>
</packageinstances>
</package3d>
<package3d name="CAPC4564X110L" urn="urn:adsk.eagle:package:16290887/3" type="model" library_version="4">
<description>Chip, 4.50 X 6.40 X 1.10 mm body
&lt;p&gt;Chip package with body size 4.50 X 6.40 X 1.10 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC4564X110"/>
</packageinstances>
</package3d>
<package3d name="CAPRD550W60D1025H1250B" urn="urn:adsk.eagle:package:16290858/2" type="model" library_version="4">
<description>Radial Non-Polarized Capacitor, 5.50 mm pitch, 10.25 mm body diameter, 12.50 mm body height
&lt;p&gt;Radial Non-Polarized Capacitor package with 5.50 mm pitch (lead spacing), 0.60 mm lead diameter, 10.25 mm body diameter and 12.50 mm body height&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPRD550W60D1025H1250B"/>
</packageinstances>
</package3d>
<package3d name="CAPRD2261W240D5080H5555B" urn="urn:adsk.eagle:package:16290864/2" type="model" library_version="4">
<description>Radial Non-Polarized Capacitor, 22.61 mm pitch, 50.80 mm body diameter, 55.55 mm body height
&lt;p&gt;Radial Non-Polarized Capacitor package with 22.61 mm pitch (lead spacing), 2.40 mm lead diameter, 50.80 mm body diameter and 55.55 mm body height&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPRD2261W240D5080H5555B"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="C" urn="urn:adsk.eagle:symbol:16290820/1" library_version="4">
<description>Capacitor</description>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="97">&gt;SPICEMODEL</text>
<text x="2.54" y="0" size="1.778" layer="96">&gt;VALUE</text>
<text x="2.54" y="-5.08" size="1.778" layer="97">&gt;SPICEEXTRA</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="C" urn="urn:adsk.eagle:component:16290909/4" prefix="C" uservalue="yes" library_version="4">
<description>&lt;B&gt;Capacitor - Generic</description>
<gates>
<gate name="G$1" symbol="C" x="0" y="0"/>
</gates>
<devices>
<device name="CHIP-0402(1005-METRIC)" package="CAPC1005X60">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16290895/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Capacitor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
<attribute name="VOLTAGE_RATING" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIP-0504(1310-METRIC)" package="CAPC1110X102">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16290904/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Capacitor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
<attribute name="VOLTAGE_RATING" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIP-0603(1608-METRIC)" package="CAPC1608X85">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16290898/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Capacitor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
<attribute name="VOLTAGE_RATING" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIP-0805(2012-METRIC)" package="CAPC2012X110">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16290897/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Capacitor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
<attribute name="VOLTAGE_RATING" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIP-1206(3216-METRIC)" package="CAPC3216X135">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16290893/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Capacitor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
<attribute name="VOLTAGE_RATING" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIP-1210(3225-METRIC)" package="CAPC3225X135">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16290903/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Capacitor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
<attribute name="VOLTAGE_RATING" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIP-1812(4532-METRIC)" package="CAPC4532X135">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16290900/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Capacitor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
<attribute name="VOLTAGE_RATING" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TANTALUM-1206(3216-METRIC)" package="CAPM3216X180">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16290894/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Capacitor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
<attribute name="VOLTAGE_RATING" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TANTALUM-1411(3528-METRIC)" package="CAPM3528X210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16290902/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Capacitor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
<attribute name="VOLTAGE_RATING" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TANTALUM-2412(6032-METRIC)" package="CAPM6032X280">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16290896/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Capacitor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
<attribute name="VOLTAGE_RATING" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TANTALUM-2917(7343-METRIC)" package="CAPM7343X310">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16290891/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Capacitor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
<attribute name="VOLTAGE_RATING" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIP-1825(4564-METRIC)" package="CAPC4564X110">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16290887/3"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Capacitor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
<attribute name="VOLTAGE_RATING" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="RADIAL-12.5MM-DIA" package="CAPRD550W60D1025H1250B">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16290858/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Capacitor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
<attribute name="VOLTAGE_RATING" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="RADIAL-55.5MM-DIA" package="CAPRD2261W240D5080H5555B">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16290864/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Capacitor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
<attribute name="VOLTAGE_RATING" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
<spice>
<pinmapping spiceprefix="C">
<pinmap gate="G$1" pin="1" pinorder="1"/>
<pinmap gate="G$1" pin="2" pinorder="2"/>
</pinmapping>
</spice>
</deviceset>
</devicesets>
</library>
<library name="Resistor" urn="urn:adsk.eagle:library:16378527">
<description>&lt;B&gt;Resistors, Potentiometers, TrimPot</description>
<packages>
<package name="RESC1608X60" urn="urn:adsk.eagle:footprint:16378537/2" library_version="5">
<description>Chip, 1.60 X 0.82 X 0.60 mm body
&lt;p&gt;Chip package with body size 1.60 X 0.82 X 0.60 mm&lt;/p&gt;</description>
<wire x1="0.85" y1="0.8009" x2="-0.85" y2="0.8009" width="0.127" layer="21"/>
<wire x1="0.85" y1="-0.8009" x2="-0.85" y2="-0.8009" width="0.127" layer="21"/>
<wire x1="0.85" y1="-0.475" x2="-0.85" y2="-0.475" width="0.12" layer="51"/>
<wire x1="-0.85" y1="-0.475" x2="-0.85" y2="0.475" width="0.12" layer="51"/>
<wire x1="-0.85" y1="0.475" x2="0.85" y2="0.475" width="0.12" layer="51"/>
<wire x1="0.85" y1="0.475" x2="0.85" y2="-0.475" width="0.12" layer="51"/>
<smd name="1" x="-0.8152" y="0" dx="0.7987" dy="0.9739" layer="1"/>
<smd name="2" x="0.8152" y="0" dx="0.7987" dy="0.9739" layer="1"/>
<text x="0" y="1.4359" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.4359" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC1005X40" urn="urn:adsk.eagle:footprint:16378540/2" library_version="5">
<description>Chip, 1.05 X 0.54 X 0.40 mm body
&lt;p&gt;Chip package with body size 1.05 X 0.54 X 0.40 mm&lt;/p&gt;</description>
<wire x1="0.55" y1="0.636" x2="-0.55" y2="0.636" width="0.127" layer="21"/>
<wire x1="0.55" y1="-0.636" x2="-0.55" y2="-0.636" width="0.127" layer="21"/>
<wire x1="0.55" y1="-0.3" x2="-0.55" y2="-0.3" width="0.12" layer="51"/>
<wire x1="-0.55" y1="-0.3" x2="-0.55" y2="0.3" width="0.12" layer="51"/>
<wire x1="-0.55" y1="0.3" x2="0.55" y2="0.3" width="0.12" layer="51"/>
<wire x1="0.55" y1="0.3" x2="0.55" y2="-0.3" width="0.12" layer="51"/>
<smd name="1" x="-0.5075" y="0" dx="0.5351" dy="0.644" layer="1"/>
<smd name="2" x="0.5075" y="0" dx="0.5351" dy="0.644" layer="1"/>
<text x="0" y="1.271" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.271" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC2012X65" urn="urn:adsk.eagle:footprint:16378532/2" library_version="5">
<description>Chip, 2.00 X 1.25 X 0.65 mm body
&lt;p&gt;Chip package with body size 2.00 X 1.25 X 0.65 mm&lt;/p&gt;</description>
<wire x1="1.075" y1="1.0241" x2="-1.075" y2="1.0241" width="0.127" layer="21"/>
<wire x1="1.075" y1="-1.0241" x2="-1.075" y2="-1.0241" width="0.127" layer="21"/>
<wire x1="1.075" y1="-0.7" x2="-1.075" y2="-0.7" width="0.12" layer="51"/>
<wire x1="-1.075" y1="-0.7" x2="-1.075" y2="0.7" width="0.12" layer="51"/>
<wire x1="-1.075" y1="0.7" x2="1.075" y2="0.7" width="0.12" layer="51"/>
<wire x1="1.075" y1="0.7" x2="1.075" y2="-0.7" width="0.12" layer="51"/>
<smd name="1" x="-0.9195" y="0" dx="1.0312" dy="1.4202" layer="1"/>
<smd name="2" x="0.9195" y="0" dx="1.0312" dy="1.4202" layer="1"/>
<text x="0" y="1.6591" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.6591" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC3216X70" urn="urn:adsk.eagle:footprint:16378539/2" library_version="5">
<description>Chip, 3.20 X 1.60 X 0.70 mm body
&lt;p&gt;Chip package with body size 3.20 X 1.60 X 0.70 mm&lt;/p&gt;</description>
<wire x1="1.7" y1="1.2217" x2="-1.7" y2="1.2217" width="0.127" layer="21"/>
<wire x1="1.7" y1="-1.2217" x2="-1.7" y2="-1.2217" width="0.127" layer="21"/>
<wire x1="1.7" y1="-0.9" x2="-1.7" y2="-0.9" width="0.12" layer="51"/>
<wire x1="-1.7" y1="-0.9" x2="-1.7" y2="0.9" width="0.12" layer="51"/>
<wire x1="-1.7" y1="0.9" x2="1.7" y2="0.9" width="0.12" layer="51"/>
<wire x1="1.7" y1="0.9" x2="1.7" y2="-0.9" width="0.12" layer="51"/>
<smd name="1" x="-1.4754" y="0" dx="1.1646" dy="1.8153" layer="1"/>
<smd name="2" x="1.4754" y="0" dx="1.1646" dy="1.8153" layer="1"/>
<text x="0" y="1.8567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.8567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC3224X71" urn="urn:adsk.eagle:footprint:16378536/2" library_version="5">
<description>Chip, 3.20 X 2.49 X 0.71 mm body
&lt;p&gt;Chip package with body size 3.20 X 2.49 X 0.71 mm&lt;/p&gt;</description>
<wire x1="1.675" y1="1.6441" x2="-1.675" y2="1.6441" width="0.127" layer="21"/>
<wire x1="1.675" y1="-1.6441" x2="-1.675" y2="-1.6441" width="0.127" layer="21"/>
<wire x1="1.675" y1="-1.32" x2="-1.675" y2="-1.32" width="0.12" layer="51"/>
<wire x1="-1.675" y1="-1.32" x2="-1.675" y2="1.32" width="0.12" layer="51"/>
<wire x1="-1.675" y1="1.32" x2="1.675" y2="1.32" width="0.12" layer="51"/>
<wire x1="1.675" y1="1.32" x2="1.675" y2="-1.32" width="0.12" layer="51"/>
<smd name="1" x="-1.4695" y="0" dx="1.1312" dy="2.6602" layer="1"/>
<smd name="2" x="1.4695" y="0" dx="1.1312" dy="2.6602" layer="1"/>
<text x="0" y="2.2791" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.2791" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC5025X71" urn="urn:adsk.eagle:footprint:16378538/2" library_version="5">
<description>Chip, 5.00 X 2.50 X 0.71 mm body
&lt;p&gt;Chip package with body size 5.00 X 2.50 X 0.71 mm&lt;/p&gt;</description>
<wire x1="2.575" y1="1.6491" x2="-2.575" y2="1.6491" width="0.127" layer="21"/>
<wire x1="2.575" y1="-1.6491" x2="-2.575" y2="-1.6491" width="0.127" layer="21"/>
<wire x1="2.575" y1="-1.325" x2="-2.575" y2="-1.325" width="0.12" layer="51"/>
<wire x1="-2.575" y1="-1.325" x2="-2.575" y2="1.325" width="0.12" layer="51"/>
<wire x1="-2.575" y1="1.325" x2="2.575" y2="1.325" width="0.12" layer="51"/>
<wire x1="2.575" y1="1.325" x2="2.575" y2="-1.325" width="0.12" layer="51"/>
<smd name="1" x="-2.3195" y="0" dx="1.2312" dy="2.6702" layer="1"/>
<smd name="2" x="2.3195" y="0" dx="1.2312" dy="2.6702" layer="1"/>
<text x="0" y="2.2841" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.2841" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC6332X71" urn="urn:adsk.eagle:footprint:16378533/2" library_version="5">
<description>Chip, 6.30 X 3.20 X 0.71 mm body
&lt;p&gt;Chip package with body size 6.30 X 3.20 X 0.71 mm&lt;/p&gt;</description>
<wire x1="3.225" y1="1.9991" x2="-3.225" y2="1.9991" width="0.127" layer="21"/>
<wire x1="3.225" y1="-1.9991" x2="-3.225" y2="-1.9991" width="0.127" layer="21"/>
<wire x1="3.225" y1="-1.675" x2="-3.225" y2="-1.675" width="0.12" layer="51"/>
<wire x1="-3.225" y1="-1.675" x2="-3.225" y2="1.675" width="0.12" layer="51"/>
<wire x1="-3.225" y1="1.675" x2="3.225" y2="1.675" width="0.12" layer="51"/>
<wire x1="3.225" y1="1.675" x2="3.225" y2="-1.675" width="0.12" layer="51"/>
<smd name="1" x="-2.9695" y="0" dx="1.2312" dy="3.3702" layer="1"/>
<smd name="2" x="2.9695" y="0" dx="1.2312" dy="3.3702" layer="1"/>
<text x="0" y="2.6341" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.6341" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESAD1176W63L850D250B" urn="urn:adsk.eagle:footprint:16378542/2" library_version="5">
<description>AXIAL Resistor, 11.76 mm pitch, 8.5 mm body length, 2.5 mm body diameter
&lt;p&gt;AXIAL Resistor package with 11.76 mm pitch, 0.63 mm lead diameter, 8.5 mm body length and 2.5 mm body diameter&lt;/p&gt;</description>
<wire x1="-4.25" y1="1.25" x2="-4.25" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-4.25" y1="-1.25" x2="4.25" y2="-1.25" width="0.127" layer="21"/>
<wire x1="4.25" y1="-1.25" x2="4.25" y2="1.25" width="0.127" layer="21"/>
<wire x1="4.25" y1="1.25" x2="-4.25" y2="1.25" width="0.127" layer="21"/>
<wire x1="-4.25" y1="0" x2="-4.911" y2="0" width="0.127" layer="21"/>
<wire x1="4.25" y1="0" x2="4.911" y2="0" width="0.127" layer="21"/>
<wire x1="4.25" y1="-1.25" x2="-4.25" y2="-1.25" width="0.12" layer="51"/>
<wire x1="-4.25" y1="-1.25" x2="-4.25" y2="1.25" width="0.12" layer="51"/>
<wire x1="-4.25" y1="1.25" x2="4.25" y2="1.25" width="0.12" layer="51"/>
<wire x1="4.25" y1="1.25" x2="4.25" y2="-1.25" width="0.12" layer="51"/>
<pad name="1" x="-5.88" y="0" drill="0.83" diameter="1.43"/>
<pad name="2" x="5.88" y="0" drill="0.83" diameter="1.43"/>
<text x="0" y="1.885" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.885" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESMELF3515" urn="urn:adsk.eagle:footprint:16378534/2" library_version="5">
<description>MELF, 3.50 mm length, 1.52 mm diameter
&lt;p&gt;MELF Resistor package with 3.50 mm length and 1.52 mm diameter&lt;/p&gt;</description>
<wire x1="1.105" y1="1.1825" x2="-1.105" y2="1.1825" width="0.127" layer="21"/>
<wire x1="-1.105" y1="-1.1825" x2="1.105" y2="-1.1825" width="0.127" layer="21"/>
<wire x1="1.85" y1="-0.8" x2="-1.85" y2="-0.8" width="0.12" layer="51"/>
<wire x1="-1.85" y1="-0.8" x2="-1.85" y2="0.8" width="0.12" layer="51"/>
<wire x1="-1.85" y1="0.8" x2="1.85" y2="0.8" width="0.12" layer="51"/>
<wire x1="1.85" y1="0.8" x2="1.85" y2="-0.8" width="0.12" layer="51"/>
<smd name="1" x="-1.6813" y="0" dx="1.1527" dy="1.7371" layer="1"/>
<smd name="2" x="1.6813" y="0" dx="1.1527" dy="1.7371" layer="1"/>
<text x="0" y="1.8175" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.8175" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESMELF2014" urn="urn:adsk.eagle:footprint:16378535/2" library_version="5">
<description>MELF, 2.00 mm length, 1.40 mm diameter
&lt;p&gt;MELF Resistor package with 2.00 mm length and 1.40 mm diameter&lt;/p&gt;</description>
<wire x1="0.5189" y1="1.114" x2="-0.5189" y2="1.114" width="0.127" layer="21"/>
<wire x1="-0.5189" y1="-1.114" x2="0.5189" y2="-1.114" width="0.127" layer="21"/>
<wire x1="1.05" y1="-0.725" x2="-1.05" y2="-0.725" width="0.12" layer="51"/>
<wire x1="-1.05" y1="-0.725" x2="-1.05" y2="0.725" width="0.12" layer="51"/>
<wire x1="-1.05" y1="0.725" x2="1.05" y2="0.725" width="0.12" layer="51"/>
<wire x1="1.05" y1="0.725" x2="1.05" y2="-0.725" width="0.12" layer="51"/>
<smd name="1" x="-0.9918" y="0" dx="0.9456" dy="1.6" layer="1"/>
<smd name="2" x="0.9918" y="0" dx="0.9456" dy="1.6" layer="1"/>
<text x="0" y="1.749" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.749" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESMELF5924" urn="urn:adsk.eagle:footprint:16378541/2" library_version="5">
<description>MELF, 5.90 mm length, 2.45 mm diameter
&lt;p&gt;MELF Resistor package with 5.90 mm length and 2.45 mm diameter&lt;/p&gt;</description>
<wire x1="2.1315" y1="1.639" x2="-2.1315" y2="1.639" width="0.127" layer="21"/>
<wire x1="-2.1315" y1="-1.639" x2="2.1315" y2="-1.639" width="0.127" layer="21"/>
<wire x1="3.05" y1="-1.25" x2="-3.05" y2="-1.25" width="0.12" layer="51"/>
<wire x1="-3.05" y1="-1.25" x2="-3.05" y2="1.25" width="0.12" layer="51"/>
<wire x1="-3.05" y1="1.25" x2="3.05" y2="1.25" width="0.12" layer="51"/>
<wire x1="3.05" y1="1.25" x2="3.05" y2="-1.25" width="0.12" layer="51"/>
<smd name="1" x="-2.7946" y="0" dx="1.3261" dy="2.65" layer="1"/>
<smd name="2" x="2.7946" y="0" dx="1.3261" dy="2.65" layer="1"/>
<text x="0" y="2.274" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.274" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESMELF3218" urn="urn:adsk.eagle:footprint:16378531/2" library_version="5">
<description>MELF, 3.20 mm length, 1.80 mm diameter
&lt;p&gt;MELF Resistor package with 3.20 mm length and 1.80 mm diameter&lt;/p&gt;</description>
<wire x1="0.8815" y1="1.314" x2="-0.8815" y2="1.314" width="0.127" layer="21"/>
<wire x1="-0.8815" y1="-1.314" x2="0.8815" y2="-1.314" width="0.127" layer="21"/>
<wire x1="1.7" y1="-0.925" x2="-1.7" y2="-0.925" width="0.12" layer="51"/>
<wire x1="-1.7" y1="-0.925" x2="-1.7" y2="0.925" width="0.12" layer="51"/>
<wire x1="-1.7" y1="0.925" x2="1.7" y2="0.925" width="0.12" layer="51"/>
<wire x1="1.7" y1="0.925" x2="1.7" y2="-0.925" width="0.12" layer="51"/>
<smd name="1" x="-1.4946" y="0" dx="1.2261" dy="2" layer="1"/>
<smd name="2" x="1.4946" y="0" dx="1.2261" dy="2" layer="1"/>
<text x="0" y="1.949" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.949" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESAD724W46L381D178B" urn="urn:adsk.eagle:footprint:16378530/2" library_version="5">
<description>Axial Resistor, 7.24 mm pitch, 3.81 mm body length, 1.78 mm body diameter
&lt;p&gt;Axial Resistor package with 7.24 mm pitch (lead spacing), 0.46 mm lead diameter, 3.81 mm body length and 1.78 mm body diameter&lt;/p&gt;</description>
<wire x1="-2.16" y1="1.015" x2="-2.16" y2="-1.015" width="0.127" layer="21"/>
<wire x1="-2.16" y1="-1.015" x2="2.16" y2="-1.015" width="0.127" layer="21"/>
<wire x1="2.16" y1="-1.015" x2="2.16" y2="1.015" width="0.127" layer="21"/>
<wire x1="2.16" y1="1.015" x2="-2.16" y2="1.015" width="0.127" layer="21"/>
<wire x1="-2.16" y1="0" x2="-2.736" y2="0" width="0.127" layer="21"/>
<wire x1="2.16" y1="0" x2="2.736" y2="0" width="0.127" layer="21"/>
<wire x1="2.16" y1="-1.015" x2="-2.16" y2="-1.015" width="0.12" layer="51"/>
<wire x1="-2.16" y1="-1.015" x2="-2.16" y2="1.015" width="0.12" layer="51"/>
<wire x1="-2.16" y1="1.015" x2="2.16" y2="1.015" width="0.12" layer="51"/>
<wire x1="2.16" y1="1.015" x2="2.16" y2="-1.015" width="0.12" layer="51"/>
<pad name="1" x="-3.62" y="0" drill="0.66" diameter="1.26"/>
<pad name="2" x="3.62" y="0" drill="0.66" diameter="1.26"/>
<text x="0" y="1.65" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.65" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="RESC1608X60" urn="urn:adsk.eagle:package:16378565/2" type="model" library_version="5">
<description>Chip, 1.60 X 0.82 X 0.60 mm body
&lt;p&gt;Chip package with body size 1.60 X 0.82 X 0.60 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC1608X60"/>
</packageinstances>
</package3d>
<package3d name="RESC1005X40" urn="urn:adsk.eagle:package:16378568/2" type="model" library_version="5">
<description>Chip, 1.05 X 0.54 X 0.40 mm body
&lt;p&gt;Chip package with body size 1.05 X 0.54 X 0.40 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC1005X40"/>
</packageinstances>
</package3d>
<package3d name="RESC2012X65" urn="urn:adsk.eagle:package:16378559/2" type="model" library_version="5">
<description>Chip, 2.00 X 1.25 X 0.65 mm body
&lt;p&gt;Chip package with body size 2.00 X 1.25 X 0.65 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC2012X65"/>
</packageinstances>
</package3d>
<package3d name="RESC3216X70" urn="urn:adsk.eagle:package:16378566/2" type="model" library_version="5">
<description>Chip, 3.20 X 1.60 X 0.70 mm body
&lt;p&gt;Chip package with body size 3.20 X 1.60 X 0.70 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC3216X70"/>
</packageinstances>
</package3d>
<package3d name="RESC3224X71" urn="urn:adsk.eagle:package:16378563/3" type="model" library_version="5">
<description>Chip, 3.20 X 2.49 X 0.71 mm body
&lt;p&gt;Chip package with body size 3.20 X 2.49 X 0.71 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC3224X71"/>
</packageinstances>
</package3d>
<package3d name="RESC5025X71" urn="urn:adsk.eagle:package:16378564/2" type="model" library_version="5">
<description>Chip, 5.00 X 2.50 X 0.71 mm body
&lt;p&gt;Chip package with body size 5.00 X 2.50 X 0.71 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC5025X71"/>
</packageinstances>
</package3d>
<package3d name="RESC6332X71L" urn="urn:adsk.eagle:package:16378557/3" type="model" library_version="5">
<description>Chip, 6.30 X 3.20 X 0.71 mm body
&lt;p&gt;Chip package with body size 6.30 X 3.20 X 0.71 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC6332X71"/>
</packageinstances>
</package3d>
<package3d name="RESAD1176W63L850D250B" urn="urn:adsk.eagle:package:16378560/2" type="model" library_version="5">
<description>AXIAL Resistor, 11.76 mm pitch, 8.5 mm body length, 2.5 mm body diameter
&lt;p&gt;AXIAL Resistor package with 11.76 mm pitch, 0.63 mm lead diameter, 8.5 mm body length and 2.5 mm body diameter&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESAD1176W63L850D250B"/>
</packageinstances>
</package3d>
<package3d name="RESMELF3515" urn="urn:adsk.eagle:package:16378562/2" type="model" library_version="5">
<description>MELF, 3.50 mm length, 1.52 mm diameter
&lt;p&gt;MELF Resistor package with 3.50 mm length and 1.52 mm diameter&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESMELF3515"/>
</packageinstances>
</package3d>
<package3d name="RESMELF2014" urn="urn:adsk.eagle:package:16378558/2" type="model" library_version="5">
<description>MELF, 2.00 mm length, 1.40 mm diameter
&lt;p&gt;MELF Resistor package with 2.00 mm length and 1.40 mm diameter&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESMELF2014"/>
</packageinstances>
</package3d>
<package3d name="RESMELF5924" urn="urn:adsk.eagle:package:16378567/3" type="model" library_version="5">
<description>MELF, 5.90 mm length, 2.45 mm diameter
&lt;p&gt;MELF Resistor package with 5.90 mm length and 2.45 mm diameter&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESMELF5924"/>
</packageinstances>
</package3d>
<package3d name="RESMELF3218" urn="urn:adsk.eagle:package:16378556/2" type="model" library_version="5">
<description>MELF, 3.20 mm length, 1.80 mm diameter
&lt;p&gt;MELF Resistor package with 3.20 mm length and 1.80 mm diameter&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESMELF3218"/>
</packageinstances>
</package3d>
<package3d name="RESAD724W46L381D178B" urn="urn:adsk.eagle:package:16378561/2" type="model" library_version="5">
<description>Axial Resistor, 7.24 mm pitch, 3.81 mm body length, 1.78 mm body diameter
&lt;p&gt;Axial Resistor package with 7.24 mm pitch (lead spacing), 0.46 mm lead diameter, 3.81 mm body length and 1.78 mm body diameter&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESAD724W46L381D178B"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="R" urn="urn:adsk.eagle:symbol:16378529/2" library_version="5">
<description>RESISTOR</description>
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="0" y="2.54" size="1.778" layer="95" align="center">&gt;NAME</text>
<text x="0" y="-5.08" size="1.778" layer="95" align="center">&gt;SPICEMODEL</text>
<text x="0" y="-2.54" size="1.778" layer="95" align="center">&gt;VALUE</text>
<text x="0" y="-7.62" size="1.778" layer="95" align="center">&gt;SPICEEXTRA</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="R" urn="urn:adsk.eagle:component:16378570/4" prefix="R" uservalue="yes" library_version="5">
<description>&lt;b&gt;Resistor Fixed - Generic</description>
<gates>
<gate name="G$1" symbol="R" x="0" y="0"/>
</gates>
<devices>
<device name="CHIP-0402(1005-METRIC)" package="RESC1005X40">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16378568/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Resistor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="RATING" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="Fixed" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIP-0603(1608-METRIC)" package="RESC1608X60">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16378565/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Resistor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="RATING" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="Fixed" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIP-0805(2012-METRIC)" package="RESC2012X65">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16378559/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Resistor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="RATING" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="Fixed" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIP-1206(3216-METRIC)" package="RESC3216X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16378566/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Resistor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="RATING" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="Fixed" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIP-1210(3225-METRIC)" package="RESC3224X71">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16378563/3"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Resistor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="RATING" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="Fixed" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIP-2010(5025-METRIC)" package="RESC5025X71">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16378564/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Resistor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="RATING" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="Fixed" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIP-2512(6332-METRIC)" package="RESC6332X71">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16378557/3"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Resistor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="RATING" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="Fixed" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="AXIAL-11.7MM-PITCH" package="RESAD1176W63L850D250B">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16378560/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Resistor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="RATING" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="Fixed" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF(3515-METRIC)" package="RESMELF3515">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16378562/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Resistor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="RATING" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="Fixed" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF(2014-METRIC)" package="RESMELF2014">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16378558/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Resistor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="RATING" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="Fixed" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF(5924-METRIC)" package="RESMELF5924">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16378567/3"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Resistor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="RATING" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="Fixed" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF(3218-METRIC)" package="RESMELF3218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16378556/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Resistor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="RATING" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="Fixed" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="AXIAL-7.2MM-PITCH" package="RESAD724W46L381D178B">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16378561/2"/>
</package3dinstances>
<technologies>
<technology name="_">
<attribute name="CATEGORY" value="Resistor" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="RATING" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="Fixed" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
<spice>
<pinmapping spiceprefix="R">
<pinmap gate="G$1" pin="1" pinorder="1"/>
<pinmap gate="G$1" pin="2" pinorder="2"/>
</pinmapping>
</spice>
</deviceset>
</devicesets>
</library>
<library name="USB4105-GF-A" urn="urn:adsk.eagle:library:21150292">
<packages>
<package name="GCT_USB4105-GF-A" urn="urn:adsk.eagle:footprint:21150293/2" library_version="4">
<wire x1="-4.32" y1="0.7" x2="-4.02" y2="0.4" width="0.0001" layer="46" curve="-90"/>
<wire x1="-4.02" y1="0.4" x2="-4.02" y2="-0.4" width="0.0001" layer="46"/>
<wire x1="-4.02" y1="-0.4" x2="-4.32" y2="-0.7" width="0.0001" layer="46" curve="-90"/>
<wire x1="-4.32" y1="-0.7" x2="-4.62" y2="-0.4" width="0.0001" layer="46" curve="-90"/>
<wire x1="-4.62" y1="-0.4" x2="-4.62" y2="0.4" width="0.0001" layer="46"/>
<wire x1="-4.62" y1="0.4" x2="-4.32" y2="0.7" width="0.0001" layer="46" curve="-90"/>
<wire x1="4.32" y1="0.7" x2="4.62" y2="0.4" width="0.0001" layer="46" curve="-90"/>
<wire x1="4.62" y1="0.4" x2="4.62" y2="-0.4" width="0.0001" layer="46"/>
<wire x1="4.62" y1="-0.4" x2="4.32" y2="-0.7" width="0.0001" layer="46" curve="-90"/>
<wire x1="4.32" y1="-0.7" x2="4.02" y2="-0.4" width="0.0001" layer="46" curve="-90"/>
<wire x1="4.02" y1="-0.4" x2="4.02" y2="0.4" width="0.0001" layer="46"/>
<wire x1="4.02" y1="0.4" x2="4.32" y2="0.7" width="0.0001" layer="46" curve="-90"/>
<wire x1="-4.32" y1="5.03" x2="-4.02" y2="4.73" width="0.0001" layer="46" curve="-90"/>
<wire x1="-4.02" y1="4.73" x2="-4.02" y2="3.63" width="0.0001" layer="46"/>
<wire x1="-4.02" y1="3.63" x2="-4.32" y2="3.33" width="0.0001" layer="46" curve="-90"/>
<wire x1="-4.32" y1="3.33" x2="-4.62" y2="3.63" width="0.0001" layer="46" curve="-90"/>
<wire x1="-4.62" y1="3.63" x2="-4.62" y2="4.73" width="0.0001" layer="46"/>
<wire x1="-4.62" y1="4.73" x2="-4.32" y2="5.03" width="0.0001" layer="46" curve="-90"/>
<wire x1="4.32" y1="5.03" x2="4.62" y2="4.73" width="0.0001" layer="46" curve="-90"/>
<wire x1="4.62" y1="4.73" x2="4.62" y2="3.63" width="0.0001" layer="46"/>
<wire x1="4.62" y1="3.63" x2="4.32" y2="3.33" width="0.0001" layer="46" curve="-90"/>
<wire x1="4.32" y1="3.33" x2="4.02" y2="3.63" width="0.0001" layer="46" curve="-90"/>
<wire x1="4.02" y1="3.63" x2="4.02" y2="4.73" width="0.0001" layer="46"/>
<wire x1="4.02" y1="4.73" x2="4.32" y2="5.03" width="0.0001" layer="46" curve="-90"/>
<wire x1="-4.79" y1="4.93" x2="4.79" y2="4.93" width="0.1" layer="51"/>
<wire x1="4.79" y1="4.93" x2="4.79" y2="-2.6" width="0.1" layer="51"/>
<wire x1="4.79" y1="-2.6" x2="-4.79" y2="-2.6" width="0.1" layer="51"/>
<wire x1="-4.79" y1="-2.6" x2="-4.79" y2="4.93" width="0.1" layer="51"/>
<wire x1="-4.79" y1="-1.32" x2="-4.79" y2="-2.6" width="0.2" layer="21"/>
<wire x1="-4.79" y1="-2.6" x2="4.79" y2="-2.6" width="0.2" layer="21"/>
<wire x1="4.79" y1="-2.6" x2="4.79" y2="-1.32" width="0.2" layer="21"/>
<wire x1="-5.1" y1="5.58" x2="-5.1" y2="-2.85" width="0.05" layer="39"/>
<wire x1="-5.1" y1="-2.85" x2="5.1" y2="-2.85" width="0.05" layer="39"/>
<wire x1="5.1" y1="-2.85" x2="5.1" y2="5.58" width="0.05" layer="39"/>
<wire x1="5.1" y1="5.58" x2="-5.1" y2="5.58" width="0.05" layer="39"/>
<wire x1="4.8" y1="-2.6" x2="8.4" y2="-2.6" width="0.1" layer="51"/>
<wire x1="-4.79" y1="2.65" x2="-4.79" y2="1.4" width="0.2" layer="21"/>
<wire x1="4.79" y1="2.65" x2="4.79" y2="1.4" width="0.2" layer="21"/>
<text x="-5.1" y="5.7" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.1" y="-4.3" size="1.27" layer="27">&gt;VALUE</text>
<text x="5.4" y="-2.5" size="0.4064" layer="51">PCB EDGE</text>
<pad name="G1" x="-4.32" y="0" drill="0.6" diameter="1" shape="long" rot="R90"/>
<pad name="G2" x="4.32" y="0" drill="0.6" diameter="1" shape="long" rot="R90"/>
<pad name="G3" x="-4.32" y="4.18" drill="0.6" diameter="1.05" shape="long" rot="R90"/>
<pad name="G4" x="4.32" y="4.18" drill="0.6" diameter="1.05" shape="long" rot="R90"/>
<hole x="2.89" y="3.68" drill="0.65"/>
<hole x="-2.89" y="3.68" drill="0.65"/>
<smd name="A1/B12" x="-3.2" y="4.85" dx="0.6" dy="1.15" layer="1"/>
<smd name="B1/A12" x="3.2" y="4.85" dx="0.6" dy="1.15" layer="1"/>
<smd name="A4/B9" x="-2.4" y="4.85" dx="0.6" dy="1.15" layer="1"/>
<smd name="B4/A9" x="2.4" y="4.85" dx="0.6" dy="1.15" layer="1"/>
<smd name="A7" x="0.25" y="4.85" dx="0.3" dy="1.15" layer="1"/>
<smd name="A6" x="-0.25" y="4.85" dx="0.3" dy="1.15" layer="1"/>
<smd name="B6" x="0.75" y="4.85" dx="0.3" dy="1.15" layer="1"/>
<smd name="A8" x="1.25" y="4.85" dx="0.3" dy="1.15" layer="1"/>
<smd name="B5" x="1.75" y="4.85" dx="0.3" dy="1.15" layer="1"/>
<smd name="B7" x="-0.75" y="4.85" dx="0.3" dy="1.15" layer="1"/>
<smd name="A5" x="-1.25" y="4.85" dx="0.3" dy="1.15" layer="1"/>
<smd name="B8" x="-1.75" y="4.85" dx="0.3" dy="1.15" layer="1"/>
</package>
</packages>
<packages3d>
<package3d name="GCT_USB4105-GF-A" urn="urn:adsk.eagle:package:21150295/3" type="model" library_version="4">
<packageinstances>
<packageinstance name="GCT_USB4105-GF-A"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="USB4105-GF-A" urn="urn:adsk.eagle:symbol:21150294/2" library_version="3">
<wire x1="-7.62" y1="12.7" x2="10.16" y2="12.7" width="0.254" layer="94"/>
<wire x1="10.16" y1="12.7" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="-7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-10.16" x2="-7.62" y2="12.7" width="0.254" layer="94"/>
<text x="-7.62" y="13.97" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
<pin name="CC1" x="-12.7" y="5.08" length="middle"/>
<pin name="DP1" x="-12.7" y="2.54" length="middle"/>
<pin name="DN1" x="-12.7" y="0" length="middle"/>
<pin name="SBU1" x="-12.7" y="-2.54" length="middle"/>
<pin name="VBUS" x="15.24" y="10.16" length="middle" direction="pwr" rot="R180"/>
<pin name="CC2" x="15.24" y="5.08" length="middle" rot="R180"/>
<pin name="DP2" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="DN2" x="15.24" y="0" length="middle" rot="R180"/>
<pin name="SBU2" x="15.24" y="-2.54" length="middle" rot="R180"/>
<pin name="GND" x="15.24" y="-7.62" length="middle" direction="pwr" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="USB4105-GF-A" urn="urn:adsk.eagle:component:21150296/4" prefix="J" library_version="4">
<description>USB Type C Receptacle for USB 2.0
SMT Type, PCB Top Mount</description>
<gates>
<gate name="G$1" symbol="USB4105-GF-A" x="0" y="0"/>
</gates>
<devices>
<device name="" package="GCT_USB4105-GF-A">
<connects>
<connect gate="G$1" pin="CC1" pad="A5"/>
<connect gate="G$1" pin="CC2" pad="B5"/>
<connect gate="G$1" pin="DN1" pad="A7"/>
<connect gate="G$1" pin="DN2" pad="B7"/>
<connect gate="G$1" pin="DP1" pad="A6"/>
<connect gate="G$1" pin="DP2" pad="B6"/>
<connect gate="G$1" pin="GND" pad="A1/B12 B1/A12 G1 G2 G3 G4"/>
<connect gate="G$1" pin="SBU1" pad="A8"/>
<connect gate="G$1" pin="SBU2" pad="B8"/>
<connect gate="G$1" pin="VBUS" pad="A4/B9 B4/A9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:21150295/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Unavailable"/>
<attribute name="DESCRIPTION" value=" USB-C (USB TYPE-C) USB 2.0 Receptacle Connector 24 (16+8 Dummy) Position Surface Mount, Right Angle; Through Hole "/>
<attribute name="MF" value="GCT"/>
<attribute name="MP" value="USB4105-GF-A"/>
<attribute name="PACKAGE" value="None"/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ilabs-pictures" urn="urn:adsk.eagle:library:21548800">
<packages>
<package name="CHALLENGER" urn="urn:adsk.eagle:footprint:21548801/1" library_version="1">
<rectangle x1="6.3875" y1="0.1125" x2="7.0625" y2="0.1375" layer="200"/>
<rectangle x1="6.3125" y1="0.1375" x2="7.2875" y2="0.1625" layer="200"/>
<rectangle x1="6.2875" y1="0.1625" x2="7.4625" y2="0.1875" layer="200"/>
<rectangle x1="6.2375" y1="0.1875" x2="7.5875" y2="0.2125" layer="200"/>
<rectangle x1="6.2125" y1="0.2125" x2="7.7125" y2="0.2375" layer="200"/>
<rectangle x1="6.1875" y1="0.2375" x2="7.8125" y2="0.2625" layer="200"/>
<rectangle x1="6.1875" y1="0.2625" x2="7.9125" y2="0.2875" layer="200"/>
<rectangle x1="6.1625" y1="0.2875" x2="8.0125" y2="0.3125" layer="200"/>
<rectangle x1="6.1625" y1="0.3125" x2="8.0875" y2="0.3375" layer="200"/>
<rectangle x1="6.1625" y1="0.3375" x2="8.1625" y2="0.3625" layer="200"/>
<rectangle x1="6.1625" y1="0.3625" x2="8.2375" y2="0.3875" layer="200"/>
<rectangle x1="6.1625" y1="0.3875" x2="6.5125" y2="0.4125" layer="200"/>
<rectangle x1="7.1375" y1="0.3875" x2="8.3125" y2="0.4125" layer="200"/>
<rectangle x1="6.1625" y1="0.4125" x2="6.4875" y2="0.4375" layer="200"/>
<rectangle x1="7.3125" y1="0.4125" x2="8.3625" y2="0.4375" layer="200"/>
<rectangle x1="6.1875" y1="0.4375" x2="6.4625" y2="0.4625" layer="200"/>
<rectangle x1="7.3875" y1="0.4375" x2="8.4375" y2="0.4625" layer="200"/>
<rectangle x1="6.1875" y1="0.4625" x2="6.4625" y2="0.4875" layer="200"/>
<rectangle x1="7.4125" y1="0.4625" x2="8.4875" y2="0.4875" layer="200"/>
<rectangle x1="6.2125" y1="0.4875" x2="6.4625" y2="0.5125" layer="200"/>
<rectangle x1="7.4375" y1="0.4875" x2="8.5375" y2="0.5125" layer="200"/>
<rectangle x1="6.2125" y1="0.5125" x2="6.4625" y2="0.5375" layer="200"/>
<rectangle x1="7.4625" y1="0.5125" x2="8.5875" y2="0.5375" layer="200"/>
<rectangle x1="6.2375" y1="0.5375" x2="6.4625" y2="0.5625" layer="200"/>
<rectangle x1="7.4875" y1="0.5375" x2="7.7375" y2="0.5625" layer="200"/>
<rectangle x1="7.8875" y1="0.5375" x2="8.6375" y2="0.5625" layer="200"/>
<rectangle x1="6.2375" y1="0.5625" x2="6.4875" y2="0.5875" layer="200"/>
<rectangle x1="7.4875" y1="0.5625" x2="7.7375" y2="0.5875" layer="200"/>
<rectangle x1="7.9875" y1="0.5625" x2="8.6875" y2="0.5875" layer="200"/>
<rectangle x1="6.2625" y1="0.5875" x2="6.4875" y2="0.6125" layer="200"/>
<rectangle x1="7.5125" y1="0.5875" x2="7.7375" y2="0.6125" layer="200"/>
<rectangle x1="8.0875" y1="0.5875" x2="8.7375" y2="0.6125" layer="200"/>
<rectangle x1="6.2625" y1="0.6125" x2="6.4875" y2="0.6375" layer="200"/>
<rectangle x1="7.5125" y1="0.6125" x2="7.7375" y2="0.6375" layer="200"/>
<rectangle x1="8.1625" y1="0.6125" x2="8.7625" y2="0.6375" layer="200"/>
<rectangle x1="6.2625" y1="0.6375" x2="6.4875" y2="0.6625" layer="200"/>
<rectangle x1="7.5125" y1="0.6375" x2="7.7375" y2="0.6625" layer="200"/>
<rectangle x1="8.2375" y1="0.6375" x2="8.8125" y2="0.6625" layer="200"/>
<rectangle x1="6.2875" y1="0.6625" x2="6.5125" y2="0.6875" layer="200"/>
<rectangle x1="7.5375" y1="0.6625" x2="7.7625" y2="0.6875" layer="200"/>
<rectangle x1="8.3125" y1="0.6625" x2="8.8625" y2="0.6875" layer="200"/>
<rectangle x1="6.2875" y1="0.6875" x2="6.5125" y2="0.7125" layer="200"/>
<rectangle x1="7.5375" y1="0.6875" x2="7.7625" y2="0.7125" layer="200"/>
<rectangle x1="8.3875" y1="0.6875" x2="8.8875" y2="0.7125" layer="200"/>
<rectangle x1="6.3125" y1="0.7125" x2="6.5375" y2="0.7375" layer="200"/>
<rectangle x1="7.5375" y1="0.7125" x2="7.7625" y2="0.7375" layer="200"/>
<rectangle x1="8.4375" y1="0.7125" x2="8.9375" y2="0.7375" layer="200"/>
<rectangle x1="6.3125" y1="0.7375" x2="6.5375" y2="0.7625" layer="200"/>
<rectangle x1="7.5625" y1="0.7375" x2="7.7875" y2="0.7625" layer="200"/>
<rectangle x1="8.4875" y1="0.7375" x2="8.9625" y2="0.7625" layer="200"/>
<rectangle x1="6.3375" y1="0.7625" x2="6.5375" y2="0.7875" layer="200"/>
<rectangle x1="7.5625" y1="0.7625" x2="7.7875" y2="0.7875" layer="200"/>
<rectangle x1="8.5375" y1="0.7625" x2="9.0125" y2="0.7875" layer="200"/>
<rectangle x1="6.3375" y1="0.7875" x2="6.5625" y2="0.8125" layer="200"/>
<rectangle x1="7.5625" y1="0.7875" x2="7.7875" y2="0.8125" layer="200"/>
<rectangle x1="8.5875" y1="0.7875" x2="9.0625" y2="0.8125" layer="200"/>
<rectangle x1="6.3625" y1="0.8125" x2="6.5625" y2="0.8375" layer="200"/>
<rectangle x1="7.5875" y1="0.8125" x2="7.8125" y2="0.8375" layer="200"/>
<rectangle x1="8.6375" y1="0.8125" x2="9.0875" y2="0.8375" layer="200"/>
<rectangle x1="6.3625" y1="0.8375" x2="6.5875" y2="0.8625" layer="200"/>
<rectangle x1="7.5875" y1="0.8375" x2="7.8125" y2="0.8625" layer="200"/>
<rectangle x1="8.6875" y1="0.8375" x2="9.1375" y2="0.8625" layer="200"/>
<rectangle x1="6.3875" y1="0.8625" x2="6.5875" y2="0.8875" layer="200"/>
<rectangle x1="7.6125" y1="0.8625" x2="7.8375" y2="0.8875" layer="200"/>
<rectangle x1="8.7375" y1="0.8625" x2="9.1625" y2="0.8875" layer="200"/>
<rectangle x1="6.3875" y1="0.8875" x2="6.6125" y2="0.9125" layer="200"/>
<rectangle x1="7.6125" y1="0.8875" x2="7.8375" y2="0.9125" layer="200"/>
<rectangle x1="8.7875" y1="0.8875" x2="9.2125" y2="0.9125" layer="200"/>
<rectangle x1="6.4125" y1="0.9125" x2="6.6125" y2="0.9375" layer="200"/>
<rectangle x1="7.6375" y1="0.9125" x2="7.8375" y2="0.9375" layer="200"/>
<rectangle x1="8.8375" y1="0.9125" x2="9.2375" y2="0.9375" layer="200"/>
<rectangle x1="6.4125" y1="0.9375" x2="6.6125" y2="0.9625" layer="200"/>
<rectangle x1="7.6375" y1="0.9375" x2="7.8625" y2="0.9625" layer="200"/>
<rectangle x1="8.8625" y1="0.9375" x2="9.2875" y2="0.9625" layer="200"/>
<rectangle x1="6.4375" y1="0.9625" x2="6.6375" y2="0.9875" layer="200"/>
<rectangle x1="7.6625" y1="0.9625" x2="7.8625" y2="0.9875" layer="200"/>
<rectangle x1="8.9125" y1="0.9625" x2="9.3125" y2="0.9875" layer="200"/>
<rectangle x1="6.4375" y1="0.9875" x2="6.6375" y2="1.0125" layer="200"/>
<rectangle x1="7.6625" y1="0.9875" x2="7.8875" y2="1.0125" layer="200"/>
<rectangle x1="8.9375" y1="0.9875" x2="9.3625" y2="1.0125" layer="200"/>
<rectangle x1="6.4625" y1="1.0125" x2="6.6625" y2="1.0375" layer="200"/>
<rectangle x1="7.6625" y1="1.0125" x2="7.8875" y2="1.0375" layer="200"/>
<rectangle x1="8.9875" y1="1.0125" x2="9.3875" y2="1.0375" layer="200"/>
<rectangle x1="6.4625" y1="1.0375" x2="6.6625" y2="1.0625" layer="200"/>
<rectangle x1="7.6875" y1="1.0375" x2="7.9125" y2="1.0625" layer="200"/>
<rectangle x1="9.0125" y1="1.0375" x2="9.4375" y2="1.0625" layer="200"/>
<rectangle x1="6.4625" y1="1.0625" x2="6.6875" y2="1.0875" layer="200"/>
<rectangle x1="7.6875" y1="1.0625" x2="7.9125" y2="1.0875" layer="200"/>
<rectangle x1="9.0625" y1="1.0625" x2="9.4625" y2="1.0875" layer="200"/>
<rectangle x1="6.4875" y1="1.0875" x2="6.6875" y2="1.1125" layer="200"/>
<rectangle x1="7.6875" y1="1.0875" x2="7.9125" y2="1.1125" layer="200"/>
<rectangle x1="9.0875" y1="1.0875" x2="9.4875" y2="1.1125" layer="200"/>
<rectangle x1="6.4875" y1="1.1125" x2="6.7125" y2="1.1375" layer="200"/>
<rectangle x1="7.7125" y1="1.1125" x2="7.9375" y2="1.1375" layer="200"/>
<rectangle x1="9.1375" y1="1.1125" x2="9.5375" y2="1.1375" layer="200"/>
<rectangle x1="6.5125" y1="1.1375" x2="6.7125" y2="1.1625" layer="200"/>
<rectangle x1="7.7125" y1="1.1375" x2="7.9375" y2="1.1625" layer="200"/>
<rectangle x1="9.1625" y1="1.1375" x2="9.5625" y2="1.1625" layer="200"/>
<rectangle x1="6.5125" y1="1.1625" x2="6.7375" y2="1.1875" layer="200"/>
<rectangle x1="7.7375" y1="1.1625" x2="7.9625" y2="1.1875" layer="200"/>
<rectangle x1="9.1875" y1="1.1625" x2="9.5875" y2="1.1875" layer="200"/>
<rectangle x1="6.5125" y1="1.1875" x2="6.7375" y2="1.2125" layer="200"/>
<rectangle x1="7.7375" y1="1.1875" x2="7.9625" y2="1.2125" layer="200"/>
<rectangle x1="9.2375" y1="1.1875" x2="9.6125" y2="1.2125" layer="200"/>
<rectangle x1="6.5375" y1="1.2125" x2="6.7625" y2="1.2375" layer="200"/>
<rectangle x1="7.7625" y1="1.2125" x2="7.9875" y2="1.2375" layer="200"/>
<rectangle x1="9.2625" y1="1.2125" x2="9.6625" y2="1.2375" layer="200"/>
<rectangle x1="6.5375" y1="1.2375" x2="6.7625" y2="1.2625" layer="200"/>
<rectangle x1="7.7625" y1="1.2375" x2="7.9875" y2="1.2625" layer="200"/>
<rectangle x1="9.2875" y1="1.2375" x2="9.6875" y2="1.2625" layer="200"/>
<rectangle x1="6.5625" y1="1.2625" x2="6.7875" y2="1.2875" layer="200"/>
<rectangle x1="7.7625" y1="1.2625" x2="8.0125" y2="1.2875" layer="200"/>
<rectangle x1="9.3375" y1="1.2625" x2="9.7375" y2="1.2875" layer="200"/>
<rectangle x1="6.5625" y1="1.2875" x2="6.7875" y2="1.3125" layer="200"/>
<rectangle x1="7.7875" y1="1.2875" x2="8.0125" y2="1.3125" layer="200"/>
<rectangle x1="9.3625" y1="1.2875" x2="9.7625" y2="1.3125" layer="200"/>
<rectangle x1="6.5875" y1="1.3125" x2="6.8125" y2="1.3375" layer="200"/>
<rectangle x1="7.7875" y1="1.3125" x2="8.0375" y2="1.3375" layer="200"/>
<rectangle x1="9.3875" y1="1.3125" x2="9.7875" y2="1.3375" layer="200"/>
<rectangle x1="6.5875" y1="1.3375" x2="6.8125" y2="1.3625" layer="200"/>
<rectangle x1="7.8125" y1="1.3375" x2="8.0375" y2="1.3625" layer="200"/>
<rectangle x1="9.4125" y1="1.3375" x2="9.8375" y2="1.3625" layer="200"/>
<rectangle x1="6.5875" y1="1.3625" x2="6.8375" y2="1.3875" layer="200"/>
<rectangle x1="7.8125" y1="1.3625" x2="8.0375" y2="1.3875" layer="200"/>
<rectangle x1="9.4625" y1="1.3625" x2="9.8625" y2="1.3875" layer="200"/>
<rectangle x1="6.6125" y1="1.3875" x2="6.8375" y2="1.4125" layer="200"/>
<rectangle x1="7.8375" y1="1.3875" x2="8.0625" y2="1.4125" layer="200"/>
<rectangle x1="9.4875" y1="1.3875" x2="9.8875" y2="1.4125" layer="200"/>
<rectangle x1="6.6125" y1="1.4125" x2="6.8375" y2="1.4375" layer="200"/>
<rectangle x1="7.8375" y1="1.4125" x2="8.0625" y2="1.4375" layer="200"/>
<rectangle x1="9.5125" y1="1.4125" x2="9.9375" y2="1.4375" layer="200"/>
<rectangle x1="6.6375" y1="1.4375" x2="6.8625" y2="1.4625" layer="200"/>
<rectangle x1="7.8625" y1="1.4375" x2="8.0875" y2="1.4625" layer="200"/>
<rectangle x1="9.5375" y1="1.4375" x2="9.9625" y2="1.4625" layer="200"/>
<rectangle x1="6.6375" y1="1.4625" x2="6.8625" y2="1.4875" layer="200"/>
<rectangle x1="7.8625" y1="1.4625" x2="8.0875" y2="1.4875" layer="200"/>
<rectangle x1="9.5875" y1="1.4625" x2="9.9875" y2="1.4875" layer="200"/>
<rectangle x1="6.6625" y1="1.4875" x2="6.8875" y2="1.5125" layer="200"/>
<rectangle x1="7.8875" y1="1.4875" x2="8.1125" y2="1.5125" layer="200"/>
<rectangle x1="9.6125" y1="1.4875" x2="10.0375" y2="1.5125" layer="200"/>
<rectangle x1="6.6625" y1="1.5125" x2="6.8875" y2="1.5375" layer="200"/>
<rectangle x1="7.8875" y1="1.5125" x2="8.1125" y2="1.5375" layer="200"/>
<rectangle x1="9.6625" y1="1.5125" x2="10.0625" y2="1.5375" layer="200"/>
<rectangle x1="6.6875" y1="1.5375" x2="6.9125" y2="1.5625" layer="200"/>
<rectangle x1="7.9125" y1="1.5375" x2="8.1375" y2="1.5625" layer="200"/>
<rectangle x1="9.6875" y1="1.5375" x2="10.0875" y2="1.5625" layer="200"/>
<rectangle x1="6.6875" y1="1.5625" x2="6.9125" y2="1.5875" layer="200"/>
<rectangle x1="7.9125" y1="1.5625" x2="8.1375" y2="1.5875" layer="200"/>
<rectangle x1="9.7625" y1="1.5625" x2="10.1125" y2="1.5875" layer="200"/>
<rectangle x1="6.6875" y1="1.5875" x2="6.9375" y2="1.6125" layer="200"/>
<rectangle x1="7.9125" y1="1.5875" x2="8.1375" y2="1.6125" layer="200"/>
<rectangle x1="9.7875" y1="1.5875" x2="10.1375" y2="1.6125" layer="200"/>
<rectangle x1="6.7125" y1="1.6125" x2="6.9375" y2="1.6375" layer="200"/>
<rectangle x1="7.9375" y1="1.6125" x2="8.1625" y2="1.6375" layer="200"/>
<rectangle x1="9.8125" y1="1.6125" x2="10.1875" y2="1.6375" layer="200"/>
<rectangle x1="6.7125" y1="1.6375" x2="6.9625" y2="1.6625" layer="200"/>
<rectangle x1="7.9375" y1="1.6375" x2="8.1625" y2="1.6625" layer="200"/>
<rectangle x1="9.8375" y1="1.6375" x2="10.2125" y2="1.6625" layer="200"/>
<rectangle x1="6.7375" y1="1.6625" x2="6.9625" y2="1.6875" layer="200"/>
<rectangle x1="7.9625" y1="1.6625" x2="8.1875" y2="1.6875" layer="200"/>
<rectangle x1="9.8625" y1="1.6625" x2="10.2375" y2="1.6875" layer="200"/>
<rectangle x1="6.7375" y1="1.6875" x2="6.9875" y2="1.7125" layer="200"/>
<rectangle x1="7.9625" y1="1.6875" x2="8.1875" y2="1.7125" layer="200"/>
<rectangle x1="9.8875" y1="1.6875" x2="10.2625" y2="1.7125" layer="200"/>
<rectangle x1="6.7625" y1="1.7125" x2="6.9875" y2="1.7375" layer="200"/>
<rectangle x1="7.9875" y1="1.7125" x2="8.2125" y2="1.7375" layer="200"/>
<rectangle x1="9.9375" y1="1.7125" x2="10.3125" y2="1.7375" layer="200"/>
<rectangle x1="6.7625" y1="1.7375" x2="7.0125" y2="1.7625" layer="200"/>
<rectangle x1="7.9875" y1="1.7375" x2="8.2125" y2="1.7625" layer="200"/>
<rectangle x1="9.9625" y1="1.7375" x2="10.3375" y2="1.7625" layer="200"/>
<rectangle x1="6.7875" y1="1.7625" x2="7.0125" y2="1.7875" layer="200"/>
<rectangle x1="8.0125" y1="1.7625" x2="8.2375" y2="1.7875" layer="200"/>
<rectangle x1="9.9875" y1="1.7625" x2="10.3625" y2="1.7875" layer="200"/>
<rectangle x1="6.7875" y1="1.7875" x2="7.0375" y2="1.8125" layer="200"/>
<rectangle x1="8.0125" y1="1.7875" x2="8.2375" y2="1.8125" layer="200"/>
<rectangle x1="10.0125" y1="1.7875" x2="10.4125" y2="1.8125" layer="200"/>
<rectangle x1="6.8125" y1="1.8125" x2="7.0375" y2="1.8375" layer="200"/>
<rectangle x1="8.0375" y1="1.8125" x2="8.2625" y2="1.8375" layer="200"/>
<rectangle x1="10.0625" y1="1.8125" x2="10.4375" y2="1.8375" layer="200"/>
<rectangle x1="6.8125" y1="1.8375" x2="7.0625" y2="1.8625" layer="200"/>
<rectangle x1="8.0375" y1="1.8375" x2="8.2625" y2="1.8625" layer="200"/>
<rectangle x1="10.0875" y1="1.8375" x2="10.4625" y2="1.8625" layer="200"/>
<rectangle x1="6.8125" y1="1.8625" x2="7.0625" y2="1.8875" layer="200"/>
<rectangle x1="8.0625" y1="1.8625" x2="8.2625" y2="1.8875" layer="200"/>
<rectangle x1="10.1375" y1="1.8625" x2="10.4875" y2="1.8875" layer="200"/>
<rectangle x1="6.8375" y1="1.8875" x2="7.0875" y2="1.9125" layer="200"/>
<rectangle x1="8.0625" y1="1.8875" x2="8.2875" y2="1.9125" layer="200"/>
<rectangle x1="10.1625" y1="1.8875" x2="10.5375" y2="1.9125" layer="200"/>
<rectangle x1="6.8375" y1="1.9125" x2="7.0875" y2="1.9375" layer="200"/>
<rectangle x1="8.0875" y1="1.9125" x2="8.2875" y2="1.9375" layer="200"/>
<rectangle x1="10.2125" y1="1.9125" x2="10.5625" y2="1.9375" layer="200"/>
<rectangle x1="6.8625" y1="1.9375" x2="7.1125" y2="1.9625" layer="200"/>
<rectangle x1="8.0875" y1="1.9375" x2="8.3125" y2="1.9625" layer="200"/>
<rectangle x1="10.2625" y1="1.9375" x2="10.5875" y2="1.9625" layer="200"/>
<rectangle x1="6.8625" y1="1.9625" x2="7.1125" y2="1.9875" layer="200"/>
<rectangle x1="8.1125" y1="1.9625" x2="8.3125" y2="1.9875" layer="200"/>
<rectangle x1="10.2875" y1="1.9625" x2="10.6375" y2="1.9875" layer="200"/>
<rectangle x1="6.8875" y1="1.9875" x2="7.1375" y2="2.0125" layer="200"/>
<rectangle x1="8.1125" y1="1.9875" x2="8.3375" y2="2.0125" layer="200"/>
<rectangle x1="10.3375" y1="1.9875" x2="10.6625" y2="2.0125" layer="200"/>
<rectangle x1="6.8875" y1="2.0125" x2="7.1375" y2="2.0375" layer="200"/>
<rectangle x1="8.1375" y1="2.0125" x2="8.3375" y2="2.0375" layer="200"/>
<rectangle x1="10.3625" y1="2.0125" x2="10.6875" y2="2.0375" layer="200"/>
<rectangle x1="6.9125" y1="2.0375" x2="7.1375" y2="2.0625" layer="200"/>
<rectangle x1="8.1375" y1="2.0375" x2="8.3625" y2="2.0625" layer="200"/>
<rectangle x1="10.4125" y1="2.0375" x2="10.7375" y2="2.0625" layer="200"/>
<rectangle x1="6.9125" y1="2.0625" x2="7.1625" y2="2.0875" layer="200"/>
<rectangle x1="8.1625" y1="2.0625" x2="8.3625" y2="2.0875" layer="200"/>
<rectangle x1="10.4375" y1="2.0625" x2="10.7625" y2="2.0875" layer="200"/>
<rectangle x1="6.9375" y1="2.0875" x2="7.1625" y2="2.1125" layer="200"/>
<rectangle x1="8.1625" y1="2.0875" x2="8.3875" y2="2.1125" layer="200"/>
<rectangle x1="10.4875" y1="2.0875" x2="10.7875" y2="2.1125" layer="200"/>
<rectangle x1="6.9375" y1="2.1125" x2="7.1875" y2="2.1375" layer="200"/>
<rectangle x1="8.1625" y1="2.1125" x2="8.3875" y2="2.1375" layer="200"/>
<rectangle x1="10.5125" y1="2.1125" x2="10.8375" y2="2.1375" layer="200"/>
<rectangle x1="6.9375" y1="2.1375" x2="7.1875" y2="2.1625" layer="200"/>
<rectangle x1="8.1875" y1="2.1375" x2="8.3875" y2="2.1625" layer="200"/>
<rectangle x1="10.5375" y1="2.1375" x2="10.8625" y2="2.1625" layer="200"/>
<rectangle x1="6.9625" y1="2.1625" x2="7.2125" y2="2.1875" layer="200"/>
<rectangle x1="8.1875" y1="2.1625" x2="8.4125" y2="2.1875" layer="200"/>
<rectangle x1="10.5625" y1="2.1625" x2="10.8875" y2="2.1875" layer="200"/>
<rectangle x1="6.9625" y1="2.1875" x2="7.2125" y2="2.2125" layer="200"/>
<rectangle x1="8.2125" y1="2.1875" x2="8.4125" y2="2.2125" layer="200"/>
<rectangle x1="10.5875" y1="2.1875" x2="10.9375" y2="2.2125" layer="200"/>
<rectangle x1="6.9875" y1="2.2125" x2="7.2375" y2="2.2375" layer="200"/>
<rectangle x1="8.2125" y1="2.2125" x2="8.4375" y2="2.2375" layer="200"/>
<rectangle x1="10.6375" y1="2.2125" x2="10.9625" y2="2.2375" layer="200"/>
<rectangle x1="6.9875" y1="2.2375" x2="7.2375" y2="2.2625" layer="200"/>
<rectangle x1="8.2375" y1="2.2375" x2="8.4375" y2="2.2625" layer="200"/>
<rectangle x1="10.6625" y1="2.2375" x2="10.9875" y2="2.2625" layer="200"/>
<rectangle x1="7.0125" y1="2.2625" x2="7.2625" y2="2.2875" layer="200"/>
<rectangle x1="8.2375" y1="2.2625" x2="8.4625" y2="2.2875" layer="200"/>
<rectangle x1="10.6875" y1="2.2625" x2="11.0125" y2="2.2875" layer="200"/>
<rectangle x1="7.0125" y1="2.2875" x2="7.2625" y2="2.3125" layer="200"/>
<rectangle x1="8.2625" y1="2.2875" x2="8.4625" y2="2.3125" layer="200"/>
<rectangle x1="10.7125" y1="2.2875" x2="11.0625" y2="2.3125" layer="200"/>
<rectangle x1="7.0375" y1="2.3125" x2="7.2875" y2="2.3375" layer="200"/>
<rectangle x1="8.2625" y1="2.3125" x2="8.4875" y2="2.3375" layer="200"/>
<rectangle x1="10.7375" y1="2.3125" x2="11.0875" y2="2.3375" layer="200"/>
<rectangle x1="7.0375" y1="2.3375" x2="7.2875" y2="2.3625" layer="200"/>
<rectangle x1="8.2875" y1="2.3375" x2="8.4875" y2="2.3625" layer="200"/>
<rectangle x1="10.7875" y1="2.3375" x2="11.1125" y2="2.3625" layer="200"/>
<rectangle x1="7.0375" y1="2.3625" x2="7.3125" y2="2.3875" layer="200"/>
<rectangle x1="8.2875" y1="2.3625" x2="8.5125" y2="2.3875" layer="200"/>
<rectangle x1="10.8125" y1="2.3625" x2="11.1375" y2="2.3875" layer="200"/>
<rectangle x1="7.0625" y1="2.3875" x2="7.3125" y2="2.4125" layer="200"/>
<rectangle x1="8.3125" y1="2.3875" x2="8.5125" y2="2.4125" layer="200"/>
<rectangle x1="10.8375" y1="2.3875" x2="11.1625" y2="2.4125" layer="200"/>
<rectangle x1="7.0625" y1="2.4125" x2="7.3375" y2="2.4375" layer="200"/>
<rectangle x1="8.3125" y1="2.4125" x2="8.5125" y2="2.4375" layer="200"/>
<rectangle x1="10.8625" y1="2.4125" x2="11.2125" y2="2.4375" layer="200"/>
<rectangle x1="7.0875" y1="2.4375" x2="7.3375" y2="2.4625" layer="200"/>
<rectangle x1="8.3375" y1="2.4375" x2="8.5375" y2="2.4625" layer="200"/>
<rectangle x1="10.8875" y1="2.4375" x2="11.2375" y2="2.4625" layer="200"/>
<rectangle x1="7.0875" y1="2.4625" x2="7.3375" y2="2.4875" layer="200"/>
<rectangle x1="8.3375" y1="2.4625" x2="8.5375" y2="2.4875" layer="200"/>
<rectangle x1="10.9375" y1="2.4625" x2="11.2625" y2="2.4875" layer="200"/>
<rectangle x1="7.1125" y1="2.4875" x2="7.3625" y2="2.5125" layer="200"/>
<rectangle x1="8.3625" y1="2.4875" x2="8.5625" y2="2.5125" layer="200"/>
<rectangle x1="10.9625" y1="2.4875" x2="11.2875" y2="2.5125" layer="200"/>
<rectangle x1="7.1125" y1="2.5125" x2="7.3625" y2="2.5375" layer="200"/>
<rectangle x1="8.3625" y1="2.5125" x2="8.5625" y2="2.5375" layer="200"/>
<rectangle x1="10.9875" y1="2.5125" x2="11.3375" y2="2.5375" layer="200"/>
<rectangle x1="7.1375" y1="2.5375" x2="7.3875" y2="2.5625" layer="200"/>
<rectangle x1="8.3875" y1="2.5375" x2="8.5875" y2="2.5625" layer="200"/>
<rectangle x1="11.0125" y1="2.5375" x2="11.3625" y2="2.5625" layer="200"/>
<rectangle x1="7.1375" y1="2.5625" x2="7.3875" y2="2.5875" layer="200"/>
<rectangle x1="8.3875" y1="2.5625" x2="8.5875" y2="2.5875" layer="200"/>
<rectangle x1="11.0375" y1="2.5625" x2="11.3875" y2="2.5875" layer="200"/>
<rectangle x1="7.1625" y1="2.5875" x2="7.4125" y2="2.6125" layer="200"/>
<rectangle x1="8.3875" y1="2.5875" x2="8.6125" y2="2.6125" layer="200"/>
<rectangle x1="11.0625" y1="2.5875" x2="11.4125" y2="2.6125" layer="200"/>
<rectangle x1="7.1625" y1="2.6125" x2="7.4125" y2="2.6375" layer="200"/>
<rectangle x1="8.4125" y1="2.6125" x2="8.6125" y2="2.6375" layer="200"/>
<rectangle x1="11.1125" y1="2.6125" x2="11.4625" y2="2.6375" layer="200"/>
<rectangle x1="7.1875" y1="2.6375" x2="7.4375" y2="2.6625" layer="200"/>
<rectangle x1="8.4125" y1="2.6375" x2="8.6375" y2="2.6625" layer="200"/>
<rectangle x1="11.1375" y1="2.6375" x2="11.4875" y2="2.6625" layer="200"/>
<rectangle x1="7.1875" y1="2.6625" x2="7.4375" y2="2.6875" layer="200"/>
<rectangle x1="8.4375" y1="2.6625" x2="8.6375" y2="2.6875" layer="200"/>
<rectangle x1="11.1625" y1="2.6625" x2="11.5125" y2="2.6875" layer="200"/>
<rectangle x1="7.2125" y1="2.6875" x2="7.4625" y2="2.7125" layer="200"/>
<rectangle x1="8.4375" y1="2.6875" x2="8.6625" y2="2.7125" layer="200"/>
<rectangle x1="11.1875" y1="2.6875" x2="11.5375" y2="2.7125" layer="200"/>
<rectangle x1="7.2125" y1="2.7125" x2="7.4625" y2="2.7375" layer="200"/>
<rectangle x1="8.4625" y1="2.7125" x2="8.6625" y2="2.7375" layer="200"/>
<rectangle x1="11.2125" y1="2.7125" x2="11.5875" y2="2.7375" layer="200"/>
<rectangle x1="7.2375" y1="2.7375" x2="7.4875" y2="2.7625" layer="200"/>
<rectangle x1="8.4625" y1="2.7375" x2="8.6625" y2="2.7625" layer="200"/>
<rectangle x1="11.2625" y1="2.7375" x2="11.6125" y2="2.7625" layer="200"/>
<rectangle x1="7.2375" y1="2.7625" x2="7.4875" y2="2.7875" layer="200"/>
<rectangle x1="8.4875" y1="2.7625" x2="8.6875" y2="2.7875" layer="200"/>
<rectangle x1="11.2875" y1="2.7625" x2="11.6375" y2="2.7875" layer="200"/>
<rectangle x1="7.2625" y1="2.7875" x2="7.5125" y2="2.8125" layer="200"/>
<rectangle x1="8.4875" y1="2.7875" x2="8.6875" y2="2.8125" layer="200"/>
<rectangle x1="11.3125" y1="2.7875" x2="11.6625" y2="2.8125" layer="200"/>
<rectangle x1="7.2625" y1="2.8125" x2="7.5625" y2="2.8375" layer="200"/>
<rectangle x1="8.4875" y1="2.8125" x2="8.7125" y2="2.8375" layer="200"/>
<rectangle x1="11.3375" y1="2.8125" x2="11.7125" y2="2.8375" layer="200"/>
<rectangle x1="7.2875" y1="2.8375" x2="7.6375" y2="2.8625" layer="200"/>
<rectangle x1="8.5125" y1="2.8375" x2="8.7125" y2="2.8625" layer="200"/>
<rectangle x1="11.3625" y1="2.8375" x2="11.7375" y2="2.8625" layer="200"/>
<rectangle x1="7.2875" y1="2.8625" x2="7.8375" y2="2.8875" layer="200"/>
<rectangle x1="8.5125" y1="2.8625" x2="8.7375" y2="2.8875" layer="200"/>
<rectangle x1="11.4125" y1="2.8625" x2="11.7625" y2="2.8875" layer="200"/>
<rectangle x1="7.3125" y1="2.8875" x2="8.7375" y2="2.9125" layer="200"/>
<rectangle x1="11.4375" y1="2.8875" x2="11.8125" y2="2.9125" layer="200"/>
<rectangle x1="7.3125" y1="2.9125" x2="8.7375" y2="2.9375" layer="200"/>
<rectangle x1="11.4625" y1="2.9125" x2="11.8375" y2="2.9375" layer="200"/>
<rectangle x1="7.3375" y1="2.9375" x2="8.7625" y2="2.9625" layer="200"/>
<rectangle x1="11.4875" y1="2.9375" x2="11.8625" y2="2.9625" layer="200"/>
<rectangle x1="7.3375" y1="2.9625" x2="8.7625" y2="2.9875" layer="200"/>
<rectangle x1="11.5375" y1="2.9625" x2="11.9125" y2="2.9875" layer="200"/>
<rectangle x1="7.3625" y1="2.9875" x2="8.7625" y2="3.0125" layer="200"/>
<rectangle x1="11.5625" y1="2.9875" x2="11.9375" y2="3.0125" layer="200"/>
<rectangle x1="7.3625" y1="3.0125" x2="8.7875" y2="3.0375" layer="200"/>
<rectangle x1="11.5875" y1="3.0125" x2="11.9625" y2="3.0375" layer="200"/>
<rectangle x1="7.3875" y1="3.0375" x2="8.8125" y2="3.0625" layer="200"/>
<rectangle x1="11.6125" y1="3.0375" x2="11.9875" y2="3.0625" layer="200"/>
<rectangle x1="7.3875" y1="3.0625" x2="8.8375" y2="3.0875" layer="200"/>
<rectangle x1="11.6375" y1="3.0625" x2="12.0375" y2="3.0875" layer="200"/>
<rectangle x1="7.4125" y1="3.0875" x2="8.8625" y2="3.1125" layer="200"/>
<rectangle x1="11.6875" y1="3.0875" x2="12.0625" y2="3.1125" layer="200"/>
<rectangle x1="7.4125" y1="3.1125" x2="8.8625" y2="3.1375" layer="200"/>
<rectangle x1="11.7125" y1="3.1125" x2="12.0875" y2="3.1375" layer="200"/>
<rectangle x1="7.4375" y1="3.1375" x2="7.7125" y2="3.1625" layer="200"/>
<rectangle x1="8.5375" y1="3.1375" x2="8.8875" y2="3.1625" layer="200"/>
<rectangle x1="11.7375" y1="3.1375" x2="12.1375" y2="3.1625" layer="200"/>
<rectangle x1="7.4375" y1="3.1625" x2="7.7375" y2="3.1875" layer="200"/>
<rectangle x1="8.6625" y1="3.1625" x2="8.9125" y2="3.1875" layer="200"/>
<rectangle x1="11.7625" y1="3.1625" x2="12.1625" y2="3.1875" layer="200"/>
<rectangle x1="7.4625" y1="3.1875" x2="7.7375" y2="3.2125" layer="200"/>
<rectangle x1="8.6875" y1="3.1875" x2="8.9125" y2="3.2125" layer="200"/>
<rectangle x1="11.8125" y1="3.1875" x2="12.1875" y2="3.2125" layer="200"/>
<rectangle x1="7.4625" y1="3.2125" x2="7.7625" y2="3.2375" layer="200"/>
<rectangle x1="8.6875" y1="3.2125" x2="8.9375" y2="3.2375" layer="200"/>
<rectangle x1="11.8375" y1="3.2125" x2="12.2375" y2="3.2375" layer="200"/>
<rectangle x1="6.5125" y1="3.2375" x2="6.6375" y2="3.2625" layer="200"/>
<rectangle x1="7.4875" y1="3.2375" x2="7.7625" y2="3.2625" layer="200"/>
<rectangle x1="8.7125" y1="3.2375" x2="8.9625" y2="3.2625" layer="200"/>
<rectangle x1="11.8625" y1="3.2375" x2="12.2625" y2="3.2625" layer="200"/>
<rectangle x1="6.5125" y1="3.2625" x2="6.7375" y2="3.2875" layer="200"/>
<rectangle x1="7.4875" y1="3.2625" x2="7.7875" y2="3.2875" layer="200"/>
<rectangle x1="8.7375" y1="3.2625" x2="8.9625" y2="3.2875" layer="200"/>
<rectangle x1="11.8875" y1="3.2625" x2="12.3125" y2="3.2875" layer="200"/>
<rectangle x1="6.5125" y1="3.2875" x2="6.8125" y2="3.3125" layer="200"/>
<rectangle x1="7.5125" y1="3.2875" x2="7.8125" y2="3.3125" layer="200"/>
<rectangle x1="8.7375" y1="3.2875" x2="8.9875" y2="3.3125" layer="200"/>
<rectangle x1="11.9125" y1="3.2875" x2="12.3625" y2="3.3125" layer="200"/>
<rectangle x1="6.4875" y1="3.3125" x2="6.9125" y2="3.3375" layer="200"/>
<rectangle x1="7.5125" y1="3.3125" x2="7.8125" y2="3.3375" layer="200"/>
<rectangle x1="8.7625" y1="3.3125" x2="9.0125" y2="3.3375" layer="200"/>
<rectangle x1="11.9625" y1="3.3125" x2="12.3875" y2="3.3375" layer="200"/>
<rectangle x1="6.4625" y1="3.3375" x2="6.9875" y2="3.3625" layer="200"/>
<rectangle x1="7.5125" y1="3.3375" x2="7.8375" y2="3.3625" layer="200"/>
<rectangle x1="8.7625" y1="3.3375" x2="9.0125" y2="3.3625" layer="200"/>
<rectangle x1="11.9875" y1="3.3375" x2="12.4375" y2="3.3625" layer="200"/>
<rectangle x1="6.4125" y1="3.3625" x2="7.0875" y2="3.3875" layer="200"/>
<rectangle x1="7.5375" y1="3.3625" x2="7.8375" y2="3.3875" layer="200"/>
<rectangle x1="8.7875" y1="3.3625" x2="9.0375" y2="3.3875" layer="200"/>
<rectangle x1="12.0125" y1="3.3625" x2="12.4875" y2="3.3875" layer="200"/>
<rectangle x1="6.3625" y1="3.3875" x2="7.1875" y2="3.4125" layer="200"/>
<rectangle x1="7.5375" y1="3.3875" x2="7.8625" y2="3.4125" layer="200"/>
<rectangle x1="8.8125" y1="3.3875" x2="9.0625" y2="3.4125" layer="200"/>
<rectangle x1="12.0625" y1="3.3875" x2="12.5375" y2="3.4125" layer="200"/>
<rectangle x1="6.2875" y1="3.4125" x2="7.2625" y2="3.4375" layer="200"/>
<rectangle x1="7.5625" y1="3.4125" x2="7.8625" y2="3.4375" layer="200"/>
<rectangle x1="8.8125" y1="3.4125" x2="9.0625" y2="3.4375" layer="200"/>
<rectangle x1="12.0875" y1="3.4125" x2="12.5875" y2="3.4375" layer="200"/>
<rectangle x1="6.1875" y1="3.4375" x2="7.3625" y2="3.4625" layer="200"/>
<rectangle x1="7.5625" y1="3.4375" x2="7.8875" y2="3.4625" layer="200"/>
<rectangle x1="8.8375" y1="3.4375" x2="9.0875" y2="3.4625" layer="200"/>
<rectangle x1="12.1375" y1="3.4375" x2="12.6375" y2="3.4625" layer="200"/>
<rectangle x1="6.0875" y1="3.4625" x2="7.4375" y2="3.4875" layer="200"/>
<rectangle x1="7.5625" y1="3.4625" x2="7.8875" y2="3.4875" layer="200"/>
<rectangle x1="8.8625" y1="3.4625" x2="9.1125" y2="3.4875" layer="200"/>
<rectangle x1="12.1625" y1="3.4625" x2="12.6875" y2="3.4875" layer="200"/>
<rectangle x1="5.9875" y1="3.4875" x2="7.9125" y2="3.5125" layer="200"/>
<rectangle x1="8.8625" y1="3.4875" x2="9.1125" y2="3.5125" layer="200"/>
<rectangle x1="12.2125" y1="3.4875" x2="12.7375" y2="3.5125" layer="200"/>
<rectangle x1="5.8625" y1="3.5125" x2="7.9375" y2="3.5375" layer="200"/>
<rectangle x1="8.8875" y1="3.5125" x2="9.1375" y2="3.5375" layer="200"/>
<rectangle x1="12.2375" y1="3.5125" x2="12.7875" y2="3.5375" layer="200"/>
<rectangle x1="5.7625" y1="3.5375" x2="7.9375" y2="3.5625" layer="200"/>
<rectangle x1="8.9125" y1="3.5375" x2="9.1375" y2="3.5625" layer="200"/>
<rectangle x1="12.2875" y1="3.5375" x2="12.8375" y2="3.5625" layer="200"/>
<rectangle x1="5.6375" y1="3.5625" x2="7.9625" y2="3.5875" layer="200"/>
<rectangle x1="8.9375" y1="3.5625" x2="9.1625" y2="3.5875" layer="200"/>
<rectangle x1="12.3375" y1="3.5625" x2="12.8875" y2="3.5875" layer="200"/>
<rectangle x1="5.5125" y1="3.5875" x2="7.9625" y2="3.6125" layer="200"/>
<rectangle x1="8.9375" y1="3.5875" x2="9.1875" y2="3.6125" layer="200"/>
<rectangle x1="12.3875" y1="3.5875" x2="12.9375" y2="3.6125" layer="200"/>
<rectangle x1="5.3875" y1="3.6125" x2="7.9875" y2="3.6375" layer="200"/>
<rectangle x1="8.9625" y1="3.6125" x2="9.1875" y2="3.6375" layer="200"/>
<rectangle x1="12.4375" y1="3.6125" x2="12.9875" y2="3.6375" layer="200"/>
<rectangle x1="5.2625" y1="3.6375" x2="8.0125" y2="3.6625" layer="200"/>
<rectangle x1="8.9625" y1="3.6375" x2="9.2125" y2="3.6625" layer="200"/>
<rectangle x1="12.4625" y1="3.6375" x2="13.0625" y2="3.6625" layer="200"/>
<rectangle x1="5.1625" y1="3.6625" x2="6.7875" y2="3.6875" layer="200"/>
<rectangle x1="6.9375" y1="3.6625" x2="8.0125" y2="3.6875" layer="200"/>
<rectangle x1="8.9875" y1="3.6625" x2="9.2375" y2="3.6875" layer="200"/>
<rectangle x1="12.5125" y1="3.6625" x2="13.1125" y2="3.6875" layer="200"/>
<rectangle x1="5.0875" y1="3.6875" x2="6.8125" y2="3.7125" layer="200"/>
<rectangle x1="7.0125" y1="3.6875" x2="8.0375" y2="3.7125" layer="200"/>
<rectangle x1="9.0125" y1="3.6875" x2="9.2375" y2="3.7125" layer="200"/>
<rectangle x1="12.5625" y1="3.6875" x2="13.1625" y2="3.7125" layer="200"/>
<rectangle x1="5.0375" y1="3.7125" x2="6.8125" y2="3.7375" layer="200"/>
<rectangle x1="7.1125" y1="3.7125" x2="8.0625" y2="3.7375" layer="200"/>
<rectangle x1="9.0125" y1="3.7125" x2="9.2625" y2="3.7375" layer="200"/>
<rectangle x1="12.6125" y1="3.7125" x2="13.2125" y2="3.7375" layer="200"/>
<rectangle x1="5.0375" y1="3.7375" x2="6.8375" y2="3.7625" layer="200"/>
<rectangle x1="7.1875" y1="3.7375" x2="8.0625" y2="3.7625" layer="200"/>
<rectangle x1="9.0375" y1="3.7375" x2="9.2625" y2="3.7625" layer="200"/>
<rectangle x1="12.6625" y1="3.7375" x2="13.2625" y2="3.7625" layer="200"/>
<rectangle x1="5.0625" y1="3.7625" x2="6.8375" y2="3.7875" layer="200"/>
<rectangle x1="7.2625" y1="3.7625" x2="8.0875" y2="3.7875" layer="200"/>
<rectangle x1="9.0625" y1="3.7625" x2="9.2875" y2="3.7875" layer="200"/>
<rectangle x1="12.7375" y1="3.7625" x2="13.3125" y2="3.7875" layer="200"/>
<rectangle x1="5.1125" y1="3.7875" x2="6.8375" y2="3.8125" layer="200"/>
<rectangle x1="7.3375" y1="3.7875" x2="8.0875" y2="3.8125" layer="200"/>
<rectangle x1="9.0625" y1="3.7875" x2="9.3125" y2="3.8125" layer="200"/>
<rectangle x1="12.7875" y1="3.7875" x2="13.3625" y2="3.8125" layer="200"/>
<rectangle x1="5.1625" y1="3.8125" x2="5.7125" y2="3.8375" layer="200"/>
<rectangle x1="5.7375" y1="3.8125" x2="5.9375" y2="3.8375" layer="200"/>
<rectangle x1="7.4125" y1="3.8125" x2="8.1125" y2="3.8375" layer="200"/>
<rectangle x1="9.0875" y1="3.8125" x2="9.3125" y2="3.8375" layer="200"/>
<rectangle x1="12.8375" y1="3.8125" x2="13.4125" y2="3.8375" layer="200"/>
<rectangle x1="5.1875" y1="3.8375" x2="5.7125" y2="3.8625" layer="200"/>
<rectangle x1="7.4875" y1="3.8375" x2="8.1375" y2="3.8625" layer="200"/>
<rectangle x1="9.0875" y1="3.8375" x2="9.3375" y2="3.8625" layer="200"/>
<rectangle x1="12.8875" y1="3.8375" x2="13.4625" y2="3.8625" layer="200"/>
<rectangle x1="5.2125" y1="3.8625" x2="5.7625" y2="3.8875" layer="200"/>
<rectangle x1="7.5625" y1="3.8625" x2="8.1375" y2="3.8875" layer="200"/>
<rectangle x1="9.1125" y1="3.8625" x2="9.3625" y2="3.8875" layer="200"/>
<rectangle x1="12.9375" y1="3.8625" x2="13.5125" y2="3.8875" layer="200"/>
<rectangle x1="5.2125" y1="3.8875" x2="5.7875" y2="3.9125" layer="200"/>
<rectangle x1="7.6375" y1="3.8875" x2="8.1625" y2="3.9125" layer="200"/>
<rectangle x1="9.1375" y1="3.8875" x2="9.3625" y2="3.9125" layer="200"/>
<rectangle x1="13.0125" y1="3.8875" x2="13.5875" y2="3.9125" layer="200"/>
<rectangle x1="5.1875" y1="3.9125" x2="5.8125" y2="3.9375" layer="200"/>
<rectangle x1="7.7125" y1="3.9125" x2="8.1875" y2="3.9375" layer="200"/>
<rectangle x1="9.1375" y1="3.9125" x2="9.3875" y2="3.9375" layer="200"/>
<rectangle x1="13.0625" y1="3.9125" x2="13.6375" y2="3.9375" layer="200"/>
<rectangle x1="5.1625" y1="3.9375" x2="5.8375" y2="3.9625" layer="200"/>
<rectangle x1="7.7875" y1="3.9375" x2="8.1875" y2="3.9625" layer="200"/>
<rectangle x1="9.1625" y1="3.9375" x2="9.4125" y2="3.9625" layer="200"/>
<rectangle x1="13.1125" y1="3.9375" x2="13.6875" y2="3.9625" layer="200"/>
<rectangle x1="5.0625" y1="3.9625" x2="5.8625" y2="3.9875" layer="200"/>
<rectangle x1="7.8375" y1="3.9625" x2="8.2125" y2="3.9875" layer="200"/>
<rectangle x1="9.1875" y1="3.9625" x2="9.4375" y2="3.9875" layer="200"/>
<rectangle x1="13.1625" y1="3.9625" x2="13.7375" y2="3.9875" layer="200"/>
<rectangle x1="5.0125" y1="3.9875" x2="5.8625" y2="4.0125" layer="200"/>
<rectangle x1="7.8875" y1="3.9875" x2="8.2375" y2="4.0125" layer="200"/>
<rectangle x1="9.1875" y1="3.9875" x2="9.4375" y2="4.0125" layer="200"/>
<rectangle x1="13.2125" y1="3.9875" x2="13.7875" y2="4.0125" layer="200"/>
<rectangle x1="4.9375" y1="4.0125" x2="5.8625" y2="4.0375" layer="200"/>
<rectangle x1="7.9375" y1="4.0125" x2="8.2375" y2="4.0375" layer="200"/>
<rectangle x1="9.2125" y1="4.0125" x2="9.4625" y2="4.0375" layer="200"/>
<rectangle x1="13.2875" y1="4.0125" x2="13.8375" y2="4.0375" layer="200"/>
<rectangle x1="4.8875" y1="4.0375" x2="5.7625" y2="4.0625" layer="200"/>
<rectangle x1="7.9625" y1="4.0375" x2="8.2625" y2="4.0625" layer="200"/>
<rectangle x1="9.2125" y1="4.0375" x2="9.4875" y2="4.0625" layer="200"/>
<rectangle x1="13.3375" y1="4.0375" x2="13.9125" y2="4.0625" layer="200"/>
<rectangle x1="4.8125" y1="4.0625" x2="5.7125" y2="4.0875" layer="200"/>
<rectangle x1="7.9875" y1="4.0625" x2="8.2875" y2="4.0875" layer="200"/>
<rectangle x1="9.2375" y1="4.0625" x2="9.5125" y2="4.0875" layer="200"/>
<rectangle x1="13.3875" y1="4.0625" x2="13.9875" y2="4.0875" layer="200"/>
<rectangle x1="4.7625" y1="4.0875" x2="5.6375" y2="4.1125" layer="200"/>
<rectangle x1="8.0125" y1="4.0875" x2="8.2875" y2="4.1125" layer="200"/>
<rectangle x1="9.2625" y1="4.0875" x2="9.5125" y2="4.1125" layer="200"/>
<rectangle x1="13.4375" y1="4.0875" x2="14.0375" y2="4.1125" layer="200"/>
<rectangle x1="4.7125" y1="4.1125" x2="5.5625" y2="4.1375" layer="200"/>
<rectangle x1="8.0375" y1="4.1125" x2="8.3125" y2="4.1375" layer="200"/>
<rectangle x1="9.2625" y1="4.1125" x2="9.5375" y2="4.1375" layer="200"/>
<rectangle x1="13.4875" y1="4.1125" x2="14.1125" y2="4.1375" layer="200"/>
<rectangle x1="4.6625" y1="4.1375" x2="5.4875" y2="4.1625" layer="200"/>
<rectangle x1="8.0375" y1="4.1375" x2="8.3375" y2="4.1625" layer="200"/>
<rectangle x1="9.2875" y1="4.1375" x2="9.5375" y2="4.1625" layer="200"/>
<rectangle x1="13.5375" y1="4.1375" x2="14.1625" y2="4.1625" layer="200"/>
<rectangle x1="4.6125" y1="4.1625" x2="5.4125" y2="4.1875" layer="200"/>
<rectangle x1="8.0625" y1="4.1625" x2="8.3375" y2="4.1875" layer="200"/>
<rectangle x1="9.3125" y1="4.1625" x2="9.5625" y2="4.1875" layer="200"/>
<rectangle x1="13.6125" y1="4.1625" x2="14.2125" y2="4.1875" layer="200"/>
<rectangle x1="4.5625" y1="4.1875" x2="5.3375" y2="4.2125" layer="200"/>
<rectangle x1="8.0875" y1="4.1875" x2="8.3625" y2="4.2125" layer="200"/>
<rectangle x1="9.3125" y1="4.1875" x2="9.5875" y2="4.2125" layer="200"/>
<rectangle x1="13.6625" y1="4.1875" x2="14.2625" y2="4.2125" layer="200"/>
<rectangle x1="4.5125" y1="4.2125" x2="5.2625" y2="4.2375" layer="200"/>
<rectangle x1="8.0875" y1="4.2125" x2="8.3875" y2="4.2375" layer="200"/>
<rectangle x1="9.3375" y1="4.2125" x2="9.5875" y2="4.2375" layer="200"/>
<rectangle x1="13.7125" y1="4.2125" x2="14.3125" y2="4.2375" layer="200"/>
<rectangle x1="4.4375" y1="4.2375" x2="5.1875" y2="4.2625" layer="200"/>
<rectangle x1="8.1125" y1="4.2375" x2="8.3875" y2="4.2625" layer="200"/>
<rectangle x1="9.3625" y1="4.2375" x2="9.6125" y2="4.2625" layer="200"/>
<rectangle x1="13.7625" y1="4.2375" x2="14.3875" y2="4.2625" layer="200"/>
<rectangle x1="4.3875" y1="4.2625" x2="5.1125" y2="4.2875" layer="200"/>
<rectangle x1="8.1125" y1="4.2625" x2="8.4125" y2="4.2875" layer="200"/>
<rectangle x1="9.3625" y1="4.2625" x2="9.6375" y2="4.2875" layer="200"/>
<rectangle x1="13.8125" y1="4.2625" x2="14.4375" y2="4.2875" layer="200"/>
<rectangle x1="4.3375" y1="4.2875" x2="5.0375" y2="4.3125" layer="200"/>
<rectangle x1="8.1125" y1="4.2875" x2="8.4375" y2="4.3125" layer="200"/>
<rectangle x1="9.3875" y1="4.2875" x2="9.6375" y2="4.3125" layer="200"/>
<rectangle x1="13.8625" y1="4.2875" x2="14.4875" y2="4.3125" layer="200"/>
<rectangle x1="4.2875" y1="4.3125" x2="4.9625" y2="4.3375" layer="200"/>
<rectangle x1="8.0875" y1="4.3125" x2="8.4625" y2="4.3375" layer="200"/>
<rectangle x1="9.4125" y1="4.3125" x2="9.6625" y2="4.3375" layer="200"/>
<rectangle x1="13.9125" y1="4.3125" x2="14.5375" y2="4.3375" layer="200"/>
<rectangle x1="4.2375" y1="4.3375" x2="4.9125" y2="4.3625" layer="200"/>
<rectangle x1="8.0875" y1="4.3375" x2="8.4625" y2="4.3625" layer="200"/>
<rectangle x1="9.4125" y1="4.3375" x2="9.6875" y2="4.3625" layer="200"/>
<rectangle x1="13.9625" y1="4.3375" x2="14.5875" y2="4.3625" layer="200"/>
<rectangle x1="4.2375" y1="4.3625" x2="4.9375" y2="4.3875" layer="200"/>
<rectangle x1="8.0375" y1="4.3625" x2="8.4875" y2="4.3875" layer="200"/>
<rectangle x1="9.4375" y1="4.3625" x2="9.6875" y2="4.3875" layer="200"/>
<rectangle x1="14.0125" y1="4.3625" x2="14.6375" y2="4.3875" layer="200"/>
<rectangle x1="4.3125" y1="4.3875" x2="4.9625" y2="4.4125" layer="200"/>
<rectangle x1="7.9375" y1="4.3875" x2="8.5125" y2="4.4125" layer="200"/>
<rectangle x1="9.4375" y1="4.3875" x2="9.7125" y2="4.4125" layer="200"/>
<rectangle x1="14.0875" y1="4.3875" x2="14.6875" y2="4.4125" layer="200"/>
<rectangle x1="4.3625" y1="4.4125" x2="4.9875" y2="4.4375" layer="200"/>
<rectangle x1="7.8375" y1="4.4125" x2="8.5375" y2="4.4375" layer="200"/>
<rectangle x1="9.4625" y1="4.4125" x2="9.7375" y2="4.4375" layer="200"/>
<rectangle x1="14.1375" y1="4.4125" x2="14.7375" y2="4.4375" layer="200"/>
<rectangle x1="4.4125" y1="4.4375" x2="5.0125" y2="4.4625" layer="200"/>
<rectangle x1="7.7625" y1="4.4375" x2="8.5375" y2="4.4625" layer="200"/>
<rectangle x1="9.4875" y1="4.4375" x2="9.7625" y2="4.4625" layer="200"/>
<rectangle x1="14.1875" y1="4.4375" x2="14.7875" y2="4.4625" layer="200"/>
<rectangle x1="4.4625" y1="4.4625" x2="5.0375" y2="4.4875" layer="200"/>
<rectangle x1="7.6875" y1="4.4625" x2="8.5625" y2="4.4875" layer="200"/>
<rectangle x1="9.4875" y1="4.4625" x2="9.7625" y2="4.4875" layer="200"/>
<rectangle x1="14.2375" y1="4.4625" x2="14.8625" y2="4.4875" layer="200"/>
<rectangle x1="4.5125" y1="4.4875" x2="5.0625" y2="4.5125" layer="200"/>
<rectangle x1="7.6375" y1="4.4875" x2="8.5875" y2="4.5125" layer="200"/>
<rectangle x1="9.5125" y1="4.4875" x2="9.7875" y2="4.5125" layer="200"/>
<rectangle x1="14.2875" y1="4.4875" x2="14.9125" y2="4.5125" layer="200"/>
<rectangle x1="4.4875" y1="4.5125" x2="5.0875" y2="4.5375" layer="200"/>
<rectangle x1="7.5875" y1="4.5125" x2="8.6125" y2="4.5375" layer="200"/>
<rectangle x1="9.5375" y1="4.5125" x2="9.8125" y2="4.5375" layer="200"/>
<rectangle x1="14.3375" y1="4.5125" x2="14.9625" y2="4.5375" layer="200"/>
<rectangle x1="4.4375" y1="4.5375" x2="5.1125" y2="4.5625" layer="200"/>
<rectangle x1="7.5375" y1="4.5375" x2="8.6375" y2="4.5625" layer="200"/>
<rectangle x1="9.5375" y1="4.5375" x2="9.8125" y2="4.5625" layer="200"/>
<rectangle x1="14.3875" y1="4.5375" x2="15.0125" y2="4.5625" layer="200"/>
<rectangle x1="4.3875" y1="4.5625" x2="5.1125" y2="4.5875" layer="200"/>
<rectangle x1="7.4875" y1="4.5625" x2="8.6625" y2="4.5875" layer="200"/>
<rectangle x1="9.5625" y1="4.5625" x2="9.8375" y2="4.5875" layer="200"/>
<rectangle x1="14.4375" y1="4.5625" x2="15.0625" y2="4.5875" layer="200"/>
<rectangle x1="4.3375" y1="4.5875" x2="5.0625" y2="4.6125" layer="200"/>
<rectangle x1="7.4625" y1="4.5875" x2="8.6875" y2="4.6125" layer="200"/>
<rectangle x1="9.5625" y1="4.5875" x2="9.8625" y2="4.6125" layer="200"/>
<rectangle x1="14.4875" y1="4.5875" x2="15.1125" y2="4.6125" layer="200"/>
<rectangle x1="4.2875" y1="4.6125" x2="5.0375" y2="4.6375" layer="200"/>
<rectangle x1="7.4125" y1="4.6125" x2="8.0875" y2="4.6375" layer="200"/>
<rectangle x1="8.3125" y1="4.6125" x2="8.7125" y2="4.6375" layer="200"/>
<rectangle x1="9.5875" y1="4.6125" x2="9.8625" y2="4.6375" layer="200"/>
<rectangle x1="14.5375" y1="4.6125" x2="15.1625" y2="4.6375" layer="200"/>
<rectangle x1="4.2375" y1="4.6375" x2="4.9875" y2="4.6625" layer="200"/>
<rectangle x1="7.3875" y1="4.6375" x2="7.9875" y2="4.6625" layer="200"/>
<rectangle x1="8.3625" y1="4.6375" x2="8.7375" y2="4.6625" layer="200"/>
<rectangle x1="9.6125" y1="4.6375" x2="9.8875" y2="4.6625" layer="200"/>
<rectangle x1="14.6125" y1="4.6375" x2="15.2125" y2="4.6625" layer="200"/>
<rectangle x1="4.1875" y1="4.6625" x2="4.9625" y2="4.6875" layer="200"/>
<rectangle x1="7.3375" y1="4.6625" x2="7.8875" y2="4.6875" layer="200"/>
<rectangle x1="8.3875" y1="4.6625" x2="8.7375" y2="4.6875" layer="200"/>
<rectangle x1="9.6125" y1="4.6625" x2="9.8875" y2="4.6875" layer="200"/>
<rectangle x1="14.6625" y1="4.6625" x2="15.2625" y2="4.6875" layer="200"/>
<rectangle x1="4.1375" y1="4.6875" x2="4.9125" y2="4.7125" layer="200"/>
<rectangle x1="7.3125" y1="4.6875" x2="7.8125" y2="4.7125" layer="200"/>
<rectangle x1="8.4125" y1="4.6875" x2="8.7625" y2="4.7125" layer="200"/>
<rectangle x1="9.6375" y1="4.6875" x2="9.9125" y2="4.7125" layer="200"/>
<rectangle x1="14.7125" y1="4.6875" x2="15.3125" y2="4.7125" layer="200"/>
<rectangle x1="4.0875" y1="4.7125" x2="4.8875" y2="4.7375" layer="200"/>
<rectangle x1="7.2875" y1="4.7125" x2="7.7375" y2="4.7375" layer="200"/>
<rectangle x1="8.3875" y1="4.7125" x2="8.7875" y2="4.7375" layer="200"/>
<rectangle x1="9.6375" y1="4.7125" x2="9.9375" y2="4.7375" layer="200"/>
<rectangle x1="14.7625" y1="4.7125" x2="15.3625" y2="4.7375" layer="200"/>
<rectangle x1="4.0375" y1="4.7375" x2="4.8375" y2="4.7625" layer="200"/>
<rectangle x1="7.2625" y1="4.7375" x2="7.6875" y2="4.7625" layer="200"/>
<rectangle x1="8.3625" y1="4.7375" x2="8.8125" y2="4.7625" layer="200"/>
<rectangle x1="9.6625" y1="4.7375" x2="9.9375" y2="4.7625" layer="200"/>
<rectangle x1="14.8125" y1="4.7375" x2="15.4125" y2="4.7625" layer="200"/>
<rectangle x1="0.1875" y1="4.7625" x2="0.3875" y2="4.7875" layer="200"/>
<rectangle x1="3.9875" y1="4.7625" x2="4.8125" y2="4.7875" layer="200"/>
<rectangle x1="7.2375" y1="4.7625" x2="7.6375" y2="4.7875" layer="200"/>
<rectangle x1="8.3125" y1="4.7625" x2="8.8375" y2="4.7875" layer="200"/>
<rectangle x1="9.6875" y1="4.7625" x2="9.9625" y2="4.7875" layer="200"/>
<rectangle x1="14.8625" y1="4.7625" x2="15.4625" y2="4.7875" layer="200"/>
<rectangle x1="0.1875" y1="4.7875" x2="0.5125" y2="4.8125" layer="200"/>
<rectangle x1="3.9375" y1="4.7875" x2="4.7625" y2="4.8125" layer="200"/>
<rectangle x1="7.2125" y1="4.7875" x2="7.5875" y2="4.8125" layer="200"/>
<rectangle x1="8.2625" y1="4.7875" x2="8.8625" y2="4.8125" layer="200"/>
<rectangle x1="9.6875" y1="4.7875" x2="9.9625" y2="4.8125" layer="200"/>
<rectangle x1="14.9125" y1="4.7875" x2="15.5125" y2="4.8125" layer="200"/>
<rectangle x1="0.1625" y1="4.8125" x2="0.6625" y2="4.8375" layer="200"/>
<rectangle x1="3.9125" y1="4.8125" x2="4.7125" y2="4.8375" layer="200"/>
<rectangle x1="7.1875" y1="4.8125" x2="7.5375" y2="4.8375" layer="200"/>
<rectangle x1="8.2125" y1="4.8125" x2="8.8875" y2="4.8375" layer="200"/>
<rectangle x1="9.7125" y1="4.8125" x2="9.9875" y2="4.8375" layer="200"/>
<rectangle x1="14.9875" y1="4.8125" x2="15.5625" y2="4.8375" layer="200"/>
<rectangle x1="0.1625" y1="4.8375" x2="0.7875" y2="4.8625" layer="200"/>
<rectangle x1="3.8625" y1="4.8375" x2="4.6625" y2="4.8625" layer="200"/>
<rectangle x1="7.1625" y1="4.8375" x2="7.4875" y2="4.8625" layer="200"/>
<rectangle x1="8.1625" y1="4.8375" x2="8.9125" y2="4.8625" layer="200"/>
<rectangle x1="9.7375" y1="4.8375" x2="10.0125" y2="4.8625" layer="200"/>
<rectangle x1="15.0375" y1="4.8375" x2="15.6125" y2="4.8625" layer="200"/>
<rectangle x1="0.1625" y1="4.8625" x2="0.9125" y2="4.8875" layer="200"/>
<rectangle x1="3.8125" y1="4.8625" x2="4.6375" y2="4.8875" layer="200"/>
<rectangle x1="7.1375" y1="4.8625" x2="7.4625" y2="4.8875" layer="200"/>
<rectangle x1="8.0875" y1="4.8625" x2="8.9375" y2="4.8875" layer="200"/>
<rectangle x1="9.7375" y1="4.8625" x2="10.0125" y2="4.8875" layer="200"/>
<rectangle x1="15.0875" y1="4.8625" x2="15.6625" y2="4.8875" layer="200"/>
<rectangle x1="0.1625" y1="4.8875" x2="1.0375" y2="4.9125" layer="200"/>
<rectangle x1="3.7625" y1="4.8875" x2="4.5875" y2="4.9125" layer="200"/>
<rectangle x1="7.1125" y1="4.8875" x2="7.4375" y2="4.9125" layer="200"/>
<rectangle x1="8.0375" y1="4.8875" x2="8.9625" y2="4.9125" layer="200"/>
<rectangle x1="9.7625" y1="4.8875" x2="10.0375" y2="4.9125" layer="200"/>
<rectangle x1="15.1375" y1="4.8875" x2="15.7125" y2="4.9125" layer="200"/>
<rectangle x1="0.1375" y1="4.9125" x2="1.1625" y2="4.9375" layer="200"/>
<rectangle x1="3.7125" y1="4.9125" x2="4.5375" y2="4.9375" layer="200"/>
<rectangle x1="7.0875" y1="4.9125" x2="7.3875" y2="4.9375" layer="200"/>
<rectangle x1="7.9625" y1="4.9125" x2="8.9875" y2="4.9375" layer="200"/>
<rectangle x1="9.7625" y1="4.9125" x2="10.0625" y2="4.9375" layer="200"/>
<rectangle x1="15.1875" y1="4.9125" x2="15.7625" y2="4.9375" layer="200"/>
<rectangle x1="0.1375" y1="4.9375" x2="1.2875" y2="4.9625" layer="200"/>
<rectangle x1="3.6875" y1="4.9375" x2="4.4875" y2="4.9625" layer="200"/>
<rectangle x1="7.0625" y1="4.9375" x2="7.3625" y2="4.9625" layer="200"/>
<rectangle x1="7.9125" y1="4.9375" x2="8.4125" y2="4.9625" layer="200"/>
<rectangle x1="8.4625" y1="4.9375" x2="9.0125" y2="4.9625" layer="200"/>
<rectangle x1="9.7875" y1="4.9375" x2="10.0875" y2="4.9625" layer="200"/>
<rectangle x1="15.2625" y1="4.9375" x2="15.8125" y2="4.9625" layer="200"/>
<rectangle x1="0.1375" y1="4.9625" x2="1.4125" y2="4.9875" layer="200"/>
<rectangle x1="3.6375" y1="4.9625" x2="4.4375" y2="4.9875" layer="200"/>
<rectangle x1="7.0625" y1="4.9625" x2="7.3375" y2="4.9875" layer="200"/>
<rectangle x1="7.8625" y1="4.9625" x2="8.3375" y2="4.9875" layer="200"/>
<rectangle x1="8.4625" y1="4.9625" x2="9.0375" y2="4.9875" layer="200"/>
<rectangle x1="9.8125" y1="4.9625" x2="10.0875" y2="4.9875" layer="200"/>
<rectangle x1="15.3125" y1="4.9625" x2="15.8625" y2="4.9875" layer="200"/>
<rectangle x1="0.1375" y1="4.9875" x2="1.5375" y2="5.0125" layer="200"/>
<rectangle x1="3.5875" y1="4.9875" x2="4.3875" y2="5.0125" layer="200"/>
<rectangle x1="7.0375" y1="4.9875" x2="7.3125" y2="5.0125" layer="200"/>
<rectangle x1="7.8125" y1="4.9875" x2="8.2625" y2="5.0125" layer="200"/>
<rectangle x1="8.4625" y1="4.9875" x2="9.0625" y2="5.0125" layer="200"/>
<rectangle x1="9.8125" y1="4.9875" x2="10.1125" y2="5.0125" layer="200"/>
<rectangle x1="15.3625" y1="4.9875" x2="15.9125" y2="5.0125" layer="200"/>
<rectangle x1="0.1375" y1="5.0125" x2="1.6625" y2="5.0375" layer="200"/>
<rectangle x1="3.5625" y1="5.0125" x2="4.3375" y2="5.0375" layer="200"/>
<rectangle x1="7.0125" y1="5.0125" x2="7.2875" y2="5.0375" layer="200"/>
<rectangle x1="7.7875" y1="5.0125" x2="8.2125" y2="5.0375" layer="200"/>
<rectangle x1="8.4375" y1="5.0125" x2="9.1125" y2="5.0375" layer="200"/>
<rectangle x1="9.8375" y1="5.0125" x2="10.1375" y2="5.0375" layer="200"/>
<rectangle x1="15.4375" y1="5.0125" x2="15.9625" y2="5.0375" layer="200"/>
<rectangle x1="0.1375" y1="5.0375" x2="1.7625" y2="5.0625" layer="200"/>
<rectangle x1="3.5125" y1="5.0375" x2="4.2875" y2="5.0625" layer="200"/>
<rectangle x1="7.0125" y1="5.0375" x2="7.2625" y2="5.0625" layer="200"/>
<rectangle x1="7.7375" y1="5.0375" x2="8.1375" y2="5.0625" layer="200"/>
<rectangle x1="8.4375" y1="5.0375" x2="9.1375" y2="5.0625" layer="200"/>
<rectangle x1="9.8375" y1="5.0375" x2="10.1375" y2="5.0625" layer="200"/>
<rectangle x1="15.4875" y1="5.0375" x2="16.0125" y2="5.0625" layer="200"/>
<rectangle x1="0.1375" y1="5.0625" x2="1.8875" y2="5.0875" layer="200"/>
<rectangle x1="3.4875" y1="5.0625" x2="4.2375" y2="5.0875" layer="200"/>
<rectangle x1="6.9875" y1="5.0625" x2="7.2625" y2="5.0875" layer="200"/>
<rectangle x1="7.7125" y1="5.0625" x2="8.0875" y2="5.0875" layer="200"/>
<rectangle x1="8.4125" y1="5.0625" x2="9.1875" y2="5.0875" layer="200"/>
<rectangle x1="9.8625" y1="5.0625" x2="10.1625" y2="5.0875" layer="200"/>
<rectangle x1="15.5375" y1="5.0625" x2="16.0625" y2="5.0875" layer="200"/>
<rectangle x1="0.1375" y1="5.0875" x2="2.0125" y2="5.1125" layer="200"/>
<rectangle x1="3.4375" y1="5.0875" x2="4.1875" y2="5.1125" layer="200"/>
<rectangle x1="6.9875" y1="5.0875" x2="7.2375" y2="5.1125" layer="200"/>
<rectangle x1="7.6625" y1="5.0875" x2="8.0375" y2="5.1125" layer="200"/>
<rectangle x1="8.4125" y1="5.0875" x2="8.6875" y2="5.1125" layer="200"/>
<rectangle x1="8.7625" y1="5.0875" x2="9.2875" y2="5.1125" layer="200"/>
<rectangle x1="9.8875" y1="5.0875" x2="10.1625" y2="5.1125" layer="200"/>
<rectangle x1="15.5875" y1="5.0875" x2="16.1125" y2="5.1125" layer="200"/>
<rectangle x1="0.1375" y1="5.1125" x2="2.1875" y2="5.1375" layer="200"/>
<rectangle x1="3.4125" y1="5.1125" x2="4.1375" y2="5.1375" layer="200"/>
<rectangle x1="6.9625" y1="5.1125" x2="7.2125" y2="5.1375" layer="200"/>
<rectangle x1="7.6375" y1="5.1125" x2="7.9875" y2="5.1375" layer="200"/>
<rectangle x1="8.3875" y1="5.1125" x2="8.6875" y2="5.1375" layer="200"/>
<rectangle x1="8.8125" y1="5.1125" x2="9.3875" y2="5.1375" layer="200"/>
<rectangle x1="9.8875" y1="5.1125" x2="10.1875" y2="5.1375" layer="200"/>
<rectangle x1="15.6375" y1="5.1125" x2="16.1625" y2="5.1375" layer="200"/>
<rectangle x1="0.1375" y1="5.1375" x2="2.3875" y2="5.1625" layer="200"/>
<rectangle x1="3.3375" y1="5.1375" x2="4.0875" y2="5.1625" layer="200"/>
<rectangle x1="6.9625" y1="5.1375" x2="7.2125" y2="5.1625" layer="200"/>
<rectangle x1="7.6125" y1="5.1375" x2="7.9375" y2="5.1625" layer="200"/>
<rectangle x1="8.3625" y1="5.1375" x2="8.6625" y2="5.1625" layer="200"/>
<rectangle x1="8.8375" y1="5.1375" x2="9.4875" y2="5.1625" layer="200"/>
<rectangle x1="9.9125" y1="5.1375" x2="10.1875" y2="5.1625" layer="200"/>
<rectangle x1="15.6875" y1="5.1375" x2="16.2125" y2="5.1625" layer="200"/>
<rectangle x1="0.1625" y1="5.1625" x2="2.6875" y2="5.1875" layer="200"/>
<rectangle x1="3.1625" y1="5.1625" x2="4.0375" y2="5.1875" layer="200"/>
<rectangle x1="6.9375" y1="5.1625" x2="7.1875" y2="5.1875" layer="200"/>
<rectangle x1="7.5875" y1="5.1625" x2="7.9125" y2="5.1875" layer="200"/>
<rectangle x1="8.3625" y1="5.1625" x2="8.6375" y2="5.1875" layer="200"/>
<rectangle x1="8.8875" y1="5.1625" x2="9.5625" y2="5.1875" layer="200"/>
<rectangle x1="9.9125" y1="5.1625" x2="10.2125" y2="5.1875" layer="200"/>
<rectangle x1="15.7375" y1="5.1625" x2="16.2625" y2="5.1875" layer="200"/>
<rectangle x1="0.2125" y1="5.1875" x2="3.9625" y2="5.2125" layer="200"/>
<rectangle x1="6.9375" y1="5.1875" x2="7.1875" y2="5.2125" layer="200"/>
<rectangle x1="7.5625" y1="5.1875" x2="7.8625" y2="5.2125" layer="200"/>
<rectangle x1="8.3375" y1="5.1875" x2="8.6375" y2="5.2125" layer="200"/>
<rectangle x1="8.9125" y1="5.1875" x2="9.6625" y2="5.2125" layer="200"/>
<rectangle x1="9.9375" y1="5.1875" x2="10.2125" y2="5.2125" layer="200"/>
<rectangle x1="15.7875" y1="5.1875" x2="16.3125" y2="5.2125" layer="200"/>
<rectangle x1="0.2875" y1="5.2125" x2="1.0875" y2="5.2375" layer="200"/>
<rectangle x1="1.1625" y1="5.2125" x2="3.9125" y2="5.2375" layer="200"/>
<rectangle x1="6.9375" y1="5.2125" x2="7.1875" y2="5.2375" layer="200"/>
<rectangle x1="7.5375" y1="5.2125" x2="7.8375" y2="5.2375" layer="200"/>
<rectangle x1="8.3125" y1="5.2125" x2="8.6125" y2="5.2375" layer="200"/>
<rectangle x1="8.9625" y1="5.2125" x2="9.7625" y2="5.2375" layer="200"/>
<rectangle x1="9.9625" y1="5.2125" x2="10.2375" y2="5.2375" layer="200"/>
<rectangle x1="15.8625" y1="5.2125" x2="16.3625" y2="5.2375" layer="200"/>
<rectangle x1="0.3375" y1="5.2375" x2="1.1125" y2="5.2625" layer="200"/>
<rectangle x1="1.3625" y1="5.2375" x2="3.8625" y2="5.2625" layer="200"/>
<rectangle x1="6.9125" y1="5.2375" x2="7.1625" y2="5.2625" layer="200"/>
<rectangle x1="7.5125" y1="5.2375" x2="7.8125" y2="5.2625" layer="200"/>
<rectangle x1="8.3125" y1="5.2375" x2="8.5875" y2="5.2625" layer="200"/>
<rectangle x1="9.0125" y1="5.2375" x2="10.2625" y2="5.2625" layer="200"/>
<rectangle x1="15.9125" y1="5.2375" x2="16.4125" y2="5.2625" layer="200"/>
<rectangle x1="0.4125" y1="5.2625" x2="1.1875" y2="5.2875" layer="200"/>
<rectangle x1="1.5375" y1="5.2625" x2="3.7875" y2="5.2875" layer="200"/>
<rectangle x1="6.9125" y1="5.2625" x2="7.1625" y2="5.2875" layer="200"/>
<rectangle x1="7.4875" y1="5.2625" x2="7.7875" y2="5.2875" layer="200"/>
<rectangle x1="8.2875" y1="5.2625" x2="8.5625" y2="5.2875" layer="200"/>
<rectangle x1="9.0875" y1="5.2625" x2="10.2875" y2="5.2875" layer="200"/>
<rectangle x1="15.9625" y1="5.2625" x2="16.4625" y2="5.2875" layer="200"/>
<rectangle x1="0.4875" y1="5.2875" x2="1.2375" y2="5.3125" layer="200"/>
<rectangle x1="1.6625" y1="5.2875" x2="3.7375" y2="5.3125" layer="200"/>
<rectangle x1="6.9125" y1="5.2875" x2="7.1375" y2="5.3125" layer="200"/>
<rectangle x1="7.4625" y1="5.2875" x2="7.7625" y2="5.3125" layer="200"/>
<rectangle x1="8.2625" y1="5.2875" x2="8.5375" y2="5.3125" layer="200"/>
<rectangle x1="9.1625" y1="5.2875" x2="10.3125" y2="5.3125" layer="200"/>
<rectangle x1="16.0125" y1="5.2875" x2="16.5125" y2="5.3125" layer="200"/>
<rectangle x1="0.5625" y1="5.3125" x2="1.3125" y2="5.3375" layer="200"/>
<rectangle x1="1.8375" y1="5.3125" x2="3.6625" y2="5.3375" layer="200"/>
<rectangle x1="6.9125" y1="5.3125" x2="7.1375" y2="5.3375" layer="200"/>
<rectangle x1="7.4375" y1="5.3125" x2="7.7375" y2="5.3375" layer="200"/>
<rectangle x1="8.2625" y1="5.3125" x2="8.5375" y2="5.3375" layer="200"/>
<rectangle x1="9.2375" y1="5.3125" x2="10.3375" y2="5.3375" layer="200"/>
<rectangle x1="16.0625" y1="5.3125" x2="16.5625" y2="5.3375" layer="200"/>
<rectangle x1="0.6375" y1="5.3375" x2="1.3875" y2="5.3625" layer="200"/>
<rectangle x1="2.0125" y1="5.3375" x2="3.5875" y2="5.3625" layer="200"/>
<rectangle x1="6.8875" y1="5.3375" x2="7.1375" y2="5.3625" layer="200"/>
<rectangle x1="7.4125" y1="5.3375" x2="7.7125" y2="5.3625" layer="200"/>
<rectangle x1="8.2375" y1="5.3375" x2="8.5125" y2="5.3625" layer="200"/>
<rectangle x1="9.3625" y1="5.3375" x2="10.3875" y2="5.3625" layer="200"/>
<rectangle x1="16.1125" y1="5.3375" x2="16.6125" y2="5.3625" layer="200"/>
<rectangle x1="0.7125" y1="5.3625" x2="1.4625" y2="5.3875" layer="200"/>
<rectangle x1="2.2125" y1="5.3625" x2="3.4625" y2="5.3875" layer="200"/>
<rectangle x1="6.8875" y1="5.3625" x2="7.1375" y2="5.3875" layer="200"/>
<rectangle x1="7.3875" y1="5.3625" x2="7.6875" y2="5.3875" layer="200"/>
<rectangle x1="8.2125" y1="5.3625" x2="8.5125" y2="5.3875" layer="200"/>
<rectangle x1="9.4625" y1="5.3625" x2="10.4625" y2="5.3875" layer="200"/>
<rectangle x1="16.1625" y1="5.3625" x2="16.6625" y2="5.3875" layer="200"/>
<rectangle x1="0.7875" y1="5.3875" x2="1.5375" y2="5.4125" layer="200"/>
<rectangle x1="2.5375" y1="5.3875" x2="3.2625" y2="5.4125" layer="200"/>
<rectangle x1="6.8875" y1="5.3875" x2="7.1125" y2="5.4125" layer="200"/>
<rectangle x1="7.3875" y1="5.3875" x2="7.6625" y2="5.4125" layer="200"/>
<rectangle x1="8.2125" y1="5.3875" x2="8.4875" y2="5.4125" layer="200"/>
<rectangle x1="9.5625" y1="5.3875" x2="10.5125" y2="5.4125" layer="200"/>
<rectangle x1="16.2125" y1="5.3875" x2="16.7125" y2="5.4125" layer="200"/>
<rectangle x1="0.8625" y1="5.4125" x2="1.6125" y2="5.4375" layer="200"/>
<rectangle x1="6.8875" y1="5.4125" x2="7.1125" y2="5.4375" layer="200"/>
<rectangle x1="7.3625" y1="5.4125" x2="7.6375" y2="5.4375" layer="200"/>
<rectangle x1="8.1875" y1="5.4125" x2="8.4625" y2="5.4375" layer="200"/>
<rectangle x1="9.6375" y1="5.4125" x2="10.5875" y2="5.4375" layer="200"/>
<rectangle x1="16.2625" y1="5.4125" x2="16.7625" y2="5.4375" layer="200"/>
<rectangle x1="0.9375" y1="5.4375" x2="1.6625" y2="5.4625" layer="200"/>
<rectangle x1="6.8875" y1="5.4375" x2="7.1125" y2="5.4625" layer="200"/>
<rectangle x1="7.3375" y1="5.4375" x2="7.6125" y2="5.4625" layer="200"/>
<rectangle x1="8.1625" y1="5.4375" x2="8.4625" y2="5.4625" layer="200"/>
<rectangle x1="9.6875" y1="5.4375" x2="10.6875" y2="5.4625" layer="200"/>
<rectangle x1="16.3125" y1="5.4375" x2="16.8125" y2="5.4625" layer="200"/>
<rectangle x1="1.0125" y1="5.4625" x2="1.7375" y2="5.4875" layer="200"/>
<rectangle x1="6.8875" y1="5.4625" x2="7.1125" y2="5.4875" layer="200"/>
<rectangle x1="7.3375" y1="5.4625" x2="7.6125" y2="5.4875" layer="200"/>
<rectangle x1="8.1625" y1="5.4625" x2="8.4375" y2="5.4875" layer="200"/>
<rectangle x1="9.7875" y1="5.4625" x2="10.7625" y2="5.4875" layer="200"/>
<rectangle x1="16.3375" y1="5.4625" x2="16.8625" y2="5.4875" layer="200"/>
<rectangle x1="1.0875" y1="5.4875" x2="1.8125" y2="5.5125" layer="200"/>
<rectangle x1="6.8875" y1="5.4875" x2="7.1125" y2="5.5125" layer="200"/>
<rectangle x1="7.3125" y1="5.4875" x2="7.5875" y2="5.5125" layer="200"/>
<rectangle x1="8.1375" y1="5.4875" x2="8.4375" y2="5.5125" layer="200"/>
<rectangle x1="9.8875" y1="5.4875" x2="10.8625" y2="5.5125" layer="200"/>
<rectangle x1="16.3875" y1="5.4875" x2="16.9125" y2="5.5125" layer="200"/>
<rectangle x1="1.1375" y1="5.5125" x2="1.8875" y2="5.5375" layer="200"/>
<rectangle x1="6.8875" y1="5.5125" x2="7.1125" y2="5.5375" layer="200"/>
<rectangle x1="7.2875" y1="5.5125" x2="7.5875" y2="5.5375" layer="200"/>
<rectangle x1="8.1125" y1="5.5125" x2="8.4125" y2="5.5375" layer="200"/>
<rectangle x1="9.9875" y1="5.5125" x2="10.9625" y2="5.5375" layer="200"/>
<rectangle x1="16.4375" y1="5.5125" x2="16.9375" y2="5.5375" layer="200"/>
<rectangle x1="1.2125" y1="5.5375" x2="1.9625" y2="5.5625" layer="200"/>
<rectangle x1="6.8875" y1="5.5375" x2="7.1125" y2="5.5625" layer="200"/>
<rectangle x1="7.2875" y1="5.5375" x2="7.5625" y2="5.5625" layer="200"/>
<rectangle x1="8.0875" y1="5.5375" x2="8.3875" y2="5.5625" layer="200"/>
<rectangle x1="10.1125" y1="5.5375" x2="11.0625" y2="5.5625" layer="200"/>
<rectangle x1="16.4875" y1="5.5375" x2="16.9875" y2="5.5625" layer="200"/>
<rectangle x1="1.2875" y1="5.5625" x2="2.0375" y2="5.5875" layer="200"/>
<rectangle x1="6.8875" y1="5.5625" x2="7.1125" y2="5.5875" layer="200"/>
<rectangle x1="7.2875" y1="5.5625" x2="7.5375" y2="5.5875" layer="200"/>
<rectangle x1="8.0875" y1="5.5625" x2="8.3875" y2="5.5875" layer="200"/>
<rectangle x1="10.2375" y1="5.5625" x2="11.1625" y2="5.5875" layer="200"/>
<rectangle x1="16.5375" y1="5.5625" x2="17.0375" y2="5.5875" layer="200"/>
<rectangle x1="1.3625" y1="5.5875" x2="2.1125" y2="5.6125" layer="200"/>
<rectangle x1="6.8625" y1="5.5875" x2="7.1125" y2="5.6125" layer="200"/>
<rectangle x1="7.2625" y1="5.5875" x2="7.5375" y2="5.6125" layer="200"/>
<rectangle x1="8.0625" y1="5.5875" x2="8.3625" y2="5.6125" layer="200"/>
<rectangle x1="10.3625" y1="5.5875" x2="11.2875" y2="5.6125" layer="200"/>
<rectangle x1="16.5875" y1="5.5875" x2="17.0875" y2="5.6125" layer="200"/>
<rectangle x1="1.4125" y1="5.6125" x2="2.1875" y2="5.6375" layer="200"/>
<rectangle x1="6.8625" y1="5.6125" x2="7.1125" y2="5.6375" layer="200"/>
<rectangle x1="7.2625" y1="5.6125" x2="7.5125" y2="5.6375" layer="200"/>
<rectangle x1="8.0375" y1="5.6125" x2="8.3625" y2="5.6375" layer="200"/>
<rectangle x1="10.4625" y1="5.6125" x2="11.3875" y2="5.6375" layer="200"/>
<rectangle x1="16.6375" y1="5.6125" x2="17.1375" y2="5.6375" layer="200"/>
<rectangle x1="1.4875" y1="5.6375" x2="2.2375" y2="5.6625" layer="200"/>
<rectangle x1="6.8625" y1="5.6375" x2="7.1125" y2="5.6625" layer="200"/>
<rectangle x1="7.2375" y1="5.6375" x2="7.5125" y2="5.6625" layer="200"/>
<rectangle x1="8.0375" y1="5.6375" x2="8.3375" y2="5.6625" layer="200"/>
<rectangle x1="10.5625" y1="5.6375" x2="11.4875" y2="5.6625" layer="200"/>
<rectangle x1="16.6875" y1="5.6375" x2="17.1875" y2="5.6625" layer="200"/>
<rectangle x1="1.5375" y1="5.6625" x2="2.3125" y2="5.6875" layer="200"/>
<rectangle x1="6.8625" y1="5.6625" x2="7.1125" y2="5.6875" layer="200"/>
<rectangle x1="7.2375" y1="5.6625" x2="7.5125" y2="5.6875" layer="200"/>
<rectangle x1="8.0125" y1="5.6625" x2="8.3125" y2="5.6875" layer="200"/>
<rectangle x1="10.6625" y1="5.6625" x2="11.5875" y2="5.6875" layer="200"/>
<rectangle x1="16.7375" y1="5.6625" x2="17.2125" y2="5.6875" layer="200"/>
<rectangle x1="1.5875" y1="5.6875" x2="2.3875" y2="5.7125" layer="200"/>
<rectangle x1="6.8625" y1="5.6875" x2="7.1125" y2="5.7125" layer="200"/>
<rectangle x1="7.2375" y1="5.6875" x2="7.4875" y2="5.7125" layer="200"/>
<rectangle x1="7.9875" y1="5.6875" x2="8.3125" y2="5.7125" layer="200"/>
<rectangle x1="10.7625" y1="5.6875" x2="11.6875" y2="5.7125" layer="200"/>
<rectangle x1="16.7875" y1="5.6875" x2="17.2625" y2="5.7125" layer="200"/>
<rectangle x1="1.6375" y1="5.7125" x2="2.4375" y2="5.7375" layer="200"/>
<rectangle x1="6.8625" y1="5.7125" x2="7.1125" y2="5.7375" layer="200"/>
<rectangle x1="7.2125" y1="5.7125" x2="7.4875" y2="5.7375" layer="200"/>
<rectangle x1="7.9625" y1="5.7125" x2="8.2875" y2="5.7375" layer="200"/>
<rectangle x1="10.8375" y1="5.7125" x2="11.7875" y2="5.7375" layer="200"/>
<rectangle x1="16.8375" y1="5.7125" x2="17.2875" y2="5.7375" layer="200"/>
<rectangle x1="1.7125" y1="5.7375" x2="2.4875" y2="5.7625" layer="200"/>
<rectangle x1="6.8625" y1="5.7375" x2="7.1125" y2="5.7625" layer="200"/>
<rectangle x1="7.1875" y1="5.7375" x2="7.4875" y2="5.7625" layer="200"/>
<rectangle x1="7.9375" y1="5.7375" x2="8.2625" y2="5.7625" layer="200"/>
<rectangle x1="10.9375" y1="5.7375" x2="11.8875" y2="5.7625" layer="200"/>
<rectangle x1="16.8625" y1="5.7375" x2="17.3375" y2="5.7625" layer="200"/>
<rectangle x1="1.7625" y1="5.7625" x2="2.5625" y2="5.7875" layer="200"/>
<rectangle x1="6.8375" y1="5.7625" x2="7.1375" y2="5.7875" layer="200"/>
<rectangle x1="7.1875" y1="5.7625" x2="7.4875" y2="5.7875" layer="200"/>
<rectangle x1="7.9375" y1="5.7625" x2="8.2625" y2="5.7875" layer="200"/>
<rectangle x1="11.0375" y1="5.7625" x2="11.9875" y2="5.7875" layer="200"/>
<rectangle x1="16.9125" y1="5.7625" x2="17.3875" y2="5.7875" layer="200"/>
<rectangle x1="1.8375" y1="5.7875" x2="2.6125" y2="5.8125" layer="200"/>
<rectangle x1="6.8375" y1="5.7875" x2="7.4875" y2="5.8125" layer="200"/>
<rectangle x1="7.9125" y1="5.7875" x2="8.2375" y2="5.8125" layer="200"/>
<rectangle x1="11.1375" y1="5.7875" x2="12.0875" y2="5.8125" layer="200"/>
<rectangle x1="16.9625" y1="5.7875" x2="17.4375" y2="5.8125" layer="200"/>
<rectangle x1="1.9125" y1="5.8125" x2="2.6625" y2="5.8375" layer="200"/>
<rectangle x1="6.8375" y1="5.8125" x2="7.4875" y2="5.8375" layer="200"/>
<rectangle x1="7.8625" y1="5.8125" x2="8.2125" y2="5.8375" layer="200"/>
<rectangle x1="11.2375" y1="5.8125" x2="12.1875" y2="5.8375" layer="200"/>
<rectangle x1="17.0125" y1="5.8125" x2="17.4875" y2="5.8375" layer="200"/>
<rectangle x1="1.9875" y1="5.8375" x2="2.7125" y2="5.8625" layer="200"/>
<rectangle x1="6.8125" y1="5.8375" x2="7.5125" y2="5.8625" layer="200"/>
<rectangle x1="7.8375" y1="5.8375" x2="8.1875" y2="5.8625" layer="200"/>
<rectangle x1="11.3625" y1="5.8375" x2="12.2875" y2="5.8625" layer="200"/>
<rectangle x1="17.0625" y1="5.8375" x2="17.5375" y2="5.8625" layer="200"/>
<rectangle x1="2.0625" y1="5.8625" x2="2.7625" y2="5.8875" layer="200"/>
<rectangle x1="6.8125" y1="5.8625" x2="7.5375" y2="5.8875" layer="200"/>
<rectangle x1="7.7875" y1="5.8625" x2="8.1625" y2="5.8875" layer="200"/>
<rectangle x1="11.4625" y1="5.8625" x2="12.3875" y2="5.8875" layer="200"/>
<rectangle x1="17.1125" y1="5.8625" x2="17.5875" y2="5.8875" layer="200"/>
<rectangle x1="2.1375" y1="5.8875" x2="2.8125" y2="5.9125" layer="200"/>
<rectangle x1="6.7875" y1="5.8875" x2="8.1625" y2="5.9125" layer="200"/>
<rectangle x1="11.5875" y1="5.8875" x2="12.4875" y2="5.9125" layer="200"/>
<rectangle x1="17.1375" y1="5.8875" x2="17.6375" y2="5.9125" layer="200"/>
<rectangle x1="2.2125" y1="5.9125" x2="2.8375" y2="5.9375" layer="200"/>
<rectangle x1="6.7875" y1="5.9125" x2="8.1875" y2="5.9375" layer="200"/>
<rectangle x1="11.6875" y1="5.9125" x2="12.5875" y2="5.9375" layer="200"/>
<rectangle x1="17.1875" y1="5.9125" x2="17.6875" y2="5.9375" layer="200"/>
<rectangle x1="2.2875" y1="5.9375" x2="2.8875" y2="5.9625" layer="200"/>
<rectangle x1="6.7625" y1="5.9375" x2="8.2375" y2="5.9625" layer="200"/>
<rectangle x1="11.7875" y1="5.9375" x2="12.6875" y2="5.9625" layer="200"/>
<rectangle x1="17.2375" y1="5.9375" x2="17.7125" y2="5.9625" layer="200"/>
<rectangle x1="2.3375" y1="5.9625" x2="2.9375" y2="5.9875" layer="200"/>
<rectangle x1="6.7375" y1="5.9625" x2="7.0875" y2="5.9875" layer="200"/>
<rectangle x1="7.1375" y1="5.9625" x2="8.3375" y2="5.9875" layer="200"/>
<rectangle x1="11.8875" y1="5.9625" x2="12.7875" y2="5.9875" layer="200"/>
<rectangle x1="17.2875" y1="5.9625" x2="17.7625" y2="5.9875" layer="200"/>
<rectangle x1="2.4125" y1="5.9875" x2="2.9875" y2="6.0125" layer="200"/>
<rectangle x1="6.7375" y1="5.9875" x2="7.0125" y2="6.0125" layer="200"/>
<rectangle x1="7.3125" y1="5.9875" x2="8.4625" y2="6.0125" layer="200"/>
<rectangle x1="11.9875" y1="5.9875" x2="12.8875" y2="6.0125" layer="200"/>
<rectangle x1="17.3375" y1="5.9875" x2="17.8125" y2="6.0125" layer="200"/>
<rectangle x1="2.4625" y1="6.0125" x2="3.0125" y2="6.0375" layer="200"/>
<rectangle x1="6.7125" y1="6.0125" x2="6.9875" y2="6.0375" layer="200"/>
<rectangle x1="7.4625" y1="6.0125" x2="8.5625" y2="6.0375" layer="200"/>
<rectangle x1="12.0875" y1="6.0125" x2="12.9875" y2="6.0375" layer="200"/>
<rectangle x1="17.3625" y1="6.0125" x2="17.8375" y2="6.0375" layer="200"/>
<rectangle x1="2.5125" y1="6.0375" x2="3.0625" y2="6.0625" layer="200"/>
<rectangle x1="6.7125" y1="6.0375" x2="6.9625" y2="6.0625" layer="200"/>
<rectangle x1="7.6375" y1="6.0375" x2="8.6625" y2="6.0625" layer="200"/>
<rectangle x1="12.1875" y1="6.0375" x2="13.0875" y2="6.0625" layer="200"/>
<rectangle x1="17.4125" y1="6.0375" x2="17.8875" y2="6.0625" layer="200"/>
<rectangle x1="2.5625" y1="6.0625" x2="3.0875" y2="6.0875" layer="200"/>
<rectangle x1="6.6875" y1="6.0625" x2="6.9375" y2="6.0875" layer="200"/>
<rectangle x1="7.8125" y1="6.0625" x2="8.7375" y2="6.0875" layer="200"/>
<rectangle x1="12.2875" y1="6.0625" x2="13.1875" y2="6.0875" layer="200"/>
<rectangle x1="17.4625" y1="6.0625" x2="17.9125" y2="6.0875" layer="200"/>
<rectangle x1="2.6125" y1="6.0875" x2="3.1375" y2="6.1125" layer="200"/>
<rectangle x1="6.6875" y1="6.0875" x2="6.9375" y2="6.1125" layer="200"/>
<rectangle x1="7.9875" y1="6.0875" x2="8.8125" y2="6.1125" layer="200"/>
<rectangle x1="12.3625" y1="6.0875" x2="13.2875" y2="6.1125" layer="200"/>
<rectangle x1="17.4875" y1="6.0875" x2="17.9375" y2="6.1125" layer="200"/>
<rectangle x1="2.6875" y1="6.1125" x2="3.1875" y2="6.1375" layer="200"/>
<rectangle x1="6.6625" y1="6.1125" x2="6.9125" y2="6.1375" layer="200"/>
<rectangle x1="8.1875" y1="6.1125" x2="8.8875" y2="6.1375" layer="200"/>
<rectangle x1="12.4625" y1="6.1125" x2="13.3875" y2="6.1375" layer="200"/>
<rectangle x1="17.5375" y1="6.1125" x2="17.9875" y2="6.1375" layer="200"/>
<rectangle x1="2.7375" y1="6.1375" x2="3.2125" y2="6.1625" layer="200"/>
<rectangle x1="6.6625" y1="6.1375" x2="6.9125" y2="6.1625" layer="200"/>
<rectangle x1="8.3625" y1="6.1375" x2="8.9375" y2="6.1625" layer="200"/>
<rectangle x1="12.5625" y1="6.1375" x2="13.4875" y2="6.1625" layer="200"/>
<rectangle x1="17.5875" y1="6.1375" x2="18.0125" y2="6.1625" layer="200"/>
<rectangle x1="2.7625" y1="6.1625" x2="3.2625" y2="6.1875" layer="200"/>
<rectangle x1="6.6375" y1="6.1625" x2="6.9125" y2="6.1875" layer="200"/>
<rectangle x1="8.4875" y1="6.1625" x2="9.0125" y2="6.1875" layer="200"/>
<rectangle x1="12.6875" y1="6.1625" x2="13.5875" y2="6.1875" layer="200"/>
<rectangle x1="17.6375" y1="6.1625" x2="18.0625" y2="6.1875" layer="200"/>
<rectangle x1="2.8125" y1="6.1875" x2="3.2875" y2="6.2125" layer="200"/>
<rectangle x1="6.6375" y1="6.1875" x2="6.8875" y2="6.2125" layer="200"/>
<rectangle x1="8.5875" y1="6.1875" x2="9.0625" y2="6.2125" layer="200"/>
<rectangle x1="12.7875" y1="6.1875" x2="13.6875" y2="6.2125" layer="200"/>
<rectangle x1="17.6625" y1="6.1875" x2="18.0875" y2="6.2125" layer="200"/>
<rectangle x1="2.8625" y1="6.2125" x2="3.3125" y2="6.2375" layer="200"/>
<rectangle x1="6.6375" y1="6.2125" x2="6.8875" y2="6.2375" layer="200"/>
<rectangle x1="8.6875" y1="6.2125" x2="9.1125" y2="6.2375" layer="200"/>
<rectangle x1="12.8875" y1="6.2125" x2="13.8125" y2="6.2375" layer="200"/>
<rectangle x1="17.7125" y1="6.2125" x2="18.1375" y2="6.2375" layer="200"/>
<rectangle x1="2.9125" y1="6.2375" x2="3.3625" y2="6.2625" layer="200"/>
<rectangle x1="6.6375" y1="6.2375" x2="6.8875" y2="6.2625" layer="200"/>
<rectangle x1="8.7375" y1="6.2375" x2="9.1625" y2="6.2625" layer="200"/>
<rectangle x1="12.9875" y1="6.2375" x2="13.9125" y2="6.2625" layer="200"/>
<rectangle x1="17.7375" y1="6.2375" x2="18.1625" y2="6.2625" layer="200"/>
<rectangle x1="2.9625" y1="6.2625" x2="3.3875" y2="6.2875" layer="200"/>
<rectangle x1="6.6375" y1="6.2625" x2="6.8875" y2="6.2875" layer="200"/>
<rectangle x1="8.8125" y1="6.2625" x2="9.2125" y2="6.2875" layer="200"/>
<rectangle x1="13.0875" y1="6.2625" x2="14.0125" y2="6.2875" layer="200"/>
<rectangle x1="17.7875" y1="6.2625" x2="18.2125" y2="6.2875" layer="200"/>
<rectangle x1="2.9875" y1="6.2875" x2="3.4375" y2="6.3125" layer="200"/>
<rectangle x1="6.6125" y1="6.2875" x2="6.8875" y2="6.3125" layer="200"/>
<rectangle x1="8.8625" y1="6.2875" x2="9.2625" y2="6.3125" layer="200"/>
<rectangle x1="11.3875" y1="6.2875" x2="11.5375" y2="6.3125" layer="200"/>
<rectangle x1="13.2125" y1="6.2875" x2="14.1125" y2="6.3125" layer="200"/>
<rectangle x1="17.8125" y1="6.2875" x2="18.2375" y2="6.3125" layer="200"/>
<rectangle x1="3.0375" y1="6.3125" x2="3.4625" y2="6.3375" layer="200"/>
<rectangle x1="6.6125" y1="6.3125" x2="6.9125" y2="6.3375" layer="200"/>
<rectangle x1="8.9125" y1="6.3125" x2="9.3125" y2="6.3375" layer="200"/>
<rectangle x1="11.3375" y1="6.3125" x2="11.6875" y2="6.3375" layer="200"/>
<rectangle x1="13.3125" y1="6.3125" x2="14.2125" y2="6.3375" layer="200"/>
<rectangle x1="17.8625" y1="6.3125" x2="18.2875" y2="6.3375" layer="200"/>
<rectangle x1="3.0875" y1="6.3375" x2="3.5125" y2="6.3625" layer="200"/>
<rectangle x1="6.6125" y1="6.3375" x2="6.9125" y2="6.3625" layer="200"/>
<rectangle x1="8.9625" y1="6.3375" x2="9.3625" y2="6.3625" layer="200"/>
<rectangle x1="11.3125" y1="6.3375" x2="11.7625" y2="6.3625" layer="200"/>
<rectangle x1="13.4125" y1="6.3375" x2="14.3125" y2="6.3625" layer="200"/>
<rectangle x1="17.8875" y1="6.3375" x2="18.3125" y2="6.3625" layer="200"/>
<rectangle x1="3.1125" y1="6.3625" x2="3.5375" y2="6.3875" layer="200"/>
<rectangle x1="6.6125" y1="6.3625" x2="6.9375" y2="6.3875" layer="200"/>
<rectangle x1="9.0125" y1="6.3625" x2="9.4125" y2="6.3875" layer="200"/>
<rectangle x1="11.2875" y1="6.3625" x2="11.8125" y2="6.3875" layer="200"/>
<rectangle x1="13.5125" y1="6.3625" x2="14.4125" y2="6.3875" layer="200"/>
<rectangle x1="17.9125" y1="6.3625" x2="18.3625" y2="6.3875" layer="200"/>
<rectangle x1="3.1625" y1="6.3875" x2="3.5625" y2="6.4125" layer="200"/>
<rectangle x1="6.5875" y1="6.3875" x2="6.9875" y2="6.4125" layer="200"/>
<rectangle x1="9.0625" y1="6.3875" x2="9.4625" y2="6.4125" layer="200"/>
<rectangle x1="11.2625" y1="6.3875" x2="11.8875" y2="6.4125" layer="200"/>
<rectangle x1="13.6375" y1="6.3875" x2="14.5125" y2="6.4125" layer="200"/>
<rectangle x1="17.9375" y1="6.3875" x2="18.3875" y2="6.4125" layer="200"/>
<rectangle x1="3.1875" y1="6.4125" x2="3.6125" y2="6.4375" layer="200"/>
<rectangle x1="6.5875" y1="6.4125" x2="7.0875" y2="6.4375" layer="200"/>
<rectangle x1="9.1375" y1="6.4125" x2="9.5125" y2="6.4375" layer="200"/>
<rectangle x1="11.2375" y1="6.4125" x2="11.9875" y2="6.4375" layer="200"/>
<rectangle x1="13.7375" y1="6.4125" x2="14.6125" y2="6.4375" layer="200"/>
<rectangle x1="18.0125" y1="6.4125" x2="18.4375" y2="6.4375" layer="200"/>
<rectangle x1="3.2375" y1="6.4375" x2="3.6375" y2="6.4625" layer="200"/>
<rectangle x1="6.5625" y1="6.4375" x2="7.1625" y2="6.4625" layer="200"/>
<rectangle x1="9.1875" y1="6.4375" x2="9.5375" y2="6.4625" layer="200"/>
<rectangle x1="11.2125" y1="6.4375" x2="12.0875" y2="6.4625" layer="200"/>
<rectangle x1="13.8625" y1="6.4375" x2="14.7125" y2="6.4625" layer="200"/>
<rectangle x1="18.0375" y1="6.4375" x2="18.4625" y2="6.4625" layer="200"/>
<rectangle x1="3.2625" y1="6.4625" x2="3.6625" y2="6.4875" layer="200"/>
<rectangle x1="6.5625" y1="6.4625" x2="7.2125" y2="6.4875" layer="200"/>
<rectangle x1="9.2375" y1="6.4625" x2="9.5875" y2="6.4875" layer="200"/>
<rectangle x1="11.2125" y1="6.4625" x2="12.1875" y2="6.4875" layer="200"/>
<rectangle x1="13.9625" y1="6.4625" x2="14.8125" y2="6.4875" layer="200"/>
<rectangle x1="18.0875" y1="6.4625" x2="18.4875" y2="6.4875" layer="200"/>
<rectangle x1="3.2875" y1="6.4875" x2="3.7125" y2="6.5125" layer="200"/>
<rectangle x1="6.5375" y1="6.4875" x2="7.2875" y2="6.5125" layer="200"/>
<rectangle x1="9.2875" y1="6.4875" x2="9.6375" y2="6.5125" layer="200"/>
<rectangle x1="11.1875" y1="6.4875" x2="12.3125" y2="6.5125" layer="200"/>
<rectangle x1="14.0625" y1="6.4875" x2="14.9125" y2="6.5125" layer="200"/>
<rectangle x1="18.1375" y1="6.4875" x2="18.5375" y2="6.5125" layer="200"/>
<rectangle x1="3.3375" y1="6.5125" x2="3.7375" y2="6.5375" layer="200"/>
<rectangle x1="6.5375" y1="6.5125" x2="7.3375" y2="6.5375" layer="200"/>
<rectangle x1="9.3375" y1="6.5125" x2="9.6875" y2="6.5375" layer="200"/>
<rectangle x1="11.1875" y1="6.5125" x2="12.4375" y2="6.5375" layer="200"/>
<rectangle x1="14.1875" y1="6.5125" x2="15.0125" y2="6.5375" layer="200"/>
<rectangle x1="18.1625" y1="6.5125" x2="18.5625" y2="6.5375" layer="200"/>
<rectangle x1="3.3625" y1="6.5375" x2="3.7875" y2="6.5625" layer="200"/>
<rectangle x1="6.5125" y1="6.5375" x2="7.4125" y2="6.5625" layer="200"/>
<rectangle x1="9.3875" y1="6.5375" x2="9.7125" y2="6.5625" layer="200"/>
<rectangle x1="11.2375" y1="6.5375" x2="12.5625" y2="6.5625" layer="200"/>
<rectangle x1="14.2875" y1="6.5375" x2="15.1125" y2="6.5625" layer="200"/>
<rectangle x1="18.1875" y1="6.5375" x2="18.6125" y2="6.5625" layer="200"/>
<rectangle x1="3.3875" y1="6.5625" x2="3.8125" y2="6.5875" layer="200"/>
<rectangle x1="6.5125" y1="6.5625" x2="7.4625" y2="6.5875" layer="200"/>
<rectangle x1="9.4375" y1="6.5625" x2="9.7625" y2="6.5875" layer="200"/>
<rectangle x1="11.2875" y1="6.5625" x2="12.6875" y2="6.5875" layer="200"/>
<rectangle x1="14.3875" y1="6.5625" x2="15.2125" y2="6.5875" layer="200"/>
<rectangle x1="18.2375" y1="6.5625" x2="18.6375" y2="6.5875" layer="200"/>
<rectangle x1="3.4125" y1="6.5875" x2="3.8375" y2="6.6125" layer="200"/>
<rectangle x1="6.4875" y1="6.5875" x2="7.5125" y2="6.6125" layer="200"/>
<rectangle x1="9.4625" y1="6.5875" x2="9.8125" y2="6.6125" layer="200"/>
<rectangle x1="11.3625" y1="6.5875" x2="12.8125" y2="6.6125" layer="200"/>
<rectangle x1="14.4875" y1="6.5875" x2="15.3125" y2="6.6125" layer="200"/>
<rectangle x1="18.2625" y1="6.5875" x2="18.6625" y2="6.6125" layer="200"/>
<rectangle x1="3.4375" y1="6.6125" x2="3.8875" y2="6.6375" layer="200"/>
<rectangle x1="6.4625" y1="6.6125" x2="6.8875" y2="6.6375" layer="200"/>
<rectangle x1="6.9375" y1="6.6125" x2="7.5625" y2="6.6375" layer="200"/>
<rectangle x1="9.5125" y1="6.6125" x2="9.8625" y2="6.6375" layer="200"/>
<rectangle x1="11.4375" y1="6.6125" x2="12.8625" y2="6.6375" layer="200"/>
<rectangle x1="14.5875" y1="6.6125" x2="15.3875" y2="6.6375" layer="200"/>
<rectangle x1="18.2875" y1="6.6125" x2="18.7125" y2="6.6375" layer="200"/>
<rectangle x1="3.4625" y1="6.6375" x2="3.9125" y2="6.6625" layer="200"/>
<rectangle x1="6.4375" y1="6.6375" x2="6.8375" y2="6.6625" layer="200"/>
<rectangle x1="6.9375" y1="6.6375" x2="7.6375" y2="6.6625" layer="200"/>
<rectangle x1="9.5625" y1="6.6375" x2="9.8875" y2="6.6625" layer="200"/>
<rectangle x1="11.5375" y1="6.6375" x2="12.8625" y2="6.6625" layer="200"/>
<rectangle x1="14.6875" y1="6.6375" x2="15.4875" y2="6.6625" layer="200"/>
<rectangle x1="18.3375" y1="6.6375" x2="18.7375" y2="6.6625" layer="200"/>
<rectangle x1="3.4875" y1="6.6625" x2="3.9625" y2="6.6875" layer="200"/>
<rectangle x1="6.4375" y1="6.6625" x2="6.8125" y2="6.6875" layer="200"/>
<rectangle x1="6.9375" y1="6.6625" x2="7.6875" y2="6.6875" layer="200"/>
<rectangle x1="9.6125" y1="6.6625" x2="9.9375" y2="6.6875" layer="200"/>
<rectangle x1="11.6375" y1="6.6625" x2="12.8875" y2="6.6875" layer="200"/>
<rectangle x1="14.7875" y1="6.6625" x2="15.5875" y2="6.6875" layer="200"/>
<rectangle x1="18.3625" y1="6.6625" x2="18.7625" y2="6.6875" layer="200"/>
<rectangle x1="3.5375" y1="6.6875" x2="3.9875" y2="6.7125" layer="200"/>
<rectangle x1="6.4125" y1="6.6875" x2="6.7625" y2="6.7125" layer="200"/>
<rectangle x1="6.9375" y1="6.6875" x2="7.7375" y2="6.7125" layer="200"/>
<rectangle x1="9.6625" y1="6.6875" x2="9.9875" y2="6.7125" layer="200"/>
<rectangle x1="11.7375" y1="6.6875" x2="12.8875" y2="6.7125" layer="200"/>
<rectangle x1="14.8875" y1="6.6875" x2="15.7125" y2="6.7125" layer="200"/>
<rectangle x1="18.3875" y1="6.6875" x2="18.7875" y2="6.7125" layer="200"/>
<rectangle x1="3.5625" y1="6.7125" x2="4.0375" y2="6.7375" layer="200"/>
<rectangle x1="6.3875" y1="6.7125" x2="6.7375" y2="6.7375" layer="200"/>
<rectangle x1="6.9125" y1="6.7125" x2="7.7875" y2="6.7375" layer="200"/>
<rectangle x1="9.7125" y1="6.7125" x2="10.0125" y2="6.7375" layer="200"/>
<rectangle x1="11.8375" y1="6.7125" x2="12.8875" y2="6.7375" layer="200"/>
<rectangle x1="14.9875" y1="6.7125" x2="15.8875" y2="6.7375" layer="200"/>
<rectangle x1="18.4125" y1="6.7125" x2="18.8375" y2="6.7375" layer="200"/>
<rectangle x1="3.5875" y1="6.7375" x2="4.0625" y2="6.7625" layer="200"/>
<rectangle x1="6.3625" y1="6.7375" x2="6.7125" y2="6.7625" layer="200"/>
<rectangle x1="6.9125" y1="6.7375" x2="7.2125" y2="6.7625" layer="200"/>
<rectangle x1="7.2625" y1="6.7375" x2="7.8375" y2="6.7625" layer="200"/>
<rectangle x1="9.7625" y1="6.7375" x2="10.0625" y2="6.7625" layer="200"/>
<rectangle x1="11.9375" y1="6.7375" x2="12.8625" y2="6.7625" layer="200"/>
<rectangle x1="15.0875" y1="6.7375" x2="16.0625" y2="6.7625" layer="200"/>
<rectangle x1="18.4375" y1="6.7375" x2="18.8875" y2="6.7625" layer="200"/>
<rectangle x1="3.6375" y1="6.7625" x2="4.1125" y2="6.7875" layer="200"/>
<rectangle x1="6.3625" y1="6.7625" x2="6.6875" y2="6.7875" layer="200"/>
<rectangle x1="6.9125" y1="6.7625" x2="7.1625" y2="6.7875" layer="200"/>
<rectangle x1="7.2875" y1="6.7625" x2="7.8875" y2="6.7875" layer="200"/>
<rectangle x1="9.8125" y1="6.7625" x2="10.1125" y2="6.7875" layer="200"/>
<rectangle x1="12.0375" y1="6.7625" x2="12.8625" y2="6.7875" layer="200"/>
<rectangle x1="15.2375" y1="6.7625" x2="16.2375" y2="6.7875" layer="200"/>
<rectangle x1="18.4625" y1="6.7625" x2="18.9375" y2="6.7875" layer="200"/>
<rectangle x1="3.6625" y1="6.7875" x2="4.1375" y2="6.8125" layer="200"/>
<rectangle x1="6.3375" y1="6.7875" x2="6.6625" y2="6.8125" layer="200"/>
<rectangle x1="6.8875" y1="6.7875" x2="7.1375" y2="6.8125" layer="200"/>
<rectangle x1="7.2875" y1="6.7875" x2="7.9375" y2="6.8125" layer="200"/>
<rectangle x1="9.8625" y1="6.7875" x2="10.1375" y2="6.8125" layer="200"/>
<rectangle x1="12.1375" y1="6.7875" x2="12.8625" y2="6.8125" layer="200"/>
<rectangle x1="13.2625" y1="6.7875" x2="13.4125" y2="6.8125" layer="200"/>
<rectangle x1="15.3625" y1="6.7875" x2="16.4625" y2="6.8125" layer="200"/>
<rectangle x1="18.4625" y1="6.7875" x2="18.9875" y2="6.8125" layer="200"/>
<rectangle x1="3.6875" y1="6.8125" x2="4.1875" y2="6.8375" layer="200"/>
<rectangle x1="6.3125" y1="6.8125" x2="6.6375" y2="6.8375" layer="200"/>
<rectangle x1="6.8875" y1="6.8125" x2="7.1375" y2="6.8375" layer="200"/>
<rectangle x1="7.2875" y1="6.8125" x2="7.9875" y2="6.8375" layer="200"/>
<rectangle x1="9.9125" y1="6.8125" x2="10.1875" y2="6.8375" layer="200"/>
<rectangle x1="12.2375" y1="6.8125" x2="12.8375" y2="6.8375" layer="200"/>
<rectangle x1="13.2375" y1="6.8125" x2="13.5375" y2="6.8375" layer="200"/>
<rectangle x1="15.5375" y1="6.8125" x2="16.6875" y2="6.8375" layer="200"/>
<rectangle x1="18.4375" y1="6.8125" x2="19.0375" y2="6.8375" layer="200"/>
<rectangle x1="3.7375" y1="6.8375" x2="4.2125" y2="6.8625" layer="200"/>
<rectangle x1="6.3125" y1="6.8375" x2="6.6125" y2="6.8625" layer="200"/>
<rectangle x1="6.8875" y1="6.8375" x2="7.1125" y2="6.8625" layer="200"/>
<rectangle x1="7.2875" y1="6.8375" x2="8.0125" y2="6.8625" layer="200"/>
<rectangle x1="9.9375" y1="6.8375" x2="10.2125" y2="6.8625" layer="200"/>
<rectangle x1="12.3375" y1="6.8375" x2="12.8375" y2="6.8625" layer="200"/>
<rectangle x1="13.2375" y1="6.8375" x2="13.6375" y2="6.8625" layer="200"/>
<rectangle x1="15.6875" y1="6.8375" x2="16.9125" y2="6.8625" layer="200"/>
<rectangle x1="18.4125" y1="6.8375" x2="19.1375" y2="6.8625" layer="200"/>
<rectangle x1="3.7625" y1="6.8625" x2="4.2625" y2="6.8875" layer="200"/>
<rectangle x1="6.2875" y1="6.8625" x2="6.5875" y2="6.8875" layer="200"/>
<rectangle x1="6.8625" y1="6.8625" x2="7.0875" y2="6.8875" layer="200"/>
<rectangle x1="7.2875" y1="6.8625" x2="8.0125" y2="6.8875" layer="200"/>
<rectangle x1="9.9875" y1="6.8625" x2="10.2625" y2="6.8875" layer="200"/>
<rectangle x1="12.4625" y1="6.8625" x2="12.8125" y2="6.8875" layer="200"/>
<rectangle x1="13.2125" y1="6.8625" x2="13.7625" y2="6.8875" layer="200"/>
<rectangle x1="15.8375" y1="6.8625" x2="17.1375" y2="6.8875" layer="200"/>
<rectangle x1="18.3625" y1="6.8625" x2="19.1875" y2="6.8875" layer="200"/>
<rectangle x1="3.7875" y1="6.8875" x2="4.2875" y2="6.9125" layer="200"/>
<rectangle x1="6.2875" y1="6.8875" x2="6.5875" y2="6.9125" layer="200"/>
<rectangle x1="6.8625" y1="6.8875" x2="7.0875" y2="6.9125" layer="200"/>
<rectangle x1="7.2875" y1="6.8875" x2="8.0125" y2="6.9125" layer="200"/>
<rectangle x1="10.0375" y1="6.8875" x2="10.2875" y2="6.9125" layer="200"/>
<rectangle x1="12.5875" y1="6.8875" x2="12.7625" y2="6.9125" layer="200"/>
<rectangle x1="13.1875" y1="6.8875" x2="13.8625" y2="6.9125" layer="200"/>
<rectangle x1="16.0125" y1="6.8875" x2="17.3375" y2="6.9125" layer="200"/>
<rectangle x1="18.2625" y1="6.8875" x2="19.2625" y2="6.9125" layer="200"/>
<rectangle x1="3.8375" y1="6.9125" x2="4.3375" y2="6.9375" layer="200"/>
<rectangle x1="6.2625" y1="6.9125" x2="6.5625" y2="6.9375" layer="200"/>
<rectangle x1="6.8625" y1="6.9125" x2="7.0625" y2="6.9375" layer="200"/>
<rectangle x1="7.2875" y1="6.9125" x2="7.6125" y2="6.9375" layer="200"/>
<rectangle x1="7.6625" y1="6.9125" x2="7.9875" y2="6.9375" layer="200"/>
<rectangle x1="10.0625" y1="6.9125" x2="10.3375" y2="6.9375" layer="200"/>
<rectangle x1="13.1625" y1="6.9125" x2="13.9625" y2="6.9375" layer="200"/>
<rectangle x1="16.1625" y1="6.9125" x2="17.5375" y2="6.9375" layer="200"/>
<rectangle x1="18.1125" y1="6.9125" x2="18.6875" y2="6.9375" layer="200"/>
<rectangle x1="18.7125" y1="6.9125" x2="19.3375" y2="6.9375" layer="200"/>
<rectangle x1="3.8625" y1="6.9375" x2="4.3875" y2="6.9625" layer="200"/>
<rectangle x1="6.2625" y1="6.9375" x2="6.5375" y2="6.9625" layer="200"/>
<rectangle x1="6.8375" y1="6.9375" x2="7.0625" y2="6.9625" layer="200"/>
<rectangle x1="7.2875" y1="6.9375" x2="7.5625" y2="6.9625" layer="200"/>
<rectangle x1="7.6875" y1="6.9375" x2="7.9875" y2="6.9625" layer="200"/>
<rectangle x1="10.1125" y1="6.9375" x2="10.3625" y2="6.9625" layer="200"/>
<rectangle x1="13.1625" y1="6.9375" x2="14.0875" y2="6.9625" layer="200"/>
<rectangle x1="16.3125" y1="6.9375" x2="17.6875" y2="6.9625" layer="200"/>
<rectangle x1="17.8875" y1="6.9375" x2="18.6625" y2="6.9625" layer="200"/>
<rectangle x1="18.7875" y1="6.9375" x2="19.3875" y2="6.9625" layer="200"/>
<rectangle x1="3.8625" y1="6.9625" x2="4.4125" y2="6.9875" layer="200"/>
<rectangle x1="6.2625" y1="6.9625" x2="6.5375" y2="6.9875" layer="200"/>
<rectangle x1="6.8375" y1="6.9625" x2="7.0375" y2="6.9875" layer="200"/>
<rectangle x1="7.2875" y1="6.9625" x2="7.5375" y2="6.9875" layer="200"/>
<rectangle x1="7.6875" y1="6.9625" x2="7.9875" y2="6.9875" layer="200"/>
<rectangle x1="10.1375" y1="6.9625" x2="10.3875" y2="6.9875" layer="200"/>
<rectangle x1="13.1375" y1="6.9625" x2="14.1875" y2="6.9875" layer="200"/>
<rectangle x1="16.4625" y1="6.9625" x2="18.6375" y2="6.9875" layer="200"/>
<rectangle x1="18.8625" y1="6.9625" x2="19.4625" y2="6.9875" layer="200"/>
<rectangle x1="3.8875" y1="6.9875" x2="4.4625" y2="7.0125" layer="200"/>
<rectangle x1="6.2375" y1="6.9875" x2="6.5125" y2="7.0125" layer="200"/>
<rectangle x1="6.8375" y1="6.9875" x2="7.0375" y2="7.0125" layer="200"/>
<rectangle x1="7.2875" y1="6.9875" x2="7.5375" y2="7.0125" layer="200"/>
<rectangle x1="7.6875" y1="6.9875" x2="7.9625" y2="7.0125" layer="200"/>
<rectangle x1="10.1625" y1="6.9875" x2="10.3875" y2="7.0125" layer="200"/>
<rectangle x1="13.1375" y1="6.9875" x2="14.2875" y2="7.0125" layer="200"/>
<rectangle x1="16.6125" y1="6.9875" x2="18.6125" y2="7.0125" layer="200"/>
<rectangle x1="18.9375" y1="6.9875" x2="19.5375" y2="7.0125" layer="200"/>
<rectangle x1="3.8875" y1="7.0125" x2="4.4875" y2="7.0375" layer="200"/>
<rectangle x1="6.2375" y1="7.0125" x2="6.5125" y2="7.0375" layer="200"/>
<rectangle x1="6.8125" y1="7.0125" x2="7.0125" y2="7.0375" layer="200"/>
<rectangle x1="7.2875" y1="7.0125" x2="7.5125" y2="7.0375" layer="200"/>
<rectangle x1="7.6875" y1="7.0125" x2="7.9625" y2="7.0375" layer="200"/>
<rectangle x1="10.1625" y1="7.0125" x2="10.4125" y2="7.0375" layer="200"/>
<rectangle x1="13.1125" y1="7.0125" x2="14.4125" y2="7.0375" layer="200"/>
<rectangle x1="16.7875" y1="7.0125" x2="18.5875" y2="7.0375" layer="200"/>
<rectangle x1="19.0125" y1="7.0125" x2="19.6125" y2="7.0375" layer="200"/>
<rectangle x1="3.8125" y1="7.0375" x2="4.5375" y2="7.0625" layer="200"/>
<rectangle x1="6.2375" y1="7.0375" x2="6.4875" y2="7.0625" layer="200"/>
<rectangle x1="6.8125" y1="7.0375" x2="7.0125" y2="7.0625" layer="200"/>
<rectangle x1="7.2875" y1="7.0375" x2="7.5125" y2="7.0625" layer="200"/>
<rectangle x1="7.6875" y1="7.0375" x2="7.9375" y2="7.0625" layer="200"/>
<rectangle x1="10.1625" y1="7.0375" x2="10.4125" y2="7.0625" layer="200"/>
<rectangle x1="13.1125" y1="7.0375" x2="14.5125" y2="7.0625" layer="200"/>
<rectangle x1="16.9625" y1="7.0375" x2="18.5625" y2="7.0625" layer="200"/>
<rectangle x1="19.0875" y1="7.0375" x2="19.6875" y2="7.0625" layer="200"/>
<rectangle x1="3.5875" y1="7.0625" x2="4.5625" y2="7.0875" layer="200"/>
<rectangle x1="6.2125" y1="7.0625" x2="6.4875" y2="7.0875" layer="200"/>
<rectangle x1="6.8125" y1="7.0625" x2="6.9875" y2="7.0875" layer="200"/>
<rectangle x1="7.2875" y1="7.0625" x2="7.5125" y2="7.0875" layer="200"/>
<rectangle x1="7.6875" y1="7.0625" x2="7.9375" y2="7.0875" layer="200"/>
<rectangle x1="10.1625" y1="7.0625" x2="10.4125" y2="7.0875" layer="200"/>
<rectangle x1="13.1125" y1="7.0625" x2="14.6125" y2="7.0875" layer="200"/>
<rectangle x1="17.1875" y1="7.0625" x2="18.5125" y2="7.0875" layer="200"/>
<rectangle x1="19.1625" y1="7.0625" x2="19.7625" y2="7.0875" layer="200"/>
<rectangle x1="3.5625" y1="7.0875" x2="4.5875" y2="7.1125" layer="200"/>
<rectangle x1="6.2125" y1="7.0875" x2="6.4625" y2="7.1125" layer="200"/>
<rectangle x1="6.8125" y1="7.0875" x2="6.9875" y2="7.1125" layer="200"/>
<rectangle x1="7.2875" y1="7.0875" x2="7.5125" y2="7.1125" layer="200"/>
<rectangle x1="7.6875" y1="7.0875" x2="7.9125" y2="7.1125" layer="200"/>
<rectangle x1="10.1625" y1="7.0875" x2="10.4125" y2="7.1125" layer="200"/>
<rectangle x1="13.2125" y1="7.0875" x2="14.7375" y2="7.1125" layer="200"/>
<rectangle x1="17.4875" y1="7.0875" x2="18.4125" y2="7.1125" layer="200"/>
<rectangle x1="19.2375" y1="7.0875" x2="19.8375" y2="7.1125" layer="200"/>
<rectangle x1="3.5625" y1="7.1125" x2="4.6125" y2="7.1375" layer="200"/>
<rectangle x1="6.2125" y1="7.1125" x2="6.4625" y2="7.1375" layer="200"/>
<rectangle x1="6.8125" y1="7.1125" x2="6.9625" y2="7.1375" layer="200"/>
<rectangle x1="7.2875" y1="7.1125" x2="7.4875" y2="7.1375" layer="200"/>
<rectangle x1="7.6875" y1="7.1125" x2="7.9125" y2="7.1375" layer="200"/>
<rectangle x1="10.1625" y1="7.1125" x2="10.4125" y2="7.1375" layer="200"/>
<rectangle x1="13.2875" y1="7.1125" x2="14.8375" y2="7.1375" layer="200"/>
<rectangle x1="19.3125" y1="7.1125" x2="19.9125" y2="7.1375" layer="200"/>
<rectangle x1="3.5875" y1="7.1375" x2="4.5875" y2="7.1625" layer="200"/>
<rectangle x1="6.2125" y1="7.1375" x2="6.4625" y2="7.1625" layer="200"/>
<rectangle x1="6.7875" y1="7.1375" x2="6.9625" y2="7.1625" layer="200"/>
<rectangle x1="7.2875" y1="7.1375" x2="7.4875" y2="7.1625" layer="200"/>
<rectangle x1="7.6875" y1="7.1375" x2="7.9125" y2="7.1625" layer="200"/>
<rectangle x1="10.1375" y1="7.1375" x2="10.4125" y2="7.1625" layer="200"/>
<rectangle x1="11.1875" y1="7.1375" x2="11.2875" y2="7.1625" layer="200"/>
<rectangle x1="13.3875" y1="7.1375" x2="14.9375" y2="7.1625" layer="200"/>
<rectangle x1="19.3625" y1="7.1375" x2="19.9875" y2="7.1625" layer="200"/>
<rectangle x1="3.6125" y1="7.1625" x2="4.5375" y2="7.1875" layer="200"/>
<rectangle x1="6.1875" y1="7.1625" x2="6.4375" y2="7.1875" layer="200"/>
<rectangle x1="6.7875" y1="7.1625" x2="6.9625" y2="7.1875" layer="200"/>
<rectangle x1="7.2875" y1="7.1625" x2="7.4875" y2="7.1875" layer="200"/>
<rectangle x1="7.6625" y1="7.1625" x2="7.8875" y2="7.1875" layer="200"/>
<rectangle x1="10.1375" y1="7.1625" x2="10.4125" y2="7.1875" layer="200"/>
<rectangle x1="11.1625" y1="7.1625" x2="11.3875" y2="7.1875" layer="200"/>
<rectangle x1="13.4875" y1="7.1625" x2="15.0375" y2="7.1875" layer="200"/>
<rectangle x1="19.4375" y1="7.1625" x2="20.0375" y2="7.1875" layer="200"/>
<rectangle x1="3.6375" y1="7.1875" x2="4.5125" y2="7.2125" layer="200"/>
<rectangle x1="6.1875" y1="7.1875" x2="6.4375" y2="7.2125" layer="200"/>
<rectangle x1="6.7875" y1="7.1875" x2="6.9375" y2="7.2125" layer="200"/>
<rectangle x1="7.2875" y1="7.1875" x2="7.4875" y2="7.2125" layer="200"/>
<rectangle x1="7.6625" y1="7.1875" x2="7.8875" y2="7.2125" layer="200"/>
<rectangle x1="10.1375" y1="7.1875" x2="10.4125" y2="7.2125" layer="200"/>
<rectangle x1="11.1375" y1="7.1875" x2="11.4625" y2="7.2125" layer="200"/>
<rectangle x1="13.5875" y1="7.1875" x2="15.1125" y2="7.2125" layer="200"/>
<rectangle x1="19.5125" y1="7.1875" x2="20.1125" y2="7.2125" layer="200"/>
<rectangle x1="3.6875" y1="7.2125" x2="4.4625" y2="7.2375" layer="200"/>
<rectangle x1="6.1875" y1="7.2125" x2="6.4375" y2="7.2375" layer="200"/>
<rectangle x1="6.7875" y1="7.2125" x2="6.9375" y2="7.2375" layer="200"/>
<rectangle x1="7.2875" y1="7.2125" x2="7.4875" y2="7.2375" layer="200"/>
<rectangle x1="7.6625" y1="7.2125" x2="7.8875" y2="7.2375" layer="200"/>
<rectangle x1="10.1125" y1="7.2125" x2="10.4125" y2="7.2375" layer="200"/>
<rectangle x1="11.1125" y1="7.2125" x2="11.5875" y2="7.2375" layer="200"/>
<rectangle x1="13.6875" y1="7.2125" x2="15.1875" y2="7.2375" layer="200"/>
<rectangle x1="19.5875" y1="7.2125" x2="20.1625" y2="7.2375" layer="200"/>
<rectangle x1="3.7125" y1="7.2375" x2="4.4375" y2="7.2625" layer="200"/>
<rectangle x1="6.1875" y1="7.2375" x2="6.4375" y2="7.2625" layer="200"/>
<rectangle x1="6.7875" y1="7.2375" x2="6.9375" y2="7.2625" layer="200"/>
<rectangle x1="7.2625" y1="7.2375" x2="7.4875" y2="7.2625" layer="200"/>
<rectangle x1="7.6625" y1="7.2375" x2="7.8875" y2="7.2625" layer="200"/>
<rectangle x1="10.1125" y1="7.2375" x2="10.3875" y2="7.2625" layer="200"/>
<rectangle x1="11.0875" y1="7.2375" x2="11.6625" y2="7.2625" layer="200"/>
<rectangle x1="13.7875" y1="7.2375" x2="15.2625" y2="7.2625" layer="200"/>
<rectangle x1="19.6375" y1="7.2375" x2="20.2125" y2="7.2625" layer="200"/>
<rectangle x1="3.7375" y1="7.2625" x2="4.4125" y2="7.2875" layer="200"/>
<rectangle x1="6.1875" y1="7.2625" x2="6.4375" y2="7.2875" layer="200"/>
<rectangle x1="6.7875" y1="7.2625" x2="6.9375" y2="7.2875" layer="200"/>
<rectangle x1="7.2625" y1="7.2625" x2="7.4875" y2="7.2875" layer="200"/>
<rectangle x1="7.6625" y1="7.2625" x2="7.8625" y2="7.2875" layer="200"/>
<rectangle x1="10.1125" y1="7.2625" x2="10.3875" y2="7.2875" layer="200"/>
<rectangle x1="11.0625" y1="7.2625" x2="11.7625" y2="7.2875" layer="200"/>
<rectangle x1="13.8875" y1="7.2625" x2="15.2875" y2="7.2875" layer="200"/>
<rectangle x1="19.7125" y1="7.2625" x2="20.2625" y2="7.2875" layer="200"/>
<rectangle x1="3.7625" y1="7.2875" x2="4.3625" y2="7.3125" layer="200"/>
<rectangle x1="6.1875" y1="7.2875" x2="6.4125" y2="7.3125" layer="200"/>
<rectangle x1="6.7875" y1="7.2875" x2="6.9375" y2="7.3125" layer="200"/>
<rectangle x1="7.2625" y1="7.2875" x2="7.4875" y2="7.3125" layer="200"/>
<rectangle x1="7.6625" y1="7.2875" x2="7.8625" y2="7.3125" layer="200"/>
<rectangle x1="10.1125" y1="7.2875" x2="10.3875" y2="7.3125" layer="200"/>
<rectangle x1="11.0375" y1="7.2875" x2="11.8625" y2="7.3125" layer="200"/>
<rectangle x1="13.9875" y1="7.2875" x2="15.2625" y2="7.3125" layer="200"/>
<rectangle x1="19.7625" y1="7.2875" x2="20.3125" y2="7.3125" layer="200"/>
<rectangle x1="3.7875" y1="7.3125" x2="4.3375" y2="7.3375" layer="200"/>
<rectangle x1="6.1875" y1="7.3125" x2="6.4125" y2="7.3375" layer="200"/>
<rectangle x1="6.7875" y1="7.3125" x2="6.9125" y2="7.3375" layer="200"/>
<rectangle x1="7.2625" y1="7.3125" x2="7.4875" y2="7.3375" layer="200"/>
<rectangle x1="7.6625" y1="7.3125" x2="7.8625" y2="7.3375" layer="200"/>
<rectangle x1="10.0875" y1="7.3125" x2="10.3625" y2="7.3375" layer="200"/>
<rectangle x1="11.0125" y1="7.3125" x2="11.9625" y2="7.3375" layer="200"/>
<rectangle x1="14.0875" y1="7.3125" x2="15.2625" y2="7.3375" layer="200"/>
<rectangle x1="19.8375" y1="7.3125" x2="20.3875" y2="7.3375" layer="200"/>
<rectangle x1="3.8125" y1="7.3375" x2="4.3125" y2="7.3625" layer="200"/>
<rectangle x1="6.1875" y1="7.3375" x2="6.4125" y2="7.3625" layer="200"/>
<rectangle x1="6.7875" y1="7.3375" x2="6.9125" y2="7.3625" layer="200"/>
<rectangle x1="7.2625" y1="7.3375" x2="7.4875" y2="7.3625" layer="200"/>
<rectangle x1="7.6625" y1="7.3375" x2="7.8625" y2="7.3625" layer="200"/>
<rectangle x1="10.0875" y1="7.3375" x2="10.3125" y2="7.3625" layer="200"/>
<rectangle x1="10.9875" y1="7.3375" x2="12.0625" y2="7.3625" layer="200"/>
<rectangle x1="14.1875" y1="7.3375" x2="15.2375" y2="7.3625" layer="200"/>
<rectangle x1="15.7125" y1="7.3375" x2="15.8625" y2="7.3625" layer="200"/>
<rectangle x1="19.8875" y1="7.3375" x2="20.4375" y2="7.3625" layer="200"/>
<rectangle x1="3.8375" y1="7.3625" x2="4.2875" y2="7.3875" layer="200"/>
<rectangle x1="6.1875" y1="7.3625" x2="6.4125" y2="7.3875" layer="200"/>
<rectangle x1="6.7875" y1="7.3625" x2="6.9125" y2="7.3875" layer="200"/>
<rectangle x1="7.2625" y1="7.3625" x2="7.4875" y2="7.3875" layer="200"/>
<rectangle x1="7.6625" y1="7.3625" x2="7.8625" y2="7.3875" layer="200"/>
<rectangle x1="10.0875" y1="7.3625" x2="10.3125" y2="7.3875" layer="200"/>
<rectangle x1="10.9625" y1="7.3625" x2="11.2875" y2="7.3875" layer="200"/>
<rectangle x1="11.3125" y1="7.3625" x2="12.1875" y2="7.3875" layer="200"/>
<rectangle x1="14.2875" y1="7.3625" x2="15.2125" y2="7.3875" layer="200"/>
<rectangle x1="15.6875" y1="7.3625" x2="15.9875" y2="7.3875" layer="200"/>
<rectangle x1="19.9625" y1="7.3625" x2="20.5125" y2="7.3875" layer="200"/>
<rectangle x1="3.8875" y1="7.3875" x2="4.3375" y2="7.4125" layer="200"/>
<rectangle x1="6.1875" y1="7.3875" x2="6.4125" y2="7.4125" layer="200"/>
<rectangle x1="6.7875" y1="7.3875" x2="6.9125" y2="7.4125" layer="200"/>
<rectangle x1="7.2625" y1="7.3875" x2="7.4875" y2="7.4125" layer="200"/>
<rectangle x1="7.6375" y1="7.3875" x2="7.8625" y2="7.4125" layer="200"/>
<rectangle x1="10.0625" y1="7.3875" x2="10.2875" y2="7.4125" layer="200"/>
<rectangle x1="10.9375" y1="7.3875" x2="11.2625" y2="7.4125" layer="200"/>
<rectangle x1="11.4375" y1="7.3875" x2="12.2875" y2="7.4125" layer="200"/>
<rectangle x1="14.3875" y1="7.3875" x2="15.2125" y2="7.4125" layer="200"/>
<rectangle x1="15.6875" y1="7.3875" x2="16.1125" y2="7.4125" layer="200"/>
<rectangle x1="20.0375" y1="7.3875" x2="20.5625" y2="7.4125" layer="200"/>
<rectangle x1="3.9125" y1="7.4125" x2="4.3875" y2="7.4375" layer="200"/>
<rectangle x1="6.1875" y1="7.4125" x2="6.4125" y2="7.4375" layer="200"/>
<rectangle x1="6.7875" y1="7.4125" x2="6.9125" y2="7.4375" layer="200"/>
<rectangle x1="7.2625" y1="7.4125" x2="7.4875" y2="7.4375" layer="200"/>
<rectangle x1="7.6375" y1="7.4125" x2="7.8375" y2="7.4375" layer="200"/>
<rectangle x1="10.0625" y1="7.4125" x2="10.2875" y2="7.4375" layer="200"/>
<rectangle x1="10.9125" y1="7.4125" x2="11.2375" y2="7.4375" layer="200"/>
<rectangle x1="11.5375" y1="7.4125" x2="12.3875" y2="7.4375" layer="200"/>
<rectangle x1="14.5125" y1="7.4125" x2="15.1875" y2="7.4375" layer="200"/>
<rectangle x1="15.6625" y1="7.4125" x2="16.2125" y2="7.4375" layer="200"/>
<rectangle x1="20.1125" y1="7.4125" x2="20.6375" y2="7.4375" layer="200"/>
<rectangle x1="3.9375" y1="7.4375" x2="4.4125" y2="7.4625" layer="200"/>
<rectangle x1="6.1875" y1="7.4375" x2="6.4125" y2="7.4625" layer="200"/>
<rectangle x1="6.7875" y1="7.4375" x2="6.9125" y2="7.4625" layer="200"/>
<rectangle x1="7.2625" y1="7.4375" x2="7.4875" y2="7.4625" layer="200"/>
<rectangle x1="7.6375" y1="7.4375" x2="7.8375" y2="7.4625" layer="200"/>
<rectangle x1="10.0625" y1="7.4375" x2="10.2875" y2="7.4625" layer="200"/>
<rectangle x1="10.8875" y1="7.4375" x2="11.2125" y2="7.4625" layer="200"/>
<rectangle x1="11.6625" y1="7.4375" x2="12.4875" y2="7.4625" layer="200"/>
<rectangle x1="14.6125" y1="7.4375" x2="15.1625" y2="7.4625" layer="200"/>
<rectangle x1="15.6375" y1="7.4375" x2="16.3375" y2="7.4625" layer="200"/>
<rectangle x1="20.1875" y1="7.4375" x2="20.6875" y2="7.4625" layer="200"/>
<rectangle x1="3.9625" y1="7.4625" x2="4.4625" y2="7.4875" layer="200"/>
<rectangle x1="6.1875" y1="7.4625" x2="6.4125" y2="7.4875" layer="200"/>
<rectangle x1="6.7875" y1="7.4625" x2="6.9125" y2="7.4875" layer="200"/>
<rectangle x1="7.2625" y1="7.4625" x2="7.4875" y2="7.4875" layer="200"/>
<rectangle x1="7.6375" y1="7.4625" x2="7.8375" y2="7.4875" layer="200"/>
<rectangle x1="10.0375" y1="7.4625" x2="10.2625" y2="7.4875" layer="200"/>
<rectangle x1="10.8875" y1="7.4625" x2="11.1875" y2="7.4875" layer="200"/>
<rectangle x1="11.7625" y1="7.4625" x2="12.5875" y2="7.4875" layer="200"/>
<rectangle x1="14.7375" y1="7.4625" x2="15.1375" y2="7.4875" layer="200"/>
<rectangle x1="15.6125" y1="7.4625" x2="16.4375" y2="7.4875" layer="200"/>
<rectangle x1="20.2625" y1="7.4625" x2="20.7625" y2="7.4875" layer="200"/>
<rectangle x1="3.9875" y1="7.4875" x2="4.5125" y2="7.5125" layer="200"/>
<rectangle x1="6.1875" y1="7.4875" x2="6.4125" y2="7.5125" layer="200"/>
<rectangle x1="6.7875" y1="7.4875" x2="6.9125" y2="7.5125" layer="200"/>
<rectangle x1="7.2625" y1="7.4875" x2="7.4875" y2="7.5125" layer="200"/>
<rectangle x1="7.6375" y1="7.4875" x2="7.8375" y2="7.5125" layer="200"/>
<rectangle x1="10.0375" y1="7.4875" x2="10.2625" y2="7.5125" layer="200"/>
<rectangle x1="10.8625" y1="7.4875" x2="11.1625" y2="7.5125" layer="200"/>
<rectangle x1="11.8875" y1="7.4875" x2="12.6875" y2="7.5125" layer="200"/>
<rectangle x1="14.8875" y1="7.4875" x2="15.0625" y2="7.5125" layer="200"/>
<rectangle x1="15.6125" y1="7.4875" x2="16.4875" y2="7.5125" layer="200"/>
<rectangle x1="20.3375" y1="7.4875" x2="20.8125" y2="7.5125" layer="200"/>
<rectangle x1="4.0375" y1="7.5125" x2="4.5625" y2="7.5375" layer="200"/>
<rectangle x1="6.1875" y1="7.5125" x2="6.4125" y2="7.5375" layer="200"/>
<rectangle x1="6.7875" y1="7.5125" x2="6.9125" y2="7.5375" layer="200"/>
<rectangle x1="7.2625" y1="7.5125" x2="7.4875" y2="7.5375" layer="200"/>
<rectangle x1="7.6375" y1="7.5125" x2="7.8375" y2="7.5375" layer="200"/>
<rectangle x1="10.0375" y1="7.5125" x2="10.2625" y2="7.5375" layer="200"/>
<rectangle x1="10.8375" y1="7.5125" x2="11.1375" y2="7.5375" layer="200"/>
<rectangle x1="11.9875" y1="7.5125" x2="12.8125" y2="7.5375" layer="200"/>
<rectangle x1="15.5875" y1="7.5125" x2="16.4875" y2="7.5375" layer="200"/>
<rectangle x1="20.3875" y1="7.5125" x2="20.8625" y2="7.5375" layer="200"/>
<rectangle x1="4.0625" y1="7.5375" x2="4.6125" y2="7.5625" layer="200"/>
<rectangle x1="6.1875" y1="7.5375" x2="6.4125" y2="7.5625" layer="200"/>
<rectangle x1="6.7875" y1="7.5375" x2="6.9375" y2="7.5625" layer="200"/>
<rectangle x1="7.2625" y1="7.5375" x2="7.4875" y2="7.5625" layer="200"/>
<rectangle x1="7.6375" y1="7.5375" x2="7.8375" y2="7.5625" layer="200"/>
<rectangle x1="10.0125" y1="7.5375" x2="10.2375" y2="7.5625" layer="200"/>
<rectangle x1="10.8375" y1="7.5375" x2="11.1125" y2="7.5625" layer="200"/>
<rectangle x1="12.0875" y1="7.5375" x2="12.9125" y2="7.5625" layer="200"/>
<rectangle x1="15.5625" y1="7.5375" x2="16.4875" y2="7.5625" layer="200"/>
<rectangle x1="20.4625" y1="7.5375" x2="20.9375" y2="7.5625" layer="200"/>
<rectangle x1="4.0875" y1="7.5625" x2="4.6625" y2="7.5875" layer="200"/>
<rectangle x1="6.1875" y1="7.5625" x2="6.4125" y2="7.5875" layer="200"/>
<rectangle x1="6.7875" y1="7.5625" x2="6.9375" y2="7.5875" layer="200"/>
<rectangle x1="7.2625" y1="7.5625" x2="7.4875" y2="7.5875" layer="200"/>
<rectangle x1="7.6375" y1="7.5625" x2="7.8375" y2="7.5875" layer="200"/>
<rectangle x1="10.0125" y1="7.5625" x2="10.2375" y2="7.5875" layer="200"/>
<rectangle x1="10.8375" y1="7.5625" x2="11.0875" y2="7.5875" layer="200"/>
<rectangle x1="12.1875" y1="7.5625" x2="13.0125" y2="7.5875" layer="200"/>
<rectangle x1="15.5625" y1="7.5625" x2="16.4625" y2="7.5875" layer="200"/>
<rectangle x1="20.4875" y1="7.5625" x2="20.9625" y2="7.5875" layer="200"/>
<rectangle x1="4.1125" y1="7.5875" x2="4.7125" y2="7.6125" layer="200"/>
<rectangle x1="6.1875" y1="7.5875" x2="6.4125" y2="7.6125" layer="200"/>
<rectangle x1="6.7875" y1="7.5875" x2="6.9375" y2="7.6125" layer="200"/>
<rectangle x1="7.2625" y1="7.5875" x2="7.4875" y2="7.6125" layer="200"/>
<rectangle x1="7.6375" y1="7.5875" x2="7.8375" y2="7.6125" layer="200"/>
<rectangle x1="9.9875" y1="7.5875" x2="10.2375" y2="7.6125" layer="200"/>
<rectangle x1="10.8125" y1="7.5875" x2="11.0625" y2="7.6125" layer="200"/>
<rectangle x1="12.2875" y1="7.5875" x2="13.1375" y2="7.6125" layer="200"/>
<rectangle x1="15.5625" y1="7.5875" x2="16.4625" y2="7.6125" layer="200"/>
<rectangle x1="20.5375" y1="7.5875" x2="21.0125" y2="7.6125" layer="200"/>
<rectangle x1="4.1625" y1="7.6125" x2="4.7625" y2="7.6375" layer="200"/>
<rectangle x1="6.1875" y1="7.6125" x2="6.4375" y2="7.6375" layer="200"/>
<rectangle x1="6.7875" y1="7.6125" x2="6.9375" y2="7.6375" layer="200"/>
<rectangle x1="7.2625" y1="7.6125" x2="7.4875" y2="7.6375" layer="200"/>
<rectangle x1="7.6375" y1="7.6125" x2="7.8375" y2="7.6375" layer="200"/>
<rectangle x1="9.9875" y1="7.6125" x2="10.2125" y2="7.6375" layer="200"/>
<rectangle x1="10.8125" y1="7.6125" x2="11.0625" y2="7.6375" layer="200"/>
<rectangle x1="12.3875" y1="7.6125" x2="13.2375" y2="7.6375" layer="200"/>
<rectangle x1="15.5625" y1="7.6125" x2="16.4625" y2="7.6375" layer="200"/>
<rectangle x1="20.5875" y1="7.6125" x2="21.0625" y2="7.6375" layer="200"/>
<rectangle x1="4.1875" y1="7.6375" x2="4.8125" y2="7.6625" layer="200"/>
<rectangle x1="6.1875" y1="7.6375" x2="6.4375" y2="7.6625" layer="200"/>
<rectangle x1="6.7875" y1="7.6375" x2="6.9375" y2="7.6625" layer="200"/>
<rectangle x1="7.2625" y1="7.6375" x2="7.4875" y2="7.6625" layer="200"/>
<rectangle x1="7.6375" y1="7.6375" x2="7.8375" y2="7.6625" layer="200"/>
<rectangle x1="9.9875" y1="7.6375" x2="10.2125" y2="7.6625" layer="200"/>
<rectangle x1="10.7875" y1="7.6375" x2="11.0375" y2="7.6625" layer="200"/>
<rectangle x1="12.4875" y1="7.6375" x2="13.3375" y2="7.6625" layer="200"/>
<rectangle x1="15.5625" y1="7.6375" x2="16.4375" y2="7.6625" layer="200"/>
<rectangle x1="20.6625" y1="7.6375" x2="21.1375" y2="7.6625" layer="200"/>
<rectangle x1="4.2125" y1="7.6625" x2="4.8625" y2="7.6875" layer="200"/>
<rectangle x1="6.1875" y1="7.6625" x2="6.4375" y2="7.6875" layer="200"/>
<rectangle x1="6.7875" y1="7.6625" x2="6.9375" y2="7.6875" layer="200"/>
<rectangle x1="7.2625" y1="7.6625" x2="7.4875" y2="7.6875" layer="200"/>
<rectangle x1="7.6375" y1="7.6625" x2="7.8625" y2="7.6875" layer="200"/>
<rectangle x1="9.9625" y1="7.6625" x2="10.2125" y2="7.6875" layer="200"/>
<rectangle x1="10.7625" y1="7.6625" x2="11.0125" y2="7.6875" layer="200"/>
<rectangle x1="12.6125" y1="7.6625" x2="13.4625" y2="7.6875" layer="200"/>
<rectangle x1="15.5875" y1="7.6625" x2="16.4125" y2="7.6875" layer="200"/>
<rectangle x1="20.7125" y1="7.6625" x2="21.1875" y2="7.6875" layer="200"/>
<rectangle x1="4.2625" y1="7.6875" x2="4.9125" y2="7.7125" layer="200"/>
<rectangle x1="6.1875" y1="7.6875" x2="6.4375" y2="7.7125" layer="200"/>
<rectangle x1="6.7875" y1="7.6875" x2="6.9375" y2="7.7125" layer="200"/>
<rectangle x1="7.2625" y1="7.6875" x2="7.4875" y2="7.7125" layer="200"/>
<rectangle x1="7.6375" y1="7.6875" x2="7.8625" y2="7.7125" layer="200"/>
<rectangle x1="9.9625" y1="7.6875" x2="10.1875" y2="7.7125" layer="200"/>
<rectangle x1="10.7375" y1="7.6875" x2="10.9875" y2="7.7125" layer="200"/>
<rectangle x1="12.7125" y1="7.6875" x2="13.5625" y2="7.7125" layer="200"/>
<rectangle x1="15.6375" y1="7.6875" x2="16.4125" y2="7.7125" layer="200"/>
<rectangle x1="20.7625" y1="7.6875" x2="21.2375" y2="7.7125" layer="200"/>
<rectangle x1="4.2875" y1="7.7125" x2="4.9625" y2="7.7375" layer="200"/>
<rectangle x1="6.2125" y1="7.7125" x2="6.4375" y2="7.7375" layer="200"/>
<rectangle x1="6.7875" y1="7.7125" x2="6.9375" y2="7.7375" layer="200"/>
<rectangle x1="7.2625" y1="7.7125" x2="7.4875" y2="7.7375" layer="200"/>
<rectangle x1="7.6375" y1="7.7125" x2="7.8625" y2="7.7375" layer="200"/>
<rectangle x1="9.9625" y1="7.7125" x2="10.1875" y2="7.7375" layer="200"/>
<rectangle x1="10.7375" y1="7.7125" x2="10.9625" y2="7.7375" layer="200"/>
<rectangle x1="12.8125" y1="7.7125" x2="13.6625" y2="7.7375" layer="200"/>
<rectangle x1="15.6875" y1="7.7125" x2="16.3875" y2="7.7375" layer="200"/>
<rectangle x1="20.8125" y1="7.7125" x2="21.2875" y2="7.7375" layer="200"/>
<rectangle x1="4.3625" y1="7.7375" x2="5.0375" y2="7.7625" layer="200"/>
<rectangle x1="6.2125" y1="7.7375" x2="6.4625" y2="7.7625" layer="200"/>
<rectangle x1="6.7875" y1="7.7375" x2="6.9375" y2="7.7625" layer="200"/>
<rectangle x1="7.2625" y1="7.7375" x2="7.4875" y2="7.7625" layer="200"/>
<rectangle x1="7.6375" y1="7.7375" x2="7.8625" y2="7.7625" layer="200"/>
<rectangle x1="9.9375" y1="7.7375" x2="10.1625" y2="7.7625" layer="200"/>
<rectangle x1="10.7125" y1="7.7375" x2="10.9625" y2="7.7625" layer="200"/>
<rectangle x1="12.9375" y1="7.7375" x2="13.7875" y2="7.7625" layer="200"/>
<rectangle x1="15.8125" y1="7.7375" x2="16.3625" y2="7.7625" layer="200"/>
<rectangle x1="20.8875" y1="7.7375" x2="21.3375" y2="7.7625" layer="200"/>
<rectangle x1="4.3875" y1="7.7625" x2="5.0875" y2="7.7875" layer="200"/>
<rectangle x1="6.2125" y1="7.7625" x2="6.4625" y2="7.7875" layer="200"/>
<rectangle x1="6.7875" y1="7.7625" x2="6.9375" y2="7.7875" layer="200"/>
<rectangle x1="7.2625" y1="7.7625" x2="7.5125" y2="7.7875" layer="200"/>
<rectangle x1="7.6375" y1="7.7625" x2="7.8625" y2="7.7875" layer="200"/>
<rectangle x1="9.9375" y1="7.7625" x2="10.1625" y2="7.7875" layer="200"/>
<rectangle x1="10.6875" y1="7.7625" x2="10.9375" y2="7.7875" layer="200"/>
<rectangle x1="13.0375" y1="7.7625" x2="13.8875" y2="7.7875" layer="200"/>
<rectangle x1="15.9125" y1="7.7625" x2="16.3625" y2="7.7875" layer="200"/>
<rectangle x1="20.9375" y1="7.7625" x2="21.3875" y2="7.7875" layer="200"/>
<rectangle x1="4.3625" y1="7.7875" x2="5.1125" y2="7.8125" layer="200"/>
<rectangle x1="6.2125" y1="7.7875" x2="6.4625" y2="7.8125" layer="200"/>
<rectangle x1="6.7875" y1="7.7875" x2="6.9375" y2="7.8125" layer="200"/>
<rectangle x1="7.2625" y1="7.7875" x2="7.5125" y2="7.8125" layer="200"/>
<rectangle x1="7.6625" y1="7.7875" x2="7.8875" y2="7.8125" layer="200"/>
<rectangle x1="9.9125" y1="7.7875" x2="10.1625" y2="7.8125" layer="200"/>
<rectangle x1="10.6875" y1="7.7875" x2="10.9125" y2="7.8125" layer="200"/>
<rectangle x1="13.1625" y1="7.7875" x2="13.9875" y2="7.8125" layer="200"/>
<rectangle x1="15.9875" y1="7.7875" x2="16.3375" y2="7.8125" layer="200"/>
<rectangle x1="21.0125" y1="7.7875" x2="21.4375" y2="7.8125" layer="200"/>
<rectangle x1="4.3375" y1="7.8125" x2="5.1125" y2="7.8375" layer="200"/>
<rectangle x1="6.2375" y1="7.8125" x2="6.4875" y2="7.8375" layer="200"/>
<rectangle x1="6.7875" y1="7.8125" x2="6.9375" y2="7.8375" layer="200"/>
<rectangle x1="7.2625" y1="7.8125" x2="7.5125" y2="7.8375" layer="200"/>
<rectangle x1="7.6625" y1="7.8125" x2="7.8875" y2="7.8375" layer="200"/>
<rectangle x1="9.9125" y1="7.8125" x2="10.1375" y2="7.8375" layer="200"/>
<rectangle x1="10.6625" y1="7.8125" x2="10.8875" y2="7.8375" layer="200"/>
<rectangle x1="13.2875" y1="7.8125" x2="14.0875" y2="7.8375" layer="200"/>
<rectangle x1="16.0625" y1="7.8125" x2="16.3125" y2="7.8375" layer="200"/>
<rectangle x1="21.0625" y1="7.8125" x2="21.4875" y2="7.8375" layer="200"/>
<rectangle x1="4.2375" y1="7.8375" x2="5.0875" y2="7.8625" layer="200"/>
<rectangle x1="6.2375" y1="7.8375" x2="6.4875" y2="7.8625" layer="200"/>
<rectangle x1="6.7875" y1="7.8375" x2="6.9625" y2="7.8625" layer="200"/>
<rectangle x1="7.2625" y1="7.8375" x2="7.5125" y2="7.8625" layer="200"/>
<rectangle x1="7.6625" y1="7.8375" x2="7.9125" y2="7.8625" layer="200"/>
<rectangle x1="9.9125" y1="7.8375" x2="10.1375" y2="7.8625" layer="200"/>
<rectangle x1="10.6625" y1="7.8375" x2="10.8875" y2="7.8625" layer="200"/>
<rectangle x1="13.3875" y1="7.8375" x2="14.1875" y2="7.8625" layer="200"/>
<rectangle x1="16.1625" y1="7.8375" x2="16.2875" y2="7.8625" layer="200"/>
<rectangle x1="21.1125" y1="7.8375" x2="21.5375" y2="7.8625" layer="200"/>
<rectangle x1="4.2125" y1="7.8625" x2="5.0625" y2="7.8875" layer="200"/>
<rectangle x1="6.2375" y1="7.8625" x2="6.4875" y2="7.8875" layer="200"/>
<rectangle x1="6.8125" y1="7.8625" x2="6.9625" y2="7.8875" layer="200"/>
<rectangle x1="7.2625" y1="7.8625" x2="7.5125" y2="7.8875" layer="200"/>
<rectangle x1="7.6875" y1="7.8625" x2="7.9375" y2="7.8875" layer="200"/>
<rectangle x1="9.8875" y1="7.8625" x2="10.1125" y2="7.8875" layer="200"/>
<rectangle x1="10.6375" y1="7.8625" x2="10.8625" y2="7.8875" layer="200"/>
<rectangle x1="13.5125" y1="7.8625" x2="14.2875" y2="7.8875" layer="200"/>
<rectangle x1="21.1875" y1="7.8625" x2="21.5875" y2="7.8875" layer="200"/>
<rectangle x1="4.2375" y1="7.8875" x2="5.0375" y2="7.9125" layer="200"/>
<rectangle x1="6.2375" y1="7.8875" x2="6.5125" y2="7.9125" layer="200"/>
<rectangle x1="6.8125" y1="7.8875" x2="6.9625" y2="7.9125" layer="200"/>
<rectangle x1="7.2625" y1="7.8875" x2="7.5125" y2="7.9125" layer="200"/>
<rectangle x1="7.6875" y1="7.8875" x2="7.9625" y2="7.9125" layer="200"/>
<rectangle x1="9.8875" y1="7.8875" x2="10.1125" y2="7.9125" layer="200"/>
<rectangle x1="10.6125" y1="7.8875" x2="10.8375" y2="7.9125" layer="200"/>
<rectangle x1="13.6125" y1="7.8875" x2="14.3875" y2="7.9125" layer="200"/>
<rectangle x1="21.2375" y1="7.8875" x2="21.6125" y2="7.9125" layer="200"/>
<rectangle x1="4.2625" y1="7.9125" x2="5.0125" y2="7.9375" layer="200"/>
<rectangle x1="6.2625" y1="7.9125" x2="6.5125" y2="7.9375" layer="200"/>
<rectangle x1="6.8125" y1="7.9125" x2="6.9625" y2="7.9375" layer="200"/>
<rectangle x1="7.2625" y1="7.9125" x2="7.5125" y2="7.9375" layer="200"/>
<rectangle x1="7.7125" y1="7.9125" x2="8.0375" y2="7.9375" layer="200"/>
<rectangle x1="9.8625" y1="7.9125" x2="10.1125" y2="7.9375" layer="200"/>
<rectangle x1="10.6125" y1="7.9125" x2="10.8375" y2="7.9375" layer="200"/>
<rectangle x1="13.7375" y1="7.9125" x2="14.4625" y2="7.9375" layer="200"/>
<rectangle x1="21.2875" y1="7.9125" x2="21.6625" y2="7.9375" layer="200"/>
<rectangle x1="4.2875" y1="7.9375" x2="4.9875" y2="7.9625" layer="200"/>
<rectangle x1="6.2625" y1="7.9375" x2="6.5375" y2="7.9625" layer="200"/>
<rectangle x1="6.8125" y1="7.9375" x2="6.9875" y2="7.9625" layer="200"/>
<rectangle x1="7.2875" y1="7.9375" x2="7.5125" y2="7.9625" layer="200"/>
<rectangle x1="7.7375" y1="7.9375" x2="8.1125" y2="7.9625" layer="200"/>
<rectangle x1="9.8625" y1="7.9375" x2="10.0875" y2="7.9625" layer="200"/>
<rectangle x1="10.6125" y1="7.9375" x2="10.8125" y2="7.9625" layer="200"/>
<rectangle x1="13.8375" y1="7.9375" x2="14.5625" y2="7.9625" layer="200"/>
<rectangle x1="21.3375" y1="7.9375" x2="21.7125" y2="7.9625" layer="200"/>
<rectangle x1="4.3125" y1="7.9625" x2="4.9625" y2="7.9875" layer="200"/>
<rectangle x1="6.2875" y1="7.9625" x2="6.5375" y2="7.9875" layer="200"/>
<rectangle x1="6.8125" y1="7.9625" x2="6.9875" y2="7.9875" layer="200"/>
<rectangle x1="7.2875" y1="7.9625" x2="7.5375" y2="7.9875" layer="200"/>
<rectangle x1="7.7625" y1="7.9625" x2="8.2125" y2="7.9875" layer="200"/>
<rectangle x1="9.8375" y1="7.9625" x2="10.0875" y2="7.9875" layer="200"/>
<rectangle x1="10.5875" y1="7.9625" x2="10.8125" y2="7.9875" layer="200"/>
<rectangle x1="13.9625" y1="7.9625" x2="14.6625" y2="7.9875" layer="200"/>
<rectangle x1="21.3875" y1="7.9625" x2="21.7625" y2="7.9875" layer="200"/>
<rectangle x1="4.3375" y1="7.9875" x2="4.9125" y2="8.0125" layer="200"/>
<rectangle x1="6.2875" y1="7.9875" x2="6.5625" y2="8.0125" layer="200"/>
<rectangle x1="6.8375" y1="7.9875" x2="7.0125" y2="8.0125" layer="200"/>
<rectangle x1="7.2875" y1="7.9875" x2="7.5375" y2="8.0125" layer="200"/>
<rectangle x1="7.7875" y1="7.9875" x2="8.3375" y2="8.0125" layer="200"/>
<rectangle x1="9.8375" y1="7.9875" x2="10.0625" y2="8.0125" layer="200"/>
<rectangle x1="10.5875" y1="7.9875" x2="10.7875" y2="8.0125" layer="200"/>
<rectangle x1="14.0875" y1="7.9875" x2="14.7625" y2="8.0125" layer="200"/>
<rectangle x1="21.4375" y1="7.9875" x2="21.7875" y2="8.0125" layer="200"/>
<rectangle x1="4.3625" y1="8.0125" x2="4.8375" y2="8.0375" layer="200"/>
<rectangle x1="6.2875" y1="8.0125" x2="6.5625" y2="8.0375" layer="200"/>
<rectangle x1="6.8375" y1="8.0125" x2="7.0125" y2="8.0375" layer="200"/>
<rectangle x1="7.2875" y1="8.0125" x2="7.5375" y2="8.0375" layer="200"/>
<rectangle x1="7.8125" y1="8.0125" x2="8.4625" y2="8.0375" layer="200"/>
<rectangle x1="9.8125" y1="8.0125" x2="10.0625" y2="8.0375" layer="200"/>
<rectangle x1="10.5625" y1="8.0125" x2="10.7875" y2="8.0375" layer="200"/>
<rectangle x1="14.1875" y1="8.0125" x2="14.8875" y2="8.0375" layer="200"/>
<rectangle x1="21.4875" y1="8.0125" x2="21.8375" y2="8.0375" layer="200"/>
<rectangle x1="4.3875" y1="8.0375" x2="4.8375" y2="8.0625" layer="200"/>
<rectangle x1="6.3125" y1="8.0375" x2="6.5875" y2="8.0625" layer="200"/>
<rectangle x1="6.8375" y1="8.0375" x2="7.0375" y2="8.0625" layer="200"/>
<rectangle x1="7.3125" y1="8.0375" x2="7.5375" y2="8.0625" layer="200"/>
<rectangle x1="7.8625" y1="8.0375" x2="8.6125" y2="8.0625" layer="200"/>
<rectangle x1="9.7875" y1="8.0375" x2="10.0375" y2="8.0625" layer="200"/>
<rectangle x1="10.5625" y1="8.0375" x2="10.7625" y2="8.0625" layer="200"/>
<rectangle x1="14.3125" y1="8.0375" x2="14.9875" y2="8.0625" layer="200"/>
<rectangle x1="21.5375" y1="8.0375" x2="21.8875" y2="8.0625" layer="200"/>
<rectangle x1="4.4125" y1="8.0625" x2="4.8625" y2="8.0875" layer="200"/>
<rectangle x1="6.3125" y1="8.0625" x2="6.6125" y2="8.0875" layer="200"/>
<rectangle x1="6.8625" y1="8.0625" x2="7.0375" y2="8.0875" layer="200"/>
<rectangle x1="7.3125" y1="8.0625" x2="7.5625" y2="8.0875" layer="200"/>
<rectangle x1="7.9375" y1="8.0625" x2="8.7375" y2="8.0875" layer="200"/>
<rectangle x1="9.7625" y1="8.0625" x2="10.0375" y2="8.0875" layer="200"/>
<rectangle x1="10.5625" y1="8.0625" x2="10.7625" y2="8.0875" layer="200"/>
<rectangle x1="14.4375" y1="8.0625" x2="15.0875" y2="8.0875" layer="200"/>
<rectangle x1="21.5625" y1="8.0625" x2="21.9125" y2="8.0875" layer="200"/>
<rectangle x1="4.4625" y1="8.0875" x2="4.8875" y2="8.1125" layer="200"/>
<rectangle x1="6.3375" y1="8.0875" x2="6.6125" y2="8.1125" layer="200"/>
<rectangle x1="6.8625" y1="8.0875" x2="7.0625" y2="8.1125" layer="200"/>
<rectangle x1="7.3125" y1="8.0875" x2="7.5625" y2="8.1125" layer="200"/>
<rectangle x1="7.9875" y1="8.0875" x2="8.8875" y2="8.1125" layer="200"/>
<rectangle x1="9.7375" y1="8.0875" x2="10.0125" y2="8.1125" layer="200"/>
<rectangle x1="10.5375" y1="8.0875" x2="10.7375" y2="8.1125" layer="200"/>
<rectangle x1="14.5625" y1="8.0875" x2="15.1875" y2="8.1125" layer="200"/>
<rectangle x1="21.6125" y1="8.0875" x2="21.9625" y2="8.1125" layer="200"/>
<rectangle x1="4.4875" y1="8.1125" x2="4.9125" y2="8.1375" layer="200"/>
<rectangle x1="6.3625" y1="8.1125" x2="6.6375" y2="8.1375" layer="200"/>
<rectangle x1="6.8625" y1="8.1125" x2="7.0875" y2="8.1375" layer="200"/>
<rectangle x1="7.3375" y1="8.1125" x2="7.5875" y2="8.1375" layer="200"/>
<rectangle x1="8.0875" y1="8.1125" x2="9.0375" y2="8.1375" layer="200"/>
<rectangle x1="9.7125" y1="8.1125" x2="9.9875" y2="8.1375" layer="200"/>
<rectangle x1="10.5375" y1="8.1125" x2="10.7375" y2="8.1375" layer="200"/>
<rectangle x1="14.6625" y1="8.1125" x2="15.3125" y2="8.1375" layer="200"/>
<rectangle x1="21.6625" y1="8.1125" x2="22.0125" y2="8.1375" layer="200"/>
<rectangle x1="4.5125" y1="8.1375" x2="4.9625" y2="8.1625" layer="200"/>
<rectangle x1="6.3625" y1="8.1375" x2="6.6625" y2="8.1625" layer="200"/>
<rectangle x1="6.8875" y1="8.1375" x2="7.1125" y2="8.1625" layer="200"/>
<rectangle x1="7.3375" y1="8.1375" x2="7.5875" y2="8.1625" layer="200"/>
<rectangle x1="8.1625" y1="8.1375" x2="9.1875" y2="8.1625" layer="200"/>
<rectangle x1="9.6625" y1="8.1375" x2="9.9875" y2="8.1625" layer="200"/>
<rectangle x1="10.5375" y1="8.1375" x2="10.7125" y2="8.1625" layer="200"/>
<rectangle x1="14.7625" y1="8.1375" x2="15.4125" y2="8.1625" layer="200"/>
<rectangle x1="21.6875" y1="8.1375" x2="22.0375" y2="8.1625" layer="200"/>
<rectangle x1="4.5375" y1="8.1625" x2="4.9875" y2="8.1875" layer="200"/>
<rectangle x1="6.3875" y1="8.1625" x2="6.6875" y2="8.1875" layer="200"/>
<rectangle x1="6.8875" y1="8.1625" x2="7.1125" y2="8.1875" layer="200"/>
<rectangle x1="7.3375" y1="8.1625" x2="7.6125" y2="8.1875" layer="200"/>
<rectangle x1="8.2625" y1="8.1625" x2="9.3375" y2="8.1875" layer="200"/>
<rectangle x1="9.6125" y1="8.1625" x2="9.9625" y2="8.1875" layer="200"/>
<rectangle x1="10.5125" y1="8.1625" x2="10.7125" y2="8.1875" layer="200"/>
<rectangle x1="14.8625" y1="8.1625" x2="15.5375" y2="8.1875" layer="200"/>
<rectangle x1="21.7375" y1="8.1625" x2="22.0875" y2="8.1875" layer="200"/>
<rectangle x1="4.5625" y1="8.1875" x2="5.0375" y2="8.2125" layer="200"/>
<rectangle x1="6.4125" y1="8.1875" x2="6.7125" y2="8.2125" layer="200"/>
<rectangle x1="6.9125" y1="8.1875" x2="7.1375" y2="8.2125" layer="200"/>
<rectangle x1="7.3625" y1="8.1875" x2="7.6375" y2="8.2125" layer="200"/>
<rectangle x1="8.3875" y1="8.1875" x2="9.4875" y2="8.2125" layer="200"/>
<rectangle x1="9.5625" y1="8.1875" x2="9.9625" y2="8.2125" layer="200"/>
<rectangle x1="10.5125" y1="8.1875" x2="10.7125" y2="8.2125" layer="200"/>
<rectangle x1="14.9375" y1="8.1875" x2="15.6375" y2="8.2125" layer="200"/>
<rectangle x1="21.7625" y1="8.1875" x2="22.1375" y2="8.2125" layer="200"/>
<rectangle x1="4.5875" y1="8.2125" x2="5.0875" y2="8.2375" layer="200"/>
<rectangle x1="6.4125" y1="8.2125" x2="6.7375" y2="8.2375" layer="200"/>
<rectangle x1="6.9375" y1="8.2125" x2="7.1625" y2="8.2375" layer="200"/>
<rectangle x1="7.3625" y1="8.2125" x2="7.6625" y2="8.2375" layer="200"/>
<rectangle x1="8.4875" y1="8.2125" x2="9.9375" y2="8.2375" layer="200"/>
<rectangle x1="10.5125" y1="8.2125" x2="10.6875" y2="8.2375" layer="200"/>
<rectangle x1="15.0125" y1="8.2125" x2="15.7375" y2="8.2375" layer="200"/>
<rectangle x1="21.8125" y1="8.2125" x2="22.1625" y2="8.2375" layer="200"/>
<rectangle x1="4.6375" y1="8.2375" x2="5.1375" y2="8.2625" layer="200"/>
<rectangle x1="6.4375" y1="8.2375" x2="6.7625" y2="8.2625" layer="200"/>
<rectangle x1="6.9375" y1="8.2375" x2="7.1875" y2="8.2625" layer="200"/>
<rectangle x1="7.3625" y1="8.2375" x2="7.7125" y2="8.2625" layer="200"/>
<rectangle x1="8.6125" y1="8.2375" x2="9.9125" y2="8.2625" layer="200"/>
<rectangle x1="10.5125" y1="8.2375" x2="10.6875" y2="8.2625" layer="200"/>
<rectangle x1="15.1125" y1="8.2375" x2="15.8625" y2="8.2625" layer="200"/>
<rectangle x1="21.8625" y1="8.2375" x2="22.2125" y2="8.2625" layer="200"/>
<rectangle x1="4.6625" y1="8.2625" x2="5.1875" y2="8.2875" layer="200"/>
<rectangle x1="6.4625" y1="8.2625" x2="6.7875" y2="8.2875" layer="200"/>
<rectangle x1="6.9625" y1="8.2625" x2="7.2375" y2="8.2875" layer="200"/>
<rectangle x1="7.3875" y1="8.2625" x2="7.7625" y2="8.2875" layer="200"/>
<rectangle x1="8.7375" y1="8.2625" x2="9.8875" y2="8.2875" layer="200"/>
<rectangle x1="10.5125" y1="8.2625" x2="10.6875" y2="8.2875" layer="200"/>
<rectangle x1="15.2125" y1="8.2625" x2="15.9625" y2="8.2875" layer="200"/>
<rectangle x1="21.8875" y1="8.2625" x2="22.2375" y2="8.2875" layer="200"/>
<rectangle x1="4.6875" y1="8.2875" x2="5.2375" y2="8.3125" layer="200"/>
<rectangle x1="6.4875" y1="8.2875" x2="6.8125" y2="8.3125" layer="200"/>
<rectangle x1="6.9875" y1="8.2875" x2="7.2875" y2="8.3125" layer="200"/>
<rectangle x1="7.3375" y1="8.2875" x2="7.8375" y2="8.3125" layer="200"/>
<rectangle x1="8.8875" y1="8.2875" x2="9.8875" y2="8.3125" layer="200"/>
<rectangle x1="10.4875" y1="8.2875" x2="10.6625" y2="8.3125" layer="200"/>
<rectangle x1="15.3375" y1="8.2875" x2="16.0625" y2="8.3125" layer="200"/>
<rectangle x1="21.9375" y1="8.2875" x2="22.2875" y2="8.3125" layer="200"/>
<rectangle x1="4.7375" y1="8.3125" x2="5.2875" y2="8.3375" layer="200"/>
<rectangle x1="6.5125" y1="8.3125" x2="6.8375" y2="8.3375" layer="200"/>
<rectangle x1="7.0125" y1="8.3125" x2="7.9125" y2="8.3375" layer="200"/>
<rectangle x1="9.0375" y1="8.3125" x2="9.8625" y2="8.3375" layer="200"/>
<rectangle x1="10.4875" y1="8.3125" x2="10.6625" y2="8.3375" layer="200"/>
<rectangle x1="15.4625" y1="8.3125" x2="16.1625" y2="8.3375" layer="200"/>
<rectangle x1="21.9625" y1="8.3125" x2="22.3375" y2="8.3375" layer="200"/>
<rectangle x1="4.7625" y1="8.3375" x2="5.3875" y2="8.3625" layer="200"/>
<rectangle x1="6.5375" y1="8.3375" x2="6.8625" y2="8.3625" layer="200"/>
<rectangle x1="7.0375" y1="8.3375" x2="7.9875" y2="8.3625" layer="200"/>
<rectangle x1="9.2125" y1="8.3375" x2="9.8125" y2="8.3625" layer="200"/>
<rectangle x1="10.4875" y1="8.3375" x2="10.6625" y2="8.3625" layer="200"/>
<rectangle x1="15.5875" y1="8.3375" x2="16.2625" y2="8.3625" layer="200"/>
<rectangle x1="22.0125" y1="8.3375" x2="22.3625" y2="8.3625" layer="200"/>
<rectangle x1="4.7875" y1="8.3625" x2="5.5125" y2="8.3875" layer="200"/>
<rectangle x1="6.5375" y1="8.3625" x2="6.9125" y2="8.3875" layer="200"/>
<rectangle x1="7.0375" y1="8.3625" x2="8.0625" y2="8.3875" layer="200"/>
<rectangle x1="9.4375" y1="8.3625" x2="9.7375" y2="8.3875" layer="200"/>
<rectangle x1="10.4875" y1="8.3625" x2="10.6625" y2="8.3875" layer="200"/>
<rectangle x1="15.7125" y1="8.3625" x2="16.3625" y2="8.3875" layer="200"/>
<rectangle x1="22.0375" y1="8.3625" x2="22.4125" y2="8.3875" layer="200"/>
<rectangle x1="4.8375" y1="8.3875" x2="5.9375" y2="8.4125" layer="200"/>
<rectangle x1="6.5875" y1="8.3875" x2="6.9375" y2="8.4125" layer="200"/>
<rectangle x1="7.0625" y1="8.3875" x2="8.1625" y2="8.4125" layer="200"/>
<rectangle x1="10.4875" y1="8.3875" x2="10.6625" y2="8.4125" layer="200"/>
<rectangle x1="15.8375" y1="8.3875" x2="16.4625" y2="8.4125" layer="200"/>
<rectangle x1="22.0875" y1="8.3875" x2="22.4375" y2="8.4125" layer="200"/>
<rectangle x1="4.8625" y1="8.4125" x2="5.9375" y2="8.4375" layer="200"/>
<rectangle x1="6.6125" y1="8.4125" x2="6.9875" y2="8.4375" layer="200"/>
<rectangle x1="7.0625" y1="8.4125" x2="8.2375" y2="8.4375" layer="200"/>
<rectangle x1="10.4875" y1="8.4125" x2="10.6375" y2="8.4375" layer="200"/>
<rectangle x1="15.9375" y1="8.4125" x2="16.4625" y2="8.4375" layer="200"/>
<rectangle x1="22.1375" y1="8.4125" x2="22.4875" y2="8.4375" layer="200"/>
<rectangle x1="4.9125" y1="8.4375" x2="5.9125" y2="8.4625" layer="200"/>
<rectangle x1="6.6375" y1="8.4375" x2="8.3125" y2="8.4625" layer="200"/>
<rectangle x1="10.4875" y1="8.4375" x2="10.6375" y2="8.4625" layer="200"/>
<rectangle x1="16.0375" y1="8.4375" x2="16.4375" y2="8.4625" layer="200"/>
<rectangle x1="22.1625" y1="8.4375" x2="22.5125" y2="8.4625" layer="200"/>
<rectangle x1="4.9375" y1="8.4625" x2="5.9125" y2="8.4875" layer="200"/>
<rectangle x1="6.6625" y1="8.4625" x2="8.3875" y2="8.4875" layer="200"/>
<rectangle x1="10.4625" y1="8.4625" x2="10.6375" y2="8.4875" layer="200"/>
<rectangle x1="16.1375" y1="8.4625" x2="16.4375" y2="8.4875" layer="200"/>
<rectangle x1="22.2125" y1="8.4625" x2="22.5375" y2="8.4875" layer="200"/>
<rectangle x1="4.9875" y1="8.4875" x2="5.8875" y2="8.5125" layer="200"/>
<rectangle x1="6.6875" y1="8.4875" x2="8.4625" y2="8.5125" layer="200"/>
<rectangle x1="10.4625" y1="8.4875" x2="10.6375" y2="8.5125" layer="200"/>
<rectangle x1="16.1875" y1="8.4875" x2="16.4125" y2="8.5125" layer="200"/>
<rectangle x1="22.2375" y1="8.4875" x2="22.5875" y2="8.5125" layer="200"/>
<rectangle x1="5.0125" y1="8.5125" x2="5.8875" y2="8.5375" layer="200"/>
<rectangle x1="6.7125" y1="8.5125" x2="8.5625" y2="8.5375" layer="200"/>
<rectangle x1="10.4625" y1="8.5125" x2="10.6375" y2="8.5375" layer="200"/>
<rectangle x1="16.1875" y1="8.5125" x2="16.3875" y2="8.5375" layer="200"/>
<rectangle x1="22.2875" y1="8.5125" x2="22.6125" y2="8.5375" layer="200"/>
<rectangle x1="5.0625" y1="8.5375" x2="5.8625" y2="8.5625" layer="200"/>
<rectangle x1="6.7625" y1="8.5375" x2="8.6375" y2="8.5625" layer="200"/>
<rectangle x1="10.4625" y1="8.5375" x2="10.6375" y2="8.5625" layer="200"/>
<rectangle x1="16.1875" y1="8.5375" x2="16.3875" y2="8.5625" layer="200"/>
<rectangle x1="22.3375" y1="8.5375" x2="22.6375" y2="8.5625" layer="200"/>
<rectangle x1="5.0875" y1="8.5625" x2="5.8625" y2="8.5875" layer="200"/>
<rectangle x1="6.7875" y1="8.5625" x2="8.7125" y2="8.5875" layer="200"/>
<rectangle x1="10.4625" y1="8.5625" x2="10.6375" y2="8.5875" layer="200"/>
<rectangle x1="16.1875" y1="8.5625" x2="16.3625" y2="8.5875" layer="200"/>
<rectangle x1="22.3625" y1="8.5625" x2="22.6625" y2="8.5875" layer="200"/>
<rectangle x1="5.1375" y1="8.5875" x2="5.8375" y2="8.6125" layer="200"/>
<rectangle x1="6.8125" y1="8.5875" x2="7.7125" y2="8.6125" layer="200"/>
<rectangle x1="7.8625" y1="8.5875" x2="8.7875" y2="8.6125" layer="200"/>
<rectangle x1="10.4625" y1="8.5875" x2="10.6375" y2="8.6125" layer="200"/>
<rectangle x1="16.1625" y1="8.5875" x2="16.3625" y2="8.6125" layer="200"/>
<rectangle x1="22.4125" y1="8.5875" x2="22.6875" y2="8.6125" layer="200"/>
<rectangle x1="5.1875" y1="8.6125" x2="5.8375" y2="8.6375" layer="200"/>
<rectangle x1="6.8375" y1="8.6125" x2="7.6625" y2="8.6375" layer="200"/>
<rectangle x1="7.9625" y1="8.6125" x2="8.8625" y2="8.6375" layer="200"/>
<rectangle x1="10.4625" y1="8.6125" x2="10.6375" y2="8.6375" layer="200"/>
<rectangle x1="16.1375" y1="8.6125" x2="16.3375" y2="8.6375" layer="200"/>
<rectangle x1="22.4375" y1="8.6125" x2="22.7375" y2="8.6375" layer="200"/>
<rectangle x1="5.2375" y1="8.6375" x2="5.8375" y2="8.6625" layer="200"/>
<rectangle x1="6.8625" y1="8.6375" x2="7.6125" y2="8.6625" layer="200"/>
<rectangle x1="8.0375" y1="8.6375" x2="8.9625" y2="8.6625" layer="200"/>
<rectangle x1="10.4625" y1="8.6375" x2="10.6375" y2="8.6625" layer="200"/>
<rectangle x1="16.1125" y1="8.6375" x2="16.3125" y2="8.6625" layer="200"/>
<rectangle x1="22.4875" y1="8.6375" x2="22.7625" y2="8.6625" layer="200"/>
<rectangle x1="5.2875" y1="8.6625" x2="5.8125" y2="8.6875" layer="200"/>
<rectangle x1="6.8875" y1="8.6625" x2="7.5625" y2="8.6875" layer="200"/>
<rectangle x1="8.1375" y1="8.6625" x2="9.0375" y2="8.6875" layer="200"/>
<rectangle x1="10.4625" y1="8.6625" x2="10.6375" y2="8.6875" layer="200"/>
<rectangle x1="16.1125" y1="8.6625" x2="16.3125" y2="8.6875" layer="200"/>
<rectangle x1="22.5125" y1="8.6625" x2="22.7875" y2="8.6875" layer="200"/>
<rectangle x1="5.3375" y1="8.6875" x2="5.8125" y2="8.7125" layer="200"/>
<rectangle x1="6.8625" y1="8.6875" x2="7.5375" y2="8.7125" layer="200"/>
<rectangle x1="8.2125" y1="8.6875" x2="9.1125" y2="8.7125" layer="200"/>
<rectangle x1="10.4625" y1="8.6875" x2="10.6375" y2="8.7125" layer="200"/>
<rectangle x1="16.0875" y1="8.6875" x2="16.2875" y2="8.7125" layer="200"/>
<rectangle x1="22.5125" y1="8.6875" x2="22.8375" y2="8.7125" layer="200"/>
<rectangle x1="5.3375" y1="8.7125" x2="5.9125" y2="8.7375" layer="200"/>
<rectangle x1="6.8375" y1="8.7125" x2="7.5125" y2="8.7375" layer="200"/>
<rectangle x1="8.2875" y1="8.7125" x2="9.1875" y2="8.7375" layer="200"/>
<rectangle x1="10.4625" y1="8.7125" x2="10.6375" y2="8.7375" layer="200"/>
<rectangle x1="16.0875" y1="8.7125" x2="16.2875" y2="8.7375" layer="200"/>
<rectangle x1="22.5125" y1="8.7125" x2="22.8625" y2="8.7375" layer="200"/>
<rectangle x1="5.3125" y1="8.7375" x2="6.0625" y2="8.7625" layer="200"/>
<rectangle x1="6.7875" y1="8.7375" x2="7.4875" y2="8.7625" layer="200"/>
<rectangle x1="8.3375" y1="8.7375" x2="9.2625" y2="8.7625" layer="200"/>
<rectangle x1="10.4625" y1="8.7375" x2="10.6375" y2="8.7625" layer="200"/>
<rectangle x1="16.0625" y1="8.7375" x2="16.2625" y2="8.7625" layer="200"/>
<rectangle x1="22.4875" y1="8.7375" x2="22.8875" y2="8.7625" layer="200"/>
<rectangle x1="5.3125" y1="8.7625" x2="6.3125" y2="8.7875" layer="200"/>
<rectangle x1="6.7125" y1="8.7625" x2="7.4375" y2="8.7875" layer="200"/>
<rectangle x1="8.4125" y1="8.7625" x2="9.3625" y2="8.7875" layer="200"/>
<rectangle x1="10.4625" y1="8.7625" x2="10.6375" y2="8.7875" layer="200"/>
<rectangle x1="16.0625" y1="8.7625" x2="16.2375" y2="8.7875" layer="200"/>
<rectangle x1="22.4625" y1="8.7625" x2="22.9375" y2="8.7875" layer="200"/>
<rectangle x1="5.2875" y1="8.7875" x2="7.4125" y2="8.8125" layer="200"/>
<rectangle x1="8.4625" y1="8.7875" x2="9.4375" y2="8.8125" layer="200"/>
<rectangle x1="10.4625" y1="8.7875" x2="10.6625" y2="8.8125" layer="200"/>
<rectangle x1="16.0375" y1="8.7875" x2="16.2375" y2="8.8125" layer="200"/>
<rectangle x1="22.4625" y1="8.7875" x2="22.9625" y2="8.8125" layer="200"/>
<rectangle x1="5.2625" y1="8.8125" x2="7.3875" y2="8.8375" layer="200"/>
<rectangle x1="8.5375" y1="8.8125" x2="9.5125" y2="8.8375" layer="200"/>
<rectangle x1="10.4625" y1="8.8125" x2="10.6625" y2="8.8375" layer="200"/>
<rectangle x1="16.0375" y1="8.8125" x2="16.2125" y2="8.8375" layer="200"/>
<rectangle x1="22.4375" y1="8.8125" x2="22.9875" y2="8.8375" layer="200"/>
<rectangle x1="5.2375" y1="8.8375" x2="7.3875" y2="8.8625" layer="200"/>
<rectangle x1="8.5875" y1="8.8375" x2="9.5875" y2="8.8625" layer="200"/>
<rectangle x1="10.4625" y1="8.8375" x2="10.6625" y2="8.8625" layer="200"/>
<rectangle x1="16.0125" y1="8.8375" x2="16.2125" y2="8.8625" layer="200"/>
<rectangle x1="22.4125" y1="8.8375" x2="23.0125" y2="8.8625" layer="200"/>
<rectangle x1="5.2375" y1="8.8625" x2="7.3875" y2="8.8875" layer="200"/>
<rectangle x1="8.6625" y1="8.8625" x2="9.6625" y2="8.8875" layer="200"/>
<rectangle x1="10.4875" y1="8.8625" x2="10.6625" y2="8.8875" layer="200"/>
<rectangle x1="16.0125" y1="8.8625" x2="16.1875" y2="8.8875" layer="200"/>
<rectangle x1="20.0875" y1="8.8625" x2="20.2875" y2="8.8875" layer="200"/>
<rectangle x1="22.4125" y1="8.8625" x2="22.6375" y2="8.8875" layer="200"/>
<rectangle x1="22.7125" y1="8.8625" x2="23.0375" y2="8.8875" layer="200"/>
<rectangle x1="5.2625" y1="8.8875" x2="7.4375" y2="8.9125" layer="200"/>
<rectangle x1="8.7375" y1="8.8875" x2="9.7625" y2="8.9125" layer="200"/>
<rectangle x1="10.4875" y1="8.8875" x2="10.6625" y2="8.9125" layer="200"/>
<rectangle x1="15.9875" y1="8.8875" x2="16.1875" y2="8.9125" layer="200"/>
<rectangle x1="20.0625" y1="8.8875" x2="20.4375" y2="8.9125" layer="200"/>
<rectangle x1="22.3875" y1="8.8875" x2="22.6125" y2="8.9125" layer="200"/>
<rectangle x1="22.7375" y1="8.8875" x2="23.0875" y2="8.9125" layer="200"/>
<rectangle x1="5.2875" y1="8.9125" x2="7.4875" y2="8.9375" layer="200"/>
<rectangle x1="8.8125" y1="8.9125" x2="9.8375" y2="8.9375" layer="200"/>
<rectangle x1="10.4875" y1="8.9125" x2="10.6875" y2="8.9375" layer="200"/>
<rectangle x1="15.9875" y1="8.9125" x2="16.1625" y2="8.9375" layer="200"/>
<rectangle x1="20.0375" y1="8.9125" x2="20.5875" y2="8.9375" layer="200"/>
<rectangle x1="22.3625" y1="8.9125" x2="22.5875" y2="8.9375" layer="200"/>
<rectangle x1="22.7625" y1="8.9125" x2="23.1125" y2="8.9375" layer="200"/>
<rectangle x1="5.3375" y1="8.9375" x2="7.5625" y2="8.9625" layer="200"/>
<rectangle x1="8.8875" y1="8.9375" x2="9.9125" y2="8.9625" layer="200"/>
<rectangle x1="10.4875" y1="8.9375" x2="10.6875" y2="8.9625" layer="200"/>
<rectangle x1="15.9625" y1="8.9375" x2="16.1625" y2="8.9625" layer="200"/>
<rectangle x1="17.6375" y1="8.9375" x2="17.7875" y2="8.9625" layer="200"/>
<rectangle x1="20.0125" y1="8.9375" x2="20.7125" y2="8.9625" layer="200"/>
<rectangle x1="22.3625" y1="8.9375" x2="22.5625" y2="8.9625" layer="200"/>
<rectangle x1="22.7875" y1="8.9375" x2="23.1375" y2="8.9625" layer="200"/>
<rectangle x1="5.4125" y1="8.9625" x2="7.6125" y2="8.9875" layer="200"/>
<rectangle x1="8.9625" y1="8.9625" x2="9.9875" y2="8.9875" layer="200"/>
<rectangle x1="10.4875" y1="8.9625" x2="10.6875" y2="8.9875" layer="200"/>
<rectangle x1="15.9625" y1="8.9625" x2="16.1375" y2="8.9875" layer="200"/>
<rectangle x1="17.6125" y1="8.9625" x2="17.8375" y2="8.9875" layer="200"/>
<rectangle x1="19.9875" y1="8.9625" x2="20.8625" y2="8.9875" layer="200"/>
<rectangle x1="22.3375" y1="8.9625" x2="22.5375" y2="8.9875" layer="200"/>
<rectangle x1="22.8375" y1="8.9625" x2="23.1625" y2="8.9875" layer="200"/>
<rectangle x1="5.4875" y1="8.9875" x2="7.6875" y2="9.0125" layer="200"/>
<rectangle x1="9.0375" y1="8.9875" x2="10.0625" y2="9.0125" layer="200"/>
<rectangle x1="10.4875" y1="8.9875" x2="10.6875" y2="9.0125" layer="200"/>
<rectangle x1="15.9375" y1="8.9875" x2="16.1375" y2="9.0125" layer="200"/>
<rectangle x1="17.5875" y1="8.9875" x2="17.9125" y2="9.0125" layer="200"/>
<rectangle x1="19.9625" y1="8.9875" x2="20.9875" y2="9.0125" layer="200"/>
<rectangle x1="22.3375" y1="8.9875" x2="22.5125" y2="9.0125" layer="200"/>
<rectangle x1="22.8625" y1="8.9875" x2="23.1875" y2="9.0125" layer="200"/>
<rectangle x1="5.5625" y1="9.0125" x2="7.0625" y2="9.0375" layer="200"/>
<rectangle x1="7.2125" y1="9.0125" x2="7.7375" y2="9.0375" layer="200"/>
<rectangle x1="9.1125" y1="9.0125" x2="10.1125" y2="9.0375" layer="200"/>
<rectangle x1="10.4875" y1="9.0125" x2="10.7125" y2="9.0375" layer="200"/>
<rectangle x1="15.9375" y1="9.0125" x2="16.1125" y2="9.0375" layer="200"/>
<rectangle x1="17.5625" y1="9.0125" x2="17.9625" y2="9.0375" layer="200"/>
<rectangle x1="19.9625" y1="9.0125" x2="21.1125" y2="9.0375" layer="200"/>
<rectangle x1="22.3125" y1="9.0125" x2="22.4875" y2="9.0375" layer="200"/>
<rectangle x1="22.8875" y1="9.0125" x2="23.2125" y2="9.0375" layer="200"/>
<rectangle x1="5.6625" y1="9.0375" x2="7.0125" y2="9.0625" layer="200"/>
<rectangle x1="7.2875" y1="9.0375" x2="7.8125" y2="9.0625" layer="200"/>
<rectangle x1="9.1875" y1="9.0375" x2="10.1375" y2="9.0625" layer="200"/>
<rectangle x1="10.5125" y1="9.0375" x2="10.7125" y2="9.0625" layer="200"/>
<rectangle x1="15.9375" y1="9.0375" x2="16.1125" y2="9.0625" layer="200"/>
<rectangle x1="17.5625" y1="9.0375" x2="18.0125" y2="9.0625" layer="200"/>
<rectangle x1="19.9375" y1="9.0375" x2="21.2375" y2="9.0625" layer="200"/>
<rectangle x1="22.3125" y1="9.0375" x2="22.4875" y2="9.0625" layer="200"/>
<rectangle x1="22.9125" y1="9.0375" x2="23.2375" y2="9.0625" layer="200"/>
<rectangle x1="5.7875" y1="9.0625" x2="6.9625" y2="9.0875" layer="200"/>
<rectangle x1="7.3375" y1="9.0625" x2="7.8875" y2="9.0875" layer="200"/>
<rectangle x1="9.2625" y1="9.0625" x2="10.1625" y2="9.0875" layer="200"/>
<rectangle x1="10.5125" y1="9.0625" x2="10.7125" y2="9.0875" layer="200"/>
<rectangle x1="15.9125" y1="9.0625" x2="16.0875" y2="9.0875" layer="200"/>
<rectangle x1="17.5375" y1="9.0625" x2="18.0625" y2="9.0875" layer="200"/>
<rectangle x1="19.9125" y1="9.0625" x2="21.3375" y2="9.0875" layer="200"/>
<rectangle x1="22.2875" y1="9.0625" x2="22.4625" y2="9.0875" layer="200"/>
<rectangle x1="22.9375" y1="9.0625" x2="23.2625" y2="9.0875" layer="200"/>
<rectangle x1="5.9375" y1="9.0875" x2="6.9125" y2="9.1125" layer="200"/>
<rectangle x1="7.3875" y1="9.0875" x2="7.9625" y2="9.1125" layer="200"/>
<rectangle x1="9.3375" y1="9.0875" x2="10.1875" y2="9.1125" layer="200"/>
<rectangle x1="10.5125" y1="9.0875" x2="10.7125" y2="9.1125" layer="200"/>
<rectangle x1="15.9125" y1="9.0875" x2="16.0875" y2="9.1125" layer="200"/>
<rectangle x1="17.5125" y1="9.0875" x2="18.0875" y2="9.1125" layer="200"/>
<rectangle x1="19.9125" y1="9.0875" x2="20.2875" y2="9.1125" layer="200"/>
<rectangle x1="20.3875" y1="9.0875" x2="21.4625" y2="9.1125" layer="200"/>
<rectangle x1="22.2875" y1="9.0875" x2="22.4625" y2="9.1125" layer="200"/>
<rectangle x1="22.9875" y1="9.0875" x2="23.2875" y2="9.1125" layer="200"/>
<rectangle x1="6.0625" y1="9.1125" x2="6.8625" y2="9.1375" layer="200"/>
<rectangle x1="7.4375" y1="9.1125" x2="7.9625" y2="9.1375" layer="200"/>
<rectangle x1="9.4125" y1="9.1125" x2="10.1875" y2="9.1375" layer="200"/>
<rectangle x1="10.5125" y1="9.1125" x2="10.7375" y2="9.1375" layer="200"/>
<rectangle x1="15.9125" y1="9.1125" x2="16.0875" y2="9.1375" layer="200"/>
<rectangle x1="17.5125" y1="9.1125" x2="18.1125" y2="9.1375" layer="200"/>
<rectangle x1="19.8875" y1="9.1125" x2="20.1875" y2="9.1375" layer="200"/>
<rectangle x1="20.5875" y1="9.1125" x2="21.5625" y2="9.1375" layer="200"/>
<rectangle x1="22.2625" y1="9.1125" x2="22.4375" y2="9.1375" layer="200"/>
<rectangle x1="23.0125" y1="9.1125" x2="23.3125" y2="9.1375" layer="200"/>
<rectangle x1="6.1625" y1="9.1375" x2="6.8125" y2="9.1625" layer="200"/>
<rectangle x1="7.4625" y1="9.1375" x2="7.9375" y2="9.1625" layer="200"/>
<rectangle x1="9.4875" y1="9.1375" x2="10.1875" y2="9.1625" layer="200"/>
<rectangle x1="10.5375" y1="9.1375" x2="10.7375" y2="9.1625" layer="200"/>
<rectangle x1="15.8875" y1="9.1375" x2="16.0625" y2="9.1625" layer="200"/>
<rectangle x1="17.4875" y1="9.1375" x2="18.1125" y2="9.1625" layer="200"/>
<rectangle x1="19.8625" y1="9.1375" x2="20.1375" y2="9.1625" layer="200"/>
<rectangle x1="20.7375" y1="9.1375" x2="21.6125" y2="9.1625" layer="200"/>
<rectangle x1="22.2625" y1="9.1375" x2="22.4375" y2="9.1625" layer="200"/>
<rectangle x1="23.0375" y1="9.1375" x2="23.3375" y2="9.1625" layer="200"/>
<rectangle x1="6.1375" y1="9.1625" x2="6.7625" y2="9.1875" layer="200"/>
<rectangle x1="7.4625" y1="9.1625" x2="7.9125" y2="9.1875" layer="200"/>
<rectangle x1="9.5625" y1="9.1625" x2="10.1875" y2="9.1875" layer="200"/>
<rectangle x1="10.5375" y1="9.1625" x2="10.7375" y2="9.1875" layer="200"/>
<rectangle x1="15.8875" y1="9.1625" x2="16.0625" y2="9.1875" layer="200"/>
<rectangle x1="17.4875" y1="9.1625" x2="18.1125" y2="9.1875" layer="200"/>
<rectangle x1="19.8625" y1="9.1625" x2="20.1125" y2="9.1875" layer="200"/>
<rectangle x1="20.8625" y1="9.1625" x2="21.6125" y2="9.1875" layer="200"/>
<rectangle x1="22.2375" y1="9.1625" x2="22.4125" y2="9.1875" layer="200"/>
<rectangle x1="23.0625" y1="9.1625" x2="23.3625" y2="9.1875" layer="200"/>
<rectangle x1="6.1125" y1="9.1875" x2="6.7375" y2="9.2125" layer="200"/>
<rectangle x1="7.4625" y1="9.1875" x2="7.8875" y2="9.2125" layer="200"/>
<rectangle x1="9.6125" y1="9.1875" x2="10.1875" y2="9.2125" layer="200"/>
<rectangle x1="10.5375" y1="9.1875" x2="10.7875" y2="9.2125" layer="200"/>
<rectangle x1="15.8875" y1="9.1875" x2="16.0625" y2="9.2125" layer="200"/>
<rectangle x1="17.4625" y1="9.1875" x2="18.0875" y2="9.2125" layer="200"/>
<rectangle x1="19.8375" y1="9.1875" x2="20.0875" y2="9.2125" layer="200"/>
<rectangle x1="20.9875" y1="9.1875" x2="21.6125" y2="9.2125" layer="200"/>
<rectangle x1="22.2375" y1="9.1875" x2="22.4125" y2="9.2125" layer="200"/>
<rectangle x1="23.0875" y1="9.1875" x2="23.3625" y2="9.2125" layer="200"/>
<rectangle x1="6.0875" y1="9.2125" x2="6.6875" y2="9.2375" layer="200"/>
<rectangle x1="7.4625" y1="9.2125" x2="7.8625" y2="9.2375" layer="200"/>
<rectangle x1="9.6875" y1="9.2125" x2="10.1875" y2="9.2375" layer="200"/>
<rectangle x1="10.5375" y1="9.2125" x2="10.8375" y2="9.2375" layer="200"/>
<rectangle x1="15.8875" y1="9.2125" x2="16.0375" y2="9.2375" layer="200"/>
<rectangle x1="17.4625" y1="9.2125" x2="18.0875" y2="9.2375" layer="200"/>
<rectangle x1="19.8375" y1="9.2125" x2="20.0625" y2="9.2375" layer="200"/>
<rectangle x1="21.1125" y1="9.2125" x2="21.6125" y2="9.2375" layer="200"/>
<rectangle x1="22.2375" y1="9.2125" x2="22.4125" y2="9.2375" layer="200"/>
<rectangle x1="23.1125" y1="9.2125" x2="23.3875" y2="9.2375" layer="200"/>
<rectangle x1="6.0625" y1="9.2375" x2="6.6375" y2="9.2625" layer="200"/>
<rectangle x1="7.4375" y1="9.2375" x2="7.8375" y2="9.2625" layer="200"/>
<rectangle x1="9.7625" y1="9.2375" x2="10.1875" y2="9.2625" layer="200"/>
<rectangle x1="10.5625" y1="9.2375" x2="10.9125" y2="9.2625" layer="200"/>
<rectangle x1="15.8875" y1="9.2375" x2="16.0375" y2="9.2625" layer="200"/>
<rectangle x1="17.4375" y1="9.2375" x2="18.0625" y2="9.2625" layer="200"/>
<rectangle x1="19.8125" y1="9.2375" x2="20.0375" y2="9.2625" layer="200"/>
<rectangle x1="21.2125" y1="9.2375" x2="21.6125" y2="9.2625" layer="200"/>
<rectangle x1="22.2125" y1="9.2375" x2="22.3875" y2="9.2625" layer="200"/>
<rectangle x1="23.1375" y1="9.2375" x2="23.4125" y2="9.2625" layer="200"/>
<rectangle x1="6.0125" y1="9.2625" x2="6.6125" y2="9.2875" layer="200"/>
<rectangle x1="7.4125" y1="9.2625" x2="7.8125" y2="9.2875" layer="200"/>
<rectangle x1="9.8125" y1="9.2625" x2="10.1875" y2="9.2875" layer="200"/>
<rectangle x1="10.5625" y1="9.2625" x2="10.9875" y2="9.2875" layer="200"/>
<rectangle x1="15.8625" y1="9.2625" x2="16.0375" y2="9.2875" layer="200"/>
<rectangle x1="17.4375" y1="9.2625" x2="18.0625" y2="9.2875" layer="200"/>
<rectangle x1="19.8125" y1="9.2625" x2="20.0125" y2="9.2875" layer="200"/>
<rectangle x1="21.3375" y1="9.2625" x2="21.6125" y2="9.2875" layer="200"/>
<rectangle x1="22.2125" y1="9.2625" x2="22.3875" y2="9.2875" layer="200"/>
<rectangle x1="23.1375" y1="9.2625" x2="23.4375" y2="9.2875" layer="200"/>
<rectangle x1="5.9875" y1="9.2875" x2="6.5625" y2="9.3125" layer="200"/>
<rectangle x1="7.3875" y1="9.2875" x2="7.7875" y2="9.3125" layer="200"/>
<rectangle x1="9.8625" y1="9.2875" x2="10.1875" y2="9.3125" layer="200"/>
<rectangle x1="10.5625" y1="9.2875" x2="11.0875" y2="9.3125" layer="200"/>
<rectangle x1="15.8625" y1="9.2875" x2="16.0375" y2="9.3125" layer="200"/>
<rectangle x1="17.4375" y1="9.2875" x2="18.0375" y2="9.3125" layer="200"/>
<rectangle x1="19.7875" y1="9.2875" x2="20.0125" y2="9.3125" layer="200"/>
<rectangle x1="21.4125" y1="9.2875" x2="21.6125" y2="9.3125" layer="200"/>
<rectangle x1="22.2125" y1="9.2875" x2="22.3875" y2="9.3125" layer="200"/>
<rectangle x1="23.1625" y1="9.2875" x2="23.4625" y2="9.3125" layer="200"/>
<rectangle x1="5.9375" y1="9.3125" x2="6.5375" y2="9.3375" layer="200"/>
<rectangle x1="7.3625" y1="9.3125" x2="7.7625" y2="9.3375" layer="200"/>
<rectangle x1="9.9125" y1="9.3125" x2="10.1875" y2="9.3375" layer="200"/>
<rectangle x1="10.5875" y1="9.3125" x2="11.1875" y2="9.3375" layer="200"/>
<rectangle x1="15.8625" y1="9.3125" x2="16.0375" y2="9.3375" layer="200"/>
<rectangle x1="17.4125" y1="9.3125" x2="18.0125" y2="9.3375" layer="200"/>
<rectangle x1="19.7875" y1="9.3125" x2="19.9875" y2="9.3375" layer="200"/>
<rectangle x1="21.4375" y1="9.3125" x2="21.6125" y2="9.3375" layer="200"/>
<rectangle x1="22.1875" y1="9.3125" x2="22.3625" y2="9.3375" layer="200"/>
<rectangle x1="23.1875" y1="9.3125" x2="23.4875" y2="9.3375" layer="200"/>
<rectangle x1="5.9125" y1="9.3375" x2="6.5125" y2="9.3625" layer="200"/>
<rectangle x1="7.3375" y1="9.3375" x2="7.7375" y2="9.3625" layer="200"/>
<rectangle x1="9.9125" y1="9.3375" x2="10.1625" y2="9.3625" layer="200"/>
<rectangle x1="10.5875" y1="9.3375" x2="11.2875" y2="9.3625" layer="200"/>
<rectangle x1="15.8625" y1="9.3375" x2="16.0375" y2="9.3625" layer="200"/>
<rectangle x1="17.4375" y1="9.3375" x2="18.0125" y2="9.3625" layer="200"/>
<rectangle x1="19.7625" y1="9.3375" x2="19.9625" y2="9.3625" layer="200"/>
<rectangle x1="21.4375" y1="9.3375" x2="21.6125" y2="9.3625" layer="200"/>
<rectangle x1="22.1875" y1="9.3375" x2="22.3625" y2="9.3625" layer="200"/>
<rectangle x1="23.2125" y1="9.3375" x2="23.4875" y2="9.3625" layer="200"/>
<rectangle x1="5.8625" y1="9.3625" x2="6.4875" y2="9.3875" layer="200"/>
<rectangle x1="7.3125" y1="9.3625" x2="7.7125" y2="9.3875" layer="200"/>
<rectangle x1="9.8125" y1="9.3625" x2="10.1625" y2="9.3875" layer="200"/>
<rectangle x1="10.6125" y1="9.3625" x2="11.3625" y2="9.3875" layer="200"/>
<rectangle x1="15.8625" y1="9.3625" x2="16.0375" y2="9.3875" layer="200"/>
<rectangle x1="17.4375" y1="9.3625" x2="17.9875" y2="9.3875" layer="200"/>
<rectangle x1="18.4875" y1="9.3625" x2="18.5625" y2="9.3875" layer="200"/>
<rectangle x1="19.7625" y1="9.3625" x2="19.9625" y2="9.3875" layer="200"/>
<rectangle x1="21.4375" y1="9.3625" x2="21.6125" y2="9.3875" layer="200"/>
<rectangle x1="22.1875" y1="9.3625" x2="22.3625" y2="9.3875" layer="200"/>
<rectangle x1="23.2375" y1="9.3625" x2="23.5125" y2="9.3875" layer="200"/>
<rectangle x1="5.8375" y1="9.3875" x2="6.4375" y2="9.4125" layer="200"/>
<rectangle x1="7.2875" y1="9.3875" x2="7.6875" y2="9.4125" layer="200"/>
<rectangle x1="9.7375" y1="9.3875" x2="10.1375" y2="9.4125" layer="200"/>
<rectangle x1="10.6125" y1="9.3875" x2="11.4375" y2="9.4125" layer="200"/>
<rectangle x1="15.8625" y1="9.3875" x2="16.0375" y2="9.4125" layer="200"/>
<rectangle x1="17.4625" y1="9.3875" x2="17.9625" y2="9.4125" layer="200"/>
<rectangle x1="18.4625" y1="9.3875" x2="18.5875" y2="9.4125" layer="200"/>
<rectangle x1="19.7625" y1="9.3875" x2="19.9375" y2="9.4125" layer="200"/>
<rectangle x1="21.4375" y1="9.3875" x2="21.6125" y2="9.4125" layer="200"/>
<rectangle x1="22.1875" y1="9.3875" x2="22.3375" y2="9.4125" layer="200"/>
<rectangle x1="23.2375" y1="9.3875" x2="23.5375" y2="9.4125" layer="200"/>
<rectangle x1="5.7875" y1="9.4125" x2="6.4125" y2="9.4375" layer="200"/>
<rectangle x1="7.2625" y1="9.4125" x2="7.6625" y2="9.4375" layer="200"/>
<rectangle x1="9.6875" y1="9.4125" x2="10.1125" y2="9.4375" layer="200"/>
<rectangle x1="10.6125" y1="9.4125" x2="11.5375" y2="9.4375" layer="200"/>
<rectangle x1="15.8625" y1="9.4125" x2="16.0375" y2="9.4375" layer="200"/>
<rectangle x1="17.4875" y1="9.4125" x2="17.9375" y2="9.4375" layer="200"/>
<rectangle x1="18.4375" y1="9.4125" x2="18.6125" y2="9.4375" layer="200"/>
<rectangle x1="19.7625" y1="9.4125" x2="19.9375" y2="9.4375" layer="200"/>
<rectangle x1="21.4375" y1="9.4125" x2="21.6125" y2="9.4375" layer="200"/>
<rectangle x1="22.1625" y1="9.4125" x2="22.3375" y2="9.4375" layer="200"/>
<rectangle x1="23.2625" y1="9.4125" x2="23.5625" y2="9.4375" layer="200"/>
<rectangle x1="5.7375" y1="9.4375" x2="6.3875" y2="9.4625" layer="200"/>
<rectangle x1="7.2375" y1="9.4375" x2="7.6375" y2="9.4625" layer="200"/>
<rectangle x1="9.6125" y1="9.4375" x2="10.0625" y2="9.4625" layer="200"/>
<rectangle x1="10.6375" y1="9.4375" x2="11.6125" y2="9.4625" layer="200"/>
<rectangle x1="15.8625" y1="9.4375" x2="16.0125" y2="9.4625" layer="200"/>
<rectangle x1="17.5125" y1="9.4375" x2="17.9375" y2="9.4625" layer="200"/>
<rectangle x1="18.4125" y1="9.4375" x2="18.6375" y2="9.4625" layer="200"/>
<rectangle x1="19.7625" y1="9.4375" x2="19.9375" y2="9.4625" layer="200"/>
<rectangle x1="21.4375" y1="9.4375" x2="21.5875" y2="9.4625" layer="200"/>
<rectangle x1="22.1625" y1="9.4375" x2="22.3375" y2="9.4625" layer="200"/>
<rectangle x1="23.2875" y1="9.4375" x2="23.5625" y2="9.4625" layer="200"/>
<rectangle x1="5.7125" y1="9.4625" x2="6.3125" y2="9.4875" layer="200"/>
<rectangle x1="7.1875" y1="9.4625" x2="7.6125" y2="9.4875" layer="200"/>
<rectangle x1="9.5625" y1="9.4625" x2="9.9875" y2="9.4875" layer="200"/>
<rectangle x1="10.6625" y1="9.4625" x2="11.7125" y2="9.4875" layer="200"/>
<rectangle x1="15.8625" y1="9.4625" x2="16.0125" y2="9.4875" layer="200"/>
<rectangle x1="17.5625" y1="9.4625" x2="17.9125" y2="9.4875" layer="200"/>
<rectangle x1="18.3625" y1="9.4625" x2="18.6375" y2="9.4875" layer="200"/>
<rectangle x1="19.7625" y1="9.4625" x2="19.9375" y2="9.4875" layer="200"/>
<rectangle x1="21.4375" y1="9.4625" x2="21.5875" y2="9.4875" layer="200"/>
<rectangle x1="22.1625" y1="9.4625" x2="22.3375" y2="9.4875" layer="200"/>
<rectangle x1="23.2875" y1="9.4625" x2="23.5875" y2="9.4875" layer="200"/>
<rectangle x1="5.6625" y1="9.4875" x2="6.2625" y2="9.5125" layer="200"/>
<rectangle x1="7.1625" y1="9.4875" x2="7.5875" y2="9.5125" layer="200"/>
<rectangle x1="9.5125" y1="9.4875" x2="9.9125" y2="9.5125" layer="200"/>
<rectangle x1="10.6625" y1="9.4875" x2="11.8125" y2="9.5125" layer="200"/>
<rectangle x1="15.8625" y1="9.4875" x2="16.0125" y2="9.5125" layer="200"/>
<rectangle x1="17.5875" y1="9.4875" x2="17.8875" y2="9.5125" layer="200"/>
<rectangle x1="18.3375" y1="9.4875" x2="18.6625" y2="9.5125" layer="200"/>
<rectangle x1="19.7625" y1="9.4875" x2="19.9375" y2="9.5125" layer="200"/>
<rectangle x1="21.4375" y1="9.4875" x2="21.5875" y2="9.5125" layer="200"/>
<rectangle x1="22.1625" y1="9.4875" x2="22.3375" y2="9.5125" layer="200"/>
<rectangle x1="23.3125" y1="9.4875" x2="23.6125" y2="9.5125" layer="200"/>
<rectangle x1="5.6125" y1="9.5125" x2="6.2125" y2="9.5375" layer="200"/>
<rectangle x1="7.1375" y1="9.5125" x2="7.5625" y2="9.5375" layer="200"/>
<rectangle x1="9.4625" y1="9.5125" x2="9.8375" y2="9.5375" layer="200"/>
<rectangle x1="10.6875" y1="9.5125" x2="10.8875" y2="9.5375" layer="200"/>
<rectangle x1="11.0625" y1="9.5125" x2="11.9125" y2="9.5375" layer="200"/>
<rectangle x1="15.8625" y1="9.5125" x2="16.0125" y2="9.5375" layer="200"/>
<rectangle x1="17.6375" y1="9.5125" x2="17.8625" y2="9.5375" layer="200"/>
<rectangle x1="18.3125" y1="9.5125" x2="18.6875" y2="9.5375" layer="200"/>
<rectangle x1="19.7625" y1="9.5125" x2="19.9125" y2="9.5375" layer="200"/>
<rectangle x1="21.4375" y1="9.5125" x2="21.5875" y2="9.5375" layer="200"/>
<rectangle x1="22.1625" y1="9.5125" x2="22.3375" y2="9.5375" layer="200"/>
<rectangle x1="23.3375" y1="9.5125" x2="23.6375" y2="9.5375" layer="200"/>
<rectangle x1="5.5625" y1="9.5375" x2="6.1875" y2="9.5625" layer="200"/>
<rectangle x1="7.1125" y1="9.5375" x2="7.5375" y2="9.5625" layer="200"/>
<rectangle x1="9.4125" y1="9.5375" x2="9.8375" y2="9.5625" layer="200"/>
<rectangle x1="10.6875" y1="9.5375" x2="10.9375" y2="9.5625" layer="200"/>
<rectangle x1="11.1875" y1="9.5375" x2="11.9875" y2="9.5625" layer="200"/>
<rectangle x1="15.8625" y1="9.5375" x2="16.0125" y2="9.5625" layer="200"/>
<rectangle x1="17.6875" y1="9.5375" x2="17.8375" y2="9.5625" layer="200"/>
<rectangle x1="18.2875" y1="9.5375" x2="18.7125" y2="9.5625" layer="200"/>
<rectangle x1="19.7625" y1="9.5375" x2="19.9125" y2="9.5625" layer="200"/>
<rectangle x1="21.4375" y1="9.5375" x2="21.5875" y2="9.5625" layer="200"/>
<rectangle x1="22.1375" y1="9.5375" x2="22.3125" y2="9.5625" layer="200"/>
<rectangle x1="23.3375" y1="9.5375" x2="23.6375" y2="9.5625" layer="200"/>
<rectangle x1="5.5375" y1="9.5625" x2="6.1375" y2="9.5875" layer="200"/>
<rectangle x1="7.0625" y1="9.5625" x2="7.5125" y2="9.5875" layer="200"/>
<rectangle x1="9.3375" y1="9.5625" x2="9.9125" y2="9.5875" layer="200"/>
<rectangle x1="10.7125" y1="9.5625" x2="10.9625" y2="9.5875" layer="200"/>
<rectangle x1="11.2875" y1="9.5625" x2="12.0875" y2="9.5875" layer="200"/>
<rectangle x1="15.8625" y1="9.5625" x2="16.0125" y2="9.5875" layer="200"/>
<rectangle x1="17.7375" y1="9.5625" x2="17.8125" y2="9.5875" layer="200"/>
<rectangle x1="18.2625" y1="9.5625" x2="18.7375" y2="9.5875" layer="200"/>
<rectangle x1="19.7625" y1="9.5625" x2="19.9125" y2="9.5875" layer="200"/>
<rectangle x1="21.4375" y1="9.5625" x2="21.5875" y2="9.5875" layer="200"/>
<rectangle x1="22.1375" y1="9.5625" x2="22.3125" y2="9.5875" layer="200"/>
<rectangle x1="23.3625" y1="9.5625" x2="23.6625" y2="9.5875" layer="200"/>
<rectangle x1="5.4875" y1="9.5875" x2="6.0875" y2="9.6125" layer="200"/>
<rectangle x1="7.0375" y1="9.5875" x2="7.4875" y2="9.6125" layer="200"/>
<rectangle x1="9.2875" y1="9.5875" x2="9.9625" y2="9.6125" layer="200"/>
<rectangle x1="10.7375" y1="9.5875" x2="10.9875" y2="9.6125" layer="200"/>
<rectangle x1="11.3875" y1="9.5875" x2="12.1875" y2="9.6125" layer="200"/>
<rectangle x1="15.8625" y1="9.5875" x2="16.0125" y2="9.6125" layer="200"/>
<rectangle x1="18.2125" y1="9.5875" x2="18.7625" y2="9.6125" layer="200"/>
<rectangle x1="19.7375" y1="9.5875" x2="19.9375" y2="9.6125" layer="200"/>
<rectangle x1="21.4375" y1="9.5875" x2="21.5875" y2="9.6125" layer="200"/>
<rectangle x1="22.1375" y1="9.5875" x2="22.3125" y2="9.6125" layer="200"/>
<rectangle x1="23.3625" y1="9.5875" x2="23.6875" y2="9.6125" layer="200"/>
<rectangle x1="5.4625" y1="9.6125" x2="6.0625" y2="9.6375" layer="200"/>
<rectangle x1="7.0125" y1="9.6125" x2="7.4625" y2="9.6375" layer="200"/>
<rectangle x1="9.2375" y1="9.6125" x2="10.0375" y2="9.6375" layer="200"/>
<rectangle x1="10.7375" y1="9.6125" x2="10.9875" y2="9.6375" layer="200"/>
<rectangle x1="11.5125" y1="9.6125" x2="12.2875" y2="9.6375" layer="200"/>
<rectangle x1="15.8625" y1="9.6125" x2="16.0125" y2="9.6375" layer="200"/>
<rectangle x1="18.1875" y1="9.6125" x2="18.7875" y2="9.6375" layer="200"/>
<rectangle x1="19.7375" y1="9.6125" x2="19.9375" y2="9.6375" layer="200"/>
<rectangle x1="21.4375" y1="9.6125" x2="21.5875" y2="9.6375" layer="200"/>
<rectangle x1="22.1375" y1="9.6125" x2="22.3125" y2="9.6375" layer="200"/>
<rectangle x1="23.3875" y1="9.6125" x2="23.7125" y2="9.6375" layer="200"/>
<rectangle x1="5.4125" y1="9.6375" x2="6.0125" y2="9.6625" layer="200"/>
<rectangle x1="6.9625" y1="9.6375" x2="7.4125" y2="9.6625" layer="200"/>
<rectangle x1="9.1625" y1="9.6375" x2="10.1125" y2="9.6625" layer="200"/>
<rectangle x1="10.7625" y1="9.6375" x2="11.0125" y2="9.6625" layer="200"/>
<rectangle x1="11.6125" y1="9.6375" x2="12.3875" y2="9.6625" layer="200"/>
<rectangle x1="15.8625" y1="9.6375" x2="16.0125" y2="9.6625" layer="200"/>
<rectangle x1="18.1875" y1="9.6375" x2="18.8125" y2="9.6625" layer="200"/>
<rectangle x1="19.7375" y1="9.6375" x2="19.9375" y2="9.6625" layer="200"/>
<rectangle x1="21.4375" y1="9.6375" x2="21.5875" y2="9.6625" layer="200"/>
<rectangle x1="22.1375" y1="9.6375" x2="22.3125" y2="9.6625" layer="200"/>
<rectangle x1="23.3875" y1="9.6375" x2="23.7125" y2="9.6625" layer="200"/>
<rectangle x1="5.3875" y1="9.6625" x2="5.9625" y2="9.6875" layer="200"/>
<rectangle x1="6.9375" y1="9.6625" x2="7.3875" y2="9.6875" layer="200"/>
<rectangle x1="9.1125" y1="9.6625" x2="10.2125" y2="9.6875" layer="200"/>
<rectangle x1="10.7875" y1="9.6625" x2="11.0375" y2="9.6875" layer="200"/>
<rectangle x1="11.6875" y1="9.6625" x2="12.4875" y2="9.6875" layer="200"/>
<rectangle x1="15.8625" y1="9.6625" x2="16.0125" y2="9.6875" layer="200"/>
<rectangle x1="18.1625" y1="9.6625" x2="18.8125" y2="9.6875" layer="200"/>
<rectangle x1="19.7625" y1="9.6625" x2="19.9375" y2="9.6875" layer="200"/>
<rectangle x1="21.4375" y1="9.6625" x2="21.5875" y2="9.6875" layer="200"/>
<rectangle x1="22.1375" y1="9.6625" x2="22.3125" y2="9.6875" layer="200"/>
<rectangle x1="23.4125" y1="9.6625" x2="23.7375" y2="9.6875" layer="200"/>
<rectangle x1="5.3625" y1="9.6875" x2="5.9375" y2="9.7125" layer="200"/>
<rectangle x1="6.9125" y1="9.6875" x2="7.3625" y2="9.7125" layer="200"/>
<rectangle x1="9.0375" y1="9.6875" x2="9.4375" y2="9.7125" layer="200"/>
<rectangle x1="9.5125" y1="9.6875" x2="10.3125" y2="9.7125" layer="200"/>
<rectangle x1="10.8125" y1="9.6875" x2="11.0625" y2="9.7125" layer="200"/>
<rectangle x1="11.7875" y1="9.6875" x2="12.5875" y2="9.7125" layer="200"/>
<rectangle x1="15.8625" y1="9.6875" x2="16.0125" y2="9.7125" layer="200"/>
<rectangle x1="18.1375" y1="9.6875" x2="18.8375" y2="9.7125" layer="200"/>
<rectangle x1="19.7625" y1="9.6875" x2="19.9375" y2="9.7125" layer="200"/>
<rectangle x1="21.4375" y1="9.6875" x2="21.5875" y2="9.7125" layer="200"/>
<rectangle x1="22.1375" y1="9.6875" x2="22.3125" y2="9.7125" layer="200"/>
<rectangle x1="23.4125" y1="9.6875" x2="23.7625" y2="9.7125" layer="200"/>
<rectangle x1="5.3125" y1="9.7125" x2="5.8875" y2="9.7375" layer="200"/>
<rectangle x1="6.8625" y1="9.7125" x2="7.3125" y2="9.7375" layer="200"/>
<rectangle x1="8.9875" y1="9.7125" x2="9.3625" y2="9.7375" layer="200"/>
<rectangle x1="9.6625" y1="9.7125" x2="10.4125" y2="9.7375" layer="200"/>
<rectangle x1="10.8375" y1="9.7125" x2="11.0875" y2="9.7375" layer="200"/>
<rectangle x1="11.8875" y1="9.7125" x2="12.6875" y2="9.7375" layer="200"/>
<rectangle x1="15.8625" y1="9.7125" x2="16.0125" y2="9.7375" layer="200"/>
<rectangle x1="18.1125" y1="9.7125" x2="18.8625" y2="9.7375" layer="200"/>
<rectangle x1="19.7625" y1="9.7125" x2="19.9375" y2="9.7375" layer="200"/>
<rectangle x1="21.4375" y1="9.7125" x2="21.5875" y2="9.7375" layer="200"/>
<rectangle x1="22.1375" y1="9.7125" x2="22.3125" y2="9.7375" layer="200"/>
<rectangle x1="23.4375" y1="9.7125" x2="23.7875" y2="9.7375" layer="200"/>
<rectangle x1="5.2875" y1="9.7375" x2="5.8375" y2="9.7625" layer="200"/>
<rectangle x1="6.8375" y1="9.7375" x2="7.2875" y2="9.7625" layer="200"/>
<rectangle x1="8.9125" y1="9.7375" x2="9.3625" y2="9.7625" layer="200"/>
<rectangle x1="9.7875" y1="9.7375" x2="10.5125" y2="9.7625" layer="200"/>
<rectangle x1="10.8375" y1="9.7375" x2="11.0875" y2="9.7625" layer="200"/>
<rectangle x1="11.9875" y1="9.7375" x2="12.7875" y2="9.7625" layer="200"/>
<rectangle x1="15.8625" y1="9.7375" x2="16.0125" y2="9.7625" layer="200"/>
<rectangle x1="18.1125" y1="9.7375" x2="18.8625" y2="9.7625" layer="200"/>
<rectangle x1="19.7625" y1="9.7375" x2="19.9375" y2="9.7625" layer="200"/>
<rectangle x1="21.4375" y1="9.7375" x2="21.5875" y2="9.7625" layer="200"/>
<rectangle x1="22.1375" y1="9.7375" x2="22.3125" y2="9.7625" layer="200"/>
<rectangle x1="23.4375" y1="9.7375" x2="23.7875" y2="9.7625" layer="200"/>
<rectangle x1="5.2375" y1="9.7625" x2="5.8125" y2="9.7875" layer="200"/>
<rectangle x1="6.7875" y1="9.7625" x2="7.2625" y2="9.7875" layer="200"/>
<rectangle x1="8.8625" y1="9.7625" x2="9.4125" y2="9.7875" layer="200"/>
<rectangle x1="9.8875" y1="9.7625" x2="10.6125" y2="9.7875" layer="200"/>
<rectangle x1="10.8625" y1="9.7625" x2="11.1125" y2="9.7875" layer="200"/>
<rectangle x1="12.0875" y1="9.7625" x2="12.8875" y2="9.7875" layer="200"/>
<rectangle x1="15.8625" y1="9.7625" x2="16.0125" y2="9.7875" layer="200"/>
<rectangle x1="18.1125" y1="9.7625" x2="18.8625" y2="9.7875" layer="200"/>
<rectangle x1="19.7625" y1="9.7625" x2="19.9625" y2="9.7875" layer="200"/>
<rectangle x1="21.4375" y1="9.7625" x2="21.5875" y2="9.7875" layer="200"/>
<rectangle x1="22.1375" y1="9.7625" x2="22.3125" y2="9.7875" layer="200"/>
<rectangle x1="23.4375" y1="9.7625" x2="23.7625" y2="9.7875" layer="200"/>
<rectangle x1="5.2125" y1="9.7875" x2="5.7625" y2="9.8125" layer="200"/>
<rectangle x1="6.7625" y1="9.7875" x2="7.2125" y2="9.8125" layer="200"/>
<rectangle x1="8.8125" y1="9.7875" x2="9.4625" y2="9.8125" layer="200"/>
<rectangle x1="10.0125" y1="9.7875" x2="10.7125" y2="9.8125" layer="200"/>
<rectangle x1="10.8625" y1="9.7875" x2="11.1375" y2="9.8125" layer="200"/>
<rectangle x1="12.1875" y1="9.7875" x2="12.9875" y2="9.8125" layer="200"/>
<rectangle x1="15.8625" y1="9.7875" x2="16.0125" y2="9.8125" layer="200"/>
<rectangle x1="18.1125" y1="9.7875" x2="18.8375" y2="9.8125" layer="200"/>
<rectangle x1="19.7625" y1="9.7875" x2="19.9625" y2="9.8125" layer="200"/>
<rectangle x1="21.4375" y1="9.7875" x2="21.5875" y2="9.8125" layer="200"/>
<rectangle x1="22.1375" y1="9.7875" x2="22.3375" y2="9.8125" layer="200"/>
<rectangle x1="23.4375" y1="9.7875" x2="23.7375" y2="9.8125" layer="200"/>
<rectangle x1="5.1625" y1="9.8125" x2="5.7375" y2="9.8375" layer="200"/>
<rectangle x1="6.7125" y1="9.8125" x2="7.1625" y2="9.8375" layer="200"/>
<rectangle x1="8.7375" y1="9.8125" x2="9.5375" y2="9.8375" layer="200"/>
<rectangle x1="10.1125" y1="9.8125" x2="11.1625" y2="9.8375" layer="200"/>
<rectangle x1="12.2875" y1="9.8125" x2="13.0875" y2="9.8375" layer="200"/>
<rectangle x1="15.8625" y1="9.8125" x2="16.0125" y2="9.8375" layer="200"/>
<rectangle x1="18.1125" y1="9.8125" x2="18.8125" y2="9.8375" layer="200"/>
<rectangle x1="19.7625" y1="9.8125" x2="19.9625" y2="9.8375" layer="200"/>
<rectangle x1="21.4375" y1="9.8125" x2="21.5875" y2="9.8375" layer="200"/>
<rectangle x1="22.1375" y1="9.8125" x2="22.3375" y2="9.8375" layer="200"/>
<rectangle x1="23.4125" y1="9.8125" x2="23.7375" y2="9.8375" layer="200"/>
<rectangle x1="5.1375" y1="9.8375" x2="5.6875" y2="9.8625" layer="200"/>
<rectangle x1="6.6875" y1="9.8375" x2="7.1375" y2="9.8625" layer="200"/>
<rectangle x1="8.6875" y1="9.8375" x2="9.0625" y2="9.8625" layer="200"/>
<rectangle x1="9.1875" y1="9.8375" x2="9.6125" y2="9.8625" layer="200"/>
<rectangle x1="10.2125" y1="9.8375" x2="11.1875" y2="9.8625" layer="200"/>
<rectangle x1="12.3875" y1="9.8375" x2="13.1875" y2="9.8625" layer="200"/>
<rectangle x1="15.8625" y1="9.8375" x2="16.0125" y2="9.8625" layer="200"/>
<rectangle x1="18.1375" y1="9.8375" x2="18.7875" y2="9.8625" layer="200"/>
<rectangle x1="19.7625" y1="9.8375" x2="19.9625" y2="9.8625" layer="200"/>
<rectangle x1="21.4375" y1="9.8375" x2="21.6125" y2="9.8625" layer="200"/>
<rectangle x1="22.1375" y1="9.8375" x2="22.3375" y2="9.8625" layer="200"/>
<rectangle x1="23.3875" y1="9.8375" x2="23.7125" y2="9.8625" layer="200"/>
<rectangle x1="5.0875" y1="9.8625" x2="5.6625" y2="9.8875" layer="200"/>
<rectangle x1="6.6375" y1="9.8625" x2="7.0875" y2="9.8875" layer="200"/>
<rectangle x1="8.6125" y1="9.8625" x2="8.9875" y2="9.8875" layer="200"/>
<rectangle x1="9.2375" y1="9.8625" x2="9.6875" y2="9.8875" layer="200"/>
<rectangle x1="10.3125" y1="9.8625" x2="11.2125" y2="9.8875" layer="200"/>
<rectangle x1="12.4625" y1="9.8625" x2="13.2625" y2="9.8875" layer="200"/>
<rectangle x1="15.8625" y1="9.8625" x2="16.0125" y2="9.8875" layer="200"/>
<rectangle x1="18.1625" y1="9.8625" x2="18.7625" y2="9.8875" layer="200"/>
<rectangle x1="19.7625" y1="9.8625" x2="19.9875" y2="9.8875" layer="200"/>
<rectangle x1="21.4375" y1="9.8625" x2="21.6125" y2="9.8875" layer="200"/>
<rectangle x1="22.1375" y1="9.8625" x2="22.3375" y2="9.8875" layer="200"/>
<rectangle x1="23.3875" y1="9.8625" x2="23.6875" y2="9.8875" layer="200"/>
<rectangle x1="5.0375" y1="9.8875" x2="5.6125" y2="9.9125" layer="200"/>
<rectangle x1="6.5875" y1="9.8875" x2="7.0375" y2="9.9125" layer="200"/>
<rectangle x1="8.5625" y1="9.8875" x2="8.9375" y2="9.9125" layer="200"/>
<rectangle x1="9.2875" y1="9.8875" x2="9.7375" y2="9.9125" layer="200"/>
<rectangle x1="10.3875" y1="9.8875" x2="11.2625" y2="9.9125" layer="200"/>
<rectangle x1="12.5625" y1="9.8875" x2="13.3625" y2="9.9125" layer="200"/>
<rectangle x1="15.8625" y1="9.8875" x2="16.0125" y2="9.9125" layer="200"/>
<rectangle x1="18.1625" y1="9.8875" x2="18.7125" y2="9.9125" layer="200"/>
<rectangle x1="19.7875" y1="9.8875" x2="19.9875" y2="9.9125" layer="200"/>
<rectangle x1="21.4375" y1="9.8875" x2="21.6125" y2="9.9125" layer="200"/>
<rectangle x1="22.1375" y1="9.8875" x2="22.3625" y2="9.9125" layer="200"/>
<rectangle x1="23.3625" y1="9.8875" x2="23.6625" y2="9.9125" layer="200"/>
<rectangle x1="5.0125" y1="9.9125" x2="5.5875" y2="9.9375" layer="200"/>
<rectangle x1="6.5625" y1="9.9125" x2="6.9875" y2="9.9375" layer="200"/>
<rectangle x1="8.5125" y1="9.9125" x2="8.8625" y2="9.9375" layer="200"/>
<rectangle x1="9.3375" y1="9.9125" x2="9.8125" y2="9.9375" layer="200"/>
<rectangle x1="10.4875" y1="9.9125" x2="11.3125" y2="9.9375" layer="200"/>
<rectangle x1="12.6625" y1="9.9125" x2="13.4625" y2="9.9375" layer="200"/>
<rectangle x1="15.8875" y1="9.9125" x2="16.0375" y2="9.9375" layer="200"/>
<rectangle x1="18.1875" y1="9.9125" x2="18.6875" y2="9.9375" layer="200"/>
<rectangle x1="19.7875" y1="9.9125" x2="19.9875" y2="9.9375" layer="200"/>
<rectangle x1="21.4375" y1="9.9125" x2="21.6125" y2="9.9375" layer="200"/>
<rectangle x1="22.1625" y1="9.9125" x2="22.3625" y2="9.9375" layer="200"/>
<rectangle x1="23.3375" y1="9.9125" x2="23.6625" y2="9.9375" layer="200"/>
<rectangle x1="4.9625" y1="9.9375" x2="5.5375" y2="9.9625" layer="200"/>
<rectangle x1="6.5125" y1="9.9375" x2="6.9375" y2="9.9625" layer="200"/>
<rectangle x1="8.4375" y1="9.9375" x2="8.7875" y2="9.9625" layer="200"/>
<rectangle x1="9.3625" y1="9.9375" x2="9.9125" y2="9.9625" layer="200"/>
<rectangle x1="10.5625" y1="9.9375" x2="11.3625" y2="9.9625" layer="200"/>
<rectangle x1="12.7625" y1="9.9375" x2="13.5625" y2="9.9625" layer="200"/>
<rectangle x1="15.8875" y1="9.9375" x2="16.0375" y2="9.9625" layer="200"/>
<rectangle x1="18.2125" y1="9.9375" x2="18.6625" y2="9.9625" layer="200"/>
<rectangle x1="19.7875" y1="9.9375" x2="20.0125" y2="9.9625" layer="200"/>
<rectangle x1="21.4625" y1="9.9375" x2="21.6125" y2="9.9625" layer="200"/>
<rectangle x1="22.1625" y1="9.9375" x2="22.3625" y2="9.9625" layer="200"/>
<rectangle x1="23.3375" y1="9.9375" x2="23.6375" y2="9.9625" layer="200"/>
<rectangle x1="4.9375" y1="9.9625" x2="5.5125" y2="9.9875" layer="200"/>
<rectangle x1="6.4625" y1="9.9625" x2="6.9125" y2="9.9875" layer="200"/>
<rectangle x1="8.3875" y1="9.9625" x2="8.7375" y2="9.9875" layer="200"/>
<rectangle x1="9.4125" y1="9.9625" x2="9.9875" y2="9.9875" layer="200"/>
<rectangle x1="10.6625" y1="9.9625" x2="11.4125" y2="9.9875" layer="200"/>
<rectangle x1="12.8375" y1="9.9625" x2="13.6625" y2="9.9875" layer="200"/>
<rectangle x1="15.8875" y1="9.9625" x2="16.0375" y2="9.9875" layer="200"/>
<rectangle x1="18.2375" y1="9.9625" x2="18.6125" y2="9.9875" layer="200"/>
<rectangle x1="19.7875" y1="9.9625" x2="20.0125" y2="9.9875" layer="200"/>
<rectangle x1="21.4625" y1="9.9625" x2="21.6375" y2="9.9875" layer="200"/>
<rectangle x1="22.1625" y1="9.9625" x2="22.3875" y2="9.9875" layer="200"/>
<rectangle x1="23.3125" y1="9.9625" x2="23.6375" y2="9.9875" layer="200"/>
<rectangle x1="4.8875" y1="9.9875" x2="5.4875" y2="10.0125" layer="200"/>
<rectangle x1="6.4125" y1="9.9875" x2="6.8625" y2="10.0125" layer="200"/>
<rectangle x1="8.3375" y1="9.9875" x2="8.6625" y2="10.0125" layer="200"/>
<rectangle x1="9.4625" y1="9.9875" x2="10.1125" y2="10.0125" layer="200"/>
<rectangle x1="10.6875" y1="9.9875" x2="11.4875" y2="10.0125" layer="200"/>
<rectangle x1="12.9375" y1="9.9875" x2="13.7625" y2="10.0125" layer="200"/>
<rectangle x1="15.8875" y1="9.9875" x2="16.0375" y2="10.0125" layer="200"/>
<rectangle x1="18.2625" y1="9.9875" x2="18.5875" y2="10.0125" layer="200"/>
<rectangle x1="19.8125" y1="9.9875" x2="20.0125" y2="10.0125" layer="200"/>
<rectangle x1="21.4625" y1="9.9875" x2="21.6375" y2="10.0125" layer="200"/>
<rectangle x1="22.1625" y1="9.9875" x2="22.3875" y2="10.0125" layer="200"/>
<rectangle x1="23.2875" y1="9.9875" x2="23.6125" y2="10.0125" layer="200"/>
<rectangle x1="4.8625" y1="10.0125" x2="5.4625" y2="10.0375" layer="200"/>
<rectangle x1="6.3625" y1="10.0125" x2="6.8125" y2="10.0375" layer="200"/>
<rectangle x1="8.2625" y1="10.0125" x2="8.6125" y2="10.0375" layer="200"/>
<rectangle x1="9.5375" y1="10.0125" x2="10.2625" y2="10.0375" layer="200"/>
<rectangle x1="10.5375" y1="10.0125" x2="11.5875" y2="10.0375" layer="200"/>
<rectangle x1="13.0375" y1="10.0125" x2="13.8625" y2="10.0375" layer="200"/>
<rectangle x1="15.8875" y1="10.0125" x2="16.0375" y2="10.0375" layer="200"/>
<rectangle x1="18.2875" y1="10.0125" x2="18.5625" y2="10.0375" layer="200"/>
<rectangle x1="19.8125" y1="10.0125" x2="20.0375" y2="10.0375" layer="200"/>
<rectangle x1="21.4625" y1="10.0125" x2="21.6375" y2="10.0375" layer="200"/>
<rectangle x1="22.1625" y1="10.0125" x2="22.4125" y2="10.0375" layer="200"/>
<rectangle x1="23.2625" y1="10.0125" x2="23.6125" y2="10.0375" layer="200"/>
<rectangle x1="4.8125" y1="10.0375" x2="5.4625" y2="10.0625" layer="200"/>
<rectangle x1="6.3125" y1="10.0375" x2="6.7625" y2="10.0625" layer="200"/>
<rectangle x1="8.2125" y1="10.0375" x2="8.5375" y2="10.0625" layer="200"/>
<rectangle x1="9.5875" y1="10.0375" x2="11.6625" y2="10.0625" layer="200"/>
<rectangle x1="13.1375" y1="10.0375" x2="13.9875" y2="10.0625" layer="200"/>
<rectangle x1="15.8875" y1="10.0375" x2="16.0375" y2="10.0625" layer="200"/>
<rectangle x1="18.3125" y1="10.0375" x2="18.5125" y2="10.0625" layer="200"/>
<rectangle x1="19.8125" y1="10.0375" x2="20.0375" y2="10.0625" layer="200"/>
<rectangle x1="21.4875" y1="10.0375" x2="21.6625" y2="10.0625" layer="200"/>
<rectangle x1="22.1875" y1="10.0375" x2="22.4125" y2="10.0625" layer="200"/>
<rectangle x1="23.2375" y1="10.0375" x2="23.5875" y2="10.0625" layer="200"/>
<rectangle x1="4.7875" y1="10.0625" x2="5.4625" y2="10.0875" layer="200"/>
<rectangle x1="6.2625" y1="10.0625" x2="6.7125" y2="10.0875" layer="200"/>
<rectangle x1="8.1625" y1="10.0625" x2="8.4875" y2="10.0875" layer="200"/>
<rectangle x1="9.6625" y1="10.0625" x2="11.7625" y2="10.0875" layer="200"/>
<rectangle x1="13.2375" y1="10.0625" x2="14.0875" y2="10.0875" layer="200"/>
<rectangle x1="15.8875" y1="10.0625" x2="16.0375" y2="10.0875" layer="200"/>
<rectangle x1="18.3375" y1="10.0625" x2="18.4875" y2="10.0875" layer="200"/>
<rectangle x1="19.8125" y1="10.0625" x2="20.0625" y2="10.0875" layer="200"/>
<rectangle x1="21.4875" y1="10.0625" x2="21.6625" y2="10.0875" layer="200"/>
<rectangle x1="22.1875" y1="10.0625" x2="22.4375" y2="10.0875" layer="200"/>
<rectangle x1="23.2125" y1="10.0625" x2="23.5875" y2="10.0875" layer="200"/>
<rectangle x1="4.7375" y1="10.0875" x2="5.4875" y2="10.1125" layer="200"/>
<rectangle x1="6.1875" y1="10.0875" x2="6.6625" y2="10.1125" layer="200"/>
<rectangle x1="8.0875" y1="10.0875" x2="8.4125" y2="10.1125" layer="200"/>
<rectangle x1="9.7375" y1="10.0875" x2="11.8625" y2="10.1125" layer="200"/>
<rectangle x1="13.3375" y1="10.0875" x2="14.1875" y2="10.1125" layer="200"/>
<rectangle x1="15.8875" y1="10.0875" x2="16.0375" y2="10.1125" layer="200"/>
<rectangle x1="18.3625" y1="10.0875" x2="18.4375" y2="10.1125" layer="200"/>
<rectangle x1="19.8375" y1="10.0875" x2="20.0625" y2="10.1125" layer="200"/>
<rectangle x1="21.5125" y1="10.0875" x2="21.6625" y2="10.1125" layer="200"/>
<rectangle x1="22.1875" y1="10.0875" x2="22.4375" y2="10.1125" layer="200"/>
<rectangle x1="23.1875" y1="10.0875" x2="23.5625" y2="10.1125" layer="200"/>
<rectangle x1="4.7125" y1="10.1125" x2="5.5125" y2="10.1375" layer="200"/>
<rectangle x1="6.1375" y1="10.1125" x2="6.6125" y2="10.1375" layer="200"/>
<rectangle x1="8.0375" y1="10.1125" x2="8.3625" y2="10.1375" layer="200"/>
<rectangle x1="9.8125" y1="10.1125" x2="11.9625" y2="10.1375" layer="200"/>
<rectangle x1="13.4375" y1="10.1125" x2="14.2875" y2="10.1375" layer="200"/>
<rectangle x1="15.8875" y1="10.1125" x2="16.0375" y2="10.1375" layer="200"/>
<rectangle x1="18.3625" y1="10.1125" x2="18.4125" y2="10.1375" layer="200"/>
<rectangle x1="19.1375" y1="10.1125" x2="19.2625" y2="10.1375" layer="200"/>
<rectangle x1="19.8375" y1="10.1125" x2="20.0875" y2="10.1375" layer="200"/>
<rectangle x1="21.5125" y1="10.1125" x2="21.6875" y2="10.1375" layer="200"/>
<rectangle x1="22.2125" y1="10.1125" x2="22.4625" y2="10.1375" layer="200"/>
<rectangle x1="23.1375" y1="10.1125" x2="23.5375" y2="10.1375" layer="200"/>
<rectangle x1="4.6875" y1="10.1375" x2="5.5375" y2="10.1625" layer="200"/>
<rectangle x1="6.0625" y1="10.1375" x2="6.5625" y2="10.1625" layer="200"/>
<rectangle x1="7.9875" y1="10.1375" x2="8.2875" y2="10.1625" layer="200"/>
<rectangle x1="9.9125" y1="10.1375" x2="12.0375" y2="10.1625" layer="200"/>
<rectangle x1="13.5375" y1="10.1375" x2="14.3875" y2="10.1625" layer="200"/>
<rectangle x1="15.8875" y1="10.1375" x2="16.0375" y2="10.1625" layer="200"/>
<rectangle x1="19.0625" y1="10.1375" x2="19.2875" y2="10.1625" layer="200"/>
<rectangle x1="19.8625" y1="10.1375" x2="20.0875" y2="10.1625" layer="200"/>
<rectangle x1="21.5375" y1="10.1375" x2="21.6875" y2="10.1625" layer="200"/>
<rectangle x1="22.2125" y1="10.1375" x2="22.4625" y2="10.1625" layer="200"/>
<rectangle x1="23.1125" y1="10.1375" x2="23.5125" y2="10.1625" layer="200"/>
<rectangle x1="4.6625" y1="10.1625" x2="5.5625" y2="10.1875" layer="200"/>
<rectangle x1="6.0125" y1="10.1625" x2="6.5125" y2="10.1875" layer="200"/>
<rectangle x1="7.9125" y1="10.1625" x2="8.2375" y2="10.1875" layer="200"/>
<rectangle x1="10.0125" y1="10.1625" x2="11.0875" y2="10.1875" layer="200"/>
<rectangle x1="11.3625" y1="10.1625" x2="12.1625" y2="10.1875" layer="200"/>
<rectangle x1="13.6375" y1="10.1625" x2="14.4875" y2="10.1875" layer="200"/>
<rectangle x1="15.9125" y1="10.1625" x2="16.0625" y2="10.1875" layer="200"/>
<rectangle x1="18.9875" y1="10.1625" x2="19.2875" y2="10.1875" layer="200"/>
<rectangle x1="19.8625" y1="10.1625" x2="20.1125" y2="10.1875" layer="200"/>
<rectangle x1="21.5375" y1="10.1625" x2="21.6875" y2="10.1875" layer="200"/>
<rectangle x1="22.2125" y1="10.1625" x2="22.4875" y2="10.1875" layer="200"/>
<rectangle x1="23.0625" y1="10.1625" x2="23.4625" y2="10.1875" layer="200"/>
<rectangle x1="4.6125" y1="10.1875" x2="5.6125" y2="10.2125" layer="200"/>
<rectangle x1="5.9125" y1="10.1875" x2="6.4625" y2="10.2125" layer="200"/>
<rectangle x1="7.8625" y1="10.1875" x2="8.1875" y2="10.2125" layer="200"/>
<rectangle x1="10.1375" y1="10.1875" x2="10.8625" y2="10.2125" layer="200"/>
<rectangle x1="11.4875" y1="10.1875" x2="12.2375" y2="10.2125" layer="200"/>
<rectangle x1="13.7375" y1="10.1875" x2="14.5875" y2="10.2125" layer="200"/>
<rectangle x1="15.9125" y1="10.1875" x2="16.0625" y2="10.2125" layer="200"/>
<rectangle x1="18.9375" y1="10.1875" x2="19.3125" y2="10.2125" layer="200"/>
<rectangle x1="19.8875" y1="10.1875" x2="20.1375" y2="10.2125" layer="200"/>
<rectangle x1="21.5625" y1="10.1875" x2="21.6875" y2="10.2125" layer="200"/>
<rectangle x1="22.2375" y1="10.1875" x2="22.4875" y2="10.2125" layer="200"/>
<rectangle x1="23.0125" y1="10.1875" x2="23.4375" y2="10.2125" layer="200"/>
<rectangle x1="4.5875" y1="10.2125" x2="5.7125" y2="10.2375" layer="200"/>
<rectangle x1="5.7875" y1="10.2125" x2="6.4125" y2="10.2375" layer="200"/>
<rectangle x1="7.7875" y1="10.2125" x2="8.1125" y2="10.2375" layer="200"/>
<rectangle x1="10.2875" y1="10.2125" x2="10.6625" y2="10.2375" layer="200"/>
<rectangle x1="11.5625" y1="10.2125" x2="12.3375" y2="10.2375" layer="200"/>
<rectangle x1="13.8375" y1="10.2125" x2="14.6875" y2="10.2375" layer="200"/>
<rectangle x1="15.9125" y1="10.2125" x2="16.0625" y2="10.2375" layer="200"/>
<rectangle x1="18.8875" y1="10.2125" x2="19.3375" y2="10.2375" layer="200"/>
<rectangle x1="19.8875" y1="10.2125" x2="20.1375" y2="10.2375" layer="200"/>
<rectangle x1="21.5625" y1="10.2125" x2="21.7125" y2="10.2375" layer="200"/>
<rectangle x1="22.2375" y1="10.2125" x2="22.5125" y2="10.2375" layer="200"/>
<rectangle x1="22.9875" y1="10.2125" x2="23.3875" y2="10.2375" layer="200"/>
<rectangle x1="4.5375" y1="10.2375" x2="5.0375" y2="10.2625" layer="200"/>
<rectangle x1="5.2375" y1="10.2375" x2="6.3625" y2="10.2625" layer="200"/>
<rectangle x1="7.7125" y1="10.2375" x2="8.0625" y2="10.2625" layer="200"/>
<rectangle x1="11.6375" y1="10.2375" x2="12.4375" y2="10.2625" layer="200"/>
<rectangle x1="13.9125" y1="10.2375" x2="14.7875" y2="10.2625" layer="200"/>
<rectangle x1="15.9125" y1="10.2375" x2="16.0625" y2="10.2625" layer="200"/>
<rectangle x1="18.8375" y1="10.2375" x2="19.3375" y2="10.2625" layer="200"/>
<rectangle x1="19.9125" y1="10.2375" x2="20.1625" y2="10.2625" layer="200"/>
<rectangle x1="21.5875" y1="10.2375" x2="21.7125" y2="10.2625" layer="200"/>
<rectangle x1="22.2625" y1="10.2375" x2="22.5125" y2="10.2625" layer="200"/>
<rectangle x1="22.9375" y1="10.2375" x2="23.3625" y2="10.2625" layer="200"/>
<rectangle x1="4.5125" y1="10.2625" x2="5.0125" y2="10.2875" layer="200"/>
<rectangle x1="5.2875" y1="10.2625" x2="6.3125" y2="10.2875" layer="200"/>
<rectangle x1="7.6375" y1="10.2625" x2="7.9875" y2="10.2875" layer="200"/>
<rectangle x1="11.7125" y1="10.2625" x2="12.5125" y2="10.2875" layer="200"/>
<rectangle x1="14.0125" y1="10.2625" x2="14.8625" y2="10.2875" layer="200"/>
<rectangle x1="15.9125" y1="10.2625" x2="16.0625" y2="10.2875" layer="200"/>
<rectangle x1="18.7875" y1="10.2625" x2="19.3625" y2="10.2875" layer="200"/>
<rectangle x1="19.9125" y1="10.2625" x2="20.1875" y2="10.2875" layer="200"/>
<rectangle x1="21.5875" y1="10.2625" x2="21.7375" y2="10.2875" layer="200"/>
<rectangle x1="22.2625" y1="10.2625" x2="22.5375" y2="10.2875" layer="200"/>
<rectangle x1="22.8875" y1="10.2625" x2="23.3125" y2="10.2875" layer="200"/>
<rectangle x1="4.5125" y1="10.2875" x2="4.9625" y2="10.3125" layer="200"/>
<rectangle x1="5.3625" y1="10.2875" x2="6.2625" y2="10.3125" layer="200"/>
<rectangle x1="7.5875" y1="10.2875" x2="7.9375" y2="10.3125" layer="200"/>
<rectangle x1="11.7625" y1="10.2875" x2="12.6125" y2="10.3125" layer="200"/>
<rectangle x1="14.1125" y1="10.2875" x2="14.9625" y2="10.3125" layer="200"/>
<rectangle x1="15.9375" y1="10.2875" x2="16.0875" y2="10.3125" layer="200"/>
<rectangle x1="18.7375" y1="10.2875" x2="19.3625" y2="10.3125" layer="200"/>
<rectangle x1="19.9375" y1="10.2875" x2="20.1875" y2="10.3125" layer="200"/>
<rectangle x1="21.6125" y1="10.2875" x2="21.7375" y2="10.3125" layer="200"/>
<rectangle x1="22.2875" y1="10.2875" x2="22.5625" y2="10.3125" layer="200"/>
<rectangle x1="22.8125" y1="10.2875" x2="23.2625" y2="10.3125" layer="200"/>
<rectangle x1="4.5125" y1="10.3125" x2="4.9375" y2="10.3375" layer="200"/>
<rectangle x1="5.4125" y1="10.3125" x2="6.2125" y2="10.3375" layer="200"/>
<rectangle x1="7.5375" y1="10.3125" x2="7.8875" y2="10.3375" layer="200"/>
<rectangle x1="11.8125" y1="10.3125" x2="12.6875" y2="10.3375" layer="200"/>
<rectangle x1="14.1875" y1="10.3125" x2="15.0375" y2="10.3375" layer="200"/>
<rectangle x1="15.9375" y1="10.3125" x2="16.0875" y2="10.3375" layer="200"/>
<rectangle x1="18.6875" y1="10.3125" x2="19.3875" y2="10.3375" layer="200"/>
<rectangle x1="19.9375" y1="10.3125" x2="20.1875" y2="10.3375" layer="200"/>
<rectangle x1="21.6125" y1="10.3125" x2="21.7375" y2="10.3375" layer="200"/>
<rectangle x1="22.3125" y1="10.3125" x2="22.5625" y2="10.3375" layer="200"/>
<rectangle x1="22.7625" y1="10.3125" x2="23.2375" y2="10.3375" layer="200"/>
<rectangle x1="4.5125" y1="10.3375" x2="4.9125" y2="10.3625" layer="200"/>
<rectangle x1="5.4625" y1="10.3375" x2="6.1625" y2="10.3625" layer="200"/>
<rectangle x1="7.4625" y1="10.3375" x2="7.8125" y2="10.3625" layer="200"/>
<rectangle x1="11.8375" y1="10.3375" x2="12.7625" y2="10.3625" layer="200"/>
<rectangle x1="14.2375" y1="10.3375" x2="15.1375" y2="10.3625" layer="200"/>
<rectangle x1="15.9375" y1="10.3375" x2="16.0875" y2="10.3625" layer="200"/>
<rectangle x1="18.6625" y1="10.3375" x2="19.3875" y2="10.3625" layer="200"/>
<rectangle x1="19.9625" y1="10.3375" x2="20.2125" y2="10.3625" layer="200"/>
<rectangle x1="21.6375" y1="10.3375" x2="21.7625" y2="10.3625" layer="200"/>
<rectangle x1="22.3125" y1="10.3375" x2="22.5875" y2="10.3625" layer="200"/>
<rectangle x1="22.7125" y1="10.3375" x2="23.1875" y2="10.3625" layer="200"/>
<rectangle x1="4.5125" y1="10.3625" x2="4.8875" y2="10.3875" layer="200"/>
<rectangle x1="5.5125" y1="10.3625" x2="6.1125" y2="10.3875" layer="200"/>
<rectangle x1="7.4125" y1="10.3625" x2="7.7625" y2="10.3875" layer="200"/>
<rectangle x1="11.8625" y1="10.3625" x2="12.8375" y2="10.3875" layer="200"/>
<rectangle x1="14.3375" y1="10.3625" x2="15.2375" y2="10.3875" layer="200"/>
<rectangle x1="15.9375" y1="10.3625" x2="16.1125" y2="10.3875" layer="200"/>
<rectangle x1="18.6375" y1="10.3625" x2="19.3875" y2="10.3875" layer="200"/>
<rectangle x1="19.9875" y1="10.3625" x2="20.2375" y2="10.3875" layer="200"/>
<rectangle x1="21.6375" y1="10.3625" x2="21.7625" y2="10.3875" layer="200"/>
<rectangle x1="22.3375" y1="10.3625" x2="22.6125" y2="10.3875" layer="200"/>
<rectangle x1="22.6625" y1="10.3625" x2="23.1375" y2="10.3875" layer="200"/>
<rectangle x1="4.5125" y1="10.3875" x2="4.8625" y2="10.4125" layer="200"/>
<rectangle x1="5.5625" y1="10.3875" x2="6.0625" y2="10.4125" layer="200"/>
<rectangle x1="7.3625" y1="10.3875" x2="7.6875" y2="10.4125" layer="200"/>
<rectangle x1="11.8875" y1="10.3875" x2="12.1875" y2="10.4125" layer="200"/>
<rectangle x1="12.2125" y1="10.3875" x2="12.9375" y2="10.4125" layer="200"/>
<rectangle x1="14.4375" y1="10.3875" x2="15.3375" y2="10.4125" layer="200"/>
<rectangle x1="15.9375" y1="10.3875" x2="16.1125" y2="10.4125" layer="200"/>
<rectangle x1="18.6375" y1="10.3875" x2="19.4125" y2="10.4125" layer="200"/>
<rectangle x1="19.9875" y1="10.3875" x2="20.2625" y2="10.4125" layer="200"/>
<rectangle x1="21.6625" y1="10.3875" x2="21.7875" y2="10.4125" layer="200"/>
<rectangle x1="22.3875" y1="10.3875" x2="23.0875" y2="10.4125" layer="200"/>
<rectangle x1="4.5375" y1="10.4125" x2="4.8375" y2="10.4375" layer="200"/>
<rectangle x1="5.6125" y1="10.4125" x2="6.0125" y2="10.4375" layer="200"/>
<rectangle x1="7.3125" y1="10.4125" x2="7.6375" y2="10.4375" layer="200"/>
<rectangle x1="11.9125" y1="10.4125" x2="12.2125" y2="10.4375" layer="200"/>
<rectangle x1="12.3375" y1="10.4125" x2="13.0375" y2="10.4375" layer="200"/>
<rectangle x1="14.5125" y1="10.4125" x2="15.4375" y2="10.4375" layer="200"/>
<rectangle x1="15.9625" y1="10.4125" x2="16.1125" y2="10.4375" layer="200"/>
<rectangle x1="18.6375" y1="10.4125" x2="19.4125" y2="10.4375" layer="200"/>
<rectangle x1="20.0125" y1="10.4125" x2="20.2875" y2="10.4375" layer="200"/>
<rectangle x1="21.6625" y1="10.4125" x2="21.7875" y2="10.4375" layer="200"/>
<rectangle x1="22.3875" y1="10.4125" x2="23.0375" y2="10.4375" layer="200"/>
<rectangle x1="4.5625" y1="10.4375" x2="4.8125" y2="10.4625" layer="200"/>
<rectangle x1="5.6375" y1="10.4375" x2="5.9875" y2="10.4625" layer="200"/>
<rectangle x1="7.2375" y1="10.4375" x2="7.5875" y2="10.4625" layer="200"/>
<rectangle x1="11.9375" y1="10.4375" x2="12.2125" y2="10.4625" layer="200"/>
<rectangle x1="12.4375" y1="10.4375" x2="13.1125" y2="10.4625" layer="200"/>
<rectangle x1="14.6125" y1="10.4375" x2="15.5375" y2="10.4625" layer="200"/>
<rectangle x1="15.9625" y1="10.4375" x2="16.1375" y2="10.4625" layer="200"/>
<rectangle x1="18.6625" y1="10.4375" x2="19.4125" y2="10.4625" layer="200"/>
<rectangle x1="20.0375" y1="10.4375" x2="20.3125" y2="10.4625" layer="200"/>
<rectangle x1="21.6625" y1="10.4375" x2="21.8125" y2="10.4625" layer="200"/>
<rectangle x1="22.3625" y1="10.4375" x2="22.9875" y2="10.4625" layer="200"/>
<rectangle x1="4.5875" y1="10.4625" x2="4.8125" y2="10.4875" layer="200"/>
<rectangle x1="5.6875" y1="10.4625" x2="5.9375" y2="10.4875" layer="200"/>
<rectangle x1="7.1875" y1="10.4625" x2="7.5125" y2="10.4875" layer="200"/>
<rectangle x1="11.9375" y1="10.4625" x2="12.2375" y2="10.4875" layer="200"/>
<rectangle x1="12.5375" y1="10.4625" x2="13.2375" y2="10.4875" layer="200"/>
<rectangle x1="14.7375" y1="10.4625" x2="15.6375" y2="10.4875" layer="200"/>
<rectangle x1="15.9625" y1="10.4625" x2="16.1375" y2="10.4875" layer="200"/>
<rectangle x1="18.6625" y1="10.4625" x2="19.4125" y2="10.4875" layer="200"/>
<rectangle x1="20.0625" y1="10.4625" x2="20.3375" y2="10.4875" layer="200"/>
<rectangle x1="21.6875" y1="10.4625" x2="21.8375" y2="10.4875" layer="200"/>
<rectangle x1="22.2875" y1="10.4625" x2="22.9375" y2="10.4875" layer="200"/>
<rectangle x1="4.6125" y1="10.4875" x2="4.8125" y2="10.5125" layer="200"/>
<rectangle x1="5.7375" y1="10.4875" x2="5.8875" y2="10.5125" layer="200"/>
<rectangle x1="7.1125" y1="10.4875" x2="7.4625" y2="10.5125" layer="200"/>
<rectangle x1="11.9625" y1="10.4875" x2="12.2375" y2="10.5125" layer="200"/>
<rectangle x1="12.6125" y1="10.4875" x2="13.3375" y2="10.5125" layer="200"/>
<rectangle x1="14.8375" y1="10.4875" x2="15.7625" y2="10.5125" layer="200"/>
<rectangle x1="15.9625" y1="10.4875" x2="16.1625" y2="10.5125" layer="200"/>
<rectangle x1="18.6625" y1="10.4875" x2="19.4125" y2="10.5125" layer="200"/>
<rectangle x1="20.0875" y1="10.4875" x2="20.3625" y2="10.5125" layer="200"/>
<rectangle x1="21.6875" y1="10.4875" x2="21.8875" y2="10.5125" layer="200"/>
<rectangle x1="22.1375" y1="10.4875" x2="22.8875" y2="10.5125" layer="200"/>
<rectangle x1="4.6625" y1="10.5125" x2="4.8875" y2="10.5375" layer="200"/>
<rectangle x1="5.7875" y1="10.5125" x2="5.8375" y2="10.5375" layer="200"/>
<rectangle x1="7.0125" y1="10.5125" x2="7.3875" y2="10.5375" layer="200"/>
<rectangle x1="11.9875" y1="10.5125" x2="12.2625" y2="10.5375" layer="200"/>
<rectangle x1="12.6875" y1="10.5125" x2="13.4375" y2="10.5375" layer="200"/>
<rectangle x1="14.9375" y1="10.5125" x2="15.8875" y2="10.5375" layer="200"/>
<rectangle x1="15.9625" y1="10.5125" x2="16.1625" y2="10.5375" layer="200"/>
<rectangle x1="18.6875" y1="10.5125" x2="19.3875" y2="10.5375" layer="200"/>
<rectangle x1="20.1125" y1="10.5125" x2="20.3875" y2="10.5375" layer="200"/>
<rectangle x1="21.6875" y1="10.5125" x2="22.8125" y2="10.5375" layer="200"/>
<rectangle x1="4.7125" y1="10.5375" x2="4.9875" y2="10.5625" layer="200"/>
<rectangle x1="6.9625" y1="10.5375" x2="7.3375" y2="10.5625" layer="200"/>
<rectangle x1="12.0125" y1="10.5375" x2="12.2625" y2="10.5625" layer="200"/>
<rectangle x1="12.7625" y1="10.5375" x2="13.5375" y2="10.5625" layer="200"/>
<rectangle x1="15.0375" y1="10.5375" x2="16.1625" y2="10.5625" layer="200"/>
<rectangle x1="18.6875" y1="10.5375" x2="19.3625" y2="10.5625" layer="200"/>
<rectangle x1="20.1375" y1="10.5375" x2="20.4125" y2="10.5625" layer="200"/>
<rectangle x1="21.7125" y1="10.5375" x2="22.7625" y2="10.5625" layer="200"/>
<rectangle x1="4.7875" y1="10.5625" x2="5.0875" y2="10.5875" layer="200"/>
<rectangle x1="6.8875" y1="10.5625" x2="7.2875" y2="10.5875" layer="200"/>
<rectangle x1="12.0375" y1="10.5625" x2="12.2875" y2="10.5875" layer="200"/>
<rectangle x1="12.8125" y1="10.5625" x2="13.6375" y2="10.5875" layer="200"/>
<rectangle x1="15.1375" y1="10.5625" x2="16.1875" y2="10.5875" layer="200"/>
<rectangle x1="18.7125" y1="10.5625" x2="19.3125" y2="10.5875" layer="200"/>
<rectangle x1="20.1625" y1="10.5625" x2="20.4375" y2="10.5875" layer="200"/>
<rectangle x1="21.6875" y1="10.5625" x2="22.6875" y2="10.5875" layer="200"/>
<rectangle x1="4.8375" y1="10.5875" x2="5.1375" y2="10.6125" layer="200"/>
<rectangle x1="6.8625" y1="10.5875" x2="7.2125" y2="10.6125" layer="200"/>
<rectangle x1="12.0375" y1="10.5875" x2="12.2875" y2="10.6125" layer="200"/>
<rectangle x1="12.8625" y1="10.5875" x2="13.7125" y2="10.6125" layer="200"/>
<rectangle x1="15.2375" y1="10.5875" x2="16.1875" y2="10.6125" layer="200"/>
<rectangle x1="18.7125" y1="10.5875" x2="19.2875" y2="10.6125" layer="200"/>
<rectangle x1="20.1875" y1="10.5875" x2="20.4625" y2="10.6125" layer="200"/>
<rectangle x1="21.6125" y1="10.5875" x2="22.6125" y2="10.6125" layer="200"/>
<rectangle x1="4.8875" y1="10.6125" x2="5.2125" y2="10.6375" layer="200"/>
<rectangle x1="6.8125" y1="10.6125" x2="7.1625" y2="10.6375" layer="200"/>
<rectangle x1="12.0625" y1="10.6125" x2="12.3125" y2="10.6375" layer="200"/>
<rectangle x1="12.8875" y1="10.6125" x2="13.8125" y2="10.6375" layer="200"/>
<rectangle x1="15.3375" y1="10.6125" x2="16.2125" y2="10.6375" layer="200"/>
<rectangle x1="18.7375" y1="10.6125" x2="19.2375" y2="10.6375" layer="200"/>
<rectangle x1="20.1875" y1="10.6125" x2="20.4875" y2="10.6375" layer="200"/>
<rectangle x1="21.4875" y1="10.6125" x2="22.5125" y2="10.6375" layer="200"/>
<rectangle x1="4.9375" y1="10.6375" x2="5.2625" y2="10.6625" layer="200"/>
<rectangle x1="6.7625" y1="10.6375" x2="7.0875" y2="10.6625" layer="200"/>
<rectangle x1="12.0875" y1="10.6375" x2="12.3375" y2="10.6625" layer="200"/>
<rectangle x1="12.8875" y1="10.6375" x2="13.8875" y2="10.6625" layer="200"/>
<rectangle x1="15.4125" y1="10.6375" x2="16.2375" y2="10.6625" layer="200"/>
<rectangle x1="18.7375" y1="10.6375" x2="19.1875" y2="10.6625" layer="200"/>
<rectangle x1="20.2125" y1="10.6375" x2="20.5125" y2="10.6625" layer="200"/>
<rectangle x1="21.3375" y1="10.6375" x2="22.4125" y2="10.6625" layer="200"/>
<rectangle x1="5.0125" y1="10.6625" x2="5.3125" y2="10.6875" layer="200"/>
<rectangle x1="6.7125" y1="10.6625" x2="7.0375" y2="10.6875" layer="200"/>
<rectangle x1="12.1125" y1="10.6625" x2="12.3375" y2="10.6875" layer="200"/>
<rectangle x1="12.9125" y1="10.6625" x2="13.9625" y2="10.6875" layer="200"/>
<rectangle x1="15.4875" y1="10.6625" x2="16.2375" y2="10.6875" layer="200"/>
<rectangle x1="18.7625" y1="10.6625" x2="19.1375" y2="10.6875" layer="200"/>
<rectangle x1="20.2125" y1="10.6625" x2="20.5375" y2="10.6875" layer="200"/>
<rectangle x1="21.1875" y1="10.6625" x2="22.3125" y2="10.6875" layer="200"/>
<rectangle x1="5.0625" y1="10.6875" x2="5.3875" y2="10.7125" layer="200"/>
<rectangle x1="6.6375" y1="10.6875" x2="6.9625" y2="10.7125" layer="200"/>
<rectangle x1="12.1125" y1="10.6875" x2="12.3625" y2="10.7125" layer="200"/>
<rectangle x1="12.9125" y1="10.6875" x2="13.1625" y2="10.7125" layer="200"/>
<rectangle x1="13.2875" y1="10.6875" x2="14.0375" y2="10.7125" layer="200"/>
<rectangle x1="15.5625" y1="10.6875" x2="16.2625" y2="10.7125" layer="200"/>
<rectangle x1="18.7625" y1="10.6875" x2="19.0875" y2="10.7125" layer="200"/>
<rectangle x1="20.2125" y1="10.6875" x2="20.5625" y2="10.7125" layer="200"/>
<rectangle x1="21.0125" y1="10.6875" x2="22.2125" y2="10.7125" layer="200"/>
<rectangle x1="5.1125" y1="10.7125" x2="5.4375" y2="10.7375" layer="200"/>
<rectangle x1="6.5875" y1="10.7125" x2="6.9125" y2="10.7375" layer="200"/>
<rectangle x1="12.1375" y1="10.7125" x2="12.3875" y2="10.7375" layer="200"/>
<rectangle x1="12.9375" y1="10.7125" x2="13.1625" y2="10.7375" layer="200"/>
<rectangle x1="13.3875" y1="10.7125" x2="14.1375" y2="10.7375" layer="200"/>
<rectangle x1="15.6625" y1="10.7125" x2="16.2625" y2="10.7375" layer="200"/>
<rectangle x1="18.7875" y1="10.7125" x2="19.0375" y2="10.7375" layer="200"/>
<rectangle x1="20.1875" y1="10.7125" x2="20.6125" y2="10.7375" layer="200"/>
<rectangle x1="20.8125" y1="10.7125" x2="22.0625" y2="10.7375" layer="200"/>
<rectangle x1="5.1875" y1="10.7375" x2="5.4875" y2="10.7625" layer="200"/>
<rectangle x1="6.5125" y1="10.7375" x2="6.8375" y2="10.7625" layer="200"/>
<rectangle x1="12.1625" y1="10.7375" x2="12.3875" y2="10.7625" layer="200"/>
<rectangle x1="12.9375" y1="10.7375" x2="13.1875" y2="10.7625" layer="200"/>
<rectangle x1="13.4875" y1="10.7375" x2="14.2375" y2="10.7625" layer="200"/>
<rectangle x1="15.7625" y1="10.7375" x2="16.2875" y2="10.7625" layer="200"/>
<rectangle x1="18.8125" y1="10.7375" x2="18.9875" y2="10.7625" layer="200"/>
<rectangle x1="20.1625" y1="10.7375" x2="21.9125" y2="10.7625" layer="200"/>
<rectangle x1="5.2375" y1="10.7625" x2="5.5625" y2="10.7875" layer="200"/>
<rectangle x1="6.4625" y1="10.7625" x2="6.7875" y2="10.7875" layer="200"/>
<rectangle x1="12.1875" y1="10.7625" x2="12.4125" y2="10.7875" layer="200"/>
<rectangle x1="12.9625" y1="10.7625" x2="13.1875" y2="10.7875" layer="200"/>
<rectangle x1="13.6125" y1="10.7625" x2="14.3375" y2="10.7875" layer="200"/>
<rectangle x1="15.8625" y1="10.7625" x2="16.2875" y2="10.7875" layer="200"/>
<rectangle x1="18.8375" y1="10.7625" x2="18.9125" y2="10.7875" layer="200"/>
<rectangle x1="20.1125" y1="10.7625" x2="21.7375" y2="10.7875" layer="200"/>
<rectangle x1="5.3125" y1="10.7875" x2="5.6125" y2="10.8125" layer="200"/>
<rectangle x1="6.3875" y1="10.7875" x2="6.7125" y2="10.8125" layer="200"/>
<rectangle x1="12.2125" y1="10.7875" x2="12.4375" y2="10.8125" layer="200"/>
<rectangle x1="12.9875" y1="10.7875" x2="13.2125" y2="10.8125" layer="200"/>
<rectangle x1="13.7125" y1="10.7875" x2="14.4375" y2="10.8125" layer="200"/>
<rectangle x1="15.9375" y1="10.7875" x2="16.3125" y2="10.8125" layer="200"/>
<rectangle x1="20.0125" y1="10.7875" x2="21.5375" y2="10.8125" layer="200"/>
<rectangle x1="5.3625" y1="10.8125" x2="5.6875" y2="10.8375" layer="200"/>
<rectangle x1="6.3125" y1="10.8125" x2="6.6625" y2="10.8375" layer="200"/>
<rectangle x1="12.2375" y1="10.8125" x2="12.4625" y2="10.8375" layer="200"/>
<rectangle x1="12.9875" y1="10.8125" x2="13.2125" y2="10.8375" layer="200"/>
<rectangle x1="13.7875" y1="10.8125" x2="14.5375" y2="10.8375" layer="200"/>
<rectangle x1="15.9875" y1="10.8125" x2="16.3375" y2="10.8375" layer="200"/>
<rectangle x1="19.9375" y1="10.8125" x2="21.3375" y2="10.8375" layer="200"/>
<rectangle x1="5.4125" y1="10.8375" x2="5.7375" y2="10.8625" layer="200"/>
<rectangle x1="6.2375" y1="10.8375" x2="6.5875" y2="10.8625" layer="200"/>
<rectangle x1="12.2375" y1="10.8375" x2="12.4875" y2="10.8625" layer="200"/>
<rectangle x1="13.0125" y1="10.8375" x2="13.2375" y2="10.8625" layer="200"/>
<rectangle x1="13.8875" y1="10.8375" x2="14.6375" y2="10.8625" layer="200"/>
<rectangle x1="16.0125" y1="10.8375" x2="16.3375" y2="10.8625" layer="200"/>
<rectangle x1="19.2875" y1="10.8375" x2="19.4625" y2="10.8625" layer="200"/>
<rectangle x1="19.8375" y1="10.8375" x2="21.1375" y2="10.8625" layer="200"/>
<rectangle x1="5.4875" y1="10.8625" x2="5.8125" y2="10.8875" layer="200"/>
<rectangle x1="6.1625" y1="10.8625" x2="6.5375" y2="10.8875" layer="200"/>
<rectangle x1="12.2625" y1="10.8625" x2="12.5125" y2="10.8875" layer="200"/>
<rectangle x1="13.0125" y1="10.8625" x2="13.2375" y2="10.8875" layer="200"/>
<rectangle x1="13.9875" y1="10.8625" x2="14.7375" y2="10.8875" layer="200"/>
<rectangle x1="16.0625" y1="10.8625" x2="16.3625" y2="10.8875" layer="200"/>
<rectangle x1="19.1875" y1="10.8625" x2="19.4625" y2="10.8875" layer="200"/>
<rectangle x1="19.7625" y1="10.8625" x2="20.9625" y2="10.8875" layer="200"/>
<rectangle x1="5.5375" y1="10.8875" x2="5.8625" y2="10.9125" layer="200"/>
<rectangle x1="6.0875" y1="10.8875" x2="6.4625" y2="10.9125" layer="200"/>
<rectangle x1="12.2875" y1="10.8875" x2="12.5375" y2="10.9125" layer="200"/>
<rectangle x1="13.0375" y1="10.8875" x2="13.2625" y2="10.9125" layer="200"/>
<rectangle x1="14.0875" y1="10.8875" x2="14.8125" y2="10.9125" layer="200"/>
<rectangle x1="16.0875" y1="10.8875" x2="16.3625" y2="10.9125" layer="200"/>
<rectangle x1="19.1125" y1="10.8875" x2="19.4625" y2="10.9125" layer="200"/>
<rectangle x1="19.6875" y1="10.8875" x2="20.7875" y2="10.9125" layer="200"/>
<rectangle x1="5.6125" y1="10.9125" x2="5.9375" y2="10.9375" layer="200"/>
<rectangle x1="5.9875" y1="10.9125" x2="6.4125" y2="10.9375" layer="200"/>
<rectangle x1="12.3125" y1="10.9125" x2="12.5625" y2="10.9375" layer="200"/>
<rectangle x1="13.0625" y1="10.9125" x2="13.2625" y2="10.9375" layer="200"/>
<rectangle x1="14.1625" y1="10.9125" x2="14.9125" y2="10.9375" layer="200"/>
<rectangle x1="16.1125" y1="10.9125" x2="16.3875" y2="10.9375" layer="200"/>
<rectangle x1="19.0375" y1="10.9125" x2="19.4875" y2="10.9375" layer="200"/>
<rectangle x1="19.6125" y1="10.9125" x2="20.6375" y2="10.9375" layer="200"/>
<rectangle x1="5.6625" y1="10.9375" x2="6.3625" y2="10.9625" layer="200"/>
<rectangle x1="12.3375" y1="10.9375" x2="12.5625" y2="10.9625" layer="200"/>
<rectangle x1="13.0625" y1="10.9375" x2="13.2875" y2="10.9625" layer="200"/>
<rectangle x1="14.2625" y1="10.9375" x2="15.0125" y2="10.9625" layer="200"/>
<rectangle x1="16.1625" y1="10.9375" x2="16.4125" y2="10.9625" layer="200"/>
<rectangle x1="18.9625" y1="10.9375" x2="20.4875" y2="10.9625" layer="200"/>
<rectangle x1="5.7375" y1="10.9625" x2="6.3125" y2="10.9875" layer="200"/>
<rectangle x1="12.3625" y1="10.9625" x2="12.5875" y2="10.9875" layer="200"/>
<rectangle x1="13.0875" y1="10.9625" x2="13.2875" y2="10.9875" layer="200"/>
<rectangle x1="14.3625" y1="10.9625" x2="15.1125" y2="10.9875" layer="200"/>
<rectangle x1="16.1875" y1="10.9625" x2="16.4375" y2="10.9875" layer="200"/>
<rectangle x1="18.9375" y1="10.9625" x2="20.3625" y2="10.9875" layer="200"/>
<rectangle x1="5.8125" y1="10.9875" x2="6.2375" y2="11.0125" layer="200"/>
<rectangle x1="12.3875" y1="10.9875" x2="12.6125" y2="11.0125" layer="200"/>
<rectangle x1="13.0875" y1="10.9875" x2="13.3125" y2="11.0125" layer="200"/>
<rectangle x1="14.4625" y1="10.9875" x2="15.2125" y2="11.0125" layer="200"/>
<rectangle x1="16.2125" y1="10.9875" x2="16.4375" y2="11.0125" layer="200"/>
<rectangle x1="18.8875" y1="10.9875" x2="20.2375" y2="11.0125" layer="200"/>
<rectangle x1="5.8625" y1="11.0125" x2="6.1875" y2="11.0375" layer="200"/>
<rectangle x1="12.3875" y1="11.0125" x2="12.6375" y2="11.0375" layer="200"/>
<rectangle x1="13.1125" y1="11.0125" x2="13.3375" y2="11.0375" layer="200"/>
<rectangle x1="14.5625" y1="11.0125" x2="15.3125" y2="11.0375" layer="200"/>
<rectangle x1="16.2375" y1="11.0125" x2="16.4625" y2="11.0375" layer="200"/>
<rectangle x1="18.8625" y1="11.0125" x2="20.1375" y2="11.0375" layer="200"/>
<rectangle x1="5.9375" y1="11.0375" x2="6.1125" y2="11.0625" layer="200"/>
<rectangle x1="12.4125" y1="11.0375" x2="12.6375" y2="11.0625" layer="200"/>
<rectangle x1="13.1375" y1="11.0375" x2="13.3375" y2="11.0625" layer="200"/>
<rectangle x1="14.6625" y1="11.0375" x2="15.3875" y2="11.0625" layer="200"/>
<rectangle x1="16.2625" y1="11.0375" x2="16.4875" y2="11.0625" layer="200"/>
<rectangle x1="18.8625" y1="11.0375" x2="20.0375" y2="11.0625" layer="200"/>
<rectangle x1="12.4375" y1="11.0625" x2="12.6625" y2="11.0875" layer="200"/>
<rectangle x1="13.1375" y1="11.0625" x2="13.3625" y2="11.0875" layer="200"/>
<rectangle x1="14.7625" y1="11.0625" x2="15.4875" y2="11.0875" layer="200"/>
<rectangle x1="16.2875" y1="11.0625" x2="16.4875" y2="11.0875" layer="200"/>
<rectangle x1="18.8375" y1="11.0625" x2="19.9375" y2="11.0875" layer="200"/>
<rectangle x1="12.4375" y1="11.0875" x2="12.6875" y2="11.1125" layer="200"/>
<rectangle x1="13.1625" y1="11.0875" x2="13.3875" y2="11.1125" layer="200"/>
<rectangle x1="14.8375" y1="11.0875" x2="15.5875" y2="11.1125" layer="200"/>
<rectangle x1="16.3125" y1="11.0875" x2="16.5125" y2="11.1125" layer="200"/>
<rectangle x1="18.8375" y1="11.0875" x2="19.8625" y2="11.1125" layer="200"/>
<rectangle x1="12.4625" y1="11.1125" x2="12.7125" y2="11.1375" layer="200"/>
<rectangle x1="13.1625" y1="11.1125" x2="13.3875" y2="11.1375" layer="200"/>
<rectangle x1="14.9125" y1="11.1125" x2="15.6625" y2="11.1375" layer="200"/>
<rectangle x1="16.3375" y1="11.1125" x2="16.5375" y2="11.1375" layer="200"/>
<rectangle x1="18.8375" y1="11.1125" x2="19.7875" y2="11.1375" layer="200"/>
<rectangle x1="12.4875" y1="11.1375" x2="12.7125" y2="11.1625" layer="200"/>
<rectangle x1="13.1875" y1="11.1375" x2="13.4125" y2="11.1625" layer="200"/>
<rectangle x1="14.9875" y1="11.1375" x2="15.7625" y2="11.1625" layer="200"/>
<rectangle x1="16.3375" y1="11.1375" x2="16.5625" y2="11.1625" layer="200"/>
<rectangle x1="18.8375" y1="11.1375" x2="19.7375" y2="11.1625" layer="200"/>
<rectangle x1="12.4875" y1="11.1625" x2="12.7375" y2="11.1875" layer="200"/>
<rectangle x1="13.2125" y1="11.1625" x2="13.4125" y2="11.1875" layer="200"/>
<rectangle x1="14.9875" y1="11.1625" x2="15.8625" y2="11.1875" layer="200"/>
<rectangle x1="16.3625" y1="11.1625" x2="16.5875" y2="11.1875" layer="200"/>
<rectangle x1="18.8375" y1="11.1625" x2="19.6625" y2="11.1875" layer="200"/>
<rectangle x1="12.5125" y1="11.1875" x2="12.7625" y2="11.2125" layer="200"/>
<rectangle x1="13.2125" y1="11.1875" x2="13.4375" y2="11.2125" layer="200"/>
<rectangle x1="14.9625" y1="11.1875" x2="15.9375" y2="11.2125" layer="200"/>
<rectangle x1="16.3875" y1="11.1875" x2="16.6125" y2="11.2125" layer="200"/>
<rectangle x1="18.8125" y1="11.1875" x2="19.6125" y2="11.2125" layer="200"/>
<rectangle x1="12.5375" y1="11.2125" x2="12.7875" y2="11.2375" layer="200"/>
<rectangle x1="13.2375" y1="11.2125" x2="13.4625" y2="11.2375" layer="200"/>
<rectangle x1="14.9375" y1="11.2125" x2="16.0375" y2="11.2375" layer="200"/>
<rectangle x1="16.4125" y1="11.2125" x2="16.6375" y2="11.2375" layer="200"/>
<rectangle x1="18.8125" y1="11.2125" x2="19.5625" y2="11.2375" layer="200"/>
<rectangle x1="12.5625" y1="11.2375" x2="12.7875" y2="11.2625" layer="200"/>
<rectangle x1="13.2375" y1="11.2375" x2="13.4625" y2="11.2625" layer="200"/>
<rectangle x1="14.8875" y1="11.2375" x2="16.1125" y2="11.2625" layer="200"/>
<rectangle x1="16.4375" y1="11.2375" x2="16.6625" y2="11.2625" layer="200"/>
<rectangle x1="18.7875" y1="11.2375" x2="19.5125" y2="11.2625" layer="200"/>
<rectangle x1="12.5875" y1="11.2625" x2="12.8125" y2="11.2875" layer="200"/>
<rectangle x1="13.2625" y1="11.2625" x2="13.4875" y2="11.2875" layer="200"/>
<rectangle x1="14.8375" y1="11.2625" x2="16.1875" y2="11.2875" layer="200"/>
<rectangle x1="16.4625" y1="11.2625" x2="16.6875" y2="11.2875" layer="200"/>
<rectangle x1="18.7625" y1="11.2625" x2="19.4875" y2="11.2875" layer="200"/>
<rectangle x1="12.5875" y1="11.2875" x2="12.8375" y2="11.3125" layer="200"/>
<rectangle x1="13.2625" y1="11.2875" x2="13.5125" y2="11.3125" layer="200"/>
<rectangle x1="14.7625" y1="11.2875" x2="16.2625" y2="11.3125" layer="200"/>
<rectangle x1="16.4625" y1="11.2875" x2="16.7125" y2="11.3125" layer="200"/>
<rectangle x1="18.6625" y1="11.2875" x2="19.4375" y2="11.3125" layer="200"/>
<rectangle x1="12.6125" y1="11.3125" x2="12.8625" y2="11.3375" layer="200"/>
<rectangle x1="13.2875" y1="11.3125" x2="13.5125" y2="11.3375" layer="200"/>
<rectangle x1="14.6875" y1="11.3125" x2="15.2625" y2="11.3375" layer="200"/>
<rectangle x1="15.6125" y1="11.3125" x2="16.3375" y2="11.3375" layer="200"/>
<rectangle x1="16.4875" y1="11.3125" x2="16.7375" y2="11.3375" layer="200"/>
<rectangle x1="18.5125" y1="11.3125" x2="19.3625" y2="11.3375" layer="200"/>
<rectangle x1="12.6375" y1="11.3375" x2="12.8875" y2="11.3625" layer="200"/>
<rectangle x1="13.2875" y1="11.3375" x2="13.5375" y2="11.3625" layer="200"/>
<rectangle x1="14.6125" y1="11.3375" x2="15.2125" y2="11.3625" layer="200"/>
<rectangle x1="15.7375" y1="11.3375" x2="16.7875" y2="11.3625" layer="200"/>
<rectangle x1="18.3375" y1="11.3375" x2="19.3125" y2="11.3625" layer="200"/>
<rectangle x1="12.6625" y1="11.3625" x2="12.9125" y2="11.3875" layer="200"/>
<rectangle x1="13.3125" y1="11.3625" x2="13.5625" y2="11.3875" layer="200"/>
<rectangle x1="14.5375" y1="11.3625" x2="15.1375" y2="11.3875" layer="200"/>
<rectangle x1="15.8375" y1="11.3625" x2="16.8625" y2="11.3875" layer="200"/>
<rectangle x1="18.1375" y1="11.3625" x2="19.2375" y2="11.3875" layer="200"/>
<rectangle x1="12.6875" y1="11.3875" x2="12.9625" y2="11.4125" layer="200"/>
<rectangle x1="13.3125" y1="11.3875" x2="13.5875" y2="11.4125" layer="200"/>
<rectangle x1="14.4625" y1="11.3875" x2="15.0875" y2="11.4125" layer="200"/>
<rectangle x1="15.9375" y1="11.3875" x2="17.0125" y2="11.4125" layer="200"/>
<rectangle x1="17.8375" y1="11.3875" x2="19.1625" y2="11.4125" layer="200"/>
<rectangle x1="12.7125" y1="11.4125" x2="13.0125" y2="11.4375" layer="200"/>
<rectangle x1="13.3375" y1="11.4125" x2="13.5875" y2="11.4375" layer="200"/>
<rectangle x1="14.3625" y1="11.4125" x2="15.0125" y2="11.4375" layer="200"/>
<rectangle x1="16.0375" y1="11.4125" x2="19.0625" y2="11.4375" layer="200"/>
<rectangle x1="12.7375" y1="11.4375" x2="13.1625" y2="11.4625" layer="200"/>
<rectangle x1="13.3125" y1="11.4375" x2="13.6125" y2="11.4625" layer="200"/>
<rectangle x1="14.2625" y1="11.4375" x2="14.9625" y2="11.4625" layer="200"/>
<rectangle x1="16.1125" y1="11.4375" x2="18.9625" y2="11.4625" layer="200"/>
<rectangle x1="12.7625" y1="11.4625" x2="13.6375" y2="11.4875" layer="200"/>
<rectangle x1="14.1625" y1="11.4625" x2="14.8875" y2="11.4875" layer="200"/>
<rectangle x1="16.2125" y1="11.4625" x2="18.8625" y2="11.4875" layer="200"/>
<rectangle x1="12.7875" y1="11.4875" x2="13.6625" y2="11.5125" layer="200"/>
<rectangle x1="14.0375" y1="11.4875" x2="14.8375" y2="11.5125" layer="200"/>
<rectangle x1="16.3125" y1="11.4875" x2="18.7125" y2="11.5125" layer="200"/>
<rectangle x1="12.8125" y1="11.5125" x2="13.7375" y2="11.5375" layer="200"/>
<rectangle x1="13.8875" y1="11.5125" x2="14.7625" y2="11.5375" layer="200"/>
<rectangle x1="16.4125" y1="11.5125" x2="18.5625" y2="11.5375" layer="200"/>
<rectangle x1="12.8375" y1="11.5375" x2="14.6875" y2="11.5625" layer="200"/>
<rectangle x1="16.5125" y1="11.5375" x2="18.3875" y2="11.5625" layer="200"/>
<rectangle x1="12.8875" y1="11.5625" x2="14.6125" y2="11.5875" layer="200"/>
<rectangle x1="16.6375" y1="11.5625" x2="18.1625" y2="11.5875" layer="200"/>
<rectangle x1="12.9375" y1="11.5875" x2="14.5375" y2="11.6125" layer="200"/>
<rectangle x1="16.7625" y1="11.5875" x2="17.8375" y2="11.6125" layer="200"/>
<rectangle x1="13.0375" y1="11.6125" x2="14.4375" y2="11.6375" layer="200"/>
<rectangle x1="13.1625" y1="11.6375" x2="14.3375" y2="11.6625" layer="200"/>
<rectangle x1="13.3125" y1="11.6625" x2="14.1875" y2="11.6875" layer="200"/>
<rectangle x1="13.5375" y1="11.6875" x2="13.9875" y2="11.7125" layer="200"/>
</package>
</packages>
<packages3d>
<package3d name="CHALLENGER" urn="urn:adsk.eagle:package:21548803/1" type="empty" library_version="1">
<packageinstances>
<packageinstance name="CHALLENGER"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="LOGO" urn="urn:adsk.eagle:symbol:21548802/1" library_version="1">
<circle x="0" y="0" radius="2.54" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CHALENGER-11MM" urn="urn:adsk.eagle:component:21548804/1" prefix="LOGO" library_version="1">
<gates>
<gate name="G$1" symbol="LOGO" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CHALLENGER">
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:21548803/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="new_led" urn="urn:adsk.eagle:library:18621472">
<description>&lt;b&gt;LEDs&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;&lt;br&gt;
Extended by Federico Battaglin &lt;author&gt;&amp;lt;federico.rd@fdpinternational.com&amp;gt;&lt;/author&gt; with DUOLED</description>
<packages>
<package name="CHIPLED_0603" urn="urn:adsk.eagle:footprint:22365972/1" library_version="2">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_LY Q971.pdf</description>
<wire x1="-0.3" y1="0.8" x2="0.3" y2="0.8" width="0.1016" layer="51" curve="170.055574"/>
<wire x1="-0.275" y1="-0.825" x2="0.275" y2="-0.825" width="0.0508" layer="51" curve="-180"/>
<wire x1="-0.4" y1="0.375" x2="-0.4" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.35" x2="0.4" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0" y1="0.2" x2="-0.3" y2="-0.2" width="0.1" layer="21"/>
<wire x1="-0.3" y1="-0.2" x2="0.3" y2="-0.2" width="0.1" layer="21"/>
<wire x1="0.3" y1="-0.2" x2="0" y2="0.2" width="0.1" layer="21"/>
<wire x1="-0.2" y1="-0.125" x2="0.175" y2="-0.125" width="0.1" layer="21"/>
<wire x1="-0.15" y1="-0.05" x2="0.125" y2="-0.05" width="0.1" layer="21"/>
<wire x1="-0.1" y1="0.025" x2="0.075" y2="0.025" width="0.1" layer="21"/>
<wire x1="-0.05" y1="0.1" x2="0.025" y2="0.1" width="0.1" layer="21"/>
<wire x1="-0.6" y1="1.35" x2="0.6" y2="1.35" width="0.1" layer="21"/>
<wire x1="0.6" y1="1.35" x2="0.6" y2="-1.35" width="0.1" layer="21"/>
<wire x1="0.6" y1="-1.35" x2="-0.6" y2="-1.35" width="0.1" layer="21"/>
<wire x1="-0.6" y1="-1.35" x2="-0.6" y2="1.35" width="0.1" layer="21"/>
<circle x="-0.35" y="0.625" radius="0.075" width="0.0508" layer="51"/>
<smd name="C" x="0" y="0.75" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.8" layer="1"/>
<text x="0" y="1.6" size="0.8" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.4" size="0.8" layer="27" align="bottom-center">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.7" x2="-0.25" y2="0.85" layer="51"/>
<rectangle x1="-0.275" y1="0.55" x2="-0.225" y2="0.6" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="-0.4" y2="0.725" layer="51"/>
<rectangle x1="0.25" y1="0.55" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="0.45" y2="0.575" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="-0.25" y2="-0.35" layer="51"/>
<rectangle x1="0.25" y1="-0.85" x2="0.45" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.575" x2="0.275" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.65" x2="-0.175" y2="-0.55" layer="51"/>
<rectangle x1="0.175" y1="-0.65" x2="0.275" y2="-0.55" layer="51"/>
</package>
<package name="1206" urn="urn:adsk.eagle:footprint:15651/1" library_version="2">
<description>&lt;b&gt;CHICAGO MINIATURE LAMP, INC.&lt;/b&gt;&lt;p&gt;
7022X Series SMT LEDs 1206 Package Size</description>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.5" x2="0.55" y2="-0.5" width="0.1016" layer="21" curve="95.452622"/>
<wire x1="-0.55" y1="-0.5" x2="-0.55" y2="0.5" width="0.1016" layer="51" curve="-84.547378"/>
<wire x1="-0.55" y1="0.5" x2="0.55" y2="0.5" width="0.1016" layer="21" curve="-95.452622"/>
<wire x1="0.55" y1="0.5" x2="0.55" y2="-0.5" width="0.1016" layer="51" curve="-84.547378"/>
<smd name="A" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="C" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
<rectangle x1="0.45" y1="-0.7" x2="0.8" y2="-0.45" layer="51"/>
<rectangle x1="0.8" y1="-0.7" x2="0.9" y2="0.5" layer="51"/>
<rectangle x1="0.8" y1="0.55" x2="0.9" y2="0.7" layer="51"/>
<rectangle x1="-0.9" y1="-0.7" x2="-0.8" y2="0.5" layer="51"/>
<rectangle x1="-0.9" y1="0.55" x2="-0.8" y2="0.7" layer="51"/>
<rectangle x1="0.45" y1="-0.7" x2="0.6" y2="-0.45" layer="21"/>
</package>
<package name="LD260" urn="urn:adsk.eagle:footprint:15652/1" library_version="2">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, square, Siemens</description>
<wire x1="-1.27" y1="-1.27" x2="0" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.27" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="0" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.889" x2="1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.889" width="0.1524" layer="51"/>
<wire x1="0" y1="1.27" x2="0.9917" y2="0.7934" width="0.1524" layer="21" curve="-51.33923"/>
<wire x1="-0.9917" y1="0.7934" x2="0" y2="1.27" width="0.1524" layer="21" curve="-51.33923"/>
<wire x1="0" y1="-1.27" x2="0.9917" y2="-0.7934" width="0.1524" layer="21" curve="51.33923"/>
<wire x1="-0.9917" y1="-0.7934" x2="0" y2="-1.27" width="0.1524" layer="21" curve="51.33923"/>
<wire x1="0.9558" y1="-0.8363" x2="1.27" y2="0" width="0.1524" layer="51" curve="41.185419"/>
<wire x1="0.9756" y1="0.813" x2="1.2699" y2="0" width="0.1524" layer="51" curve="-39.806332"/>
<wire x1="-1.27" y1="0" x2="-0.9643" y2="-0.8265" width="0.1524" layer="51" curve="40.600331"/>
<wire x1="-1.27" y1="0" x2="-0.9643" y2="0.8265" width="0.1524" layer="51" curve="-40.600331"/>
<wire x1="-0.889" y1="0" x2="0" y2="0.889" width="0.1524" layer="51" curve="-90"/>
<wire x1="-0.508" y1="0" x2="0" y2="0.508" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.508" x2="0.508" y2="0" width="0.1524" layer="21" curve="90"/>
<wire x1="0" y1="-0.889" x2="0.889" y2="0" width="0.1524" layer="51" curve="90"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="1.4732" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.4892" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="1.27" y1="-0.635" x2="2.032" y2="0.635" layer="51"/>
<rectangle x1="1.905" y1="-0.635" x2="2.032" y2="0.635" layer="21"/>
</package>
<package name="LED2X5" urn="urn:adsk.eagle:footprint:15653/1" library_version="2">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
2 x 5 mm, rectangle</description>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="0" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0.381" x2="-0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0.381" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="-0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0.381" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-0.254" x2="1.143" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.762" x2="1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.762" x2="0.9398" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="0.9398" y1="-0.6096" x2="1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.397" y1="-0.254" x2="1.651" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-0.762" x2="1.4478" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="1.4478" y1="-0.6096" x2="1.651" y2="-0.508" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.159" y1="-1.27" x2="2.413" y2="1.27" layer="21"/>
</package>
<package name="LED3MM" urn="urn:adsk.eagle:footprint:15654/1" library_version="2">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
3 mm, round</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="-1.524" y1="0" x2="-1.1708" y2="0.9756" width="0.1524" layer="51" curve="-39.80361"/>
<wire x1="-1.524" y1="0" x2="-1.1391" y2="-1.0125" width="0.1524" layer="51" curve="41.633208"/>
<wire x1="1.1571" y1="0.9918" x2="1.524" y2="0" width="0.1524" layer="51" curve="-40.601165"/>
<wire x1="1.1708" y1="-0.9756" x2="1.524" y2="0" width="0.1524" layer="51" curve="39.80361"/>
<wire x1="0" y1="1.524" x2="1.2401" y2="0.8858" width="0.1524" layer="21" curve="-54.461337"/>
<wire x1="-1.2192" y1="0.9144" x2="0" y2="1.524" width="0.1524" layer="21" curve="-53.130102"/>
<wire x1="0" y1="-1.524" x2="1.203" y2="-0.9356" width="0.1524" layer="21" curve="52.126876"/>
<wire x1="-1.203" y1="-0.9356" x2="0" y2="-1.524" width="0.1524" layer="21" curve="52.126876"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED5MM" urn="urn:adsk.eagle:footprint:15655/1" library_version="2">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, round</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LSU260" urn="urn:adsk.eagle:footprint:15656/1" library_version="2">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
1 mm, round, Siemens</description>
<wire x1="0" y1="-0.508" x2="-1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.508" x2="-1.143" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="0.508" x2="0" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.254" x2="-1.397" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.254" x2="-1.143" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="-0.254" x2="-1.397" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="0.254" x2="-1.143" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="0.254" x2="-1.143" y2="0.508" width="0.1524" layer="51"/>
<wire x1="0.508" y1="-0.254" x2="1.397" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="1.397" y1="-0.254" x2="1.397" y2="0.254" width="0.1524" layer="51"/>
<wire x1="1.397" y1="0.254" x2="0.508" y2="0.254" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-0.381" x2="0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.508" x2="-0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="-0.381" x2="-0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0.381" x2="0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.508" x2="-0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0.381" x2="-0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.254" x2="0.254" y2="0" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.254" y1="0" x2="0" y2="0.254" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.381" y1="-0.381" x2="0.381" y2="0.381" width="0.1524" layer="21" curve="90"/>
<circle x="0" y="0" radius="0.508" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="0.8382" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-1.8542" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.397" y1="-0.254" x2="-1.143" y2="0.254" layer="51"/>
<rectangle x1="0.508" y1="-0.254" x2="1.397" y2="0.254" layer="51"/>
</package>
<package name="LZR181" urn="urn:adsk.eagle:footprint:15657/1" library_version="2">
<description>&lt;B&gt;LED BLOCK&lt;/B&gt;&lt;p&gt;
1 LED, Siemens</description>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.889" x2="1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0" x2="0" y2="0.889" width="0.1524" layer="51" curve="-90"/>
<wire x1="-0.508" y1="0" x2="0" y2="0.508" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.508" x2="0.508" y2="0" width="0.1524" layer="21" curve="90"/>
<wire x1="0" y1="-0.889" x2="0.889" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-0.8678" y1="0.7439" x2="0" y2="1.143" width="0.1524" layer="21" curve="-49.396139"/>
<wire x1="0" y1="1.143" x2="0.8678" y2="0.7439" width="0.1524" layer="21" curve="-49.396139"/>
<wire x1="-0.8678" y1="-0.7439" x2="0" y2="-1.143" width="0.1524" layer="21" curve="49.396139"/>
<wire x1="0" y1="-1.143" x2="0.8678" y2="-0.7439" width="0.1524" layer="21" curve="49.396139"/>
<wire x1="0.8678" y1="0.7439" x2="1.143" y2="0" width="0.1524" layer="51" curve="-40.604135"/>
<wire x1="0.8678" y1="-0.7439" x2="1.143" y2="0" width="0.1524" layer="51" curve="40.604135"/>
<wire x1="-1.143" y1="0" x2="-0.8678" y2="0.7439" width="0.1524" layer="51" curve="-40.604135"/>
<wire x1="-1.143" y1="0" x2="-0.8678" y2="-0.7439" width="0.1524" layer="51" curve="40.604135"/>
<wire x1="-1.27" y1="1.27" x2="1.27" y2="1.27" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="1.4732" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.4892" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="1.27" y1="-0.889" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.27" y2="0.254" layer="51"/>
</package>
<package name="Q62902-B152" urn="urn:adsk.eagle:footprint:15658/1" library_version="2">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-2.9718" y1="-1.8542" x2="-2.9718" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="-0.254" x2="-2.9718" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="0.254" x2="-2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="2.9718" y1="-1.8542" x2="-2.1082" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="-1.8542" x2="-2.54" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.8542" x2="-2.1082" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="1.8542" x2="2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.8542" x2="-2.54" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.8542" x2="-2.9718" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.8542" x2="-2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="0.254" x2="-2.9718" y2="-0.254" width="0.1524" layer="21" curve="180"/>
<wire x1="-1.1486" y1="0.8814" x2="0" y2="1.4478" width="0.1524" layer="21" curve="-52.498642"/>
<wire x1="0" y1="1.4478" x2="1.1476" y2="0.8827" width="0.1524" layer="21" curve="-52.433716"/>
<wire x1="-1.1351" y1="-0.8987" x2="0" y2="-1.4478" width="0.1524" layer="21" curve="51.629985"/>
<wire x1="0" y1="-1.4478" x2="1.1305" y2="-0.9044" width="0.1524" layer="21" curve="51.339172"/>
<wire x1="1.1281" y1="-0.9074" x2="1.4478" y2="0" width="0.1524" layer="51" curve="38.811177"/>
<wire x1="1.1401" y1="0.8923" x2="1.4478" y2="0" width="0.1524" layer="51" curve="-38.048073"/>
<wire x1="-1.4478" y1="0" x2="-1.1305" y2="-0.9044" width="0.1524" layer="51" curve="38.659064"/>
<wire x1="-1.4478" y1="0" x2="-1.1456" y2="0.8853" width="0.1524" layer="51" curve="-37.696376"/>
<wire x1="0" y1="1.7018" x2="1.4674" y2="0.8618" width="0.1524" layer="21" curve="-59.573488"/>
<wire x1="-1.4618" y1="0.8714" x2="0" y2="1.7018" width="0.1524" layer="21" curve="-59.200638"/>
<wire x1="0" y1="-1.7018" x2="1.4571" y2="-0.8793" width="0.1524" layer="21" curve="58.891781"/>
<wire x1="-1.4571" y1="-0.8793" x2="0" y2="-1.7018" width="0.1524" layer="21" curve="58.891781"/>
<wire x1="-1.7018" y1="0" x2="-1.4447" y2="0.8995" width="0.1524" layer="51" curve="-31.907626"/>
<wire x1="-1.7018" y1="0" x2="-1.4502" y2="-0.8905" width="0.1524" layer="51" curve="31.551992"/>
<wire x1="1.4521" y1="0.8874" x2="1.7018" y2="0" width="0.1524" layer="51" curve="-31.429586"/>
<wire x1="1.4459" y1="-0.8975" x2="1.7018" y2="0" width="0.1524" layer="51" curve="31.828757"/>
<wire x1="-2.1082" y1="1.8542" x2="-2.1082" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0.0539" y1="-1.0699" x2="1.0699" y2="-0.0539" width="0.1524" layer="51" curve="90"/>
<wire x1="2.9718" y1="1.8542" x2="2.9718" y2="-1.8542" width="0.1524" layer="21"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.905" y="2.286" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.905" y="-3.556" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="Q62902-B153" urn="urn:adsk.eagle:footprint:15659/1" library_version="2">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-5.5118" y1="-3.5052" x2="-5.5118" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="-0.254" x2="-5.5118" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="0.254" x2="-5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="5.5118" y1="-3.5052" x2="-4.6482" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-4.6482" y1="-3.5052" x2="-5.08" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="3.5052" x2="-4.6482" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-4.6482" y1="3.5052" x2="5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-3.5052" x2="-5.08" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-3.5052" x2="-5.5118" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="3.5052" x2="-5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="0.254" x2="-5.5118" y2="-0.254" width="0.1524" layer="21" curve="180"/>
<wire x1="-4.6482" y1="3.5052" x2="-4.6482" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="5.5118" y1="3.5052" x2="5.5118" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.254" layer="21"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.2129" y1="0.0539" x2="-0.0539" y2="2.2129" width="0.1524" layer="51" curve="-90.010616"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.254" layer="21"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-4.191" y="3.937" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-5.08" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="Q62902-B155" urn="urn:adsk.eagle:footprint:15660/1" library_version="2">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-1.27" y1="-3.048" x2="-1.27" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.033" y1="3.048" x2="2.921" y2="3.048" width="0.1524" layer="21"/>
<wire x1="10.033" y1="3.048" x2="10.033" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-3.048" x2="2.921" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-3.048" x2="2.921" y2="3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-3.048" x2="10.033" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="3.048" x2="-1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-5.207" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.54" x2="-1.27" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.54" x2="-5.207" y2="-2.54" width="0.1524" layer="21" curve="180"/>
<wire x1="-6.985" y1="0.635" x2="-6.985" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="1.397" x2="-6.096" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="1.905" x2="-5.207" y2="-1.905" width="0.1524" layer="21"/>
<pad name="K" x="7.62" y="1.27" drill="0.8128" shape="long"/>
<pad name="A" x="7.62" y="-1.27" drill="0.8128" shape="long"/>
<text x="3.302" y="-2.794" size="1.016" layer="21" ratio="14">A+</text>
<text x="3.302" y="1.778" size="1.016" layer="21" ratio="14">K-</text>
<text x="11.684" y="-2.794" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="0.635" y="-4.445" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.921" y1="1.016" x2="6.731" y2="1.524" layer="21"/>
<rectangle x1="2.921" y1="-1.524" x2="6.731" y2="-1.016" layer="21"/>
<hole x="0" y="0" drill="0.8128"/>
</package>
<package name="Q62902-B156" urn="urn:adsk.eagle:footprint:15661/1" library_version="2">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90"/>
<wire x1="0.0539" y1="-2.2129" x2="2.2129" y2="-0.0539" width="0.1524" layer="51" curve="90.005308"/>
<wire x1="2.54" y1="3.81" x2="3.81" y2="2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="3.81" x2="-3.81" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.1524" layer="21"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-3.302" x2="-2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-3.81" x2="2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-3.302" x2="2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-3.302" x2="-2.54" y2="-3.302" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.254" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="K" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-3.81" y="4.0894" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.7846" y="-5.3594" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-3.556" y="-3.302" size="1.016" layer="21" ratio="14">+</text>
<text x="2.794" y="-3.302" size="1.016" layer="21" ratio="14">-</text>
</package>
<package name="SFH480" urn="urn:adsk.eagle:footprint:15662/1" library_version="2">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking&lt;p&gt;
Inifineon</description>
<wire x1="-2.159" y1="1.524" x2="-2.794" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="0" y1="1.778" x2="1.5358" y2="0.8959" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="0.8959" x2="0" y2="1.778" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="-0.8959" x2="0" y2="-1.778" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="0" y1="-1.778" x2="1.5358" y2="-0.8959" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="1.5142" y1="0.9318" x2="1.778" y2="0" width="0.1524" layer="51" curve="-31.606487"/>
<wire x1="1.5" y1="-0.9546" x2="1.778" y2="0" width="0.1524" layer="51" curve="32.472615"/>
<wire x1="-1.778" y1="0" x2="-1.5142" y2="-0.9318" width="0.1524" layer="51" curve="31.606487"/>
<wire x1="-1.778" y1="0" x2="-1.5" y2="0.9546" width="0.1524" layer="51" curve="-32.472615"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0.0539" y1="-1.0699" x2="1.0699" y2="-0.0539" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.667" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.413" width="0.254" layer="21"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.27" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.318" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SFH482" urn="urn:adsk.eagle:footprint:15650/1" library_version="2">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking&lt;p&gt;
Inifineon</description>
<wire x1="-2.159" y1="1.524" x2="-2.794" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="0" y1="1.778" x2="1.5358" y2="0.8959" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="0.8959" x2="0" y2="1.778" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="-0.8959" x2="0" y2="-1.778" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="0" y1="-1.778" x2="1.5358" y2="-0.8959" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="1.5142" y1="0.9318" x2="1.778" y2="0" width="0.1524" layer="51" curve="-31.606487"/>
<wire x1="1.5" y1="-0.9546" x2="1.778" y2="0" width="0.1524" layer="51" curve="32.472615"/>
<wire x1="-1.778" y1="0" x2="-1.5142" y2="-0.9318" width="0.1524" layer="51" curve="31.606487"/>
<wire x1="-1.778" y1="0" x2="-1.5" y2="0.9546" width="0.1524" layer="51" curve="-32.472615"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0.0539" y1="-1.0699" x2="1.0699" y2="-0.0539" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.667" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.413" width="0.254" layer="21"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.27" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.318" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="U57X32" urn="urn:adsk.eagle:footprint:15640/1" library_version="2">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
rectangle, 5.7 x 3.2 mm</description>
<wire x1="-3.175" y1="1.905" x2="3.175" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.905" x2="3.175" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.905" x2="-3.175" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.905" x2="-3.175" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="1.397" x2="2.667" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.667" y1="-1.397" x2="2.667" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.667" y1="-1.397" x2="-2.667" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="1.397" x2="-2.667" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="2.54" y2="1.016" width="0.1524" layer="51"/>
<wire x1="2.286" y1="1.27" x2="2.286" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="0.508" x2="2.54" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-0.508" x2="2.54" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-1.016" x2="2.54" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="1.27" x2="-1.778" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.254" y1="1.27" x2="0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.762" y1="1.27" x2="0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.778" y1="1.27" x2="1.778" y2="-1.27" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="3.683" y="0.254" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.683" y="-1.524" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="IRL80A" urn="urn:adsk.eagle:footprint:15663/1" library_version="2">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
IR transmitter Siemens</description>
<wire x1="0.889" y1="2.286" x2="0.889" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.889" y1="1.778" x2="0.889" y2="0.762" width="0.1524" layer="51"/>
<wire x1="0.889" y1="0.762" x2="0.889" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-0.635" x2="0.889" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-1.778" x2="0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-2.286" x2="-0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="2.286" x2="-0.889" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.778" x2="-0.889" y2="0.762" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0.762" x2="-0.889" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-0.762" x2="-0.889" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-1.778" x2="-0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="2.286" x2="0.889" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-0.762" x2="-0.889" y2="0.762" width="0.1524" layer="21" curve="-180"/>
<wire x1="-1.397" y1="0.254" x2="-1.397" y2="-0.254" width="0.0508" layer="21"/>
<wire x1="-1.143" y1="0.508" x2="-1.143" y2="-0.508" width="0.0508" layer="21"/>
<pad name="K" x="0" y="1.27" drill="0.8128" shape="octagon"/>
<pad name="A" x="0" y="-1.27" drill="0.8128" shape="octagon"/>
<text x="1.27" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.27" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="P-LCC-2" urn="urn:adsk.eagle:footprint:15664/1" library_version="2">
<description>&lt;b&gt;TOPLED® High-optical Power LED (HOP)&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... ls_t675.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="1.8" x2="1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="1.8" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-1.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.8" x2="1.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="-1.8" x2="1.1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="51"/>
<smd name="C" x="0" y="-2.75" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.75" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-0.635" y="2.54" size="1.27" layer="21">A</text>
<text x="-0.635" y="-3.81" size="1.27" layer="21">C</text>
<rectangle x1="-1.3" y1="-2.25" x2="1.3" y2="-0.75" layer="31"/>
<rectangle x1="-1.3" y1="0.75" x2="1.3" y2="2.25" layer="31"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="21"/>
<rectangle x1="-1.4" y1="0.65" x2="1.4" y2="2.35" layer="29"/>
<rectangle x1="-1.4" y1="-2.35" x2="1.4" y2="-0.65" layer="29"/>
</package>
<package name="OSRAM-MINI-TOP-LED" urn="urn:adsk.eagle:footprint:15665/1" library_version="2">
<description>&lt;b&gt;BLUE LINETM Hyper Mini TOPLED® Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LB M676.pdf</description>
<wire x1="-0.6" y1="0.9" x2="-0.6" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-0.9" x2="-0.4" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="-0.4" y1="-0.9" x2="0.6" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="0.6" y1="-0.9" x2="0.6" y2="0.9" width="0.1016" layer="51"/>
<wire x1="0.6" y1="0.9" x2="-0.6" y2="0.9" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="0.95" x2="-0.45" y2="1.1" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="1.1" x2="0.45" y2="1.1" width="0.1016" layer="51"/>
<wire x1="0.45" y1="1.1" x2="0.45" y2="0.95" width="0.1016" layer="51"/>
<wire x1="-0.6" y1="-0.7" x2="-0.4" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-0.9" x2="-0.45" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-1.1" x2="0.45" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="0.45" y1="-1.1" x2="0.45" y2="-0.95" width="0.1016" layer="51"/>
<smd name="A" x="0" y="2.6" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="C" x="0" y="-2.6" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-0.635" y="1.905" size="1.27" layer="21">A</text>
<text x="-0.635" y="-3.175" size="1.27" layer="21">C</text>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.5" y1="0.6" x2="0.5" y2="1.4" layer="29"/>
<rectangle x1="-0.5" y1="-1.4" x2="0.5" y2="-0.6" layer="29"/>
<rectangle x1="-0.15" y1="-0.6" x2="0.15" y2="-0.3" layer="51"/>
<rectangle x1="-0.45" y1="0.65" x2="0.45" y2="1.35" layer="31"/>
<rectangle x1="-0.45" y1="-1.35" x2="0.45" y2="-0.65" layer="31"/>
</package>
<package name="OSRAM-SIDELED" urn="urn:adsk.eagle:footprint:15666/1" library_version="2">
<description>&lt;b&gt;Super SIDELED® High-Current LED&lt;/b&gt;&lt;p&gt;
LG A672, LP A672 &lt;br&gt;
Source: http://www.osram.convergy.de/ ... LG_LP_A672.pdf (2004.05.13)</description>
<wire x1="-1.85" y1="-2.05" x2="-1.85" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="-0.75" x2="-1.7" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="-0.75" x2="-1.7" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="0.75" x2="-1.85" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="0.75" x2="-1.85" y2="2.05" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="2.05" x2="0.9" y2="2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="2.05" x2="0.9" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="-2.05" x2="-1.85" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="-2.05" x2="1.05" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="-2.05" x2="1.85" y2="-1.85" width="0.1016" layer="51"/>
<wire x1="1.85" y1="-1.85" x2="1.85" y2="1.85" width="0.1016" layer="51"/>
<wire x1="1.85" y1="1.85" x2="1.05" y2="2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="2.05" x2="0.9" y2="2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="2.05" x2="1.05" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.9" x2="-0.55" y2="0.9" width="0.1016" layer="51" curve="-167.319617"/>
<wire x1="-0.55" y1="-0.9" x2="0.85" y2="-1.2" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.55" y1="0.9" x2="0.85" y2="1.2" width="0.1016" layer="51" style="shortdash"/>
<smd name="C" x="0" y="-2.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="0.635" y="-3.175" size="1.27" layer="21" rot="R90">C</text>
<text x="0.635" y="2.54" size="1.27" layer="21" rot="R90">A</text>
<text x="-2.54" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.1" y1="-2.2" x2="2.1" y2="-0.4" layer="29"/>
<rectangle x1="-2.1" y1="0.4" x2="2.1" y2="2.2" layer="29"/>
<rectangle x1="-1.9" y1="-2.1" x2="1.9" y2="-0.6" layer="31"/>
<rectangle x1="-1.9" y1="0.6" x2="1.9" y2="2.1" layer="31"/>
<rectangle x1="-1.85" y1="-2.05" x2="-0.7" y2="-1" layer="51"/>
</package>
<package name="SMART-LED" urn="urn:adsk.eagle:footprint:15667/1" library_version="2">
<description>&lt;b&gt;SmartLEDTM Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY L896.pdf</description>
<wire x1="-0.35" y1="0.6" x2="0.35" y2="0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.35" y1="0.6" x2="0.35" y2="-0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.6" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.15" y1="-0.6" x2="-0.35" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.35" y1="-0.6" x2="-0.35" y2="0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.4" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<smd name="A" x="0" y="0.725" dx="0.35" dy="0.35" layer="1"/>
<smd name="B" x="0" y="-0.725" dx="0.35" dy="0.35" layer="1"/>
<text x="-0.635" y="-0.635" size="1.016" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-0.635" size="1.016" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.15" y1="-0.35" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.15" y1="0.6" x2="0.15" y2="0.85" layer="51"/>
<rectangle x1="-0.15" y1="-0.85" x2="0.15" y2="-0.6" layer="51"/>
</package>
<package name="P-LCC-2-TOPLED-RG" urn="urn:adsk.eagle:footprint:15668/1" library_version="2">
<description>&lt;b&gt;Hyper TOPLED® RG Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY T776.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="2.45" width="0.1016" layer="51"/>
<wire x1="1.1" y1="2.45" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-1.1" y2="-2.45" width="0.1016" layer="51"/>
<wire x1="1.1" y1="-2.45" x2="1.1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="21"/>
<smd name="C" x="0" y="-3.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="3.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-0.635" y="3.29" size="1.27" layer="21">A</text>
<text x="-0.635" y="-4.56" size="1.27" layer="21">C</text>
<rectangle x1="-1.3" y1="-3" x2="1.3" y2="-1.5" layer="31"/>
<rectangle x1="-1.3" y1="1.5" x2="1.3" y2="3" layer="31"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="21"/>
<rectangle x1="-1.15" y1="2.4" x2="1.15" y2="2.7" layer="51"/>
<rectangle x1="-1.15" y1="-2.7" x2="1.15" y2="-2.4" layer="51"/>
<rectangle x1="-1.5" y1="1.5" x2="1.5" y2="3.2" layer="29"/>
<rectangle x1="-1.5" y1="-3.2" x2="1.5" y2="-1.5" layer="29"/>
<hole x="0" y="0" drill="2.8"/>
</package>
<package name="MICRO-SIDELED" urn="urn:adsk.eagle:footprint:15669/1" library_version="2">
<description>&lt;b&gt;Hyper Micro SIDELED®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY Y876.pdf</description>
<wire x1="0.65" y1="1.1" x2="-0.1" y2="1.1" width="0.1016" layer="51"/>
<wire x1="-0.1" y1="1.1" x2="-0.35" y2="1" width="0.1016" layer="51"/>
<wire x1="-0.35" y1="1" x2="-0.35" y2="-0.9" width="0.1016" layer="21"/>
<wire x1="-0.35" y1="-0.9" x2="-0.1" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="-0.1" y1="-1.1" x2="0.65" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="0.65" y1="-1.1" x2="0.65" y2="1.1" width="0.1016" layer="21"/>
<wire x1="0.6" y1="0.9" x2="0.25" y2="0.7" width="0.0508" layer="21"/>
<wire x1="0.25" y1="0.7" x2="0.25" y2="-0.7" width="0.0508" layer="21"/>
<wire x1="0.25" y1="-0.7" x2="0.6" y2="-0.9" width="0.0508" layer="21"/>
<smd name="A" x="0" y="1.95" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<smd name="C" x="0" y="-1.95" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<text x="-1.27" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.4" y1="1.1" x2="0.4" y2="1.8" layer="29"/>
<rectangle x1="-0.4" y1="-1.8" x2="0.4" y2="-1.1" layer="29"/>
<rectangle x1="-0.35" y1="-1.75" x2="0.35" y2="-1.15" layer="31"/>
<rectangle x1="-0.35" y1="1.15" x2="0.35" y2="1.75" layer="31"/>
<rectangle x1="-0.125" y1="1.125" x2="0.125" y2="1.75" layer="51"/>
<rectangle x1="-0.125" y1="-1.75" x2="0.125" y2="-1.125" layer="51"/>
</package>
<package name="P-LCC-4" urn="urn:adsk.eagle:footprint:18621508/1" library_version="2">
<description>&lt;b&gt;Power TOPLED®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LA_LY E67B.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.6" x2="-1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-1" y1="1.8" x2="-0.5" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="1.8" x2="-0.5" y2="1.65" width="0.1016" layer="51"/>
<wire x1="0.5" y1="1.65" x2="0.5" y2="1.8" width="0.1016" layer="51"/>
<wire x1="0.5" y1="1.8" x2="1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="1.8" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1" y1="-1.6" x2="-1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-1" y1="-1.8" x2="-0.5" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="-1.8" x2="-0.5" y2="-1.65" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-1.65" x2="0.5" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-1.8" x2="1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="1" y1="-1.8" x2="1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="51"/>
<smd name="A" x="-0.8" y="1.45" dx="1.1" dy="1.5" layer="1" stop="no" cream="no"/>
<smd name="C@3" x="0.8" y="1.45" dx="1.1" dy="1.5" layer="1" stop="no" cream="no"/>
<smd name="C@4" x="0.8" y="-1.45" dx="1.1" dy="1.5" layer="1" stop="no" cream="no"/>
<smd name="C@1" x="-0.8" y="-1.45" dx="1.1" dy="1.5" layer="1" stop="no" cream="no"/>
<text x="-3.81" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.08" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-1.905" y="-3.81" size="1.27" layer="21">C</text>
<text x="-1.905" y="2.54" size="1.27" layer="21">A</text>
<text x="1.27" y="2.54" size="1.27" layer="21">C</text>
<text x="1.27" y="-3.81" size="1.27" layer="21">C</text>
<rectangle x1="-1.15" y1="0.75" x2="-0.35" y2="1.85" layer="29"/>
<rectangle x1="0.35" y1="0.75" x2="1.15" y2="1.85" layer="29"/>
<rectangle x1="0.35" y1="-1.85" x2="1.15" y2="-0.75" layer="29"/>
<rectangle x1="-1.15" y1="-1.85" x2="-0.35" y2="-0.75" layer="29"/>
<rectangle x1="-1.1" y1="-1.8" x2="-0.4" y2="-0.8" layer="31"/>
<rectangle x1="0.4" y1="-1.8" x2="1.1" y2="-0.8" layer="31"/>
<rectangle x1="0.4" y1="0.8" x2="1.1" y2="1.8" layer="31"/>
<rectangle x1="-1.1" y1="0.8" x2="-0.4" y2="1.8" layer="31"/>
<rectangle x1="-0.2" y1="-0.2" x2="0.2" y2="0.2" layer="21"/>
</package>
<package name="CHIP-LED0603" urn="urn:adsk.eagle:footprint:15671/1" library_version="2">
<description>&lt;b&gt;Hyper CHIPLED Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
LB Q993&lt;br&gt;
Source: http://www.osram.convergy.de/ ... Lb_q993.pdf</description>
<wire x1="-0.4" y1="0.45" x2="-0.4" y2="-0.45" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.45" x2="0.4" y2="-0.45" width="0.1016" layer="51"/>
<smd name="C" x="0" y="0.75" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.635" y="-0.635" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-0.635" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.45" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="0.45" y2="-0.45" layer="51"/>
<rectangle x1="-0.45" y1="0" x2="-0.3" y2="0.3" layer="21"/>
<rectangle x1="0.3" y1="0" x2="0.45" y2="0.3" layer="21"/>
<rectangle x1="-0.15" y1="0" x2="0.15" y2="0.3" layer="21"/>
</package>
<package name="CHIP-LED0805" urn="urn:adsk.eagle:footprint:15672/1" library_version="2">
<description>&lt;b&gt;Hyper CHIPLED Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
LB R99A&lt;br&gt;
Source: http://www.osram.convergy.de/ ... lb_r99a.pdf</description>
<wire x1="-0.625" y1="0.45" x2="-0.625" y2="-0.45" width="0.1016" layer="51"/>
<wire x1="0.625" y1="0.45" x2="0.625" y2="-0.475" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.05" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="0" y="-1.05" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.675" y1="0" x2="-0.525" y2="0.3" layer="21"/>
<rectangle x1="0.525" y1="0" x2="0.675" y2="0.3" layer="21"/>
<rectangle x1="-0.15" y1="0" x2="0.15" y2="0.3" layer="21"/>
<rectangle x1="-0.675" y1="0.45" x2="0.675" y2="1.05" layer="51"/>
<rectangle x1="-0.675" y1="-1.05" x2="0.675" y2="-0.45" layer="51"/>
</package>
<package name="MINI-TOPLED-SANTANA" urn="urn:adsk.eagle:footprint:15673/1" library_version="2">
<description>&lt;b&gt;Mini TOPLED Santana®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG M470.pdf</description>
<wire x1="0.7" y1="-1" x2="0.35" y2="-1" width="0.1016" layer="21"/>
<wire x1="0.35" y1="-1" x2="-0.7" y2="-1" width="0.1016" layer="21"/>
<wire x1="-0.7" y1="-1" x2="-0.7" y2="1" width="0.1016" layer="21"/>
<wire x1="-0.7" y1="1" x2="0.7" y2="1" width="0.1016" layer="21"/>
<wire x1="0.7" y1="1" x2="0.7" y2="-0.65" width="0.1016" layer="21"/>
<wire x1="0.7" y1="-0.65" x2="0.7" y2="-1" width="0.1016" layer="21"/>
<wire x1="0.45" y1="-0.7" x2="-0.45" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="-0.7" x2="-0.45" y2="0.7" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="0.7" x2="0.45" y2="0.7" width="0.1016" layer="21"/>
<wire x1="0.45" y1="0.7" x2="0.45" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="0.7" y1="-0.65" x2="0.35" y2="-1" width="0.1016" layer="21"/>
<smd name="C" x="0" y="-2.2" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.2" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<text x="-1.27" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.55" y1="1.5" x2="0.55" y2="2.1" layer="29"/>
<rectangle x1="-0.55" y1="-2.1" x2="0.55" y2="-1.5" layer="29"/>
<rectangle x1="-0.5" y1="-2.05" x2="0.5" y2="-1.55" layer="31"/>
<rectangle x1="-0.5" y1="1.55" x2="0.5" y2="2.05" layer="31"/>
<rectangle x1="-0.2" y1="-0.4" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.5" y1="-2.1" x2="0.5" y2="-1.4" layer="51"/>
<rectangle x1="-0.5" y1="1.4" x2="0.5" y2="2.05" layer="51"/>
<rectangle x1="-0.5" y1="1" x2="0.5" y2="1.4" layer="21"/>
<rectangle x1="-0.5" y1="-1.4" x2="0.5" y2="-1.05" layer="21"/>
<hole x="0" y="0" drill="2.7"/>
</package>
<package name="CHIPLED_0805" urn="urn:adsk.eagle:footprint:15674/1" library_version="2">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_R971.pdf</description>
<wire x1="-0.35" y1="0.925" x2="0.35" y2="0.925" width="0.1016" layer="51" curve="162.394521"/>
<wire x1="-0.35" y1="-0.925" x2="0.35" y2="-0.925" width="0.1016" layer="51" curve="-162.394521"/>
<wire x1="0.575" y1="0.525" x2="0.575" y2="-0.525" width="0.1016" layer="51"/>
<wire x1="-0.575" y1="-0.5" x2="-0.575" y2="0.925" width="0.1016" layer="51"/>
<circle x="-0.45" y="0.85" radius="0.103" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.05" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="0" y="-1.05" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="0.3" y1="0.5" x2="0.625" y2="1" layer="51"/>
<rectangle x1="-0.325" y1="0.5" x2="-0.175" y2="0.75" layer="51"/>
<rectangle x1="0.175" y1="0.5" x2="0.325" y2="0.75" layer="51"/>
<rectangle x1="-0.2" y1="0.5" x2="0.2" y2="0.675" layer="51"/>
<rectangle x1="0.3" y1="-1" x2="0.625" y2="-0.5" layer="51"/>
<rectangle x1="-0.625" y1="-1" x2="-0.3" y2="-0.5" layer="51"/>
<rectangle x1="0.175" y1="-0.75" x2="0.325" y2="-0.5" layer="51"/>
<rectangle x1="-0.325" y1="-0.75" x2="-0.175" y2="-0.5" layer="51"/>
<rectangle x1="-0.2" y1="-0.675" x2="0.2" y2="-0.5" layer="51"/>
<rectangle x1="-0.1" y1="0" x2="0.1" y2="0.2" layer="21"/>
<rectangle x1="-0.6" y1="0.5" x2="-0.3" y2="0.8" layer="51"/>
<rectangle x1="-0.625" y1="0.925" x2="-0.3" y2="1" layer="51"/>
</package>
<package name="CHIPLED_1206" urn="urn:adsk.eagle:footprint:15675/1" library_version="2">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_LY N971.pdf</description>
<wire x1="-0.4" y1="1.6" x2="0.4" y2="1.6" width="0.1016" layer="51" curve="172.619069"/>
<wire x1="-0.8" y1="-0.95" x2="-0.8" y2="0.95" width="0.1016" layer="51"/>
<wire x1="0.8" y1="0.95" x2="0.8" y2="-0.95" width="0.1016" layer="51"/>
<circle x="-0.55" y="1.425" radius="0.1" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.75" dx="1.5" dy="1.5" layer="1"/>
<smd name="A" x="0" y="-1.75" dx="1.5" dy="1.5" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.85" y1="1.525" x2="-0.35" y2="1.65" layer="51"/>
<rectangle x1="-0.85" y1="1.225" x2="-0.625" y2="1.55" layer="51"/>
<rectangle x1="-0.45" y1="1.225" x2="-0.325" y2="1.45" layer="51"/>
<rectangle x1="-0.65" y1="1.225" x2="-0.225" y2="1.35" layer="51"/>
<rectangle x1="0.35" y1="1.3" x2="0.85" y2="1.65" layer="51"/>
<rectangle x1="0.25" y1="1.225" x2="0.85" y2="1.35" layer="51"/>
<rectangle x1="-0.85" y1="0.95" x2="0.85" y2="1.25" layer="51"/>
<rectangle x1="-0.85" y1="-1.65" x2="0.85" y2="-0.95" layer="51"/>
<rectangle x1="-0.85" y1="0.35" x2="-0.525" y2="0.775" layer="21"/>
<rectangle x1="0.525" y1="0.35" x2="0.85" y2="0.775" layer="21"/>
<rectangle x1="-0.175" y1="0" x2="0.175" y2="0.35" layer="21"/>
</package>
<package name="CHIPLED-0603-TTW" urn="urn:adsk.eagle:footprint:15677/1" library_version="2">
<description>&lt;b&gt;CHIPLED-0603&lt;/b&gt;&lt;p&gt;
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603&lt;br&gt;
Package able to withstand TTW-soldering heat&lt;br&gt;
Package suitable for TTW-soldering&lt;br&gt;
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
<wire x1="-0.3" y1="0.8" x2="0.3" y2="0.8" width="0.1016" layer="51" curve="170.055574"/>
<wire x1="-0.275" y1="-0.825" x2="0.275" y2="-0.825" width="0.0508" layer="51" curve="-180"/>
<wire x1="-0.4" y1="0.375" x2="-0.4" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.35" x2="0.4" y2="-0.35" width="0.1016" layer="51"/>
<circle x="-0.35" y="0.625" radius="0.075" width="0.0508" layer="51"/>
<smd name="C" x="0" y="0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="-0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A@1" x="0" y="-0.5" dx="0.35" dy="0.35" layer="1"/>
<smd name="C@1" x="0" y="0.5" dx="0.35" dy="0.35" layer="1"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.7" x2="-0.25" y2="0.85" layer="51"/>
<rectangle x1="-0.275" y1="0.55" x2="-0.225" y2="0.6" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="-0.4" y2="0.725" layer="51"/>
<rectangle x1="0.25" y1="0.55" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="0.45" y2="0.575" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="-0.25" y2="-0.35" layer="51"/>
<rectangle x1="0.25" y1="-0.85" x2="0.45" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.575" x2="0.275" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.65" x2="-0.175" y2="-0.55" layer="51"/>
<rectangle x1="0.175" y1="-0.65" x2="0.275" y2="-0.55" layer="51"/>
<rectangle x1="-0.125" y1="0" x2="0.125" y2="0.25" layer="21"/>
<rectangle x1="-0.175" y1="0.325" x2="0.175" y2="0.7" layer="29"/>
<rectangle x1="-0.4" y1="0.625" x2="0.4" y2="1.125" layer="29"/>
<rectangle x1="-0.4" y1="-1.125" x2="0.4" y2="-0.625" layer="29"/>
<rectangle x1="-0.175" y1="-0.675" x2="0.175" y2="-0.325" layer="29"/>
</package>
<package name="SMARTLED-TTW" urn="urn:adsk.eagle:footprint:15678/1" library_version="2">
<description>&lt;b&gt;SmartLED TTW&lt;/b&gt;&lt;p&gt;
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603&lt;br&gt;
Package able to withstand TTW-soldering heat&lt;br&gt;
Package suitable for TTW-soldering&lt;br&gt;
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
<wire x1="-0.35" y1="0.6" x2="0.35" y2="0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.35" y1="0.6" x2="0.35" y2="-0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.6" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.15" y1="-0.6" x2="-0.35" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.35" y1="-0.6" x2="-0.35" y2="0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.4" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<smd name="C" x="0" y="0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="-0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A@1" x="0" y="-0.5" dx="0.35" dy="0.35" layer="1" stop="no" cream="no"/>
<smd name="C@1" x="0" y="0.5" dx="0.35" dy="0.35" layer="1" stop="no" cream="no"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.175" y1="0.325" x2="0.175" y2="0.7" layer="29"/>
<rectangle x1="-0.15" y1="-0.35" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.15" y1="0.6" x2="0.15" y2="0.85" layer="51"/>
<rectangle x1="-0.15" y1="-0.85" x2="0.15" y2="-0.6" layer="51"/>
<rectangle x1="-0.225" y1="0.3" x2="0.225" y2="0.975" layer="31"/>
<rectangle x1="-0.175" y1="-0.7" x2="0.175" y2="-0.325" layer="29" rot="R180"/>
<rectangle x1="-0.225" y1="-0.975" x2="0.225" y2="-0.3" layer="31" rot="R180"/>
</package>
<package name="LUMILED+" urn="urn:adsk.eagle:footprint:15679/1" library_version="2">
<description>&lt;b&gt;Lumileds Lighting. LUXEON®&lt;/b&gt; with cool pad&lt;p&gt;
Source: K2.pdf</description>
<wire x1="-3.575" y1="2.3375" x2="-2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="3.575" x2="2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="2.3375" x2="3.575" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="-3.575" x2="-2.3375" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="-3.575" x2="-2.5" y2="-3.4125" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-3.4125" x2="-3.4125" y2="-2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="-3.4125" y1="-2.5" x2="-3.575" y2="-2.3375" width="0.2032" layer="21"/>
<wire x1="-3.575" y1="-2.3375" x2="-3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="2.3375" y1="3.575" x2="2.5" y2="3.4125" width="0.2032" layer="21"/>
<wire x1="2.5" y1="3.4125" x2="3.4125" y2="2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="3.4125" y1="2.5" x2="3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="-1.725" y1="2.225" x2="-1.0625" y2="2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<wire x1="1.725" y1="-2.225" x2="1.0625" y2="-2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<circle x="0" y="0" radius="2.725" width="0.2032" layer="51"/>
<smd name="1NC" x="-5.2" y="1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="2+" x="-5.2" y="-1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="3NC" x="5.2" y="-1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<smd name="4-" x="5.2" y="1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<text x="-3.175" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-5.975" y1="0.575" x2="-3.625" y2="1.6" layer="51"/>
<rectangle x1="-5.975" y1="-1.6" x2="-3.625" y2="-0.575" layer="51"/>
<rectangle x1="3.625" y1="-1.6" x2="5.975" y2="-0.575" layer="51" rot="R180"/>
<rectangle x1="3.625" y1="0.575" x2="5.975" y2="1.6" layer="51" rot="R180"/>
<polygon width="0.4064" layer="1">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
<polygon width="0.4064" layer="29">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
<polygon width="0.4064" layer="31">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
</package>
<package name="LUMILED" urn="urn:adsk.eagle:footprint:15680/1" library_version="2">
<description>&lt;b&gt;Lumileds Lighting. LUXEON®&lt;/b&gt; without cool pad&lt;p&gt;
Source: K2.pdf</description>
<wire x1="-3.575" y1="2.3375" x2="-2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="3.575" x2="2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="2.3375" x2="3.575" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="-3.575" x2="-2.3375" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="-3.575" x2="-2.5" y2="-3.4125" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-3.4125" x2="-3.4125" y2="-2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="-3.4125" y1="-2.5" x2="-3.575" y2="-2.3375" width="0.2032" layer="21"/>
<wire x1="-3.575" y1="-2.3375" x2="-3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="2.3375" y1="3.575" x2="2.5" y2="3.4125" width="0.2032" layer="21"/>
<wire x1="2.5" y1="3.4125" x2="3.4125" y2="2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="3.4125" y1="2.5" x2="3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="-1.725" y1="2.225" x2="-1.0625" y2="2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<wire x1="1.725" y1="-2.225" x2="1.0625" y2="-2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<circle x="0" y="0" radius="2.725" width="0.2032" layer="51"/>
<smd name="1NC" x="-5.2" y="1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="2+" x="-5.2" y="-1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="3NC" x="5.2" y="-1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<smd name="4-" x="5.2" y="1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<text x="-3.175" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-5.975" y1="0.575" x2="-3.625" y2="1.6" layer="51"/>
<rectangle x1="-5.975" y1="-1.6" x2="-3.625" y2="-0.575" layer="51"/>
<rectangle x1="3.625" y1="-1.6" x2="5.975" y2="-0.575" layer="51" rot="R180"/>
<rectangle x1="3.625" y1="0.575" x2="5.975" y2="1.6" layer="51" rot="R180"/>
<polygon width="0.4064" layer="29">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
<polygon width="0.4064" layer="31">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
</package>
<package name="LED10MM" urn="urn:adsk.eagle:footprint:15681/1" library_version="2">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
10 mm, round</description>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="2.54" width="0.254" layer="21" curve="-306.869898"/>
<wire x1="4.445" y1="0" x2="0" y2="-4.445" width="0.127" layer="21" curve="-90"/>
<wire x1="3.81" y1="0" x2="0" y2="-3.81" width="0.127" layer="21" curve="-90"/>
<wire x1="3.175" y1="0" x2="0" y2="-3.175" width="0.127" layer="21" curve="-90"/>
<wire x1="2.54" y1="0" x2="0" y2="-2.54" width="0.127" layer="21" curve="-90"/>
<wire x1="-4.445" y1="0" x2="0" y2="4.445" width="0.127" layer="21" curve="-90"/>
<wire x1="-3.81" y1="0" x2="0" y2="3.81" width="0.127" layer="21" curve="-90"/>
<wire x1="-3.175" y1="0" x2="0" y2="3.175" width="0.127" layer="21" curve="-90"/>
<wire x1="-2.54" y1="0" x2="0" y2="2.54" width="0.127" layer="21" curve="-90"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.254" layer="21"/>
<circle x="0" y="0" radius="5.08" width="0.127" layer="21"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.6764" shape="square"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="6.35" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="6.35" y="-1.27" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="KA-3528ASYC" urn="urn:adsk.eagle:footprint:15682/1" library_version="2">
<description>&lt;b&gt;SURFACE MOUNT LED LAMP&lt;/b&gt; 3.5x2.8mm&lt;p&gt;
Source: http://www.kingbright.com/manager/upload/pdf/KA-3528ASYC(Ver1189474662.1)</description>
<wire x1="-1.55" y1="1.35" x2="1.55" y2="1.35" width="0.1016" layer="21"/>
<wire x1="1.55" y1="1.35" x2="1.55" y2="-1.35" width="0.1016" layer="51"/>
<wire x1="1.55" y1="-1.35" x2="-1.55" y2="-1.35" width="0.1016" layer="21"/>
<wire x1="-1.55" y1="-1.35" x2="-1.55" y2="1.35" width="0.1016" layer="51"/>
<wire x1="-0.65" y1="0.95" x2="0.65" y2="0.95" width="0.1016" layer="21" curve="-68.40813"/>
<wire x1="0.65" y1="-0.95" x2="-0.65" y2="-0.95" width="0.1016" layer="21" curve="-68.40813"/>
<circle x="0" y="0" radius="1.15" width="0.1016" layer="51"/>
<smd name="A" x="-1.55" y="0" dx="1.5" dy="2.2" layer="1"/>
<smd name="C" x="1.55" y="0" dx="1.5" dy="2.2" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.75" y1="0.6" x2="-1.6" y2="1.1" layer="51"/>
<rectangle x1="-1.75" y1="-1.1" x2="-1.6" y2="-0.6" layer="51"/>
<rectangle x1="1.6" y1="-1.1" x2="1.75" y2="-0.6" layer="51" rot="R180"/>
<rectangle x1="1.6" y1="0.6" x2="1.75" y2="1.1" layer="51" rot="R180"/>
<polygon width="0.1016" layer="51">
<vertex x="1.55" y="-1.35"/>
<vertex x="1.55" y="-0.625"/>
<vertex x="0.825" y="-1.35"/>
</polygon>
<polygon width="0.1016" layer="21">
<vertex x="1.55" y="-1.35"/>
<vertex x="1.55" y="-1.175"/>
<vertex x="1" y="-1.175"/>
<vertex x="0.825" y="-1.35"/>
</polygon>
</package>
<package name="SML0805" urn="urn:adsk.eagle:footprint:15683/1" library_version="2">
<description>&lt;b&gt;SML0805-2CW-TR (0805 PROFILE)&lt;/b&gt; COOL WHITE&lt;p&gt;
Source: http://www.ledtronics.com/ds/smd-0603/Dstr0093.pdf</description>
<wire x1="-0.95" y1="-0.55" x2="0.95" y2="-0.55" width="0.1016" layer="51"/>
<wire x1="0.95" y1="-0.55" x2="0.95" y2="0.55" width="0.1016" layer="51"/>
<wire x1="0.95" y1="0.55" x2="-0.95" y2="0.55" width="0.1016" layer="51"/>
<wire x1="-0.95" y1="0.55" x2="-0.95" y2="-0.55" width="0.1016" layer="51"/>
<wire x1="-0.175" y1="-0.025" x2="0" y2="0.15" width="0.0634" layer="21"/>
<wire x1="0" y1="0.15" x2="0.15" y2="0" width="0.0634" layer="21"/>
<wire x1="0.15" y1="0" x2="-0.025" y2="-0.175" width="0.0634" layer="21"/>
<wire x1="-0.025" y1="-0.175" x2="-0.175" y2="-0.025" width="0.0634" layer="21"/>
<circle x="-0.275" y="0.4" radius="0.125" width="0" layer="21"/>
<smd name="C" x="-1.05" y="0" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="1.05" y="0" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.5" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.5" y="-2" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="SML1206" urn="urn:adsk.eagle:footprint:15684/1" library_version="2">
<description>&lt;b&gt;SML10XXKH-TR (HIGH INTENSITY) LED&lt;/b&gt;&lt;p&gt;
&lt;table&gt;
&lt;tr&gt;&lt;td&gt;SML10R3KH-TR&lt;/td&gt;&lt;td&gt;ULTRA RED&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10E3KH-TR&lt;/td&gt;&lt;td&gt;SUPER REDSUPER BLUE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10O3KH-TR&lt;/td&gt;&lt;td&gt;SUPER ORANGE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10PY3KH-TR&lt;/td&gt;&lt;td&gt;PURE YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10OY3KH-TR&lt;/td&gt;&lt;td&gt;ULTRA YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10AG3KH-TR&lt;/td&gt;&lt;td&gt;AQUA GREEN&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10BG3KH-TR&lt;/td&gt;&lt;td&gt;BLUE GREEN&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10PB1KH-TR&lt;/td&gt;&lt;td&gt;SUPER BLUE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10CW1KH-TR&lt;/td&gt;&lt;td&gt;WHITE&lt;/td&gt;&lt;/tr&gt;
&lt;/table&gt;

Source: http://www.ledtronics.com/ds/smd-1206/dstr0094.PDF</description>
<wire x1="-1.5" y1="0.5" x2="-1.5" y2="-0.5" width="0.2032" layer="51" curve="-180"/>
<wire x1="1.5" y1="-0.5" x2="1.5" y2="0.5" width="0.2032" layer="51" curve="-180"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<circle x="-0.725" y="0.525" radius="0.125" width="0" layer="21"/>
<smd name="C" x="-1.75" y="0" dx="1.5" dy="1.5" layer="1"/>
<smd name="A" x="1.75" y="0" dx="1.5" dy="1.5" layer="1"/>
<text x="-1.5" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.5" y="-2.5" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="0.4" x2="-1.15" y2="0.8" layer="51"/>
<rectangle x1="-1.6" y1="-0.8" x2="-1.15" y2="-0.4" layer="51"/>
<rectangle x1="-1.175" y1="-0.6" x2="-1" y2="-0.275" layer="51"/>
<rectangle x1="1.15" y1="-0.8" x2="1.6" y2="-0.4" layer="51" rot="R180"/>
<rectangle x1="1.15" y1="0.4" x2="1.6" y2="0.8" layer="51" rot="R180"/>
<rectangle x1="1" y1="0.275" x2="1.175" y2="0.6" layer="51" rot="R180"/>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
</package>
<package name="SML0603" urn="urn:adsk.eagle:footprint:15685/1" library_version="2">
<description>&lt;b&gt;SML0603-XXX (HIGH INTENSITY) LED&lt;/b&gt;&lt;p&gt;
&lt;table&gt;
&lt;tr&gt;&lt;td&gt;AG3K&lt;/td&gt;&lt;td&gt;AQUA GREEN&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;B1K&lt;/td&gt;&lt;td&gt;SUPER BLUE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;R1K&lt;/td&gt;&lt;td&gt;SUPER RED&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;R3K&lt;/td&gt;&lt;td&gt;ULTRA RED&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;O3K&lt;/td&gt;&lt;td&gt;SUPER ORANGE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;O3KH&lt;/td&gt;&lt;td&gt;SOFT ORANGE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;Y3KH&lt;/td&gt;&lt;td&gt;SUPER YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;Y3K&lt;/td&gt;&lt;td&gt;SUPER YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;2CW&lt;/td&gt;&lt;td&gt;WHITE&lt;/td&gt;&lt;/tr&gt;
&lt;/table&gt;
Source: http://www.ledtronics.com/ds/smd-0603/Dstr0092.pdf</description>
<wire x1="-0.75" y1="0.35" x2="0.75" y2="0.35" width="0.1016" layer="51"/>
<wire x1="0.75" y1="0.35" x2="0.75" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.75" y1="-0.35" x2="-0.75" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="-0.75" y1="-0.35" x2="-0.75" y2="0.35" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="0.3" x2="-0.45" y2="-0.3" width="0.1016" layer="51"/>
<wire x1="0.45" y1="0.3" x2="0.45" y2="-0.3" width="0.1016" layer="51"/>
<wire x1="-0.2" y1="0.35" x2="0.2" y2="0.35" width="0.1016" layer="21"/>
<wire x1="-0.2" y1="-0.35" x2="0.2" y2="-0.35" width="0.1016" layer="21"/>
<smd name="C" x="-0.75" y="0" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0.75" y="0" dx="0.8" dy="0.8" layer="1"/>
<text x="-1" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1" y="-2" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.4" y1="0.175" x2="0" y2="0.4" layer="51"/>
<rectangle x1="-0.25" y1="0.175" x2="0" y2="0.4" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="CHIPLED_0603" urn="urn:adsk.eagle:package:22365974/1" type="model" library_version="2">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_LY Q971.pdf</description>
<packageinstances>
<packageinstance name="CHIPLED_0603"/>
</packageinstances>
</package3d>
<package3d name="1206" urn="urn:adsk.eagle:package:15796/2" type="model" library_version="2">
<description>CHICAGO MINIATURE LAMP, INC.
7022X Series SMT LEDs 1206 Package Size</description>
<packageinstances>
<packageinstance name="1206"/>
</packageinstances>
</package3d>
<package3d name="LD260" urn="urn:adsk.eagle:package:15794/1" type="box" library_version="2">
<description>LED
5 mm, square, Siemens</description>
<packageinstances>
<packageinstance name="LD260"/>
</packageinstances>
</package3d>
<package3d name="LED2X5" urn="urn:adsk.eagle:package:15800/1" type="box" library_version="2">
<description>LED
2 x 5 mm, rectangle</description>
<packageinstances>
<packageinstance name="LED2X5"/>
</packageinstances>
</package3d>
<package3d name="LED3MM" urn="urn:adsk.eagle:package:15797/1" type="box" library_version="2">
<description>LED
3 mm, round</description>
<packageinstances>
<packageinstance name="LED3MM"/>
</packageinstances>
</package3d>
<package3d name="LED5MM" urn="urn:adsk.eagle:package:15799/2" type="model" library_version="2">
<description>LED
5 mm, round</description>
<packageinstances>
<packageinstance name="LED5MM"/>
</packageinstances>
</package3d>
<package3d name="LSU260" urn="urn:adsk.eagle:package:15805/1" type="box" library_version="2">
<description>LED
1 mm, round, Siemens</description>
<packageinstances>
<packageinstance name="LSU260"/>
</packageinstances>
</package3d>
<package3d name="LZR181" urn="urn:adsk.eagle:package:15808/1" type="box" library_version="2">
<description>LED BLOCK
1 LED, Siemens</description>
<packageinstances>
<packageinstance name="LZR181"/>
</packageinstances>
</package3d>
<package3d name="Q62902-B152" urn="urn:adsk.eagle:package:15803/1" type="box" library_version="2">
<description>LED HOLDER
Siemens</description>
<packageinstances>
<packageinstance name="Q62902-B152"/>
</packageinstances>
</package3d>
<package3d name="Q62902-B153" urn="urn:adsk.eagle:package:15804/1" type="box" library_version="2">
<description>LED HOLDER
Siemens</description>
<packageinstances>
<packageinstance name="Q62902-B153"/>
</packageinstances>
</package3d>
<package3d name="Q62902-B155" urn="urn:adsk.eagle:package:15807/1" type="box" library_version="2">
<description>LED HOLDER
Siemens</description>
<packageinstances>
<packageinstance name="Q62902-B155"/>
</packageinstances>
</package3d>
<package3d name="Q62902-B156" urn="urn:adsk.eagle:package:15806/1" type="box" library_version="2">
<description>LED HOLDER
Siemens</description>
<packageinstances>
<packageinstance name="Q62902-B156"/>
</packageinstances>
</package3d>
<package3d name="SFH480" urn="urn:adsk.eagle:package:15809/1" type="box" library_version="2">
<description>IR LED
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking
Inifineon</description>
<packageinstances>
<packageinstance name="SFH480"/>
</packageinstances>
</package3d>
<package3d name="SFH482" urn="urn:adsk.eagle:package:15795/1" type="box" library_version="2">
<description>IR LED
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking
Inifineon</description>
<packageinstances>
<packageinstance name="SFH482"/>
</packageinstances>
</package3d>
<package3d name="U57X32" urn="urn:adsk.eagle:package:15789/1" type="box" library_version="2">
<description>LED
rectangle, 5.7 x 3.2 mm</description>
<packageinstances>
<packageinstance name="U57X32"/>
</packageinstances>
</package3d>
<package3d name="IRL80A" urn="urn:adsk.eagle:package:15810/1" type="box" library_version="2">
<description>IR LED
IR transmitter Siemens</description>
<packageinstances>
<packageinstance name="IRL80A"/>
</packageinstances>
</package3d>
<package3d name="P-LCC-2" urn="urn:adsk.eagle:package:15817/1" type="box" library_version="2">
<description>TOPLED® High-optical Power LED (HOP)
Source: http://www.osram.convergy.de/ ... ls_t675.pdf</description>
<packageinstances>
<packageinstance name="P-LCC-2"/>
</packageinstances>
</package3d>
<package3d name="OSRAM-MINI-TOP-LED" urn="urn:adsk.eagle:package:15811/1" type="box" library_version="2">
<description>BLUE LINETM Hyper Mini TOPLED® Hyper-Bright LED
Source: http://www.osram.convergy.de/ ... LB M676.pdf</description>
<packageinstances>
<packageinstance name="OSRAM-MINI-TOP-LED"/>
</packageinstances>
</package3d>
<package3d name="OSRAM-SIDELED" urn="urn:adsk.eagle:package:15812/1" type="box" library_version="2">
<description>Super SIDELED® High-Current LED
LG A672, LP A672 
Source: http://www.osram.convergy.de/ ... LG_LP_A672.pdf (2004.05.13)</description>
<packageinstances>
<packageinstance name="OSRAM-SIDELED"/>
</packageinstances>
</package3d>
<package3d name="SMART-LED" urn="urn:adsk.eagle:package:15814/1" type="box" library_version="2">
<description>SmartLEDTM Hyper-Bright LED
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY L896.pdf</description>
<packageinstances>
<packageinstance name="SMART-LED"/>
</packageinstances>
</package3d>
<package3d name="P-LCC-2-TOPLED-RG" urn="urn:adsk.eagle:package:15813/1" type="box" library_version="2">
<description>Hyper TOPLED® RG Hyper-Bright LED
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY T776.pdf</description>
<packageinstances>
<packageinstance name="P-LCC-2-TOPLED-RG"/>
</packageinstances>
</package3d>
<package3d name="MICRO-SIDELED" urn="urn:adsk.eagle:package:15815/1" type="box" library_version="2">
<description>Hyper Micro SIDELED®
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY Y876.pdf</description>
<packageinstances>
<packageinstance name="MICRO-SIDELED"/>
</packageinstances>
</package3d>
<package3d name="P-LCC-4" urn="urn:adsk.eagle:package:18621509/1" type="empty" library_version="2">
<description>&lt;b&gt;Power TOPLED®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LA_LY E67B.pdf</description>
<packageinstances>
<packageinstance name="P-LCC-4"/>
</packageinstances>
</package3d>
<package3d name="CHIP-LED0603" urn="urn:adsk.eagle:package:15819/3" type="model" library_version="2">
<description>Hyper CHIPLED Hyper-Bright LED
LB Q993
Source: http://www.osram.convergy.de/ ... Lb_q993.pdf</description>
<packageinstances>
<packageinstance name="CHIP-LED0603"/>
</packageinstances>
</package3d>
<package3d name="CHIP-LED0805" urn="urn:adsk.eagle:package:15818/2" type="model" library_version="2">
<description>Hyper CHIPLED Hyper-Bright LED
LB R99A
Source: http://www.osram.convergy.de/ ... lb_r99a.pdf</description>
<packageinstances>
<packageinstance name="CHIP-LED0805"/>
</packageinstances>
</package3d>
<package3d name="MINI-TOPLED-SANTANA" urn="urn:adsk.eagle:package:15820/1" type="box" library_version="2">
<description>Mini TOPLED Santana®
Source: http://www.osram.convergy.de/ ... LG M470.pdf</description>
<packageinstances>
<packageinstance name="MINI-TOPLED-SANTANA"/>
</packageinstances>
</package3d>
<package3d name="CHIPLED_0805" urn="urn:adsk.eagle:package:15821/2" type="model" library_version="2">
<description>CHIPLED
Source: http://www.osram.convergy.de/ ... LG_R971.pdf</description>
<packageinstances>
<packageinstance name="CHIPLED_0805"/>
</packageinstances>
</package3d>
<package3d name="CHIPLED_1206" urn="urn:adsk.eagle:package:15823/2" type="model" library_version="2">
<description>CHIPLED
Source: http://www.osram.convergy.de/ ... LG_LY N971.pdf</description>
<packageinstances>
<packageinstance name="CHIPLED_1206"/>
</packageinstances>
</package3d>
<package3d name="CHIPLED-0603-TTW" urn="urn:adsk.eagle:package:15824/1" type="box" library_version="2">
<description>CHIPLED-0603
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603
Package able to withstand TTW-soldering heat
Package suitable for TTW-soldering
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
<packageinstances>
<packageinstance name="CHIPLED-0603-TTW"/>
</packageinstances>
</package3d>
<package3d name="SMARTLED-TTW" urn="urn:adsk.eagle:package:15825/1" type="box" library_version="2">
<description>SmartLED TTW
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603
Package able to withstand TTW-soldering heat
Package suitable for TTW-soldering
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
<packageinstances>
<packageinstance name="SMARTLED-TTW"/>
</packageinstances>
</package3d>
<package3d name="LUMILED+" urn="urn:adsk.eagle:package:15826/1" type="box" library_version="2">
<description>Lumileds Lighting. LUXEON® with cool pad
Source: K2.pdf</description>
<packageinstances>
<packageinstance name="LUMILED+"/>
</packageinstances>
</package3d>
<package3d name="LUMILED" urn="urn:adsk.eagle:package:15827/1" type="box" library_version="2">
<description>Lumileds Lighting. LUXEON® without cool pad
Source: K2.pdf</description>
<packageinstances>
<packageinstance name="LUMILED"/>
</packageinstances>
</package3d>
<package3d name="LED10MM" urn="urn:adsk.eagle:package:15828/1" type="box" library_version="2">
<description>LED
10 mm, round</description>
<packageinstances>
<packageinstance name="LED10MM"/>
</packageinstances>
</package3d>
<package3d name="KA-3528ASYC" urn="urn:adsk.eagle:package:15831/1" type="box" library_version="2">
<description>SURFACE MOUNT LED LAMP 3.5x2.8mm
Source: http://www.kingbright.com/manager/upload/pdf/KA-3528ASYC(Ver1189474662.1)</description>
<packageinstances>
<packageinstance name="KA-3528ASYC"/>
</packageinstances>
</package3d>
<package3d name="SML0805" urn="urn:adsk.eagle:package:15830/1" type="box" library_version="2">
<description>SML0805-2CW-TR (0805 PROFILE) COOL WHITE
Source: http://www.ledtronics.com/ds/smd-0603/Dstr0093.pdf</description>
<packageinstances>
<packageinstance name="SML0805"/>
</packageinstances>
</package3d>
<package3d name="SML1206" urn="urn:adsk.eagle:package:15829/1" type="box" library_version="2">
<description>SML10XXKH-TR (HIGH INTENSITY) LED

SML10R3KH-TRULTRA RED
SML10E3KH-TRSUPER REDSUPER BLUE
SML10O3KH-TRSUPER ORANGE
SML10PY3KH-TRPURE YELLOW
SML10OY3KH-TRULTRA YELLOW
SML10AG3KH-TRAQUA GREEN
SML10BG3KH-TRBLUE GREEN
SML10PB1KH-TRSUPER BLUE
SML10CW1KH-TRWHITE


Source: http://www.ledtronics.com/ds/smd-1206/dstr0094.PDF</description>
<packageinstances>
<packageinstance name="SML1206"/>
</packageinstances>
</package3d>
<package3d name="SML0603" urn="urn:adsk.eagle:package:15832/1" type="box" library_version="2">
<description>SML0603-XXX (HIGH INTENSITY) LED

AG3KAQUA GREEN
B1KSUPER BLUE
R1KSUPER RED
R3KULTRA RED
O3KSUPER ORANGE
O3KHSOFT ORANGE
Y3KHSUPER YELLOW
Y3KSUPER YELLOW
2CWWHITE

Source: http://www.ledtronics.com/ds/smd-0603/Dstr0092.pdf</description>
<packageinstances>
<packageinstance name="SML0603"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="LED" urn="urn:adsk.eagle:symbol:18621499/1" library_version="2">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" urn="urn:adsk.eagle:component:18621523/2" prefix="LED" uservalue="yes" library_version="2">
<description>&lt;b&gt;LED&lt;/b&gt;&lt;p&gt;
&lt;u&gt;OSRAM&lt;/u&gt;:&lt;br&gt;

- &lt;u&gt;CHIPLED&lt;/u&gt;&lt;br&gt;
LG R971, LG N971, LY N971, LG Q971, LY Q971, LO R971, LY R971
LH N974, LH R974&lt;br&gt;
LS Q976, LO Q976, LY Q976&lt;br&gt;
LO Q996&lt;br&gt;

- &lt;u&gt;Hyper CHIPLED&lt;/u&gt;&lt;br&gt;
LW Q18S&lt;br&gt;
LB Q993, LB Q99A, LB R99A&lt;br&gt;

- &lt;u&gt;SideLED&lt;/u&gt;&lt;br&gt;
LS A670, LO A670, LY A670, LG A670, LP A670&lt;br&gt;
LB A673, LV A673, LT A673, LW A673&lt;br&gt;
LH A674&lt;br&gt;
LY A675&lt;br&gt;
LS A676, LA A676, LO A676, LY A676, LW A676&lt;br&gt;
LS A679, LY A679, LG A679&lt;br&gt;

-  &lt;u&gt;Hyper Micro SIDELED®&lt;/u&gt;&lt;br&gt;
LS Y876, LA Y876, LO Y876, LY Y876&lt;br&gt;
LT Y87S&lt;br&gt;

- &lt;u&gt;SmartLED&lt;/u&gt;&lt;br&gt;
LW L88C, LW L88S&lt;br&gt;
LB L89C, LB L89S, LG L890&lt;br&gt;
LS L89K, LO L89K, LY L89K&lt;br&gt;
LS L896, LA L896, LO L896, LY L896&lt;br&gt;

- &lt;u&gt;TOPLED&lt;/u&gt;&lt;br&gt;
LS T670, LO T670, LY T670, LG T670, LP T670&lt;br&gt;
LSG T670, LSP T670, LSY T670, LOP T670, LYG T670&lt;br&gt;
LG T671, LOG T671, LSG T671&lt;br&gt;
LB T673, LV T673, LT T673, LW T673&lt;br&gt;
LH T674&lt;br&gt;
LS T676, LA T676, LO T676, LY T676, LB T676, LH T676, LSB T676, LW T676&lt;br&gt;
LB T67C, LV T67C, LT T67C, LS T67K, LO T67K, LY T67K, LW E67C&lt;br&gt;
LS E67B, LA E67B, LO E67B, LY E67B, LB E67C, LV E67C, LT E67C&lt;br&gt;
LW T67C&lt;br&gt;
LS T679, LY T679, LG T679&lt;br&gt;
LS T770, LO T770, LY T770, LG T770, LP T770&lt;br&gt;
LB T773, LV T773, LT T773, LW T773&lt;br&gt;
LH T774&lt;br&gt;
LS E675, LA E675, LY E675, LS T675&lt;br&gt;
LS T776, LA T776, LO T776, LY T776, LB T776&lt;br&gt;
LHGB T686&lt;br&gt;
LT T68C, LB T68C&lt;br&gt;

- &lt;u&gt;Hyper Mini TOPLED®&lt;/u&gt;&lt;br&gt;
LB M676&lt;br&gt;

- &lt;u&gt;Mini TOPLED Santana®&lt;/u&gt;&lt;br&gt;
LG M470&lt;br&gt;
LS M47K, LO M47K, LY M47K
&lt;p&gt;
Source: http://www.osram.convergy.de&lt;p&gt;

&lt;u&gt;LUXEON:&lt;/u&gt;&lt;br&gt;
- &lt;u&gt;LUMILED®&lt;/u&gt;&lt;br&gt;
LXK2-PW12-R00, LXK2-PW12-S00, LXK2-PW14-U00, LXK2-PW14-V00&lt;br&gt;
LXK2-PM12-R00, LXK2-PM12-S00, LXK2-PM14-U00&lt;br&gt;
LXK2-PE12-Q00, LXK2-PE12-R00, LXK2-PE12-S00, LXK2-PE14-T00, LXK2-PE14-U00&lt;br&gt;
LXK2-PB12-K00, LXK2-PB12-L00, LXK2-PB12-M00, LXK2-PB14-N00, LXK2-PB14-P00, LXK2-PB14-Q00&lt;br&gt;
LXK2-PR12-L00, LXK2-PR12-M00, LXK2-PR14-Q00, LXK2-PR14-R00&lt;br&gt;
LXK2-PD12-Q00, LXK2-PD12-R00, LXK2-PD12-S00&lt;br&gt;
LXK2-PH12-R00, LXK2-PH12-S00&lt;br&gt;
LXK2-PL12-P00, LXK2-PL12-Q00, LXK2-PL12-R00
&lt;p&gt;
Source: www.luxeon.com&lt;p&gt;

&lt;u&gt;KINGBRIGHT:&lt;/U&gt;&lt;p&gt;
KA-3528ASYC&lt;br&gt;
Source: www.kingbright.com</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="SMT1206" package="1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15796/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="20" constant="no"/>
</technology>
</technologies>
</device>
<device name="LD260" package="LD260">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15794/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="SQR2X5" package="LED2X5">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15800/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="2" constant="no"/>
</technology>
</technologies>
</device>
<device name="3MM" package="LED3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15797/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="97" constant="no"/>
</technology>
</technologies>
</device>
<device name="5MM" package="LED5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15799/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="93" constant="no"/>
</technology>
</technologies>
</device>
<device name="LSU260" package="LSU260">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15805/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="LZR181" package="LZR181">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15808/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
</technology>
</technologies>
</device>
<device name="B152" package="Q62902-B152">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15803/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="B153" package="Q62902-B153">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15804/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="B155" package="Q62902-B155">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15807/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="B156" package="Q62902-B156">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15806/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="SFH480" package="SFH480">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15809/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="2" constant="no"/>
</technology>
</technologies>
</device>
<device name="SFH482" package="SFH482">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15795/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="SQR5.7X3.2" package="U57X32">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15789/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="IRL80A" package="IRL80A">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15810/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="P-LCC-2" package="P-LCC-2">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15817/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="2" constant="no"/>
</technology>
</technologies>
</device>
<device name="MINI-TOP" package="OSRAM-MINI-TOP-LED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15811/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="SIDELED" package="OSRAM-SIDELED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15812/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="2" constant="no"/>
</technology>
</technologies>
</device>
<device name="SMART-LED" package="SMART-LED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="B"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15814/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="P-LCC-2-BACK" package="P-LCC-2-TOPLED-RG">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15813/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="MICRO-SIDELED" package="MICRO-SIDELED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15815/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="P-LCC-4" package="P-LCC-4">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C@4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:18621509/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIP-LED0603" package="CHIP-LED0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15819/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="26" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIP-LED0805" package="CHIP-LED0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15818/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="68" constant="no"/>
</technology>
</technologies>
</device>
<device name="TOPLED-SANTANA" package="MINI-TOPLED-SANTANA">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15820/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIPLED_0805" package="CHIPLED_0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15821/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="52" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIPLED_1206" package="CHIPLED_1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15823/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="28" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIPLED_0603" package="CHIPLED_0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22365974/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="28" constant="no"/>
</technology>
</technologies>
</device>
<device name="CHIPLED-0603-TTW" package="CHIPLED-0603-TTW">
<connects>
<connect gate="G$1" pin="A" pad="A@1"/>
<connect gate="G$1" pin="C" pad="C@1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15824/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="" package="SMARTLED-TTW">
<connects>
<connect gate="G$1" pin="A" pad="A@1"/>
<connect gate="G$1" pin="C" pad="C@1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15825/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="3" constant="no"/>
</technology>
</technologies>
</device>
<device name="-LUMILED+" package="LUMILED+">
<connects>
<connect gate="G$1" pin="A" pad="2+"/>
<connect gate="G$1" pin="C" pad="4-"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15826/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="-LUMILED" package="LUMILED">
<connects>
<connect gate="G$1" pin="A" pad="2+"/>
<connect gate="G$1" pin="C" pad="4-"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15827/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="10MM" package="LED10MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15828/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="2" constant="no"/>
</technology>
</technologies>
</device>
<device name="KA-3528ASYC" package="KA-3528ASYC">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15831/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="SML0805" package="SML0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15830/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="17" constant="no"/>
</technology>
</technologies>
</device>
<device name="SML1206" package="SML1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15829/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="6" constant="no"/>
</technology>
</technologies>
</device>
<device name="SML0603" package="SML0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15832/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="17" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Minew" urn="urn:adsk.eagle:library:28871088">
<packages>
<package name="MS88SF3" urn="urn:adsk.eagle:footprint:28871089/1" library_version="1">
<smd name="17" x="-5.45" y="-5.475" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="16" x="-5.45" y="-4.825" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="15" x="-5.45" y="-4.175" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="14" x="-5.45" y="-3.525" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="13" x="-5.45" y="-2.875" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="12" x="-5.45" y="-2.225" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="11" x="-5.45" y="-1.575" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="10" x="-5.45" y="-0.925" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="9" x="-5.45" y="-0.275" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="8" x="-5.45" y="0.375" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="7" x="-5.45" y="1.025" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="6" x="-5.45" y="1.675" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="5" x="-5.45" y="2.325" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="4" x="-5.45" y="2.975" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="3" x="-5.45" y="3.625" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="2" x="-5.45" y="4.275" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="1" x="-5.45" y="4.925" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="35" x="5.45" y="-5.475" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="36" x="5.45" y="-4.825" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="37" x="5.45" y="-4.175" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="38" x="5.45" y="-3.525" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="39" x="5.45" y="-2.875" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="40" x="5.45" y="-2.225" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="41" x="5.45" y="-1.575" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="42" x="5.45" y="-0.925" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="43" x="5.45" y="-0.275" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="44" x="5.45" y="0.375" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="45" x="5.45" y="1.025" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="46" x="5.45" y="1.675" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="47" x="5.45" y="2.325" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="48" x="5.45" y="2.975" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="49" x="5.45" y="3.625" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="50" x="5.45" y="4.275" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="51" x="5.45" y="4.925" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="57" x="3.775" y="3.625" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="58" x="3.775" y="2.975" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="59" x="3.775" y="2.325" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="60" x="3.775" y="1.675" dx="0.85" dy="0.4" layer="1" rot="R180"/>
<smd name="52" x="4.4" y="4.825" dx="0.85" dy="0.4" layer="1" rot="R90"/>
<smd name="53" x="3.75" y="4.825" dx="0.85" dy="0.4" layer="1" rot="R90"/>
<smd name="54" x="3.1" y="4.825" dx="0.85" dy="0.4" layer="1" rot="R90"/>
<smd name="55" x="2.45" y="4.825" dx="0.85" dy="0.4" layer="1" rot="R90"/>
<smd name="56" x="1.8" y="4.825" dx="0.85" dy="0.4" layer="1" rot="R90"/>
<smd name="26" x="0" y="-6.675" dx="0.85" dy="0.4" layer="1" rot="R90"/>
<smd name="27" x="0.65" y="-6.675" dx="0.85" dy="0.4" layer="1" rot="R90"/>
<smd name="28" x="1.3" y="-6.675" dx="0.85" dy="0.4" layer="1" rot="R90"/>
<smd name="29" x="1.95" y="-6.675" dx="0.85" dy="0.4" layer="1" rot="R90"/>
<smd name="30" x="2.6" y="-6.675" dx="0.85" dy="0.4" layer="1" rot="R90"/>
<smd name="31" x="3.25" y="-6.675" dx="0.85" dy="0.4" layer="1" rot="R90"/>
<smd name="32" x="3.9" y="-6.675" dx="0.85" dy="0.4" layer="1" rot="R90"/>
<smd name="33" x="4.55" y="-6.675" dx="0.85" dy="0.4" layer="1" rot="R90"/>
<smd name="34" x="5.2" y="-6.675" dx="0.85" dy="0.4" layer="1" rot="R90"/>
<smd name="25" x="-0.65" y="-6.675" dx="0.85" dy="0.4" layer="1" rot="R90"/>
<smd name="24" x="-1.3" y="-6.675" dx="0.85" dy="0.4" layer="1" rot="R90"/>
<smd name="23" x="-1.95" y="-6.675" dx="0.85" dy="0.4" layer="1" rot="R90"/>
<smd name="22" x="-2.6" y="-6.675" dx="0.85" dy="0.4" layer="1" rot="R90"/>
<smd name="21" x="-3.25" y="-6.675" dx="0.85" dy="0.4" layer="1" rot="R90"/>
<smd name="20" x="-3.9" y="-6.675" dx="0.85" dy="0.4" layer="1" rot="R90"/>
<smd name="19" x="-4.55" y="-6.675" dx="0.85" dy="0.4" layer="1" rot="R90"/>
<smd name="18" x="-5.2" y="-6.675" dx="0.85" dy="0.4" layer="1" rot="R90"/>
<smd name="61" x="1.4" y="1.425" dx="1" dy="1.5" layer="1"/>
<smd name="62" x="1.4" y="-1.425" dx="1" dy="1.5" layer="1"/>
<smd name="63" x="-1.4" y="-1.425" dx="1" dy="1.5" layer="1"/>
<smd name="64" x="-1.4" y="1.425" dx="1" dy="1.5" layer="1"/>
<wire x1="6.45" y1="11.075" x2="-6.45" y2="11.075" width="0.127" layer="21"/>
<wire x1="-6.45" y1="11.075" x2="-6.45" y2="-7.625" width="0.127" layer="21"/>
<wire x1="-6.45" y1="-7.625" x2="6.45" y2="-7.625" width="0.127" layer="21"/>
<wire x1="6.45" y1="-7.625" x2="6.45" y2="11.075" width="0.127" layer="21"/>
<wire x1="-5.85" y1="6.525" x2="5.85" y2="6.525" width="0.127" layer="51"/>
<text x="-0.05" y="6.65" size="1" layer="51" align="bottom-center">Groundplane edge</text>
<text x="0" y="-8.6" size="0.8" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-9.7" size="0.8" layer="27" align="bottom-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="MS88SF3" urn="urn:adsk.eagle:package:28871091/1" type="box" library_version="1">
<packageinstances>
<packageinstance name="MS88SF3"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="MS88SF3" urn="urn:adsk.eagle:symbol:28871090/1" library_version="1">
<wire x1="-25.4" y1="40.64" x2="-25.4" y2="-43.18" width="0.254" layer="94"/>
<wire x1="-25.4" y1="-43.18" x2="27.94" y2="-43.18" width="0.254" layer="94"/>
<wire x1="27.94" y1="-43.18" x2="27.94" y2="40.64" width="0.254" layer="94"/>
<wire x1="27.94" y1="40.64" x2="-25.4" y2="40.64" width="0.254" layer="94"/>
<text x="-5.08" y="2.54" size="1.778" layer="96" align="top-left">&gt;VALUE</text>
<text x="-25.4" y="43.18" size="1.778" layer="95" align="top-left">&gt;NAME</text>
<pin name="GND@1" x="-7.62" y="-48.26" length="middle" direction="pwr" rot="R90"/>
<pin name="GND@2" x="-5.08" y="-48.26" length="middle" direction="pwr" rot="R90"/>
<pin name="GND@3" x="-2.54" y="-48.26" length="middle" direction="pwr" rot="R90"/>
<pin name="GND@4" x="0" y="-48.26" length="middle" direction="pwr" rot="R90"/>
<pin name="GND@5" x="2.54" y="-48.26" length="middle" direction="pwr" rot="R90"/>
<pin name="GND@6" x="5.08" y="-48.26" length="middle" direction="pwr" rot="R90"/>
<pin name="GND@7" x="7.62" y="-48.26" length="middle" direction="pwr" rot="R90"/>
<pin name="P0.24" x="-30.48" y="25.4" length="middle" direction="out"/>
<pin name="P0.16" x="-30.48" y="27.94" length="middle"/>
<pin name="P0.15" x="-30.48" y="30.48" length="middle"/>
<pin name="P0.14" x="-30.48" y="33.02" length="middle"/>
<pin name="P0.13" x="-30.48" y="35.56" length="middle"/>
<pin name="P1.12" x="-30.48" y="2.54" length="middle"/>
<pin name="VDD" x="-5.08" y="45.72" length="middle" direction="pwr" rot="R270"/>
<pin name="VDDH" x="5.08" y="45.72" length="middle" direction="pwr" rot="R270"/>
<pin name="SWDCLK" x="33.02" y="-20.32" length="middle" direction="in" rot="R180"/>
<pin name="SWDIO" x="33.02" y="-22.86" length="middle" rot="R180"/>
<pin name="P1.13" x="-30.48" y="0" length="middle"/>
<pin name="P0.29/AIN5" x="-30.48" y="-2.54" length="middle"/>
<pin name="P0.30/AIN6" x="-30.48" y="-5.08" length="middle"/>
<pin name="P0.18/!RESET" x="33.02" y="-25.4" length="middle" direction="in" rot="R180"/>
<pin name="P0.03/AIN1" x="-30.48" y="15.24" length="middle"/>
<pin name="P0.28/AIN4" x="-30.48" y="12.7" length="middle"/>
<pin name="P0.25" x="-30.48" y="20.32" length="middle"/>
<pin name="P1.00" x="-30.48" y="17.78" length="middle"/>
<pin name="P0.31/AIN7" x="-30.48" y="5.08" length="middle"/>
<pin name="P0.02/AIN0" x="-30.48" y="10.16" length="middle"/>
<pin name="P0.04/AIN2" x="-30.48" y="-7.62" length="middle"/>
<pin name="P0.05/AIN3" x="-30.48" y="-10.16" length="middle"/>
<pin name="P0.09/NFC1" x="-30.48" y="-12.7" length="middle"/>
<pin name="VBUS" x="33.02" y="35.56" length="middle" rot="R180"/>
<pin name="USB_DM" x="33.02" y="33.02" length="middle" rot="R180"/>
<pin name="USB_DP" x="33.02" y="30.48" length="middle" rot="R180"/>
<pin name="P0.19" x="33.02" y="25.4" length="middle" rot="R180"/>
<pin name="P0.17" x="33.02" y="22.86" length="middle" rot="R180"/>
<pin name="P0.20" x="33.02" y="20.32" length="middle" rot="R180"/>
<pin name="P0.22" x="33.02" y="17.78" length="middle" rot="R180"/>
<pin name="P0.21" x="33.02" y="15.24" length="middle" rot="R180"/>
<pin name="P0.23" x="33.02" y="12.7" length="middle" rot="R180"/>
<pin name="P0.12" x="33.02" y="10.16" length="middle" rot="R180"/>
<pin name="P0.07" x="33.02" y="7.62" length="middle" rot="R180"/>
<pin name="P0.27" x="33.02" y="5.08" length="middle" rot="R180"/>
<pin name="P0.06" x="33.02" y="2.54" length="middle" rot="R180"/>
<pin name="P0.26" x="33.02" y="0" length="middle" rot="R180"/>
<pin name="P1.14" x="33.02" y="-2.54" length="middle" rot="R180"/>
<pin name="P1.15" x="33.02" y="-5.08" length="middle" rot="R180"/>
<pin name="P1.11" x="33.02" y="-7.62" length="middle" rot="R180"/>
<pin name="P1.10" x="33.02" y="-10.16" length="middle" rot="R180"/>
<pin name="P1.03" x="33.02" y="-12.7" length="middle" rot="R180"/>
<pin name="P0.10/NFC2" x="-30.48" y="-15.24" length="middle"/>
<pin name="P0.11" x="-30.48" y="-20.32" length="middle"/>
<pin name="P1.09" x="-30.48" y="-22.86" length="middle"/>
<pin name="P1.08" x="-30.48" y="-25.4" length="middle"/>
<pin name="P1.01" x="-30.48" y="-27.94" length="middle"/>
<pin name="P1.02" x="33.02" y="-15.24" length="middle" rot="R180"/>
<pin name="DCCH" x="-30.48" y="-40.64" length="middle"/>
<pin name="RF" x="-30.48" y="-38.1" length="middle"/>
<pin name="P0.08" x="33.02" y="-35.56" length="middle" rot="R180"/>
<pin name="P0.00" x="33.02" y="-40.64" length="middle" rot="R180"/>
<pin name="P0.01" x="33.02" y="-38.1" length="middle" rot="R180"/>
<pin name="P1.04" x="33.02" y="-33.02" length="middle" rot="R180"/>
<pin name="P1.06" x="33.02" y="-30.48" length="middle" rot="R180"/>
<pin name="P1.07" x="-30.48" y="-35.56" length="middle"/>
<pin name="P1.05" x="-30.48" y="-33.02" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="M88SF3" urn="urn:adsk.eagle:component:28871092/1" prefix="M" library_version="1">
<description>Ultra-Small nRF52840 Module</description>
<gates>
<gate name="G$1" symbol="MS88SF3" x="0" y="-2.54"/>
</gates>
<devices>
<device name="-1Y40AIR" package="MS88SF3">
<connects>
<connect gate="G$1" pin="DCCH" pad="33"/>
<connect gate="G$1" pin="GND@1" pad="1"/>
<connect gate="G$1" pin="GND@2" pad="18"/>
<connect gate="G$1" pin="GND@3" pad="34"/>
<connect gate="G$1" pin="GND@4" pad="61"/>
<connect gate="G$1" pin="GND@5" pad="62"/>
<connect gate="G$1" pin="GND@6" pad="63"/>
<connect gate="G$1" pin="GND@7" pad="64"/>
<connect gate="G$1" pin="P0.00" pad="23"/>
<connect gate="G$1" pin="P0.01" pad="24"/>
<connect gate="G$1" pin="P0.02/AIN0" pad="9"/>
<connect gate="G$1" pin="P0.03/AIN1" pad="6"/>
<connect gate="G$1" pin="P0.04/AIN2" pad="16"/>
<connect gate="G$1" pin="P0.05/AIN3" pad="15"/>
<connect gate="G$1" pin="P0.06" pad="20"/>
<connect gate="G$1" pin="P0.07" pad="19"/>
<connect gate="G$1" pin="P0.08" pad="25"/>
<connect gate="G$1" pin="P0.09/NFC1" pad="55"/>
<connect gate="G$1" pin="P0.10/NFC2" pad="56"/>
<connect gate="G$1" pin="P0.11" pad="22"/>
<connect gate="G$1" pin="P0.12" pad="27"/>
<connect gate="G$1" pin="P0.13" pad="39"/>
<connect gate="G$1" pin="P0.14" pad="30"/>
<connect gate="G$1" pin="P0.15" pad="40"/>
<connect gate="G$1" pin="P0.16" pad="29"/>
<connect gate="G$1" pin="P0.17" pad="41"/>
<connect gate="G$1" pin="P0.18/!RESET" pad="28"/>
<connect gate="G$1" pin="P0.19" pad="38"/>
<connect gate="G$1" pin="P0.20" pad="44"/>
<connect gate="G$1" pin="P0.21" pad="42"/>
<connect gate="G$1" pin="P0.22" pad="45"/>
<connect gate="G$1" pin="P0.23" pad="43"/>
<connect gate="G$1" pin="P0.24" pad="46"/>
<connect gate="G$1" pin="P0.25" pad="48"/>
<connect gate="G$1" pin="P0.26" pad="17"/>
<connect gate="G$1" pin="P0.27" pad="14"/>
<connect gate="G$1" pin="P0.28/AIN4" pad="11"/>
<connect gate="G$1" pin="P0.29/AIN5" pad="10"/>
<connect gate="G$1" pin="P0.30/AIN6" pad="12"/>
<connect gate="G$1" pin="P0.31/AIN7" pad="13"/>
<connect gate="G$1" pin="P1.00" pad="47"/>
<connect gate="G$1" pin="P1.01" pad="59"/>
<connect gate="G$1" pin="P1.02" pad="52"/>
<connect gate="G$1" pin="P1.03" pad="60"/>
<connect gate="G$1" pin="P1.04" pad="53"/>
<connect gate="G$1" pin="P1.05" pad="58"/>
<connect gate="G$1" pin="P1.06" pad="54"/>
<connect gate="G$1" pin="P1.07" pad="57"/>
<connect gate="G$1" pin="P1.08" pad="21"/>
<connect gate="G$1" pin="P1.09" pad="26"/>
<connect gate="G$1" pin="P1.10" pad="4"/>
<connect gate="G$1" pin="P1.11" pad="2"/>
<connect gate="G$1" pin="P1.12" pad="3"/>
<connect gate="G$1" pin="P1.13" pad="7"/>
<connect gate="G$1" pin="P1.14" pad="5"/>
<connect gate="G$1" pin="P1.15" pad="8"/>
<connect gate="G$1" pin="RF" pad="51"/>
<connect gate="G$1" pin="SWDCLK" pad="50"/>
<connect gate="G$1" pin="SWDIO" pad="49"/>
<connect gate="G$1" pin="USB_DM" pad="36"/>
<connect gate="G$1" pin="USB_DP" pad="37"/>
<connect gate="G$1" pin="VBUS" pad="35"/>
<connect gate="G$1" pin="VDD" pad="31"/>
<connect gate="G$1" pin="VDDH" pad="32"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:28871091/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="testpad" urn="urn:adsk.eagle:library:20289650">
<description>&lt;b&gt;Test Pins/Pads&lt;/b&gt;&lt;p&gt;
Cream on SMD OFF.&lt;br&gt;
new: Attribute TP_SIGNAL_NAME&lt;br&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="B1,27" urn="urn:adsk.eagle:footprint:27900/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<wire x1="-0.635" y1="0" x2="0.635" y2="0" width="0.0024" layer="37"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.0024" layer="37"/>
<smd name="TP" x="0" y="0" dx="1.27" dy="1.27" layer="1" roundness="100" cream="no"/>
<text x="-0.635" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-0.762" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-0.635" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="B2,54" urn="urn:adsk.eagle:footprint:27901/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<wire x1="-0.635" y1="0" x2="0.635" y2="0" width="0.0024" layer="37"/>
<wire x1="0" y1="-0.635" x2="0" y2="0.635" width="0.0024" layer="37"/>
<circle x="0" y="0" radius="0.635" width="0.254" layer="37"/>
<smd name="TP" x="0" y="0" dx="2.54" dy="2.54" layer="1" roundness="100" cream="no"/>
<text x="-1.27" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-1.397" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-1.27" y="-3.175" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="P1-13" urn="urn:adsk.eagle:footprint:27902/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.762" width="0.1524" layer="51"/>
<pad name="TP" x="0" y="0" drill="1.3208" diameter="2.159" shape="octagon"/>
<text x="-1.016" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-1.27" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
<rectangle x1="-0.3302" y1="-0.3302" x2="0.3302" y2="0.3302" layer="51"/>
</package>
<package name="P1-13Y" urn="urn:adsk.eagle:footprint:27903/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.762" width="0.1524" layer="51"/>
<pad name="TP" x="0" y="0" drill="1.3208" diameter="1.905" shape="long" rot="R90"/>
<text x="-0.889" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-1.27" y="-3.81" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
<rectangle x1="-0.3302" y1="-0.3302" x2="0.3302" y2="0.3302" layer="51"/>
</package>
<package name="P1-17" urn="urn:adsk.eagle:footprint:20289655/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.8128" width="0.1524" layer="51"/>
<pad name="TP" x="0" y="0" drill="1.75" diameter="2.54" shape="octagon"/>
<text x="-1.143" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-1.27" y="-3.175" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
<rectangle x1="-0.3302" y1="-0.3302" x2="0.3302" y2="0.3302" layer="51"/>
</package>
<package name="P1-17Y" urn="urn:adsk.eagle:footprint:27905/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.8128" width="0.1524" layer="51"/>
<pad name="TP" x="0" y="0" drill="1.7018" diameter="2.1208" shape="long" rot="R90"/>
<text x="-1.143" y="2.286" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-1.27" y="-3.81" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
<rectangle x1="-0.3302" y1="-0.3302" x2="0.3302" y2="0.3302" layer="51"/>
</package>
<package name="P1-20" urn="urn:adsk.eagle:footprint:27906/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<circle x="0" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="TP" x="0" y="0" drill="2.0066" diameter="3.1496" shape="octagon"/>
<text x="-1.524" y="1.778" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-1.27" y="-3.175" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
<rectangle x1="-0.3302" y1="-0.3302" x2="0.3302" y2="0.3302" layer="51"/>
</package>
<package name="P1-20Y" urn="urn:adsk.eagle:footprint:27907/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<circle x="0" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="TP" x="0" y="0" drill="2.0066" diameter="2.54" shape="long" rot="R90"/>
<text x="-1.27" y="2.794" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-1.27" y="-4.445" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
<rectangle x1="-0.3302" y1="-0.3302" x2="0.3302" y2="0.3302" layer="51"/>
</package>
<package name="TP06R" urn="urn:adsk.eagle:footprint:27908/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="0.6" dy="0.6" layer="1" roundness="100" cream="no"/>
<text x="-0.3" y="0.4001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.254" y="-0.381" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP06SQ" urn="urn:adsk.eagle:footprint:27909/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="0.5996" dy="0.5996" layer="1" cream="no"/>
<text x="-0.3" y="0.4001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.254" y="-0.381" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP07R" urn="urn:adsk.eagle:footprint:27910/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="0.7" dy="0.7" layer="1" roundness="100" cream="no"/>
<text x="-0.3" y="0.4001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.254" y="-0.508" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP07SQ" urn="urn:adsk.eagle:footprint:27911/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="0.7" dy="0.7" layer="1" cream="no"/>
<text x="-0.3" y="0.4001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.381" y="-0.381" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP08R" urn="urn:adsk.eagle:footprint:27912/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="0.8" dy="0.8" layer="1" roundness="100" cream="no"/>
<text x="-0.3" y="0.4001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.381" y="-0.381" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP08SQ" urn="urn:adsk.eagle:footprint:27913/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="0.8" dy="0.8" layer="1" cream="no"/>
<text x="-0.3" y="0.4001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.381" y="-0.508" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP09R" urn="urn:adsk.eagle:footprint:27914/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="0.9" dy="0.9" layer="1" roundness="100" cream="no"/>
<text x="-0.4501" y="0.5001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.381" y="-0.508" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP09SQ" urn="urn:adsk.eagle:footprint:27915/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="0.8998" dy="0.8998" layer="1" cream="no"/>
<text x="-0.4501" y="0.5001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.381" y="-0.508" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP10R" urn="urn:adsk.eagle:footprint:27916/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1" dy="1" layer="1" roundness="100" cream="no"/>
<text x="-0.5001" y="0.5499" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.381" y="-0.508" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP10SQ" urn="urn:adsk.eagle:footprint:27917/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1" dy="1" layer="1" cream="no"/>
<text x="-0.5001" y="0.5499" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.508" y="-0.635" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP11R" urn="urn:adsk.eagle:footprint:27918/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.1" dy="1.1" layer="1" roundness="100" cream="no"/>
<text x="-0.5499" y="0.5999" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.508" y="-0.508" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP11SQ" urn="urn:adsk.eagle:footprint:27919/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.1" dy="1.1" layer="1" cream="no"/>
<text x="-0.5499" y="0.5999" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.508" y="-0.635" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP12SQ" urn="urn:adsk.eagle:footprint:27920/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.1998" dy="1.1998" layer="1" cream="no"/>
<text x="-0.5999" y="0.65" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.508" y="-0.635" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP12R" urn="urn:adsk.eagle:footprint:27921/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.2" dy="1.2" layer="1" roundness="100" cream="no"/>
<text x="-0.5999" y="0.65" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.508" y="-0.635" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP13R" urn="urn:adsk.eagle:footprint:27922/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.3" dy="1.3" layer="1" roundness="100" cream="no"/>
<text x="-0.65" y="0.7" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.508" y="-0.635" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP14R" urn="urn:adsk.eagle:footprint:27923/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.4" dy="1.4" layer="1" roundness="100" cream="no"/>
<text x="-0.7" y="0.7501" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.508" y="-0.762" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP15R" urn="urn:adsk.eagle:footprint:27924/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.5" dy="1.5" layer="1" roundness="100" cream="no"/>
<text x="-0.7501" y="0.8001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-0.762" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP16R" urn="urn:adsk.eagle:footprint:27925/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.6" dy="1.6" layer="1" roundness="100" cream="no"/>
<text x="-0.8001" y="0.8499" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-0.762" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP17R" urn="urn:adsk.eagle:footprint:27926/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.7" dy="1.7" layer="1" roundness="100" cream="no"/>
<text x="-0.8499" y="0.8999" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-0.889" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP18R" urn="urn:adsk.eagle:footprint:27927/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.8" dy="1.8" layer="1" roundness="100" cream="no"/>
<text x="-0.8999" y="0.95" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-0.889" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP19R" urn="urn:adsk.eagle:footprint:27928/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.9" dy="1.9" layer="1" roundness="100" cream="no"/>
<text x="-0.95" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-0.889" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP20R" urn="urn:adsk.eagle:footprint:27929/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="2" dy="2" layer="1" roundness="100" cream="no"/>
<text x="-1" y="1.05" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-1.016" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP13SQ" urn="urn:adsk.eagle:footprint:27930/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.3" dy="1.3" layer="1" cream="no"/>
<text x="-0.65" y="0.7" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-0.762" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP14SQ" urn="urn:adsk.eagle:footprint:27931/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.4" dy="1.4" layer="1" cream="no"/>
<text x="-0.7" y="0.7501" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-0.762" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP15SQ" urn="urn:adsk.eagle:footprint:27932/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.5" dy="1.5" layer="1" cream="no"/>
<text x="-0.7501" y="0.8001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-0.889" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP16SQ" urn="urn:adsk.eagle:footprint:27933/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.5996" dy="1.5996" layer="1" cream="no"/>
<text x="-0.8001" y="0.8499" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-0.889" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP17SQ" urn="urn:adsk.eagle:footprint:27934/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.7" dy="1.7" layer="1" cream="no"/>
<text x="-0.8499" y="0.8999" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-0.889" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP18SQ" urn="urn:adsk.eagle:footprint:27935/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.8" dy="1.8" layer="1" cream="no"/>
<text x="-0.8999" y="0.95" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-1.016" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP19SQ" urn="urn:adsk.eagle:footprint:27936/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.8998" dy="1.8998" layer="1" cream="no"/>
<text x="-0.95" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-1.016" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP20SQ" urn="urn:adsk.eagle:footprint:27937/1" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="2" dy="2" layer="1" cream="no"/>
<text x="-1" y="1.05" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
</packages>
<packages3d>
<package3d name="B1,27" urn="urn:adsk.eagle:package:27944/2" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="B1,27"/>
</packageinstances>
</package3d>
<package3d name="B2,54" urn="urn:adsk.eagle:package:27948/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="B2,54"/>
</packageinstances>
</package3d>
<package3d name="P1-13" urn="urn:adsk.eagle:package:27946/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="P1-13"/>
</packageinstances>
</package3d>
<package3d name="P1-13Y" urn="urn:adsk.eagle:package:27947/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="P1-13Y"/>
</packageinstances>
</package3d>
<package3d name="P1-17" urn="urn:adsk.eagle:package:20289656/1" type="box" library_version="1">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="P1-17"/>
</packageinstances>
</package3d>
<package3d name="P1-17Y" urn="urn:adsk.eagle:package:27953/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="P1-17Y"/>
</packageinstances>
</package3d>
<package3d name="P1-20" urn="urn:adsk.eagle:package:27950/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="P1-20"/>
</packageinstances>
</package3d>
<package3d name="P1-20Y" urn="urn:adsk.eagle:package:27951/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="P1-20Y"/>
</packageinstances>
</package3d>
<package3d name="TP06R" urn="urn:adsk.eagle:package:27954/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP06R"/>
</packageinstances>
</package3d>
<package3d name="TP06SQ" urn="urn:adsk.eagle:package:27952/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP06SQ"/>
</packageinstances>
</package3d>
<package3d name="TP07R" urn="urn:adsk.eagle:package:27970/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP07R"/>
</packageinstances>
</package3d>
<package3d name="TP07SQ" urn="urn:adsk.eagle:package:27955/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP07SQ"/>
</packageinstances>
</package3d>
<package3d name="TP08R" urn="urn:adsk.eagle:package:27956/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP08R"/>
</packageinstances>
</package3d>
<package3d name="TP08SQ" urn="urn:adsk.eagle:package:27960/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP08SQ"/>
</packageinstances>
</package3d>
<package3d name="TP09R" urn="urn:adsk.eagle:package:27958/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP09R"/>
</packageinstances>
</package3d>
<package3d name="TP09SQ" urn="urn:adsk.eagle:package:27957/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP09SQ"/>
</packageinstances>
</package3d>
<package3d name="TP10R" urn="urn:adsk.eagle:package:27959/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP10R"/>
</packageinstances>
</package3d>
<package3d name="TP10SQ" urn="urn:adsk.eagle:package:27962/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP10SQ"/>
</packageinstances>
</package3d>
<package3d name="TP11R" urn="urn:adsk.eagle:package:27961/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP11R"/>
</packageinstances>
</package3d>
<package3d name="TP11SQ" urn="urn:adsk.eagle:package:27965/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP11SQ"/>
</packageinstances>
</package3d>
<package3d name="TP12SQ" urn="urn:adsk.eagle:package:27964/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP12SQ"/>
</packageinstances>
</package3d>
<package3d name="TP12R" urn="urn:adsk.eagle:package:27963/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP12R"/>
</packageinstances>
</package3d>
<package3d name="TP13R" urn="urn:adsk.eagle:package:27967/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP13R"/>
</packageinstances>
</package3d>
<package3d name="TP14R" urn="urn:adsk.eagle:package:27966/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP14R"/>
</packageinstances>
</package3d>
<package3d name="TP15R" urn="urn:adsk.eagle:package:27968/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP15R"/>
</packageinstances>
</package3d>
<package3d name="TP16R" urn="urn:adsk.eagle:package:27969/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP16R"/>
</packageinstances>
</package3d>
<package3d name="TP17R" urn="urn:adsk.eagle:package:27971/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP17R"/>
</packageinstances>
</package3d>
<package3d name="TP18R" urn="urn:adsk.eagle:package:27981/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP18R"/>
</packageinstances>
</package3d>
<package3d name="TP19R" urn="urn:adsk.eagle:package:27972/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP19R"/>
</packageinstances>
</package3d>
<package3d name="TP20R" urn="urn:adsk.eagle:package:27973/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP20R"/>
</packageinstances>
</package3d>
<package3d name="TP13SQ" urn="urn:adsk.eagle:package:27974/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP13SQ"/>
</packageinstances>
</package3d>
<package3d name="TP14SQ" urn="urn:adsk.eagle:package:27984/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP14SQ"/>
</packageinstances>
</package3d>
<package3d name="TP15SQ" urn="urn:adsk.eagle:package:27975/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP15SQ"/>
</packageinstances>
</package3d>
<package3d name="TP16SQ" urn="urn:adsk.eagle:package:27976/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP16SQ"/>
</packageinstances>
</package3d>
<package3d name="TP17SQ" urn="urn:adsk.eagle:package:27977/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP17SQ"/>
</packageinstances>
</package3d>
<package3d name="TP18SQ" urn="urn:adsk.eagle:package:27979/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP18SQ"/>
</packageinstances>
</package3d>
<package3d name="TP19SQ" urn="urn:adsk.eagle:package:27978/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP19SQ"/>
</packageinstances>
</package3d>
<package3d name="TP20SQ" urn="urn:adsk.eagle:package:27980/1" type="box" library_version="1">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP20SQ"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="TP" urn="urn:adsk.eagle:symbol:20289653/1" library_version="1">
<wire x1="-0.762" y1="-0.762" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0.762" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0.762" y1="-0.762" x2="0" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0" y1="-1.524" x2="-0.762" y2="-0.762" width="0.254" layer="94"/>
<text x="-1.27" y="1.27" size="1.778" layer="95">&gt;NAME</text>
<text x="1.27" y="-1.27" size="1.778" layer="97">&gt;TP_SIGNAL_NAME</text>
<pin name="TP" x="0" y="-2.54" visible="off" length="short" direction="in" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TP" urn="urn:adsk.eagle:component:20289660/1" prefix="TP" library_version="1">
<description>&lt;b&gt;Test pad&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="TP" x="0" y="0"/>
</gates>
<devices>
<device name="B1,27" package="B1,27">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27944/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="19" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="B2,54" package="B2,54">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27948/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="3" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="PAD1-13" package="P1-13">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27946/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="12" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="PAD1-13Y" package="P1-13Y">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27947/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="PAD1-17" package="P1-17">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:20289656/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="PAD1-17Y" package="P1-17Y">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27953/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="3" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="PAD1-20" package="P1-20">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27950/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="PAD1-20Y" package="P1-20Y">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27951/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP06R" package="TP06R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27954/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP06SQ" package="TP06SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27952/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP07R" package="TP07R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27970/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP07SQ" package="TP07SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27955/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP08R" package="TP08R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27956/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP08SQ" package="TP08SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27960/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP09R" package="TP09R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27958/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP09SQ" package="TP09SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27957/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP10R" package="TP10R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27959/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="3" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP10SQ" package="TP10SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27962/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="4" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP11R" package="TP11R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27961/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP11SQ" package="TP11SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27965/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP12SQ" package="TP12SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27964/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP12R" package="TP12R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27963/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP13R" package="TP13R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27967/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP14R" package="TP14R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27966/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP15R" package="TP15R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27968/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP16R" package="TP16R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27969/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP17R" package="TP17R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27971/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP18R" package="TP18R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27981/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP19R" package="TP19R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27972/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP20R" package="TP20R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27973/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP13SQ" package="TP13SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27974/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP14SQ" package="TP14SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27984/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP15SQ" package="TP15SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27975/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP16SQ" package="TP16SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27976/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP17SQ" package="TP17SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27977/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP18SQ" package="TP18SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27979/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP19SQ" package="TP19SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27978/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP20SQ" package="TP20SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27980/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="2" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
<attribute name="COMPANY" value="Invector Labs"/>
<attribute name="DRAWN" value="P. Oldberg"/>
<attribute name="DRGNO" value="10/98-00168-1"/>
<attribute name="REV" value="0.2"/>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
<class number="1" name="RF" width="0.3" drill="0">
<clearance class="1" value="0.1524"/>
</class>
<class number="2" name="gnd" width="0" drill="0">
</class>
</classes>
<parts>
<part name="FRAME1" library="microbuilder" deviceset="FRAME_A4" device="">
<attribute name="DRGNO" value=""/>
</part>
<part name="C6" library="Capacitor" library_urn="urn:adsk.eagle:library:16290819" deviceset="C" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16290898/2" technology="_" value="4.7uF"/>
<part name="C7" library="Capacitor" library_urn="urn:adsk.eagle:library:16290819" deviceset="C" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16290898/2" technology="_" value="1uF"/>
<part name="U$27" library="microbuilder" deviceset="GND" device=""/>
<part name="U$28" library="microbuilder" deviceset="GND" device=""/>
<part name="C8" library="Capacitor" library_urn="urn:adsk.eagle:library:16290819" deviceset="C" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16290898/2" technology="_" value="10uF"/>
<part name="U$29" library="microbuilder" deviceset="GND" device=""/>
<part name="U$30" library="microbuilder" deviceset="GND" device=""/>
<part name="GND12" library="supply1" deviceset="GND" device=""/>
<part name="L" library="new_led" library_urn="urn:adsk.eagle:library:18621472" deviceset="LED" device="CHIPLED_0603" package3d_urn="urn:adsk.eagle:package:22365974/1" value="GREEN"/>
<part name="GND7" library="supply1" deviceset="GND" device=""/>
<part name="C14" library="Capacitor" library_urn="urn:adsk.eagle:library:16290819" deviceset="C" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16290898/2" technology="_" value="4.7uF"/>
<part name="R7" library="Resistor" library_urn="urn:adsk.eagle:library:16378527" deviceset="R" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16378565/2" technology="_" value="1K"/>
<part name="GND10" library="supply1" deviceset="GND" device=""/>
<part name="SW1" library="adafruit" deviceset="SPST_TACT" device="-KMR2"/>
<part name="U$31" library="microbuilder" deviceset="MOUNTINGHOLE" device="2.5"/>
<part name="U$32" library="microbuilder" deviceset="MOUNTINGHOLE" device="2.5"/>
<part name="+3V4" library="supply1" deviceset="+3V3" device=""/>
<part name="U$3" library="microbuilder" deviceset="VBUS" device=""/>
<part name="U$21" library="microbuilder" deviceset="VBAT" device=""/>
<part name="U2" library="microbuilder" deviceset="VREG_SOT23-5" device="" value="AP2112-3.3"/>
<part name="D1" library="microbuilder" deviceset="DIODE-SCHOTTKY" device="SOD-123" value="MBR120"/>
<part name="U$19" library="microbuilder" deviceset="VBUS" device=""/>
<part name="U$20" library="microbuilder" deviceset="VBAT" device=""/>
<part name="JP1" library="microbuilder" deviceset="HEADER-1X16" device="ROUND"/>
<part name="+3V1" library="supply1" deviceset="+3V3" device=""/>
<part name="GND4" library="supply1" deviceset="GND" device=""/>
<part name="X1" library="microbuilder" deviceset="CON_JST_PH_2PIN" device="" value="JSTPH"/>
<part name="U$16" library="microbuilder" deviceset="VBAT" device=""/>
<part name="U$22" library="microbuilder" deviceset="GND" device=""/>
<part name="JP3" library="microbuilder" deviceset="HEADER-1X12" device=""/>
<part name="U3" library="microbuilder" deviceset="MCP73831/2" device="" value="MCP73831T-2ACI/OT"/>
<part name="CHG" library="new_led" library_urn="urn:adsk.eagle:library:18621472" deviceset="LED" device="CHIPLED_0603" package3d_urn="urn:adsk.eagle:package:22365974/1" value="RED"/>
<part name="R2" library="Resistor" library_urn="urn:adsk.eagle:library:16378527" deviceset="R" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16378565/2" technology="_" value="1K"/>
<part name="C3" library="Capacitor" library_urn="urn:adsk.eagle:library:16290819" deviceset="C" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16290898/2" technology="_" value="10uF"/>
<part name="U$33" library="microbuilder" deviceset="GND" device=""/>
<part name="R8" library="Resistor" library_urn="urn:adsk.eagle:library:16378527" deviceset="R" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16378565/2" technology="_" value="4K7"/>
<part name="U$36" library="microbuilder" deviceset="GND" device=""/>
<part name="U$38" library="microbuilder" deviceset="VBUS" device=""/>
<part name="U$39" library="microbuilder" deviceset="VBAT" device=""/>
<part name="GND11" library="supply1" deviceset="GND" device=""/>
<part name="R3" library="Resistor" library_urn="urn:adsk.eagle:library:16378527" deviceset="R" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16378565/2" technology="_" value="100K"/>
<part name="R6" library="Resistor" library_urn="urn:adsk.eagle:library:16378527" deviceset="R" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16378565/2" technology="_" value="100K"/>
<part name="U$2" library="microbuilder" deviceset="VBAT" device=""/>
<part name="GND13" library="supply1" deviceset="GND" device=""/>
<part name="C5" library="Capacitor" library_urn="urn:adsk.eagle:library:16290819" deviceset="C" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16290898/2" technology="_" value="1uF"/>
<part name="+3V2" library="supply1" deviceset="+3V3" device=""/>
<part name="U$11" library="microbuilder" deviceset="GND" device=""/>
<part name="R1" library="Resistor" library_urn="urn:adsk.eagle:library:16378527" deviceset="R" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16378565/2" technology="_" value="100K"/>
<part name="LED1" library="microbuilder" deviceset="WS2812B" device="3535"/>
<part name="GND15" library="supply1" deviceset="GND" device=""/>
<part name="U1" library="microbuilder" deviceset="SPIFLASH_SOIC8" device="208MIL" value="4MB Flash"/>
<part name="U$26" library="microbuilder" deviceset="GND" device=""/>
<part name="+3V5" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V6" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V7" library="supply1" deviceset="+3V3" device=""/>
<part name="C18" library="Capacitor" library_urn="urn:adsk.eagle:library:16290819" deviceset="C" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16290898/2" technology="_" value="0.1uF"/>
<part name="GND21" library="supply1" deviceset="GND" device=""/>
<part name="C19" library="Capacitor" library_urn="urn:adsk.eagle:library:16290819" deviceset="C" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16290898/2" technology="_" value="1uF"/>
<part name="U$7" library="microbuilder" deviceset="GND" device=""/>
<part name="J2" library="USB4105-GF-A" library_urn="urn:adsk.eagle:library:21150292" deviceset="USB4105-GF-A" device="" package3d_urn="urn:adsk.eagle:package:21150295/3"/>
<part name="U$9" library="microbuilder" deviceset="GND" device=""/>
<part name="U$12" library="microbuilder" deviceset="VBUS" device=""/>
<part name="R10" library="Resistor" library_urn="urn:adsk.eagle:library:16378527" deviceset="R" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16378565/2" technology="_" value="5K1"/>
<part name="U$1" library="microbuilder" deviceset="GND" device=""/>
<part name="R12" library="Resistor" library_urn="urn:adsk.eagle:library:16378527" deviceset="R" device="CHIP-0603(1608-METRIC)" package3d_urn="urn:adsk.eagle:package:16378565/2" technology="_" value="5K1"/>
<part name="U$13" library="microbuilder" deviceset="GND" device=""/>
<part name="LOGO1" library="ilabs-pictures" library_urn="urn:adsk.eagle:library:21548800" deviceset="CHALENGER-11MM" device="" package3d_urn="urn:adsk.eagle:package:21548803/1"/>
<part name="M1" library="Minew" library_urn="urn:adsk.eagle:library:28871088" deviceset="M88SF3" device="-1Y40AIR" package3d_urn="urn:adsk.eagle:package:28871091/1"/>
<part name="+3V3" library="supply1" deviceset="+3V3" device=""/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="U$4" library="microbuilder" deviceset="VBUS" device=""/>
<part name="TP1" library="testpad" library_urn="urn:adsk.eagle:library:20289650" deviceset="TP" device="B1,27" package3d_urn="urn:adsk.eagle:package:27944/2"/>
<part name="TP2" library="testpad" library_urn="urn:adsk.eagle:library:20289650" deviceset="TP" device="B1,27" package3d_urn="urn:adsk.eagle:package:27944/2"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="43.18" y="170.18" size="1.778" layer="94">POWER AND FILTERING</text>
<wire x1="5.08" y1="121.92" x2="111.76" y2="121.92" width="0.1524" layer="94" style="shortdash"/>
<wire x1="111.76" y1="121.92" x2="111.76" y2="175.26" width="0.1524" layer="94" style="shortdash"/>
<wire x1="134.62" y1="38.1" x2="134.62" y2="88.9" width="0.1524" layer="94" style="shortdash"/>
<wire x1="134.62" y1="88.9" x2="134.62" y2="121.92" width="0.1524" layer="94" style="shortdash"/>
<wire x1="134.62" y1="124.46" x2="134.62" y2="121.92" width="0.1524" layer="94" style="shortdash"/>
<wire x1="134.62" y1="121.92" x2="111.76" y2="121.92" width="0.1524" layer="94" style="shortdash"/>
<text x="119.38" y="170.18" size="1.778" layer="94">RESET</text>
<wire x1="134.62" y1="124.46" x2="134.62" y2="175.26" width="0.1524" layer="94" style="shortdash"/>
<wire x1="256.54" y1="88.9" x2="236.22" y2="88.9" width="0.1524" layer="94" style="shortdash"/>
<wire x1="236.22" y1="88.9" x2="134.62" y2="88.9" width="0.1524" layer="94" style="shortdash"/>
<wire x1="236.22" y1="38.1" x2="236.22" y2="88.9" width="0.1524" layer="94" style="shortdash"/>
<text x="162.56" y="165.1" size="1.778" layer="97">10K  = 100mA</text>
<text x="162.56" y="162.56" size="1.778" layer="97">5.0K  = 200mA</text>
<text x="162.56" y="160.02" size="1.778" layer="97">2.0K  = 500mA</text>
<text x="162.56" y="157.48" size="1.778" layer="97">1.0K  = 1000mA</text>
<text x="147.32" y="170.18" size="1.778" layer="94">USB AND BATTERY CHARGING</text>
<text x="157.48" y="43.18" size="1.778" layer="94">BREAKOUTS</text>
<text x="238.76" y="83.82" size="1.778" layer="94">RED LED</text>
<text x="171.45" y="118.11" size="1.778" layer="91">Internal SPI on SPI1</text>
<wire x1="256.54" y1="124.46" x2="134.62" y2="124.46" width="0.1524" layer="94" style="shortdash"/>
</plain>
<instances>
<instance part="FRAME1" gate="G$3" x="134.62" y="2.54" smashed="yes">
<attribute name="SHEET" x="243.5352" y="5.3848" size="2.54" layer="94" font="vector" ratio="10"/>
<attribute name="DRAWING_NAME" x="167.6527" y="5.3848" size="2.54" layer="94" ratio="10"/>
<attribute name="LAST_DATE_TIME" x="167.3352" y="11.7348" size="2.54" layer="94" ratio="10"/>
<attribute name="DRAWN" x="136.8552" y="23.1648" size="1.778" layer="94"/>
<attribute name="REV" x="246.0752" y="21.2598" size="3.81" layer="94" ratio="12"/>
<attribute name="DRGNO" x="225.7552" y="11.7348" size="2.1844" layer="94"/>
</instance>
<instance part="C6" gate="G$1" x="12.7" y="134.62" smashed="yes">
<attribute name="NAME" x="15.24" y="133.35" size="1.27" layer="95" rot="R180" align="bottom-right"/>
<attribute name="VALUE" x="15.24" y="130.81" size="1.27" layer="96" rot="R180" align="bottom-right"/>
</instance>
<instance part="C7" gate="G$1" x="58.42" y="134.62" smashed="yes">
<attribute name="NAME" x="60.96" y="134.62" size="1.27" layer="95" rot="R180" align="bottom-right"/>
<attribute name="VALUE" x="60.96" y="132.08" size="1.27" layer="96" rot="R180" align="bottom-right"/>
</instance>
<instance part="U$27" gate="G$1" x="12.7" y="127" smashed="yes">
<attribute name="VALUE" x="11.176" y="124.46" size="1.27" layer="96"/>
</instance>
<instance part="U$28" gate="G$1" x="58.42" y="127" smashed="yes">
<attribute name="VALUE" x="56.896" y="124.46" size="1.27" layer="96"/>
</instance>
<instance part="C8" gate="G$1" x="50.8" y="134.62" smashed="yes">
<attribute name="NAME" x="48.24" y="133.1" size="1.27" layer="95" align="bottom-right"/>
<attribute name="VALUE" x="48.24" y="131.05" size="1.27" layer="96" align="bottom-right"/>
</instance>
<instance part="U$29" gate="G$1" x="50.8" y="127" smashed="yes">
<attribute name="VALUE" x="49.276" y="124.46" size="1.27" layer="96"/>
</instance>
<instance part="U$30" gate="G$1" x="27.94" y="127" smashed="yes">
<attribute name="VALUE" x="26.416" y="124.46" size="1.27" layer="96"/>
</instance>
<instance part="GND12" gate="1" x="92.71" y="127" smashed="yes" rot="MR0">
<attribute name="VALUE" x="95.25" y="124.46" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="L" gate="G$1" x="246.38" y="58.42" smashed="yes">
<attribute name="NAME" x="240.03" y="57.785" size="1.27" layer="95" align="center"/>
<attribute name="VALUE" x="240.03" y="55.626" size="1.27" layer="96" align="center"/>
</instance>
<instance part="GND7" gate="1" x="129.54" y="144.78" smashed="yes">
<attribute name="VALUE" x="127" y="142.24" size="1.778" layer="96"/>
</instance>
<instance part="C14" gate="G$1" x="92.71" y="137.16" smashed="yes">
<attribute name="NAME" x="95.25" y="138.43" size="1.27" layer="95" rot="R180" align="bottom-right"/>
<attribute name="VALUE" x="95.27" y="135.65" size="1.27" layer="96" rot="R180" align="bottom-right"/>
</instance>
<instance part="R7" gate="G$1" x="246.38" y="68.58" smashed="yes" rot="R270">
<attribute name="NAME" x="248.92" y="68.58" size="1.27" layer="95" rot="R270" align="center"/>
<attribute name="VALUE" x="246.38" y="68.58" size="1.27" layer="96" ratio="15" rot="R270" align="center"/>
</instance>
<instance part="GND10" gate="1" x="246.38" y="48.26" smashed="yes" rot="MR0">
<attribute name="VALUE" x="248.92" y="45.72" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="SW1" gate="G$1" x="124.46" y="152.4" smashed="yes" rot="R270">
<attribute name="NAME" x="121.92" y="158.75" size="1.778" layer="95"/>
<attribute name="VALUE" x="123.19" y="151.765" size="1.27" layer="96" rot="R270"/>
</instance>
<instance part="U$31" gate="G$1" x="205.74" y="12.7" smashed="yes"/>
<instance part="U$32" gate="G$1" x="210.82" y="12.7" smashed="yes"/>
<instance part="+3V4" gate="G$1" x="58.42" y="160.02" smashed="yes" rot="MR0">
<attribute name="VALUE" x="60.96" y="154.94" size="1.27" layer="96" rot="MR90"/>
</instance>
<instance part="U$3" gate="G$1" x="7.62" y="160.02" smashed="yes">
<attribute name="VALUE" x="6.096" y="161.036" size="1.27" layer="96"/>
</instance>
<instance part="U$21" gate="G$1" x="12.7" y="160.02" smashed="yes">
<attribute name="VALUE" x="11.176" y="161.036" size="1.27" layer="96"/>
</instance>
<instance part="U2" gate="G$1" x="38.1" y="144.78" smashed="yes">
<attribute name="NAME" x="30.48" y="150.876" size="1.27" layer="95"/>
<attribute name="VALUE" x="30.48" y="137.16" size="1.27" layer="95"/>
</instance>
<instance part="D1" gate="G$1" x="12.7" y="154.94" smashed="yes" rot="R270">
<attribute name="NAME" x="15.24" y="154.94" size="1.27" layer="95" rot="R270" align="center"/>
<attribute name="VALUE" x="10.2" y="154.94" size="1.27" layer="96" rot="R270" align="center"/>
</instance>
<instance part="U$19" gate="G$1" x="147.32" y="76.2" smashed="yes" rot="R90">
<attribute name="VALUE" x="146.304" y="74.676" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="U$20" gate="G$1" x="147.32" y="81.28" smashed="yes" rot="R90">
<attribute name="VALUE" x="146.304" y="79.756" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="JP1" gate="A" x="210.82" y="60.96" smashed="yes" rot="R180">
<attribute name="NAME" x="217.17" y="40.005" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="217.17" y="86.36" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="+3V1" gate="G$1" x="198.12" y="83.82" smashed="yes">
<attribute name="VALUE" x="195.58" y="78.74" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND4" gate="1" x="223.52" y="68.58" smashed="yes">
<attribute name="VALUE" x="220.98" y="66.04" size="1.778" layer="96"/>
</instance>
<instance part="X1" gate="G$1" x="80.01" y="139.7" smashed="yes" rot="R180">
<attribute name="NAME" x="86.36" y="133.985" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="86.36" y="144.78" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="U$16" gate="G$1" x="82.55" y="152.4" smashed="yes">
<attribute name="VALUE" x="81.026" y="153.416" size="1.27" layer="96"/>
</instance>
<instance part="U$22" gate="G$1" x="82.55" y="127" smashed="yes">
<attribute name="VALUE" x="81.026" y="124.46" size="1.27" layer="96"/>
</instance>
<instance part="FRAME1" gate="G$1" x="0" y="0" smashed="yes"/>
<instance part="JP3" gate="G$1" x="180.34" y="66.04" smashed="yes">
<attribute name="NAME" x="173.99" y="84.455" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.99" y="48.26" size="1.778" layer="96"/>
</instance>
<instance part="U3" gate="G$1" x="163.83" y="144.78" smashed="yes">
<attribute name="NAME" x="153.67" y="156.21" size="1.27" layer="95"/>
<attribute name="VALUE" x="153.67" y="132.08" size="1.27" layer="95"/>
</instance>
<instance part="CHG" gate="G$1" x="139.7" y="149.86" smashed="yes">
<attribute name="NAME" x="143.51" y="159.385" size="1.27" layer="95" align="center"/>
<attribute name="VALUE" x="143.51" y="157.226" size="1.27" layer="96" align="center"/>
</instance>
<instance part="R2" gate="G$1" x="144.78" y="142.24" smashed="yes">
<attribute name="NAME" x="144.78" y="147.32" size="1.27" layer="95" font="vector" align="center"/>
<attribute name="VALUE" x="144.78" y="144.78" size="1.27" layer="96" font="vector" align="center"/>
</instance>
<instance part="C3" gate="G$1" x="189.23" y="139.7" smashed="yes">
<attribute name="NAME" x="193.06" y="139.95" size="1.27" layer="95" rot="R180" align="center"/>
<attribute name="VALUE" x="194.29" y="137.4" size="1.27" layer="96" align="center"/>
</instance>
<instance part="U$33" gate="G$1" x="189.23" y="129.54" smashed="yes">
<attribute name="VALUE" x="187.706" y="127" size="1.27" layer="96"/>
</instance>
<instance part="R8" gate="G$1" x="181.61" y="139.7" smashed="yes" rot="R270">
<attribute name="NAME" x="182.88" y="140.97" size="1.27" layer="95"/>
<attribute name="VALUE" x="182.88" y="139.7" size="1.27" layer="96" ratio="15"/>
</instance>
<instance part="U$36" gate="G$1" x="181.61" y="129.54" smashed="yes">
<attribute name="VALUE" x="180.086" y="127" size="1.27" layer="96"/>
</instance>
<instance part="U$38" gate="G$1" x="139.7" y="165.1" smashed="yes">
<attribute name="VALUE" x="138.176" y="166.116" size="1.27" layer="96"/>
</instance>
<instance part="U$39" gate="G$1" x="189.23" y="156.21" smashed="yes">
<attribute name="VALUE" x="187.706" y="157.226" size="1.27" layer="96"/>
</instance>
<instance part="GND11" gate="1" x="198.12" y="40.64" smashed="yes">
<attribute name="VALUE" x="195.58" y="38.1" size="1.778" layer="96"/>
</instance>
<instance part="R3" gate="G$1" x="17.78" y="25.4" smashed="yes" rot="R270">
<attribute name="NAME" x="20.32" y="25.4" size="1.27" layer="95" rot="R270" align="center"/>
<attribute name="VALUE" x="17.78" y="25.4" size="1.27" layer="96" ratio="15" rot="R270" align="center"/>
</instance>
<instance part="R6" gate="G$1" x="17.78" y="15.24" smashed="yes" rot="R270">
<attribute name="NAME" x="20.32" y="15.24" size="1.27" layer="95" rot="R270" align="center"/>
<attribute name="VALUE" x="17.78" y="15.24" size="1.27" layer="96" ratio="15" rot="R270" align="center"/>
</instance>
<instance part="U$2" gate="G$1" x="17.78" y="33.02" smashed="yes">
<attribute name="VALUE" x="16.256" y="34.036" size="1.27" layer="96"/>
</instance>
<instance part="GND13" gate="1" x="17.78" y="7.62" smashed="yes">
<attribute name="VALUE" x="15.24" y="5.08" size="1.778" layer="96"/>
</instance>
<instance part="C5" gate="G$1" x="102.87" y="137.16" smashed="yes">
<attribute name="NAME" x="105.41" y="138.43" size="1.27" layer="95" rot="R180" align="bottom-right"/>
<attribute name="VALUE" x="105.41" y="135.89" size="1.27" layer="96" rot="R180" align="bottom-right"/>
</instance>
<instance part="+3V2" gate="G$1" x="102.87" y="152.4" smashed="yes" rot="MR0">
<attribute name="VALUE" x="105.41" y="152.4" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="U$11" gate="G$1" x="102.87" y="127" smashed="yes">
<attribute name="VALUE" x="101.346" y="124.46" size="1.27" layer="96"/>
</instance>
<instance part="R1" gate="G$1" x="20.32" y="142.24" smashed="yes" rot="R270">
<attribute name="NAME" x="16.51" y="143.51" size="1.27" layer="95" align="center"/>
<attribute name="VALUE" x="16.51" y="140.97" size="1.27" layer="96" ratio="15" align="center"/>
</instance>
<instance part="LED1" gate="G$1" x="241.3" y="104.14" smashed="yes"/>
<instance part="GND15" gate="1" x="241.3" y="91.44" smashed="yes">
<attribute name="VALUE" x="238.76" y="88.9" size="1.778" layer="96"/>
</instance>
<instance part="U1" gate="G$1" x="190.5" y="101.6" smashed="yes">
<attribute name="NAME" x="177.8" y="114.3" size="1.778" layer="95"/>
<attribute name="VALUE" x="177.8" y="91.44" size="1.778" layer="95"/>
</instance>
<instance part="U$26" gate="G$1" x="210.82" y="93.98" smashed="yes">
<attribute name="VALUE" x="209.296" y="91.44" size="1.27" layer="96"/>
</instance>
<instance part="+3V5" gate="G$1" x="205.74" y="119.38" smashed="yes" rot="MR0">
<attribute name="VALUE" x="208.28" y="120.65" size="1.27" layer="96" rot="MR0"/>
</instance>
<instance part="+3V6" gate="G$1" x="246.38" y="121.92" smashed="yes" rot="MR0">
<attribute name="VALUE" x="248.92" y="116.84" size="1.27" layer="96" rot="MR90"/>
</instance>
<instance part="+3V7" gate="G$1" x="165.1" y="99.06" smashed="yes" rot="MR270">
<attribute name="VALUE" x="163.83" y="97.79" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="C18" gate="G$1" x="210.82" y="109.22" smashed="yes">
<attribute name="NAME" x="215.9" y="109.22" size="1.27" layer="95" align="center"/>
<attribute name="VALUE" x="215.9" y="106.68" size="1.27" layer="96" align="center"/>
</instance>
<instance part="GND21" gate="1" x="210.82" y="104.14" smashed="yes">
<attribute name="VALUE" x="208.28" y="101.6" size="1.778" layer="96"/>
</instance>
<instance part="C19" gate="G$1" x="68.58" y="134.62" smashed="yes">
<attribute name="NAME" x="71.12" y="134.62" size="1.27" layer="95" rot="R180" align="bottom-right"/>
<attribute name="VALUE" x="71.12" y="132.08" size="1.27" layer="96" rot="R180" align="bottom-right"/>
</instance>
<instance part="U$7" gate="G$1" x="68.58" y="127" smashed="yes">
<attribute name="VALUE" x="67.056" y="124.46" size="1.27" layer="96"/>
</instance>
<instance part="J2" gate="G$1" x="228.6" y="147.32" smashed="yes" rot="MR0">
<attribute name="NAME" x="228.6" y="161.29" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="236.22" y="134.62" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="U$9" gate="G$1" x="204.47" y="129.54" smashed="yes">
<attribute name="VALUE" x="202.946" y="127" size="1.27" layer="96"/>
</instance>
<instance part="U$12" gate="G$1" x="204.47" y="166.37" smashed="yes">
<attribute name="VALUE" x="202.946" y="167.386" size="1.27" layer="96"/>
</instance>
<instance part="R10" gate="G$1" x="199.39" y="147.32" smashed="yes" rot="R270">
<attribute name="NAME" x="195.58" y="148.59" size="1.27" layer="95" rot="R180" align="center"/>
<attribute name="VALUE" x="195.58" y="146.05" size="1.27" layer="96" ratio="15" rot="R180" align="center"/>
</instance>
<instance part="U$1" gate="G$1" x="199.39" y="139.7" smashed="yes">
<attribute name="VALUE" x="197.866" y="137.16" size="1.27" layer="96"/>
</instance>
<instance part="R12" gate="G$1" x="245.11" y="160.02" smashed="yes" rot="R270">
<attribute name="NAME" x="241.3" y="161.29" size="1.27" layer="95" rot="R180" align="center"/>
<attribute name="VALUE" x="241.3" y="158.75" size="1.27" layer="96" ratio="15" rot="R180" align="center"/>
</instance>
<instance part="U$13" gate="G$1" x="245.11" y="167.64" smashed="yes" rot="R180">
<attribute name="VALUE" x="246.634" y="170.18" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="LOGO1" gate="G$1" x="207.01" y="24.13" smashed="yes"/>
<instance part="M1" gate="G$1" x="76.2" y="63.5" smashed="yes">
<attribute name="VALUE" x="66.04" y="66.04" size="1.778" layer="96" align="top-left"/>
<attribute name="NAME" x="50.8" y="106.68" size="1.778" layer="95" align="top-left"/>
</instance>
<instance part="+3V3" gate="G$1" x="81.28" y="118.11" smashed="yes" rot="MR0">
<attribute name="VALUE" x="83.82" y="118.11" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND1" gate="1" x="76.2" y="10.16" smashed="yes">
<attribute name="VALUE" x="73.66" y="7.62" size="1.778" layer="96"/>
</instance>
<instance part="U$4" gate="G$1" x="114.3" y="109.22" smashed="yes">
<attribute name="VALUE" x="112.776" y="110.236" size="1.27" layer="96"/>
</instance>
<instance part="TP1" gate="G$1" x="129.54" y="43.18" smashed="yes" rot="R270">
<attribute name="NAME" x="127" y="44.45" size="1.778" layer="95"/>
<attribute name="TP_SIGNAL_NAME" x="128.27" y="41.91" size="1.778" layer="97" rot="R270"/>
</instance>
<instance part="TP2" gate="G$1" x="129.54" y="40.64" smashed="yes" rot="R270">
<attribute name="NAME" x="127" y="36.83" size="1.778" layer="95"/>
<attribute name="TP_SIGNAL_NAME" x="128.27" y="39.37" size="1.778" layer="97" rot="R270"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="C7" gate="G$1" pin="2"/>
<pinref part="U$28" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="2"/>
<pinref part="U$27" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="U$29" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="GND12" gate="1" pin="GND"/>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="92.71" y1="132.08" x2="92.71" y2="129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="129.54" y1="147.32" x2="129.54" y2="149.86" width="0.1524" layer="91"/>
<wire x1="129.54" y1="152.4" x2="129.54" y2="149.86" width="0.1524" layer="91"/>
<junction x="129.54" y="149.86"/>
<pinref part="GND7" gate="1" pin="GND"/>
<pinref part="SW1" gate="G$1" pin="S"/>
<pinref part="SW1" gate="G$1" pin="S1"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="GND"/>
<wire x1="27.94" y1="142.24" x2="27.94" y2="129.54" width="0.1524" layer="91"/>
<pinref part="U$30" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="L" gate="G$1" pin="C"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="246.38" y1="53.34" x2="246.38" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="13"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="213.36" y1="73.66" x2="223.52" y2="73.66" width="0.1524" layer="91"/>
<wire x1="223.52" y1="73.66" x2="223.52" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="1"/>
<pinref part="U$22" gate="G$1" pin="GND"/>
<wire x1="82.55" y1="129.54" x2="82.55" y2="137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="2"/>
<pinref part="U$33" gate="G$1" pin="GND"/>
<wire x1="189.23" y1="134.62" x2="189.23" y2="132.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<pinref part="U$36" gate="G$1" pin="GND"/>
<pinref part="U3" gate="G$1" pin="VSS"/>
<wire x1="181.61" y1="134.62" x2="181.61" y2="132.08" width="0.1524" layer="91"/>
<wire x1="176.53" y1="142.24" x2="177.8" y2="142.24" width="0.1524" layer="91"/>
<wire x1="177.8" y1="142.24" x2="177.8" y2="132.08" width="0.1524" layer="91"/>
<wire x1="177.8" y1="132.08" x2="181.61" y2="132.08" width="0.1524" layer="91"/>
<junction x="181.61" y="132.08"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="1"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="198.12" y1="43.18" x2="213.36" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<pinref part="GND13" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="2"/>
<pinref part="U$11" gate="G$1" pin="GND"/>
<wire x1="102.87" y1="129.54" x2="102.87" y2="132.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED1" gate="G$1" pin="GND"/>
<pinref part="GND15" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VSS"/>
<pinref part="U$26" gate="G$1" pin="GND"/>
<wire x1="210.82" y1="96.52" x2="205.74" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C18" gate="G$1" pin="2"/>
<pinref part="GND21" gate="1" pin="GND"/>
<wire x1="210.82" y1="106.68" x2="210.82" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C19" gate="G$1" pin="2"/>
<pinref part="U$7" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="GND"/>
<wire x1="213.36" y1="139.7" x2="204.47" y2="139.7" width="0.1524" layer="91"/>
<wire x1="204.47" y1="139.7" x2="204.47" y2="132.08" width="0.1524" layer="91"/>
<pinref part="U$9" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<pinref part="U$1" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="1"/>
<pinref part="U$13" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="GND@1"/>
<wire x1="68.58" y1="15.24" x2="68.58" y2="12.7" width="0.1524" layer="91"/>
<wire x1="68.58" y1="12.7" x2="71.12" y2="12.7" width="0.1524" layer="91"/>
<pinref part="M1" gate="G$1" pin="GND@7"/>
<wire x1="71.12" y1="12.7" x2="73.66" y2="12.7" width="0.1524" layer="91"/>
<wire x1="73.66" y1="12.7" x2="76.2" y2="12.7" width="0.1524" layer="91"/>
<wire x1="76.2" y1="12.7" x2="78.74" y2="12.7" width="0.1524" layer="91"/>
<wire x1="78.74" y1="12.7" x2="81.28" y2="12.7" width="0.1524" layer="91"/>
<wire x1="81.28" y1="12.7" x2="83.82" y2="12.7" width="0.1524" layer="91"/>
<wire x1="83.82" y1="12.7" x2="83.82" y2="15.24" width="0.1524" layer="91"/>
<pinref part="M1" gate="G$1" pin="GND@6"/>
<wire x1="81.28" y1="15.24" x2="81.28" y2="12.7" width="0.1524" layer="91"/>
<junction x="81.28" y="12.7"/>
<pinref part="M1" gate="G$1" pin="GND@5"/>
<wire x1="78.74" y1="15.24" x2="78.74" y2="12.7" width="0.1524" layer="91"/>
<junction x="78.74" y="12.7"/>
<pinref part="M1" gate="G$1" pin="GND@4"/>
<wire x1="76.2" y1="15.24" x2="76.2" y2="12.7" width="0.1524" layer="91"/>
<junction x="76.2" y="12.7"/>
<pinref part="M1" gate="G$1" pin="GND@3"/>
<wire x1="73.66" y1="15.24" x2="73.66" y2="12.7" width="0.1524" layer="91"/>
<junction x="73.66" y="12.7"/>
<pinref part="M1" gate="G$1" pin="GND@2"/>
<wire x1="71.12" y1="15.24" x2="71.12" y2="12.7" width="0.1524" layer="91"/>
<junction x="71.12" y="12.7"/>
<pinref part="GND1" gate="1" pin="GND"/>
</segment>
</net>
<net name="SDO" class="0">
<segment>
<wire x1="213.36" y1="53.34" x2="198.12" y2="53.34" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="5"/>
<label x="198.12" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="P0.13"/>
<wire x1="45.72" y1="99.06" x2="26.67" y2="99.06" width="0.1524" layer="91"/>
<label x="26.67" y="99.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="SDI" class="0">
<segment>
<wire x1="198.12" y1="50.8" x2="213.36" y2="50.8" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="4"/>
<label x="198.12" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="P0.15"/>
<wire x1="45.72" y1="93.98" x2="26.67" y2="93.98" width="0.1524" layer="91"/>
<label x="26.67" y="93.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<wire x1="213.36" y1="55.88" x2="198.12" y2="55.88" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="6"/>
<label x="198.12" y="55.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="P0.19"/>
<wire x1="109.22" y1="88.9" x2="125.73" y2="88.9" width="0.1524" layer="91"/>
<label x="125.73" y="88.9" size="1.778" layer="95" align="bottom-right"/>
</segment>
</net>
<net name="A5" class="0">
<segment>
<wire x1="198.12" y1="58.42" x2="213.36" y2="58.42" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="7"/>
<label x="198.12" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="P0.03/AIN1"/>
<wire x1="45.72" y1="78.74" x2="26.67" y2="78.74" width="0.1524" layer="91"/>
<label x="26.67" y="78.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="A4" class="0">
<segment>
<wire x1="213.36" y1="60.96" x2="198.12" y2="60.96" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="8"/>
<label x="198.12" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="P0.02/AIN0"/>
<wire x1="45.72" y1="73.66" x2="26.67" y2="73.66" width="0.1524" layer="91"/>
<label x="26.67" y="73.66" size="1.778" layer="95"/>
</segment>
</net>
<net name="A3" class="0">
<segment>
<wire x1="198.12" y1="63.5" x2="213.36" y2="63.5" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="9"/>
<label x="198.12" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="P0.29/AIN5"/>
<wire x1="45.72" y1="60.96" x2="26.67" y2="60.96" width="0.1524" layer="91"/>
<label x="26.67" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="A2" class="0">
<segment>
<wire x1="213.36" y1="66.04" x2="198.12" y2="66.04" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="10"/>
<label x="198.12" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="P0.28/AIN4"/>
<wire x1="45.72" y1="76.2" x2="26.67" y2="76.2" width="0.1524" layer="91"/>
<label x="26.67" y="76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="A1" class="0">
<segment>
<wire x1="198.12" y1="68.58" x2="213.36" y2="68.58" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="11"/>
<label x="198.12" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="P0.05/AIN3"/>
<wire x1="45.72" y1="53.34" x2="26.67" y2="53.34" width="0.1524" layer="91"/>
<label x="26.67" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="D11" class="0">
<segment>
<wire x1="177.8" y1="68.58" x2="157.48" y2="68.58" width="0.1524" layer="91"/>
<label x="157.48" y="68.58" size="1.778" layer="95"/>
<pinref part="JP3" gate="G$1" pin="6"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="P0.27"/>
<wire x1="109.22" y1="68.58" x2="125.73" y2="68.58" width="0.1524" layer="91"/>
<label x="125.73" y="68.58" size="1.778" layer="95" align="bottom-right"/>
</segment>
</net>
<net name="D12" class="0">
<segment>
<wire x1="157.48" y1="71.12" x2="177.8" y2="71.12" width="0.1524" layer="91"/>
<label x="157.48" y="71.12" size="1.778" layer="95"/>
<pinref part="JP3" gate="G$1" pin="5"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="P0.07"/>
<wire x1="109.22" y1="71.12" x2="125.73" y2="71.12" width="0.1524" layer="91"/>
<label x="125.73" y="71.12" size="1.778" layer="95" align="bottom-right"/>
</segment>
</net>
<net name="AREF" class="0">
<segment>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="92.71" y1="139.7" x2="92.71" y2="152.4" width="0.1524" layer="91"/>
<label x="92.71" y="144.78" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="14"/>
<wire x1="213.36" y1="76.2" x2="223.52" y2="76.2" width="0.1524" layer="91"/>
<label x="218.44" y="76.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="P0.31/AIN7"/>
<wire x1="45.72" y1="68.58" x2="26.67" y2="68.58" width="0.1524" layer="91"/>
<label x="26.67" y="68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<wire x1="48.26" y1="147.32" x2="50.8" y2="147.32" width="0.1524" layer="91"/>
<wire x1="50.8" y1="147.32" x2="58.42" y2="147.32" width="0.1524" layer="91"/>
<wire x1="58.42" y1="147.32" x2="58.42" y2="157.48" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="58.42" y1="147.32" x2="58.42" y2="137.16" width="0.1524" layer="91"/>
<junction x="58.42" y="147.32"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="50.8" y1="137.16" x2="50.8" y2="147.32" width="0.1524" layer="91"/>
<junction x="50.8" y="147.32"/>
<pinref part="+3V4" gate="G$1" pin="+3V3"/>
<pinref part="U2" gate="G$1" pin="OUT"/>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="58.42" y1="147.32" x2="68.58" y2="147.32" width="0.1524" layer="91"/>
<wire x1="68.58" y1="147.32" x2="68.58" y2="137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
<wire x1="198.12" y1="81.28" x2="198.12" y2="78.74" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="15"/>
<wire x1="198.12" y1="78.74" x2="213.36" y2="78.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="1"/>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
<wire x1="102.87" y1="139.7" x2="102.87" y2="149.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VCC"/>
<wire x1="205.74" y1="116.84" x2="205.74" y2="114.3" width="0.1524" layer="91"/>
<pinref part="+3V5" gate="G$1" pin="+3V3"/>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="205.74" y1="114.3" x2="205.74" y2="109.22" width="0.1524" layer="91"/>
<wire x1="210.82" y1="111.76" x2="210.82" y2="114.3" width="0.1524" layer="91"/>
<wire x1="210.82" y1="114.3" x2="205.74" y2="114.3" width="0.1524" layer="91"/>
<junction x="205.74" y="114.3"/>
</segment>
<segment>
<pinref part="LED1" gate="G$1" pin="VDD"/>
<pinref part="+3V6" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="WP#/IO2"/>
<wire x1="175.26" y1="99.06" x2="167.64" y2="99.06" width="0.1524" layer="91"/>
<wire x1="167.64" y1="99.06" x2="167.64" y2="96.52" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="HOLD#/IO3"/>
<wire x1="167.64" y1="96.52" x2="175.26" y2="96.52" width="0.1524" layer="91"/>
<pinref part="+3V7" gate="G$1" pin="+3V3"/>
<junction x="167.64" y="99.06"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="VDDH"/>
<wire x1="81.28" y1="109.22" x2="81.28" y2="111.76" width="0.1524" layer="91"/>
<pinref part="M1" gate="G$1" pin="VDD"/>
<wire x1="81.28" y1="111.76" x2="71.12" y2="111.76" width="0.1524" layer="91"/>
<wire x1="71.12" y1="111.76" x2="71.12" y2="109.22" width="0.1524" layer="91"/>
<pinref part="+3V3" gate="G$1" pin="+3V3"/>
<wire x1="81.28" y1="115.57" x2="81.28" y2="111.76" width="0.1524" layer="91"/>
<junction x="81.28" y="111.76"/>
</segment>
</net>
<net name="VBUS" class="0">
<segment>
<pinref part="U$3" gate="G$1" pin="VBUS"/>
<junction x="12.7" y="147.32"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="27.94" y1="147.32" x2="20.32" y2="147.32" width="0.1524" layer="91"/>
<wire x1="20.32" y1="147.32" x2="12.7" y2="147.32" width="0.1524" layer="91"/>
<wire x1="12.7" y1="147.32" x2="12.7" y2="137.16" width="0.1524" layer="91"/>
<wire x1="12.7" y1="152.4" x2="12.7" y2="147.32" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="IN"/>
<pinref part="D1" gate="G$1" pin="C"/>
<wire x1="7.62" y1="157.48" x2="7.62" y2="147.32" width="0.1524" layer="91"/>
<wire x1="7.62" y1="147.32" x2="12.7" y2="147.32" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
<junction x="20.32" y="147.32"/>
</segment>
<segment>
<pinref part="U$19" gate="G$1" pin="VBUS"/>
<wire x1="177.8" y1="76.2" x2="149.86" y2="76.2" width="0.1524" layer="91"/>
<pinref part="JP3" gate="G$1" pin="3"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="VDD"/>
<wire x1="139.7" y1="162.56" x2="139.7" y2="154.94" width="0.1524" layer="91"/>
<wire x1="139.7" y1="154.94" x2="147.32" y2="154.94" width="0.1524" layer="91"/>
<wire x1="147.32" y1="154.94" x2="147.32" y2="147.32" width="0.1524" layer="91"/>
<wire x1="147.32" y1="147.32" x2="151.13" y2="147.32" width="0.1524" layer="91"/>
<pinref part="CHG" gate="G$1" pin="A"/>
<wire x1="139.7" y1="152.4" x2="139.7" y2="154.94" width="0.1524" layer="91"/>
<junction x="139.7" y="154.94"/>
<pinref part="U$38" gate="G$1" pin="VBUS"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="VBUS"/>
<wire x1="213.36" y1="157.48" x2="204.47" y2="157.48" width="0.1524" layer="91"/>
<wire x1="204.47" y1="157.48" x2="204.47" y2="163.83" width="0.1524" layer="91"/>
<pinref part="U$12" gate="G$1" pin="VBUS"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="VBUS"/>
<wire x1="109.22" y1="99.06" x2="114.3" y2="99.06" width="0.1524" layer="91"/>
<wire x1="114.3" y1="99.06" x2="114.3" y2="106.68" width="0.1524" layer="91"/>
<pinref part="U$4" gate="G$1" pin="VBUS"/>
</segment>
</net>
<net name="VBAT" class="0">
<segment>
<pinref part="U$21" gate="G$1" pin="VBAT"/>
<pinref part="D1" gate="G$1" pin="A"/>
</segment>
<segment>
<pinref part="U$20" gate="G$1" pin="VBAT"/>
<wire x1="149.86" y1="81.28" x2="177.8" y2="81.28" width="0.1524" layer="91"/>
<pinref part="JP3" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="2"/>
<pinref part="U$16" gate="G$1" pin="VBAT"/>
<wire x1="82.55" y1="149.86" x2="82.55" y2="139.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="VBAT"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="189.23" y1="147.32" x2="176.53" y2="147.32" width="0.1524" layer="91"/>
<wire x1="189.23" y1="142.24" x2="189.23" y2="147.32" width="0.1524" layer="91"/>
<junction x="189.23" y="147.32"/>
<pinref part="U$39" gate="G$1" pin="VBAT"/>
<wire x1="189.23" y1="153.67" x2="189.23" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<pinref part="U$2" gate="G$1" pin="VBAT"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="L" gate="G$1" pin="A"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="246.38" y1="63.5" x2="246.38" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D13" class="0">
<segment>
<wire x1="177.8" y1="73.66" x2="157.48" y2="73.66" width="0.1524" layer="91"/>
<label x="157.48" y="73.66" size="1.778" layer="95"/>
<pinref part="JP3" gate="G$1" pin="4"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="P0.06"/>
<wire x1="109.22" y1="66.04" x2="125.73" y2="66.04" width="0.1524" layer="91"/>
<label x="125.73" y="66.04" size="1.778" layer="95" align="bottom-right"/>
</segment>
</net>
<net name="A0" class="0">
<segment>
<wire x1="213.36" y1="71.12" x2="198.12" y2="71.12" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="12"/>
<label x="198.12" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="P0.04/AIN2"/>
<wire x1="45.72" y1="55.88" x2="26.67" y2="55.88" width="0.1524" layer="91"/>
<label x="26.67" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="CHG" gate="G$1" pin="C"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="139.7" y1="144.78" x2="139.7" y2="142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="U3" gate="G$1" pin="STAT"/>
<wire x1="149.86" y1="142.24" x2="151.13" y2="142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PROG"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="176.53" y1="144.78" x2="181.61" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<wire x1="177.8" y1="55.88" x2="157.48" y2="55.88" width="0.1524" layer="91"/>
<label x="157.48" y="55.88" size="1.778" layer="95"/>
<pinref part="JP3" gate="G$1" pin="11"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="P1.12"/>
<wire x1="45.72" y1="66.04" x2="26.67" y2="66.04" width="0.1524" layer="91"/>
<label x="26.67" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<wire x1="157.48" y1="53.34" x2="177.8" y2="53.34" width="0.1524" layer="91"/>
<label x="157.48" y="53.34" size="1.778" layer="95"/>
<pinref part="JP3" gate="G$1" pin="12"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="P1.11"/>
<wire x1="109.22" y1="55.88" x2="125.73" y2="55.88" width="0.1524" layer="91"/>
<label x="125.73" y="55.88" size="1.778" layer="95" align="bottom-right"/>
</segment>
</net>
<net name="D9" class="0">
<segment>
<wire x1="177.8" y1="63.5" x2="157.48" y2="63.5" width="0.1524" layer="91"/>
<label x="157.48" y="63.5" size="1.778" layer="95"/>
<pinref part="JP3" gate="G$1" pin="8"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="P1.13"/>
<wire x1="45.72" y1="63.5" x2="26.67" y2="63.5" width="0.1524" layer="91"/>
<label x="26.67" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="D6" class="0">
<segment>
<wire x1="157.48" y1="60.96" x2="177.8" y2="60.96" width="0.1524" layer="91"/>
<label x="157.48" y="60.96" size="1.778" layer="95"/>
<pinref part="JP3" gate="G$1" pin="9"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="P1.14"/>
<wire x1="109.22" y1="60.96" x2="125.73" y2="60.96" width="0.1524" layer="91"/>
<label x="125.73" y="60.96" size="1.778" layer="95" align="bottom-right"/>
</segment>
</net>
<net name="D5" class="0">
<segment>
<wire x1="177.8" y1="58.42" x2="157.48" y2="58.42" width="0.1524" layer="91"/>
<label x="157.48" y="58.42" size="1.778" layer="95"/>
<pinref part="JP3" gate="G$1" pin="10"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="P1.10"/>
<wire x1="109.22" y1="53.34" x2="125.73" y2="53.34" width="0.1524" layer="91"/>
<label x="125.73" y="53.34" size="1.778" layer="95" align="bottom-right"/>
</segment>
</net>
<net name="FLASH_SDO" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="MOSI"/>
<wire x1="144.78" y1="106.68" x2="175.26" y2="106.68" width="0.1524" layer="91"/>
<label x="149.86" y="106.68" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="P0.16"/>
<wire x1="45.72" y1="91.44" x2="26.67" y2="91.44" width="0.1524" layer="91"/>
<label x="26.67" y="91.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="D1" class="0">
<segment>
<pinref part="JP1" gate="A" pin="2"/>
<wire x1="198.12" y1="45.72" x2="213.36" y2="45.72" width="0.1524" layer="91"/>
<label x="198.12" y="45.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="P0.21"/>
<wire x1="109.22" y1="78.74" x2="125.73" y2="78.74" width="0.1524" layer="91"/>
<label x="125.73" y="78.74" size="1.778" layer="95" align="bottom-right"/>
</segment>
</net>
<net name="D0" class="0">
<segment>
<pinref part="JP1" gate="A" pin="3"/>
<wire x1="213.36" y1="48.26" x2="198.12" y2="48.26" width="0.1524" layer="91"/>
<label x="198.12" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="P0.17"/>
<wire x1="109.22" y1="86.36" x2="125.73" y2="86.36" width="0.1524" layer="91"/>
<label x="125.73" y="86.36" size="1.778" layer="95" align="bottom-right"/>
</segment>
</net>
<net name="D10" class="0">
<segment>
<wire x1="157.48" y1="66.04" x2="177.8" y2="66.04" width="0.1524" layer="91"/>
<label x="157.48" y="66.04" size="1.778" layer="95"/>
<pinref part="JP3" gate="G$1" pin="7"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="P1.15"/>
<wire x1="109.22" y1="58.42" x2="125.73" y2="58.42" width="0.1524" layer="91"/>
<label x="125.73" y="58.42" size="1.778" layer="95" align="bottom-right"/>
</segment>
</net>
<net name="D+" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="DP2"/>
<wire x1="213.36" y1="149.86" x2="204.47" y2="149.86" width="0.1524" layer="91"/>
<label x="205.74" y="149.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="DP1"/>
<wire x1="241.3" y1="149.86" x2="250.19" y2="149.86" width="0.1524" layer="91"/>
<label x="245.11" y="149.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="USB_DP"/>
<wire x1="109.22" y1="93.98" x2="125.73" y2="93.98" width="0.1524" layer="91"/>
<label x="125.73" y="93.98" size="1.778" layer="95" align="bottom-right"/>
</segment>
</net>
<net name="D-" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="DN2"/>
<wire x1="213.36" y1="147.32" x2="204.47" y2="147.32" width="0.1524" layer="91"/>
<label x="205.74" y="147.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="DN1"/>
<wire x1="241.3" y1="147.32" x2="250.19" y2="147.32" width="0.1524" layer="91"/>
<label x="245.11" y="147.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="USB_DM"/>
<wire x1="109.22" y1="96.52" x2="125.73" y2="96.52" width="0.1524" layer="91"/>
<label x="125.73" y="96.52" size="1.778" layer="95" align="bottom-right"/>
</segment>
</net>
<net name="!RESET" class="0">
<segment>
<wire x1="119.38" y1="149.86" x2="119.38" y2="152.4" width="0.1524" layer="91"/>
<wire x1="114.3" y1="149.86" x2="119.38" y2="149.86" width="0.1524" layer="91"/>
<junction x="119.38" y="149.86"/>
<label x="114.3" y="149.86" size="1.27" layer="95"/>
<pinref part="SW1" gate="G$1" pin="P"/>
<pinref part="SW1" gate="G$1" pin="P1"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="16"/>
<wire x1="213.36" y1="81.28" x2="200.66" y2="81.28" width="0.1524" layer="91"/>
<label x="200.66" y="81.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="P0.18/!RESET"/>
<wire x1="109.22" y1="38.1" x2="125.73" y2="38.1" width="0.1524" layer="91"/>
<label x="125.73" y="38.1" size="1.778" layer="95" align="bottom-right"/>
</segment>
</net>
<net name="EN" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="20.32" y1="137.16" x2="25.4" y2="137.16" width="0.1524" layer="91"/>
<wire x1="25.4" y1="137.16" x2="25.4" y2="144.78" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="EN"/>
<wire x1="25.4" y1="144.78" x2="27.94" y2="144.78" width="0.1524" layer="91"/>
<label x="20.32" y="134.62" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="157.48" y1="78.74" x2="177.8" y2="78.74" width="0.1524" layer="91"/>
<pinref part="JP3" gate="G$1" pin="2"/>
<label x="157.48" y="78.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="FLASH_SCK" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="SCK"/>
<wire x1="175.26" y1="109.22" x2="144.78" y2="109.22" width="0.1524" layer="91"/>
<label x="149.86" y="109.22" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="P0.14"/>
<wire x1="45.72" y1="96.52" x2="26.67" y2="96.52" width="0.1524" layer="91"/>
<label x="26.67" y="96.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="!FLASH_CS" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="SSEL"/>
<wire x1="147.32" y1="101.6" x2="175.26" y2="101.6" width="0.1524" layer="91"/>
<label x="149.86" y="101.6" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="P0.00"/>
<wire x1="109.22" y1="22.86" x2="125.73" y2="22.86" width="0.1524" layer="91"/>
<label x="125.73" y="22.86" size="1.778" layer="95" align="bottom-right"/>
</segment>
</net>
<net name="FLASH_SDI" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="MISO"/>
<wire x1="175.26" y1="104.14" x2="144.78" y2="104.14" width="0.1524" layer="91"/>
<label x="149.86" y="104.14" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="P0.11"/>
<wire x1="45.72" y1="43.18" x2="26.67" y2="43.18" width="0.1524" layer="91"/>
<label x="26.67" y="43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="CC2"/>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="213.36" y1="152.4" x2="199.39" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="CC1"/>
<wire x1="241.3" y1="152.4" x2="245.11" y2="152.4" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="245.11" y1="152.4" x2="245.11" y2="154.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SWCLK" class="0">
<segment>
<pinref part="M1" gate="G$1" pin="SWDCLK"/>
<wire x1="109.22" y1="43.18" x2="127" y2="43.18" width="0.1524" layer="91"/>
<pinref part="TP1" gate="G$1" pin="TP"/>
<label x="113.03" y="43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="SWDIO" class="0">
<segment>
<pinref part="M1" gate="G$1" pin="SWDIO"/>
<wire x1="109.22" y1="40.64" x2="127" y2="40.64" width="0.1524" layer="91"/>
<pinref part="TP2" gate="G$1" pin="TP"/>
<label x="113.03" y="40.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="NEOPIX" class="0">
<segment>
<pinref part="M1" gate="G$1" pin="P1.08"/>
<wire x1="45.72" y1="38.1" x2="26.67" y2="38.1" width="0.1524" layer="91"/>
<label x="26.67" y="38.1" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="228.6" y1="101.6" x2="217.17" y2="101.6" width="0.1524" layer="91"/>
<pinref part="LED1" gate="G$1" pin="DI"/>
<label x="217.17" y="101.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="BATV" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="17.78" y1="20.32" x2="7.62" y2="20.32" width="0.1524" layer="91"/>
<junction x="17.78" y="20.32"/>
<label x="8.89" y="20.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="P0.30/AIN6"/>
<wire x1="45.72" y1="58.42" x2="26.67" y2="58.42" width="0.1524" layer="91"/>
<label x="26.67" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="LED" class="0">
<segment>
<pinref part="M1" gate="G$1" pin="P0.26"/>
<wire x1="109.22" y1="63.5" x2="125.73" y2="63.5" width="0.1524" layer="91"/>
<label x="125.73" y="63.5" size="1.778" layer="95" align="bottom-right"/>
</segment>
<segment>
<wire x1="246.38" y1="73.66" x2="246.38" y2="78.74" width="0.1524" layer="91"/>
<label x="246.38" y="78.74" size="1.778" layer="95" rot="R270"/>
<pinref part="R7" gate="G$1" pin="1"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="102,1,48.26,147.32,OUT,+3V3,,,,"/>
<approved hash="104,1,27.94,147.32,U2,IN,VBUS,,,"/>
<approved hash="104,1,175.26,147.32,U3,VDD,VBUS,,,"/>
<approved hash="104,1,200.66,142.24,U3,VSS,GND,,,"/>
<approved hash="104,1,27.94,86.36,U$4,VDDA,+3V3,,,"/>
<approved hash="104,1,27.94,81.28,U$4,VDDIN,+3V3,,,"/>
<approved hash="104,1,27.94,20.32,U$4,GNDA,GND,,,"/>
<approved hash="104,1,27.94,88.9,U$4,VDDIO,+3V3,,,"/>
<approved hash="104,1,-22.86,96.52,LED1,VDD,+3V3,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
<note version="8.4" severity="warning">
Since Version 8.4, EAGLE supports properties for SPICE simulation. 
Probes in schematics and SPICE mapping objects found in parts and library devices
will not be understood with this version. Update EAGLE to the latest version
for full support of SPICE simulation. 
</note>
<note version="9.0" severity="warning">
Since Version 9.0, EAGLE supports the align property for labels. 
Labels in schematic will not be understood with this version. Update EAGLE to the latest version 
for full support of labels. 
</note>
</compatibility>
</eagle>
