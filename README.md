# The Challenger 840
![Challenger 840](https://ilabs.se/wp-content/uploads/2021/08/iso-front-1.png)

## Challenger 840 BLE information
The Challenger 840 BLE board is an Arduino/Circuitpython compatible Adafruit Feather format micro controller board packed with loads of functionality for your projects that require low power consumption and a BLE connection.

The main controller of this board is the Nordic Semiconductor nRF52840 with 1MByte of FLASH memory and 256KByte of SRAM. The nRF52840 SoC is built around the 32-bit ARM® Cortex™-M4 CPU with floating point unit running at 64 MHz. The ARM TrustZone® CryptoCell cryptographic unit is included on-chip and brings an extensive range of cryptographic options that execute highly efficiently independent of the CPU. It has numerous digital peripherals and interfaces such as high speed SPI and QSPI for interfacing to external flash and displays, PDM and I2S for digital microphones and audio, and a full speed USB device for data transfer and power supply for battery recharging.

## nRF52840 Key features
- 64 MHz Cortex-M4 with FPU
- 1 MB Flash, 256 KB RAM
- 2.4 GHz Transceiver
- 2 Mbps, 1 Mbps, Long Range
- Bluetooth Low Energy, Bluetooth mesh
- ANT, 802.15.4, Thread, Zigbee
- +8 dBm TX Power
- 128-bit AES CCM, ARM CryptoCell
- UART, SPI, TWI, PDM, I2S, QSPI
- PWM
- 12-bit ADC
- USB 2.0

## The board
### The module
This board is built around the MS88SF3 module from Minew Technologies which contains the nRF52840 micro controller. This module has CE and FCC approvals already making it easy to use the Challenger 840 board in you products.

### Power
There is an onboard low power LDO (~1.5uA quiescent current) that runs the onboard electronic devices. This will enable you to make very low power products that can run for years on a battery.

The board also comes with a connector for external LiPo batteries as well as a LiPo charger circuit that.

### Other stuff
It also has the Challenger standard USB type C connector with ajoining LED's. A red LED for the charging circuit indicating when the attached battery is being charged and a green user programmable LED. In addition to this there is also a blue LED that can be used for signaling RF activity, or anything else really, to the user. The UF2 boot loader uses this LED to indicate that it is waiting for your code to be uploaded. Additionally there is also a user programmable tactile switch that can be used for user input. This button is also used by the boot loader to enter DFU mode.

Anf of course, there is a reset switch. You can not survive without the reset switch =)

### Pinout
<img src="https://ilabs.se/wp-content/uploads/2021/08/challenger-840-pinout-diagram-v0.1-1024x754.png" alt="drawing"/>

### Software support
The board is supported by both the Arduino environment as well as Circuitpython from Adafruit. At the time of writing this we are still waiting for a valid VID/PID combination for the board but as soon as this is approved the relevant software will be made available.

## License
This design covered by the CERN Open Hardware Licence v1.2 and a copy of this license is also available in this repo.

## iLabs
<img src="https://ilabs.se/wp-content/uploads/2021/07/cropped-Color-logo-no-background.png" alt="drawing" width="200"/>

### About us
Invector Labs is a small Swedish engineering company that designs and build electronic devices for hobbyists as well as small and medium sized businesses.

For more information about this board you can visit the product page at our [website](https://ilabs.se/product/challenger-840-ble/)

Questions about this product can be addressed to <oshwa@ilabs.se>.

/* EOF */
